# syd profile for OpenNTPD

# Seccomp sandbox
sandbox/read,stat,write,exec,net:on

# Landlock
sandbox/lock:on

# Provide isolation using namespaces.
unshare/mount,uts,pid,ipc,cgroup:1

# Allow adjtimex and keep CAP_SYS_TIME.
trace/allow_unsafe_time:1

# Mount everything ro except /var
bind+tmpfs:/dev/shm:nodev,nosuid,noexec
bind+tmpfs:/tmp:nodev,nosuid
bind+/etc:/etc:ro,nodev,noexec,nosuid,noatime
bind+/home:/home:ro,nodev,noexec,nosuid,noatime
bind+/media:/media:ro,nodev,noexec,nosuid,noatime
bind+/mnt:/mnt:ro,nodev,noexec,nosuid,noatime
bind+/opt:/opt:ro,nodev,nosuid,noatime
bind+/srv:/srv:ro,nodev,noexec,nosuid,noatime
bind+/usr:/usr:ro,nodev,noatime

# Hide syd
deny/read,stat,write+/proc/1/***

# Allow listen to the ntp port on loopback.
allow/net/bind+loopback!123

# Allow connections to NTP servers.
allow/net/connect+any!53
allow/net/connect+any!123
allow/net/connect+any!65535

# Allow logging to syslog.
allow/net/connect+/dev/log

# Allow `listen wildcard`
allow/net/bind+0.0.0.0!0
allow/net/connect+0.0.0.0!0

# Allow listen to the ntpd socket.
allow/net/bind+/run/ntpd.sock
allow/net/bind+/var/run/ntpd.sock
allow/write+/run/ntpd.sock
allow/write+/var/run/ntpd.sock

# Allow access to system paths
allow/read,stat+/dev/urandom
allow/lock/read+/dev/urandom
allow/read,stat+/etc/hosts
allow/lock/read+/etc/hosts
allow/read,stat+/etc/ntpd.conf
allow/lock/read+/etc/ntpd.conf
allow/read,stat+/etc/passwd
allow/lock/read+/etc/passwd
allow/read,stat+/etc/resolv.conf
allow/lock/read+/etc/resolv.conf
allow/read,stat+/etc/services
allow/lock/read+/etc/services
allow/read,stat+/usr/share/zoneinfo-posix/UTC

# chroot /var/empty && cd /
allow/stat+/
allow/stat+/var/empty
allow/write+/dev/null
allow/lock/write+/dev/null

# Allow executing the ntp binary.
allow/lock/read+/proc
allow/lock/read+/usr
allow/lock/write+/run
allow/lock/write+/var/run
allow/exec+/usr/**/bin/openntpd*

# Allow writing the drift file.
allow/write+/var/db/ntpd.drift
allow/lock/write+/var/db/ntpd.drift

# Lock configuration
lock:on
