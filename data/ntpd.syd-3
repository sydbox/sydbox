# syd profile for ntpd

# Seccomp sandbox
sandbox/read,stat,write,exec,net:on

# Landlock
sandbox/lock:on

# Provide isolation using namespaces.
unshare/mount,uts,pid,ipc,cgroup:1

# Allow adjtimex and keep CAP_SYS_TIME.
trace/allow_unsafe_time:1

# allow RLIMIT_MEMLOCK and RLIMIT_STACK.
trace/allow_unsafe_prlimit:1

# allow netlink routing socket for interface updates.
trace/allow_unsupp_socket:1

# Mount everything ro except /var
bind+tmpfs:/dev/shm:nodev,nosuid,noexec
bind+tmpfs:/tmp:nodev,nosuid
bind+/etc:/etc:ro,nodev,noexec,nosuid,noatime
bind+/home:/home:ro,nodev,noexec,nosuid,noatime
bind+/media:/media:ro,nodev,noexec,nosuid,noatime
bind+/mnt:/mnt:ro,nodev,noexec,nosuid,noatime
bind+/opt:/opt:ro,nodev,nosuid,noatime
bind+/srv:/srv:ro,nodev,noexec,nosuid,noatime
bind+/usr:/usr:ro,nodev,noatime

# Hide syd
deny/read,stat,write+/proc/1/***

# Allow `listen wildcard`
allow/net/bind+0.0.0.0!0
allow/net/connect+0.0.0.0!0
allow/net/connect+any!65535

# Allow bind to NTP port.
allow/net/bind+any!123

# Allow connections to NTP servers.
allow/net/connect+any!123

# Allow DNS.
allow/net/connect+any!53

# Allow logging to syslog.
allow/net/connect+/dev/log

# Allow executing the ntp binary.
allow/lock/read+/proc
allow/lock/read+/usr
allow/exec+/usr/**/bin/ntp*

# Allow /etc configuration.
allow/read,stat+/etc/hosts
allow/lock/read+/etc/hosts
allow/read,stat+/etc/resolv.conf
allow/lock/read+/etc/resolv.conf
allow/read,stat+/etc/services
allow/lock/read+/etc/services
allow/read,stat+/etc/ssl/certs/***
allow/lock/read+/etc/ssl/certs
allow/read,stat+/etc/ssl/openssl.cnf
allow/lock/read+/etc/ssl/openssl.cnf

# Allow access to timezone data.
allow/read,stat+/usr/share/zoneinfo*/***

# Allow access to libraries that ntpd links against.
allow/read,stat+/usr/**/lib*/libc{ap,rypto}.so.*

# Allow access to /dev/null
allow/lock/write+/dev/null
allow/read,stat,write+/dev/null

# Allow access to NTP specific files/directories.
allow/lock/read+/etc/ntp.conf
allow/read,stat+/etc/ntp.conf
allow/read,stat,write+/var/log/ntp.log
allow/lock/write+/var/log/ntp.log
allow/read,stat,write+/var/lib/ntp/***
allow/lock/write+/var/lib/ntp

# Lock configuration
lock:on
