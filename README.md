[![Change returns success. Going and coming without error. Action brings good fortune.](https://gitlab.exherbo.org/sydbox/sydbox/-/raw/main/data/icons/android-chrome-192x192.png)](https://gitlab.exherbo.org/sydbox/sydbox/-/blob/main/data/sydbox-logo.svg)

[![lev&#40;syd,bsd&#41;&lt;e](https://img.shields.io/badge/lev%28syd%2Cbsd%29%3Ce-pink)](https://git.hardenedbsd.org/hardenedbsd/HardenedBSD/-/wikis/home)
[![Shine On You Crazy Diamond!](https://img.shields.io/badge/Shine%20On%20You%20Crazy%20Diamond!-8A2BE2)](https://en.wikipedia.org/wiki/Syd_Barrett)
[![Try to Avoid Chance!](https://img.shields.io/badge/Try_to_Avoid_Chance!-gray)](https://en.wikipedia.org/wiki/Tigran_Petrosian)

[![syd:discord](https://img.shields.io/discord/1265966114073219083.svg?logo=discord)](https://discord.gg/KSNWRNsd)
[![#sydbox:mailstation.de](https://img.shields.io/matrix/sydbox:mailstation.de.svg?label=%23sydbox:mailstation.de)](https://matrix.to/#/#sydbox:mailstation.de)
[![#sydbox:irc.libera.chat](https://img.shields.io/badge/irc.libera.chat-%23sydbox-blue.svg)](https://web.libera.chat/#sydbox)

[![msrv](https://img.shields.io/badge/rustc-1.69%2B-green?style=plastic)](https://blog.rust-lang.org/2023/04/20/Rust-1.69.0.html)
[![repology](https://repology.org/badge/latest-versions/sydbox.svg)](https://repology.org/project/sydbox/versions)
[![build status](https://builds.sr.ht/~alip/syd.svg)](https://builds.sr.ht/~alip/syd?)
[![pipeline status](https://gitlab.exherbo.org/sydbox/sydbox/badges/main/pipeline.svg)](https://gitlab.exherbo.org/sydbox/sydbox/-/pipelines)

[![license](https://img.shields.io/badge/license-GPL%203.0-blue.svg)](https://gitlab.exherbo.org/sydbox/sydbox/-/blob/main/COPYING)
[![maintenance-status](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg)](https://git.sr.ht/~alip/syd)
[![dependency status](https://deps.rs/repo/sourcehut/~alip/syd/status.svg)](https://deps.rs/repo/sourcehut/~alip/syd)
[![OpenSSF best practices](https://www.bestpractices.dev/projects/8040/badge)](https://www.bestpractices.dev/projects/8040)

[Syd](https://en.wikipedia.org/wiki/Syd_Barrett) is a
[rock-solid](https://en.wikipedia.org/wiki/Tigran_Petrosian)
[application](https://en.wikipedia.org/wiki/Application_software)
[kernel](https://en.wikipedia.org/wiki/Kernel_(operating_system)) to
[sandbox](https://en.wikipedia.org/wiki/Sandbox_(computer_security))
[applications](https://en.wikipedia.org/wiki/Application_software) on
[Linux>=5.19](https://en.wikipedia.org/wiki/Linux).
[Syd](https://en.wikipedia.org/wiki/Syd_Barrett) is similar to
[Bubblewrap](https://github.com/containers/bubblewrap),
[Firejail](https://firejail.wordpress.com/),
[GVisor](https://en.wikipedia.org/wiki/GVisor), and
[minijail](https://google.github.io/minijail/). As an
[application](https://en.wikipedia.org/wiki/Application_software)
[kernel](https://en.wikipedia.org/wiki/Kernel_(operating_system)) it
implements a subset of the [Linux kernel
interface](https://en.wikipedia.org/wiki/Linux_kernel_interfaces) in
[user space](https://en.wikipedia.org/wiki/User_space_and_kernel_space),
intercepting [system calls](https://en.wikipedia.org/wiki/System_call)
to provide [strong
isolation](https://en.wikipedia.org/wiki/Sandbox_(computer_security))
without the overhead of full
[virtualization](https://en.wikipedia.org/wiki/Virtualization).
[Syd](https://en.wikipedia.org/wiki/Syd_Barrett) is [secure by
default](https://en.wikipedia.org/wiki/Secure_by_default), and intends
to provide a
[simple](https://en.wikipedia.org/wiki/KISS_principle)
[interface](https://en.wikipedia.org/wiki/API)
over various intricate
[Linux](https://en.wikipedia.org/wiki/Linux)
[sandboxing](https://en.wikipedia.org/wiki/Sandbox_(computer_security))
mechanisms such as
[LandLock](https://landlock.io/),
[Namespaces](https://en.wikipedia.org/wiki/Linux_namespaces),
[Ptrace](https://en.wikipedia.org/wiki/Ptrace), and
[Seccomp](https://en.wikipedia.org/wiki/Seccomp)-{[BPF](https://en.wikipedia.org/wiki/Berkeley_Packet_Filter),[Notify](https://www.man7.org/linux/man-pages/man3/seccomp_notify_receive.3.html)},
most of which have a reputation of being
[brittle](https://lwn.net/Articles/796108/) and
[difficult](https://lwn.net/Articles/795128/) to use.
You may run [Syd](https://en.wikipedia.org/wiki/Syd_Barrett) [_as a regular user,
with no extra privileges_](https://en.wikipedia.org/wiki/Privilege_separation),
and you can even set [Syd](https://en.wikipedia.org/wiki/Syd_Barrett) as your
[_login shell_](https://linuxhandbook.com/login-shell/).
[Syd](https://en.wikipedia.org/wiki/Syd_Barrett) adheres to the [UNIX
philosophy](https://en.wikipedia.org/wiki/Unix_philosophy) and intends
to [do one thing and do it
well](https://en.wikipedia.org/wiki/Unix_philosophy#Do_One_Thing_and_Do_It_Well)
with [least
privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege):
Neither [SETUID](https://en.wikipedia.org/wiki/Setuid) is required like
[Firejail](https://firejail.wordpress.com/), nor [privileged kernel
context](https://en.wikipedia.org/wiki/Privilege_(computing)) is
required like [EBPF](https://en.wikipedia.org/wiki/EBPF)-based
alternatives such as [Falco](https://falco.org/) or
[this](https://arxiv.org/pdf/2302.10366).
[Syd](https://en.wikipedia.org/wiki/Syd_Barrett) is based mostly on and
shares its [Threat
Model](https://man.exherbolinux.org/syd.7.html#Threat_Model) with
[Seccomp](https://en.wikipedia.org/wiki/Seccomp).
[Syd](https://en.wikipedia.org/wiki/Syd_Barrett) does not suffer from
[TOCTTOU](https://git.sr.ht/~alip/syd/tree/main/item/doc/toctou-or-gtfo.md)
issues like [GSWTK](https://www.exploit-db.com/exploits/30464) and
[Systrace](https://www.usenix.org/legacy/event/woot07/tech/full_papers/watson/watson.pdf):
As an [application](https://en.wikipedia.org/wiki/Application_software)
[kernel](https://en.wikipedia.org/wiki/Kernel_(operating_system)), it
executes [system calls](https://en.wikipedia.org/wiki/System_call) on
behalf of the sandboxed process rather than continuing them in the
sandbox process. [LandLock](https://landlock.io/), up to
[ABI](https://en.wikipedia.org/wiki/Application_binary_interface)
version 6, is supported for additional
[hardening](https://en.wikipedia.org/wiki/Hardening_(computing)). Use of
[Ptrace](https://en.wikipedia.org/wiki/Ptrace) is
[minimal](https://bugzilla.kernel.org/show_bug.cgi?id=218501) and
[optional](https://lkml.org/lkml/2024/8/26/1284) with a negligible
overhead. Use of [unprivileged user
namespaces](https://security.stackexchange.com/questions/209529/what-does-enabling-kernel-unprivileged-userns-clone-do)
is optional and off by default. A brief overview of
[Syd](https://en.wikipedia.org/wiki/Syd_Barrett)'s capabilities are as
follows:

- [OpenBSD](https://www.openbsd.org/)
  [_pledge_(2)](https://man.openbsd.org/pledge.2) like refined
  Sandbox [categories](https://man.exherbolinux.org/syd.7.html#SANDBOXING)
- [Stat sandboxing](https://man.exherbolinux.org/syd.7.html#Stat_Sandboxing)
  (aka **Path Hiding**)
- [Path Masking](https://man.exherbolinux.org/syd.7.html#Path_Masking)
  and
  [Append-only Paths](https://man.exherbolinux.org/syd.2.html#append)
- [Exec sandboxing](https://man.exherbolinux.org/syd.7.html#Exec_Sandboxing)
  with enforced
  [PIE](https://man.exherbolinux.org/syd.7.html#Enforcing_Position-Independent_Executables_(PIE))
  &amp; [ASLR](https://en.wikipedia.org/wiki/ASLR)
  with [non-executable
  stack](https://man.exherbolinux.org/syd.7.html#Enforcing_Non-Executable_Stack),
  and
  [SegvGuard](https://man.exherbolinux.org/syd.7.html#SegvGuard)
  with
  [TPE sandboxing](https://man.exherbolinux.org/syd.7.html#TPE_sandboxing)
  aka
  [Trusted Path Execution](https://wiki.gentoo.org/wiki/Hardened/Grsecurity_Trusted_Path_Execution)
- [Ioctl sandboxing](https://man.exherbolinux.org/syd.7.html#Ioctl_Sandboxing)
  (contain [AI/ML](https://en.wikipedia.org/wiki/Artificial_intelligence)
  workloads, access
  [PTY](https://en.wikipedia.org/wiki/Pseudo_terminal),
  [DRM](https://en.wikipedia.org/wiki/Direct_Rendering_Manager),
  [KVM](https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine)
  safely)
- [Force sandboxing](https://man.exherbolinux.org/syd.7.html#Force_Sandboxing)
  (aka **Verified execution**) like
  [Veriexec](https://netbsd.org/docs/guide/en/chap-veriexec.html)
  of [NetBSD](https://www.netbsd.org/)
  and
  [Integriforce](https://github.com/HardenedBSD/gitlab-wiki/blob/master/Home.md#security-administration-secadm)
  of
  [HardenedBSD](https://hardenedbsd.org/)
- [Network sandboxing](https://man.exherbolinux.org/syd.7.html#Network_Sandboxing)
  - feat. [UNIX](https://en.wikipedia.org/wiki/Unix_domain_socket),
    [IPv4](https://en.wikipedia.org/wiki/Internet_Protocol_version_4),
    [IPv6](https://en.wikipedia.org/wiki/IPv6),
    [Netlink](https://en.wikipedia.org/wiki/Netlink), and
    [KCAPI](https://en.wikipedia.org/wiki/Crypto_API_(Linux)) sockets
  - [Application
    Firewalls](https://en.wikipedia.org/wiki/Application_firewall)
    with [IP Blocklists](https://man.exherbolinux.org/syd.2.html#block)
- [Lock sandboxing](https://man.exherbolinux.org/syd.7.html#Lock_Sandboxing)
  (uses [Landlock LSM](https://landlock.io/))
- [Crypt sandboxing](https://man.exherbolinux.org/syd.7.html#Crypt_Sandboxing)
  (**Transparent File Encryption** with
  [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)-[CTR](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#CTR))
- [Proxy sandboxing](https://man.exherbolinux.org/syd.7.html#Proxy_Sandboxing)
  ([SOCKS](https://en.wikipedia.org/wiki/SOCKS)
  [proxy](https://en.wikipedia.org/wiki/Proxy_server)
  forwarding with
  [network namespace](https://en.wikipedia.org/wiki/Linux_namespaces#Network_(net))
  isolation, defaults to
  [TOR](https://www.torproject.org/))
- [Memory sandboxing](https://man.exherbolinux.org/syd.7.html#Memory_Sandboxing)
- [PID sandboxing](https://man.exherbolinux.org/syd.7.html#PID_sandboxing)
  (simpler alternatives to [Control Groups](https://en.wikipedia.org/wiki/Cgroups))
- [SafeSetID](https://man.exherbolinux.org/syd.7.html#SafeSetID)
  (Safe [user/group switching](https://en.wikipedia.org/wiki/Privilege_separation)
  with predefined
  [UID/GID](https://en.wikipedia.org/wiki/User_identifier_(Unix))
  [transitions](https://en.wikipedia.org/wiki/Transition_system))
- [Ghost mode](https://man.exherbolinux.org/syd.7.html#Ghost_mode)
  (similar to [Seccomp](https://en.wikipedia.org/wiki/Seccomp) Level 1 aka **Strict Mode**)
- [Hardened procfs and
  devfs](https://man.exherbolinux.org/syd.7.html#Hardened_procfs_and_devfs) against
  [Side-channel Attacks](https://en.wikipedia.org/wiki/Side-channel_attack)
- [Namespaces and Containerization](https://en.wikipedia.org/wiki/Containerization_(computing))
- **Learning mode** with [Pandora](https://crates.io/crates/pandora_box)

Read the fine manuals of [syd](https://man.exherbolinux.org/),
[libsyd](https://libsyd.exherbolinux.org/),
[gosyd](https://gosyd.exherbolinux.org/),
[plsyd](https://plsyd.exherbolinux.org/),
[pysyd](https://pysyd.exherbolinux.org/),
[rbsyd](https://rbsyd.exherbolinux.org/),
[syd.el](https://sydel.exherbolinux.org/) and watch the asciicasts [Memory
Sandboxing](https://asciinema.org/a/625243), [PID
Sandboxing](https://asciinema.org/a/625170), [Network
Sandboxing](https://asciinema.org/a/623664), and [Sandboxing Emacs with
syd](https://asciinema.org/a/627055). Join the CTF event at
https://ctftime.org/event/2178 and try to read the file `/etc/CTF`¹ on
syd.chesswob.org with ssh user/pass: syd.²

- Use cargo to install from source, requires [libseccomp](https://github.com/seccomp/libseccomp).
- To use with
  [Docker](https://www.docker.com/),
  [Podman](https://podman.io/), or
  [CRI-O](https://cri-o.io/)
  build with the "oci" feature,
  see: https://man.exherbolinux.org/syd-oci.1.html
- Packaged for
  [Alpine](https://pkgs.alpinelinux.org/packages?name=sydbox),
  [Exherbo](https://summer.exherbolinux.org/packages/sys-apps/sydbox/index.html),
  and
  [Gentoo](https://packages.gentoo.org/packages/sys-apps/syd).
- Binary releases for [arm64](https://en.wikipedia.org/wiki/Arm64),
  [armv7](https://en.wikipedia.org/wiki/Armv7),
  [loongarch64](https://en.wikipedia.org/wiki/Loongson#LoongArch),
  [ppc64le](https://en.wikipedia.org/wiki/Ppc64),
  [riscv64](https://en.wikipedia.org/wiki/RISC-V),
  [s390x](https://en.wikipedia.org/wiki/Linux_on_IBM_Z#Hardware),
  [x86](https://en.wikipedia.org/wiki/X86), and
  [x86-64](https://en.wikipedia.org/wiki/X86-64)
  are located at https://distfiles.exherbolinux.org/#sydbox/
- Releases are signed with this key: https://distfiles.exherbolinux.org/sydbox/syd.asc
- Report security issues to `syd AT chesswob DOT org`. Encrypt with the key above.
- Change Log is here: https://gitlab.exherbo.org/sydbox/sydbox/-/blob/main/ChangeLog.md
- [VIM](https://www.vim.org/)
  [syntax highlighting](https://en.wikipedia.org/wiki/Syntax_highlighting)
  file for
  [Syd](https://en.wikipedia.org/wiki/Syd_Barrett)
  profiles is here: https://gitlab.exherbo.org/sydbox/sydbox/-/tree/main/vim
- Tested on [arm64](https://en.wikipedia.org/wiki/Arm64),
  [armv7](https://en.wikipedia.org/wiki/Armv7),
  [loongarch64](https://en.wikipedia.org/wiki/Loongson#LoongArch),
  [mips](https://en.wikipedia.org/wiki/MIPS_architecture),
  [ppc64le](https://en.wikipedia.org/wiki/Ppc64),
  [riscv64](https://en.wikipedia.org/wiki/RISC-V),
  [s390x](https://en.wikipedia.org/wiki/Linux_on_IBM_Z#Hardware),
  [x86](https://en.wikipedia.org/wiki/X86), and
  [x86-64](https://en.wikipedia.org/wiki/X86-64) with
  [GitLab Pipelines](https://gitlab.exherbo.org/sydbox/sydbox/-/pipelines), and
  [SourceHut Builds](https://builds.sr.ht/~alip/syd?).

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be submitted to
<https://gitlab.exherbo.org/groups/sydbox/-/issues>. Follow toots with the [#sydbox
hashtag](https://mastodon.online/tags/sydbox) and discuss in [#sydbox on Libera
Chat](ircs://irc.libera.chat/#sydbox).

¹: [SHA256](https://en.wikipedia.org/wiki/SHA-2)(`/etc/CTF`)=`f1af8d3946546f9d3b1af4fe15f0209b2298166208d51a481cf51ac8c5f4b294`

²: Start by reading [the CTF sandbox profile](https://gitlab.exherbo.org/sydbox/sydbox/-/raw/main/data/ctf.syd-3).

³: [That cat's something I can't explain!](https://gitlab.exherbo.org/paludis/paludis/-/commit/dd0566f16e27f2110581234fe1c48a11d18a7d64)

<!-- vim: set spell spelllang=en : -->
