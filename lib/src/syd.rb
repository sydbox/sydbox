#!/usr/bin/env ruby
# frozen_string_literal: true

#
# Syd: rock-solid application kernel
#
# lib/src/syd.rb: Ruby FFI bindings of libsyd, the syd API C Library
#
# Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0

require "ffi"

# Ruby FFI bindings of libsyd, the syd API C Library
module Syd
  extend FFI::Library
  ffi_lib "syd"

  public

  # Enum for lock states with detailed documentation for each state.

  # LOCK_OFF: The sandbox lock is off, allowing all sandbox commands.
  # This state means that there are no restrictions on sandbox commands,
  # providing full access to sandbox functionalities.
  LOCK_OFF = 0

  # LOCK_EXEC: The sandbox lock is set to on for all processes except
  # the initial process (syd exec child). This is the default state.
  # In this state, the sandbox is locked for all new processes except
  # for the initial process that executed the syd command. This
  # provides a balance between security and functionality, allowing the
  # initial process some level of control while restricting others.
  LOCK_EXEC = 1

  # LOCK_ON: The sandbox lock is on, disallowing all sandbox commands.
  # This state imposes a complete lock down on the sandbox, preventing
  # any sandbox commands from being executed. This is the most
  # restrictive state, ensuring maximum security.
  LOCK_ON = 2

  # Enum for actions for Sandboxing.

  # Allow system call.
  ACTION_ALLOW = 0

  # Allow system call and warn.
  ACTION_WARN = 1

  # Deny system call silently.
  ACTION_FILTER = 2

  # Deny system call and warn.
  ACTION_DENY = 3

  # Deny system call, warn and panic the current Syd thread.
  ACTION_PANIC = 4

  # Deny system call, warn and stop offending process.
  ACTION_STOP = 5

  # Deny system call, warn and kill offending process.
  ACTION_KILL = 6

  # Warn, and exit Syd immediately with deny errno as exit value.
  ACTION_EXIT = 7

  # Reads the state of the syd sandbox from /dev/syd and returns it
  # as a Ruby hash.
  #
  # This method opens the special file /dev/syd, which contains the
  # current state of the syd sandbox in JSON format. It then parses
  # this state and returns it as a Ruby hash.
  #
  # @return [Hash, NilClass] The current state of the syd sandbox as
  # a Ruby hash, or nil if JSON module is not available.
  # @raise [Errno::ENOENT] If the file /dev/syd cannot be opened.
  # @raise [JSON::ParserError] If the content of /dev/syd is not valid JSON.
  def self.info
    begin
      require "json"
    rescue LoadError
      return nil
    end

    JSON.parse File.read("/dev/syd"), symbolize_names: true
  end

  # Performs a check by calling the 'syd_check' function from the 'syd'
  # library. This function essentially performs an lstat system call on the
  # file "/dev/syd".
  #
  # @return [TrueClass] Returns `true` if the operation is successful.
  # @raise [SystemCallError] Raises the appropriate Ruby exception
  # corresponding to the errno on failure.
  #
  # The 'syd_check' function returns 0 on success and negated errno on failure.
  # In Ruby, this method translates a non-zero return value into a
  # corresponding SystemCallError exception, providing a more idiomatic way of
  # error handling.
  def self.check
    check_return syd_check
  end

  # Performs a syd API check by calling the 'syd_api' function from the
  # 'syd' library.
  #
  # This method is intended to be used as a preliminary check before making any
  # other syd API calls. It is advisable to perform this check to ensure
  # the API is accessible and functioning as expected.
  #
  # @return [Integer] The API number on success.
  # @raise [SystemCallError] A Ruby exception corresponding to the negated errno on failure.
  def self.api
    check_return syd_api
  end

  # Causes syd to exit immediately with code 127.
  #
  # This function is designed to trigger an immediate exit of syd with a
  # specific exit code (127). It should be used in scenarios where an immediate
  # and complete termination of syd is necessary.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.panic
    check_return syd_panic
  end

  # Causes syd to reset sandboxing to the default state. This
  # includes clearing any allowlists, denylists, and filters.
  #
  # This function should be used when it is necessary to reset the state
  # of syd sandboxing environment to its default settings. It's
  # particularly useful in scenarios where the sandboxing environment
  # needs to be reconfigured or cleared of all previous configurations.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.reset
    check_return syd_reset
  end

  # Causes syd to read configuration from the given file descriptor.
  #
  # This function is utilized to load configuration settings for syd
  # from a file represented by the provided file descriptor. It's an
  # essential function for initializing or reconfiguring syd based on
  # external configuration files.
  #
  # @param fd [Integer] The file descriptor of the configuration file.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.load(fd)
    check_return syd_load(fd)
  end

  # Sets the state of the sandbox lock.
  #
  # @param state [Integer] The desired state of the sandbox lock, should be one of LOCK_OFF, LOCK_EXEC, or LOCK_ON.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.lock(state)
    raise Errno::EINVAL unless state.is_a?(Integer) && (LOCK_OFF..LOCK_ON).cover?(state)

    check_return syd_lock(state)
  end

  # Execute a command outside the sandbox without sandboxing.
  #
  # This method is used to execute a command in the operating system, bypassing
  # the sandbox. It takes a file path and an array of arguments, converts them
  # to the appropriate C types, and then invokes the syd_exec function from the
  # syd library.
  #
  # @param file [String] The file path of the command to be executed.
  # @param argv [Array<String>] The arguments to the command.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.exec(file, argv)
    # Convert each argument into a memory pointer to a string
    argv_ptrs = argv.map { |arg| FFI::MemoryPointer.from_string(arg) }
    # Append a null pointer to the end of the array to signify the end of arguments
    argv_ptrs << nil

    # Create a memory pointer that will hold pointers to each argument string
    argv_ptr = FFI::MemoryPointer.new(:pointer, argv_ptrs.length)
    # Copy the pointers to the argument strings into the newly created memory pointer
    argv_ptr.put_array_of_pointer(0, argv_ptrs)

    # Call the syd_exec function and handle the return value
    check_return syd_exec(file, argv_ptr)
  end

  # Enable stat sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_stat
    check_return syd_enable_stat
  end

  # Disable stat sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_stat
    check_return syd_disable_stat
  end

  # Checks if stat sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if stat sandboxing is enabled, `false` otherwise.
  def self.enabled_stat
    syd_enabled_stat
  end

  # Enable read sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_read
    check_return syd_enable_read
  end

  # Disable read sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_read
    check_return syd_disable_read
  end

  # Checks if read sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if Read sandboxing is enabled, `false` otherwise.
  def self.enabled_read
    syd_enabled_read
  end

  # Enable write sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_write
    check_return syd_enable_write
  end

  # Disable write sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_write
    check_return syd_disable_write
  end

  # Checks if write sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if write sandboxing is enabled, `false` otherwise.
  def self.enabled_write
    syd_enabled_write
  end

  # Enable exec sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_exec
    check_return syd_enable_exec
  end

  # Disable exec sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_exec
    check_return syd_disable_exec
  end

  # Checks if exec sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if exec sandboxing is enabled, `false` otherwise.
  def self.enabled_exec
    syd_enabled_exec
  end

  # Enable ioctl sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_ioctl
    check_return syd_enable_ioctl
  end

  # Disable ioctl sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_ioctl
    check_return syd_disable_ioctl
  end

  # Checks if ioctl sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if ioctl sandboxing is enabled, `false` otherwise.
  def self.enabled_ioctl
    syd_enabled_ioctl
  end

  # Enable create sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_create
    check_return syd_enable_create
  end

  # Disable create sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_create
    check_return syd_disable_create
  end

  # Checks if create sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if create sandboxing is enabled, `false` otherwise.
  def self.enabled_create
    syd_enabled_create
  end

  # Enable delete sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_delete
    check_return syd_enable_delete
  end

  # Disable delete sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_delete
    check_return syd_disable_delete
  end

  # Checks if delete sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if delete sandboxing is enabled, `false` otherwise.
  def self.enabled_delete
    syd_enabled_delete
  end

  # Enable rename sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_rename
    check_return syd_enable_rename
  end

  # Disable rename sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_rename
    check_return syd_disable_rename
  end

  # Checks if rename sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if rename sandboxing is enabled, `false` otherwise.
  def self.enabled_rename
    syd_enabled_rename
  end

  # Enable symlink sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_symlink
    check_return syd_enable_symlink
  end

  # Disable symlink sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_symlink
    check_return syd_disable_symlink
  end

  # Checks if symlink sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if symlink sandboxing is enabled, `false` otherwise.
  def self.enabled_symlink
    syd_enabled_symlink
  end

  # Enable truncate sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_truncate
    check_return syd_enable_truncate
  end

  # Disable truncate sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_truncate
    check_return syd_disable_truncate
  end

  # Checks if truncate sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if truncate sandboxing is enabled, `false` otherwise.
  def self.enabled_truncate
    syd_enabled_truncate
  end

  # Enable chdir sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_chdir
    check_return syd_enable_chdir
  end

  # Disable chdir sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_chdir
    check_return syd_disable_chdir
  end

  # Checks if chdir sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if chdir sandboxing is enabled, `false` otherwise.
  def self.enabled_chdir
    syd_enabled_chdir
  end

  # Enable readdir sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_readdir
    check_return syd_enable_readdir
  end

  # Disable readdir sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_readdir
    check_return syd_disable_readdir
  end

  # Checks if readdir sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if readdir sandboxing is enabled, `false` otherwise.
  def self.enabled_readdir
    syd_enabled_readdir
  end

  # Enable mkdir sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_mkdir
    check_return syd_enable_mkdir
  end

  # Disable mkdir sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_mkdir
    check_return syd_disable_mkdir
  end

  # Checks if mkdir sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if mkdir sandboxing is enabled, `false` otherwise.
  def self.enabled_mkdir
    syd_enabled_mkdir
  end

  # Enable chown sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_chown
    check_return syd_enable_chown
  end

  # Disable chown sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_chown
    check_return syd_disable_chown
  end

  # Checks if chown sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if chown sandboxing is enabled, `false` otherwise.
  def self.enabled_chown
    syd_enabled_chown
  end

  # Enable chgrp sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_chgrp
    check_return syd_enable_chgrp
  end

  # Disable chgrp sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_chgrp
    check_return syd_disable_chgrp
  end

  # Checks if chgrp sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if chgrp sandboxing is enabled, `false` otherwise.
  def self.enabled_chgrp
    syd_enabled_chgrp
  end

  # Enable chmod sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_chmod
    check_return syd_enable_chmod
  end

  # Disable chmod sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_chmod
    check_return syd_disable_chmod
  end

  # Checks if chmod sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if chmod sandboxing is enabled, `false` otherwise.
  def self.enabled_chmod
    syd_enabled_chmod
  end

  # Enable chattr sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_chattr
    check_return syd_enable_chattr
  end

  # Disable chattr sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_chattr
    check_return syd_disable_chattr
  end

  # Checks if chattr sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if chattr sandboxing is enabled, `false` otherwise.
  def self.enabled_chattr
    syd_enabled_chattr
  end

  # Enable chroot sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_chroot
    check_return syd_enable_chroot
  end

  # Disable chroot sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_chroot
    check_return syd_disable_chroot
  end

  # Checks if chroot sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if chroot sandboxing is enabled, `false` otherwise.
  def self.enabled_chroot
    syd_enabled_chroot
  end

  # Enable utime sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_utime
    check_return syd_enable_utime
  end

  # Disable utime sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_utime
    check_return syd_disable_utime
  end

  # Checks if utime sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if utime sandboxing is enabled, `false` otherwise.
  def self.enabled_utime
    syd_enabled_utime
  end

  # Enable mkdev sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_mkdev
    check_return syd_enable_mkdev
  end

  # Disable mkdev sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_mkdev
    check_return syd_disable_mkdev
  end

  # Checks if mkdev sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if mkdev sandboxing is enabled, `false` otherwise.
  def self.enabled_mkdev
    syd_enabled_mkdev
  end

  # Enable mkfifo sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_mkfifo
    check_return syd_enable_mkfifo
  end

  # Disable mkfifo sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_mkfifo
    check_return syd_disable_mkfifo
  end

  # Checks if mkfifo sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if mkfifo sandboxing is enabled, `false` otherwise.
  def self.enabled_mkfifo
    syd_enabled_mkfifo
  end

  # Enable mktemp sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_mktemp
    check_return syd_enable_mktemp
  end

  # Disable mktemp sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_mktemp
    check_return syd_disable_mktemp
  end

  # Checks if mktemp sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if mktemp sandboxing is enabled, `false` otherwise.
  def self.enabled_mktemp
    syd_enabled_mktemp
  end

  # Enable net sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_net
    check_return syd_enable_net
  end

  # Disable net sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_net
    check_return syd_disable_net
  end

  # Checks if net sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if net sandboxing is enabled, `false` otherwise.
  def self.enabled_net
    syd_enabled_net
  end

  # Checks if lock sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if lock sandboxing is enabled, `false` otherwise.
  def self.enabled_lock
    syd_enabled_lock
  end

  # Checks if crypt sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if crypt sandboxing is enabled, `false` otherwise.
  def self.enabled_crypt
    syd_enabled_crypt
  end

  # Checks if proxy sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if proxy sandboxing is enabled, `false` otherwise.
  def self.enabled_proxy
    syd_enabled_proxy
  end

  # Enable memory sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_mem
    check_return syd_enable_mem
  end

  # Disable memory sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_mem
    check_return syd_disable_mem
  end

  # Checks if memory sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if memory sandboxing is enabled, `false` otherwise.
  def self.enabled_mem
    syd_enabled_mem
  end

  # Enable PID sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_pid
    check_return syd_enable_pid
  end

  # Disable PID sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_pid
    check_return syd_disable_pid
  end

  # Checks if PID sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if PID sandboxing is enabled, `false` otherwise.
  def self.enabled_pid
    syd_enabled_pid
  end

  # Enable force sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_force
    check_return syd_enable_force
  end

  # Disable force sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_force
    check_return syd_disable_force
  end

  # Checks if force sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if force sandboxing is enabled, `false` otherwise.
  def self.enabled_force
    syd_enabled_force
  end

  # Enable TPE sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_tpe
    check_return syd_enable_tpe
  end

  # Disable TPE sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_tpe
    check_return syd_disable_tpe
  end

  # Checks if TPE sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if TPE sandboxing is enabled, `false` otherwise.
  def self.enabled_tpe
    syd_enabled_tpe
  end

  # Set default action for stat sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_stat(action)
    check_return syd_default_stat(check_action(action))
  end

  # Set default action for read sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_read(action)
    check_return syd_default_read(check_action(action))
  end

  # Set default action for write sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_write(action)
    check_return syd_default_write(check_action(action))
  end

  # Set default action for exec sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_exec(action)
    check_return syd_default_exec(check_action(action))
  end

  # Set default action for ioctl sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_ioctl(action)
    check_return syd_default_ioctl(check_action(action))
  end

  # Set default action for create sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_create(action)
    check_return syd_default_create(check_action(action))
  end

  # Set default action for delete sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_delete(action)
    check_return syd_default_delete(check_action(action))
  end

  # Set default action for rename sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_rename(action)
    check_return syd_default_rename(check_action(action))
  end

  # Set default action for symlink sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_symlink(action)
    check_return syd_default_symlink(check_action(action))
  end

  # Set default action for truncate sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_truncate(action)
    check_return syd_default_truncate(check_action(action))
  end

  # Set default action for chdir sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_chdir(action)
    check_return syd_default_chdir(check_action(action))
  end

  # Set default action for readdir sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_readdir(action)
    check_return syd_default_readdir(check_action(action))
  end

  # Set default action for mkdir sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_mkdir(action)
    check_return syd_default_mkdir(check_action(action))
  end

  # Set default action for chown sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_chown(action)
    check_return syd_default_chown(check_action(action))
  end

  # Set default action for chgrp sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_chgrp(action)
    check_return syd_default_chgrp(check_action(action))
  end

  # Set default action for chmod sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_chmod(action)
    check_return syd_default_chmod(check_action(action))
  end

  # Set default action for chattr sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_chattr(action)
    check_return syd_default_chattr(check_action(action))
  end

  # Set default action for chroot sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_chroot(action)
    check_return syd_default_chroot(check_action(action))
  end

  # Set default action for utime sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_utime(action)
    check_return syd_default_utime(check_action(action))
  end

  # Set default action for mkdev sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_mkdev(action)
    check_return syd_default_mkdev(check_action(action))
  end

  # Set default action for mkfifo sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_mkfifo(action)
    check_return syd_default_mkfifo(check_action(action))
  end

  # Set default action for mktemp sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_mktemp(action)
    check_return syd_default_mktemp(check_action(action))
  end

  # Set default action for net sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_net(action)
    check_return syd_default_net(check_action(action))
  end

  # Set default action for block sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_block(action)
    check_return syd_default_block(check_action(action))
  end

  # Set default action for memory sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_mem(action)
    check_return syd_default_mem(check_action(action))
  end

  # Set default action for PID sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_pid(action)
    check_return syd_default_pid(check_action(action))
  end

  # Set default action for force sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_force(action)
    check_return syd_default_force(check_action(action))
  end

  # Set default action for SegvGuard.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_segvguard(action)
    check_return syd_default_segvguard(check_action(action))
  end

  # Set default action for TPE sandboxing.
  #
  # @param action [Integer] The desired default action.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.default_tpe(action)
    check_return syd_default_tpe(check_action(action))
  end

  # Adds a request to the _ioctl_(2) denylist.
  #
  # param request [Integer] The _ioctl_(2) request to deny.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.ioctl_deny(request)
    check_return syd_ioctl_deny(request)
  end

  # Adds a path to the given actionlist for stat sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.stat_add(action, glob)
    check_return syd_stat_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # stat sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.stat_del(action, glob)
    check_return syd_stat_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for stat
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.stat_rem(action, glob)
    check_return syd_stat_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for read sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.read_add(action, glob)
    check_return syd_read_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # read sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.read_del(action, glob)
    check_return syd_read_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for read
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.read_rem(action, glob)
    check_return syd_read_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for write sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.write_add(action, glob)
    check_return syd_write_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # write sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.write_del(action, glob)
    check_return syd_write_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for write
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.write_rem(action, glob)
    check_return syd_write_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for exec sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.exec_add(action, glob)
    check_return syd_exec_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # exec sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.exec_del(action, glob)
    check_return syd_exec_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for exec
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.exec_rem(action, glob)
    check_return syd_exec_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for ioctl sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.ioctl_add(action, glob)
    check_return syd_ioctl_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # ioctl sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.ioctl_del(action, glob)
    check_return syd_ioctl_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for ioctl
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.ioctl_rem(action, glob)
    check_return syd_ioctl_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for create sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.create_add(action, glob)
    check_return syd_create_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # create sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.create_del(action, glob)
    check_return syd_create_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for create
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.create_rem(action, glob)
    check_return syd_create_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for delete sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.delete_add(action, glob)
    check_return syd_delete_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # delete sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.delete_del(action, glob)
    check_return syd_delete_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for delete
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.delete_rem(action, glob)
    check_return syd_delete_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for rename sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.rename_add(action, glob)
    check_return syd_rename_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # rename sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.rename_del(action, glob)
    check_return syd_rename_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for rename
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.rename_rem(action, glob)
    check_return syd_rename_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for symlink sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.symlink_add(action, glob)
    check_return syd_symlink_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # symlink sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.symlink_del(action, glob)
    check_return syd_symlink_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for symlink
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.symlink_rem(action, glob)
    check_return syd_symlink_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for truncate sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.truncate_add(action, glob)
    check_return syd_truncate_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # truncate sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.truncate_del(action, glob)
    check_return syd_truncate_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for truncate
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.truncate_rem(action, glob)
    check_return syd_truncate_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for chdir sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chdir_add(action, glob)
    check_return syd_chdir_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # chdir sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chdir_del(action, glob)
    check_return syd_chdir_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for chdir
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chdir_rem(action, glob)
    check_return syd_chdir_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for readdir sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.readdir_add(action, glob)
    check_return syd_readdir_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # readdir sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.readdir_del(action, glob)
    check_return syd_readdir_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for readdir
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.readdir_rem(action, glob)
    check_return syd_readdir_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for mkdir sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkdir_add(action, glob)
    check_return syd_mkdir_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # mkdir sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkdir_del(action, glob)
    check_return syd_mkdir_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for mkdir
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkdir_rem(action, glob)
    check_return syd_mkdir_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for chown sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chown_add(action, glob)
    check_return syd_chown_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # chown sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chown_del(action, glob)
    check_return syd_chown_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for chown
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chown_rem(action, glob)
    check_return syd_chown_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for chgrp sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chgrp_add(action, glob)
    check_return syd_chgrp_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # chgrp sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chgrp_del(action, glob)
    check_return syd_chgrp_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for chgrp
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chgrp_rem(action, glob)
    check_return syd_chgrp_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for chmod sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chmod_add(action, glob)
    check_return syd_chmod_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # chmod sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chmod_del(action, glob)
    check_return syd_chmod_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for chmod
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chmod_rem(action, glob)
    check_return syd_chmod_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for chattr sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chattr_add(action, glob)
    check_return syd_chattr_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # chattr sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chattr_del(action, glob)
    check_return syd_chattr_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for chattr
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chattr_rem(action, glob)
    check_return syd_chattr_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for chroot sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chroot_add(action, glob)
    check_return syd_chroot_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # chroot sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chroot_del(action, glob)
    check_return syd_chroot_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for chroot
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.chroot_rem(action, glob)
    check_return syd_chroot_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for utime sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.utime_add(action, glob)
    check_return syd_utime_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # utime sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.utime_del(action, glob)
    check_return syd_utime_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for utime
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.utime_rem(action, glob)
    check_return syd_utime_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for mkdev sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkdev_add(action, glob)
    check_return syd_mkdev_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # mkdev sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkdev_del(action, glob)
    check_return syd_mkdev_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for mkdev
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkdev_rem(action, glob)
    check_return syd_mkdev_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for mkfifo sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkfifo_add(action, glob)
    check_return syd_mkfifo_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # mkfifo sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkfifo_del(action, glob)
    check_return syd_mkfifo_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for mkfifo
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mkfifo_rem(action, glob)
    check_return syd_mkfifo_rem(check_action(action), glob)
  end

  # Adds a path to the given actionlist for mktemp sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mktemp_add(action, glob)
    check_return syd_mktemp_add(check_action(action), glob)
  end

  # Removes the first instance from the end of the given actionlist for
  # mktemp sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mktemp_del(action, glob)
    check_return syd_mktemp_del(check_action(action), glob)
  end

  # Removes all matching patterns from the given actionlist for mktemp
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mktemp_rem(action, glob)
    check_return syd_mktemp_rem(check_action(action), glob)
  end

  # Adds an address to the given actionlist for net/bind sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_bind_add(action, addr)
    check_return syd_net_bind_add(check_action(action), addr)
  end

  # Removes the first instance from the end of the given actionlist for
  # net/bind sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_bind_del(action, addr)
    check_return syd_net_bind_del(check_action(action), addr)
  end

  # Removes all matching patterns from the given actionlist for net/bind
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_bind_rem(action, addr)
    check_return syd_net_bind_rem(check_action(action), addr)
  end

  # Adds an address to the given actionlist for net/connect sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_connect_add(action, addr)
    check_return syd_net_connect_add(check_action(action), addr)
  end

  # Removes the first instance from the end of the given actionlist for
  # net/connect sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_connect_del(action, addr)
    check_return syd_net_connect_del(check_action(action), addr)
  end

  # Removes all matching patterns from the given actionlist for net/connect
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_connect_rem(action, addr)
    check_return syd_net_connect_rem(check_action(action), addr)
  end

  # Adds an address to the given actionlist for net/sendfd sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_sendfd_add(action, addr)
    check_return syd_net_sendfd_add(check_action(action), addr)
  end

  # Removes the first instance from the end of the given actionlist for
  # net/sendfd sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_sendfd_del(action, addr)
    check_return syd_net_sendfd_del(check_action(action), addr)
  end

  # Removes all matching patterns from the given actionlist for net/sendfd
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_sendfd_rem(action, addr)
    check_return syd_net_sendfd_rem(check_action(action), addr)
  end

  # Adds an address to the given actionlist for net/link sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_link_add(action, addr)
    check_return syd_net_link_add(check_action(action), addr)
  end

  # Removes the first instance from the end of the given actionlist for
  # net/link sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_link_del(action, addr)
    check_return syd_net_link_del(check_action(action), addr)
  end

  # Removes all matching patterns from the given actionlist for net/link
  # sandboxing.
  #
  # @param action [Integer] The desired action of for the rule.
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.net_link_rem(action, addr)
    check_return syd_net_link_rem(check_action(action), addr)
  end

  # Adds an entry to the Integrity Force map for Force Sandboxing.
  #
  # @param path [String] Fully-qualified file name as string.
  # @param hash [String] Checksum as hexadecimal encoded string.
  # @param action [Integer] The desired action of for the rule.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.force_add(path, hash, action)
    check_return syd_force_add(path, hash, check_action(action))
  end

  # Removes an entry from the Integrity Force map for Force Sandboxing.
  #
  # @param path [String] Fully-qualified file name as string.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.force_del(path)
    check_return syd_force_del(path)
  end

  # Clears the Integrity Force map for Force Sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.force_clr
    check_return syd_force_clr
  end

  # Set syd maximum per-process memory usage limit for memory sandboxing,
  # parse-size crate is used to parse the value so formatted strings are OK.
  #
  # @param size [String] Limit size.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mem_max(size)
    check_return syd_mem_max(size)
  end

  # Set syd maximum per-process virtual memory usage limit for memory sandboxing,
  # parse-size crate is used to parse the value so formatted strings are OK.
  #
  # @param size [String] Limit size.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mem_vm_max(size)
    check_return syd_mem_vm_max(size)
  end

  # Set syd maximum process id limit for PID sandboxing
  #
  # @param size [Integer] Limit size, must be greater than or equal to zero.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.pid_max(size)
    check_return syd_pid_max(size)
  end

  # Specify SegvGuard expiry timeout in seconds, must be greater than or equal to zero.
  # Setting this timeout to 0 effectively disables SegvGuard.
  #
  # @param timeout [Integer] Expiry timeout in seconds, must be greater than or equal to zero.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.segvguard_expiry(timeout)
    check_return syd_segvguard_expiry(timeout)
  end

  # Specify SegvGuard suspension timeout in seconds, must be greater than or equal to zero.
  #
  # @param timeout [Integer] Suspension timeout in seconds, must be greater than or equal to zero.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.segvguard_suspension(timeout)
    check_return syd_segvguard_suspension(timeout)
  end

  # Specify SegvGuard max number of crashes before suspension.
  #
  # @param limit [Integer] Limit, must be greater than or equal to zero.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.segvguard_maxcrashes(limit)
    check_return syd_segvguard_maxcrashes(limit)
  end

  # Helper method to check if the action is valid.
  def self.check_action(action)
    raise Errno::EINVAL unless action.is_a?(Integer) && (ACTION_ALLOW..ACTION_EXIT).cover?(action)

    action
  end

  # Helper method to process return values from libsyd calls
  def self.check_return(r)
    # Convert negative errno to Ruby exception.
    raise Errno.const_get(Errno.constants.find { |e| -r == Errno.const_get(e)::Errno }) unless r >= 0

    r.zero? ? true : r
  end

  attach_function :syd_check, [], :int
  attach_function :syd_api, [], :int
  attach_function :syd_panic, [], :int
  attach_function :syd_reset, [], :int
  attach_function :syd_load, [:int], :int
  attach_function :syd_lock, [:uint8], :int
  attach_function :syd_exec, %i[string pointer], :int

  attach_function :syd_enable_stat, [], :int
  attach_function :syd_disable_stat, [], :int
  attach_function :syd_enabled_stat, [], :bool
  attach_function :syd_enable_read, [], :int
  attach_function :syd_disable_read, [], :int
  attach_function :syd_enabled_read, [], :bool
  attach_function :syd_enable_write, [], :int
  attach_function :syd_disable_write, [], :int
  attach_function :syd_enabled_write, [], :bool
  attach_function :syd_enable_exec, [], :int
  attach_function :syd_disable_exec, [], :int
  attach_function :syd_enabled_exec, [], :bool
  attach_function :syd_enable_ioctl, [], :int
  attach_function :syd_disable_ioctl, [], :int
  attach_function :syd_enabled_ioctl, [], :bool
  attach_function :syd_enable_create, [], :int
  attach_function :syd_disable_create, [], :int
  attach_function :syd_enabled_create, [], :bool
  attach_function :syd_enable_delete, [], :int
  attach_function :syd_disable_delete, [], :int
  attach_function :syd_enabled_delete, [], :bool
  attach_function :syd_enable_rename, [], :int
  attach_function :syd_disable_rename, [], :int
  attach_function :syd_enabled_rename, [], :bool
  attach_function :syd_enable_symlink, [], :int
  attach_function :syd_disable_symlink, [], :int
  attach_function :syd_enabled_symlink, [], :bool
  attach_function :syd_enable_truncate, [], :int
  attach_function :syd_disable_truncate, [], :int
  attach_function :syd_enabled_truncate, [], :bool
  attach_function :syd_enable_chdir, [], :int
  attach_function :syd_disable_chdir, [], :int
  attach_function :syd_enabled_chdir, [], :bool
  attach_function :syd_enable_readdir, [], :int
  attach_function :syd_disable_readdir, [], :int
  attach_function :syd_enabled_readdir, [], :bool
  attach_function :syd_enable_mkdir, [], :int
  attach_function :syd_disable_mkdir, [], :int
  attach_function :syd_enabled_mkdir, [], :bool
  attach_function :syd_enable_chown, [], :int
  attach_function :syd_disable_chown, [], :int
  attach_function :syd_enabled_chown, [], :bool
  attach_function :syd_enable_chgrp, [], :int
  attach_function :syd_disable_chgrp, [], :int
  attach_function :syd_enabled_chgrp, [], :bool
  attach_function :syd_enable_chmod, [], :int
  attach_function :syd_disable_chmod, [], :int
  attach_function :syd_enabled_chmod, [], :bool
  attach_function :syd_enable_chattr, [], :int
  attach_function :syd_disable_chattr, [], :int
  attach_function :syd_enabled_chattr, [], :bool
  attach_function :syd_enable_chroot, [], :int
  attach_function :syd_disable_chroot, [], :int
  attach_function :syd_enabled_chroot, [], :bool
  attach_function :syd_enable_utime, [], :int
  attach_function :syd_disable_utime, [], :int
  attach_function :syd_enabled_utime, [], :bool
  attach_function :syd_enable_mkdev, [], :int
  attach_function :syd_disable_mkdev, [], :int
  attach_function :syd_enabled_mkdev, [], :bool
  attach_function :syd_enable_mkfifo, [], :int
  attach_function :syd_disable_mkfifo, [], :int
  attach_function :syd_enabled_mkfifo, [], :bool
  attach_function :syd_enable_mktemp, [], :int
  attach_function :syd_disable_mktemp, [], :int
  attach_function :syd_enabled_mktemp, [], :bool

  attach_function :syd_enable_net, [], :int
  attach_function :syd_disable_net, [], :int
  attach_function :syd_enabled_net, [], :bool

  attach_function :syd_enabled_lock, [], :bool
  attach_function :syd_enabled_crypt, [], :bool
  attach_function :syd_enabled_proxy, [], :bool

  attach_function :syd_enable_mem, [], :int
  attach_function :syd_disable_mem, [], :int
  attach_function :syd_enabled_mem, [], :bool
  attach_function :syd_enable_pid, [], :int
  attach_function :syd_disable_pid, [], :int
  attach_function :syd_enabled_pid, [], :bool

  attach_function :syd_enable_force, [], :int
  attach_function :syd_disable_force, [], :int
  attach_function :syd_enabled_force, [], :bool
  attach_function :syd_enable_tpe, [], :int
  attach_function :syd_disable_tpe, [], :int
  attach_function :syd_enabled_tpe, [], :bool

  attach_function :syd_default_stat, [:uint8], :int
  attach_function :syd_default_read, [:uint8], :int
  attach_function :syd_default_write, [:uint8], :int
  attach_function :syd_default_exec, [:uint8], :int
  attach_function :syd_default_ioctl, [:uint8], :int
  attach_function :syd_default_create, [:uint8], :int
  attach_function :syd_default_delete, [:uint8], :int
  attach_function :syd_default_rename, [:uint8], :int
  attach_function :syd_default_symlink, [:uint8], :int
  attach_function :syd_default_truncate, [:uint8], :int
  attach_function :syd_default_chdir, [:uint8], :int
  attach_function :syd_default_readdir, [:uint8], :int
  attach_function :syd_default_mkdir, [:uint8], :int
  attach_function :syd_default_chown, [:uint8], :int
  attach_function :syd_default_chgrp, [:uint8], :int
  attach_function :syd_default_chmod, [:uint8], :int
  attach_function :syd_default_chattr, [:uint8], :int
  attach_function :syd_default_chroot, [:uint8], :int
  attach_function :syd_default_utime, [:uint8], :int
  attach_function :syd_default_mkdev, [:uint8], :int
  attach_function :syd_default_mkfifo, [:uint8], :int
  attach_function :syd_default_mktemp, [:uint8], :int

  attach_function :syd_default_net, [:uint8], :int
  attach_function :syd_default_block, [:uint8], :int
  attach_function :syd_default_mem, [:uint8], :int
  attach_function :syd_default_pid, [:uint8], :int
  attach_function :syd_default_force, [:uint8], :int
  attach_function :syd_default_segvguard, [:uint8], :int
  attach_function :syd_default_tpe, [:uint8], :int

  attach_function :syd_ioctl_deny, [:uint64], :int

  attach_function :syd_stat_add, %i[uint8 string], :int
  attach_function :syd_stat_del, %i[uint8 string], :int
  attach_function :syd_stat_rem, %i[uint8 string], :int
  attach_function :syd_read_add, %i[uint8 string], :int
  attach_function :syd_read_del, %i[uint8 string], :int
  attach_function :syd_read_rem, %i[uint8 string], :int
  attach_function :syd_write_add, %i[uint8 string], :int
  attach_function :syd_write_del, %i[uint8 string], :int
  attach_function :syd_write_rem, %i[uint8 string], :int
  attach_function :syd_exec_add, %i[uint8 string], :int
  attach_function :syd_exec_del, %i[uint8 string], :int
  attach_function :syd_exec_rem, %i[uint8 string], :int
  attach_function :syd_ioctl_add, %i[uint8 string], :int
  attach_function :syd_ioctl_del, %i[uint8 string], :int
  attach_function :syd_ioctl_rem, %i[uint8 string], :int
  attach_function :syd_create_add, %i[uint8 string], :int
  attach_function :syd_create_del, %i[uint8 string], :int
  attach_function :syd_create_rem, %i[uint8 string], :int
  attach_function :syd_delete_add, %i[uint8 string], :int
  attach_function :syd_delete_del, %i[uint8 string], :int
  attach_function :syd_delete_rem, %i[uint8 string], :int
  attach_function :syd_rename_add, %i[uint8 string], :int
  attach_function :syd_rename_del, %i[uint8 string], :int
  attach_function :syd_rename_rem, %i[uint8 string], :int
  attach_function :syd_symlink_add, %i[uint8 string], :int
  attach_function :syd_symlink_del, %i[uint8 string], :int
  attach_function :syd_symlink_rem, %i[uint8 string], :int
  attach_function :syd_truncate_add, %i[uint8 string], :int
  attach_function :syd_truncate_del, %i[uint8 string], :int
  attach_function :syd_truncate_rem, %i[uint8 string], :int
  attach_function :syd_chdir_add, %i[uint8 string], :int
  attach_function :syd_chdir_del, %i[uint8 string], :int
  attach_function :syd_chdir_rem, %i[uint8 string], :int
  attach_function :syd_readdir_add, %i[uint8 string], :int
  attach_function :syd_readdir_del, %i[uint8 string], :int
  attach_function :syd_readdir_rem, %i[uint8 string], :int
  attach_function :syd_mkdir_add, %i[uint8 string], :int
  attach_function :syd_mkdir_del, %i[uint8 string], :int
  attach_function :syd_mkdir_rem, %i[uint8 string], :int
  attach_function :syd_chown_add, %i[uint8 string], :int
  attach_function :syd_chown_del, %i[uint8 string], :int
  attach_function :syd_chown_rem, %i[uint8 string], :int
  attach_function :syd_chgrp_add, %i[uint8 string], :int
  attach_function :syd_chgrp_del, %i[uint8 string], :int
  attach_function :syd_chgrp_rem, %i[uint8 string], :int
  attach_function :syd_chmod_add, %i[uint8 string], :int
  attach_function :syd_chmod_del, %i[uint8 string], :int
  attach_function :syd_chmod_rem, %i[uint8 string], :int
  attach_function :syd_chattr_add, %i[uint8 string], :int
  attach_function :syd_chattr_del, %i[uint8 string], :int
  attach_function :syd_chattr_rem, %i[uint8 string], :int
  attach_function :syd_chroot_add, %i[uint8 string], :int
  attach_function :syd_chroot_del, %i[uint8 string], :int
  attach_function :syd_chroot_rem, %i[uint8 string], :int
  attach_function :syd_utime_add, %i[uint8 string], :int
  attach_function :syd_utime_del, %i[uint8 string], :int
  attach_function :syd_utime_rem, %i[uint8 string], :int
  attach_function :syd_mkdev_add, %i[uint8 string], :int
  attach_function :syd_mkdev_del, %i[uint8 string], :int
  attach_function :syd_mkdev_rem, %i[uint8 string], :int
  attach_function :syd_mkfifo_add, %i[uint8 string], :int
  attach_function :syd_mkfifo_del, %i[uint8 string], :int
  attach_function :syd_mkfifo_rem, %i[uint8 string], :int
  attach_function :syd_mktemp_add, %i[uint8 string], :int
  attach_function :syd_mktemp_del, %i[uint8 string], :int
  attach_function :syd_mktemp_rem, %i[uint8 string], :int

  attach_function :syd_net_bind_add, %i[uint8 string], :int
  attach_function :syd_net_bind_del, %i[uint8 string], :int
  attach_function :syd_net_bind_rem, %i[uint8 string], :int
  attach_function :syd_net_connect_add, %i[uint8 string], :int
  attach_function :syd_net_connect_del, %i[uint8 string], :int
  attach_function :syd_net_connect_rem, %i[uint8 string], :int
  attach_function :syd_net_sendfd_add, %i[uint8 string], :int
  attach_function :syd_net_sendfd_del, %i[uint8 string], :int
  attach_function :syd_net_sendfd_rem, %i[uint8 string], :int

  attach_function :syd_net_link_add, %i[uint8 string], :int
  attach_function :syd_net_link_del, %i[uint8 string], :int
  attach_function :syd_net_link_rem, %i[uint8 string], :int

  attach_function :syd_force_add, %i[string string uint8], :int
  attach_function :syd_force_del, [:string], :int
  attach_function :syd_force_clr, [], :int

  attach_function :syd_mem_max, [:string], :int
  attach_function :syd_mem_vm_max, [:string], :int

  attach_function :syd_pid_max, [:uint], :int

  attach_function :syd_segvguard_expiry, [:uint64], :int
  attach_function :syd_segvguard_suspension, [:uint64], :int
  attach_function :syd_segvguard_maxcrashes, [:uint8], :int

  freeze
end

if __FILE__ == $PROGRAM_NAME
  require "minitest/autorun"

  # @api private
  class SydTest < Minitest::Test
    # This line ensures tests run sequentially
    # We need this because once you lock the sandbox,
    # there is no going back...
    i_suck_and_my_tests_are_order_dependent!

    # This method is run before each test
    def setup
      Syd.check
    rescue SystemCallError => e
      skip "check() raised SystemCallError, skipping tests: #{e}"
    end

    def test_01_api
      assert_equal 3, Syd.api, "Syd.api should return 3"
    end

    def test_02_stat
      state = Syd.enabled_stat
      assert_equal true, Syd.enable_stat
      assert_equal true, Syd.enabled_stat
      assert_equal true, Syd.disable_stat
      assert_equal false, Syd.enabled_stat
      if state
        Syd.enable_stat
      else
        Syd.disable_stat
      end

      state = Syd.enabled_read
      assert_equal true, Syd.enable_read
      assert_equal true, Syd.enabled_read
      assert_equal true, Syd.disable_read
      assert_equal false, Syd.enabled_read
      if state
        Syd.enable_read
      else
        Syd.disable_read
      end

      state = Syd.enabled_write
      assert_equal true, Syd.enable_write
      assert_equal true, Syd.enabled_write
      assert_equal true, Syd.disable_write
      assert_equal false, Syd.enabled_write
      if state
        Syd.enable_write
      else
        Syd.disable_write
      end

      state = Syd.enabled_exec
      assert_equal true, Syd.enable_exec
      assert_equal true, Syd.enabled_exec
      assert_equal true, Syd.disable_exec
      assert_equal false, Syd.enabled_exec
      if state
        Syd.enable_exec
      else
        Syd.disable_exec
      end

      state = Syd.enabled_ioctl
      assert_equal true, Syd.enable_ioctl
      assert_equal true, Syd.enabled_ioctl
      assert_equal true, Syd.disable_ioctl
      assert_equal false, Syd.enabled_ioctl
      if state
        Syd.enable_ioctl
      else
        Syd.disable_ioctl
      end

      state = Syd.enabled_create
      assert_equal true, Syd.enable_create
      assert_equal true, Syd.enabled_create
      assert_equal true, Syd.disable_create
      assert_equal false, Syd.enabled_create
      if state
        Syd.enable_create
      else
        Syd.disable_create
      end

      state = Syd.enabled_delete
      assert_equal true, Syd.enable_delete
      assert_equal true, Syd.enabled_delete
      assert_equal true, Syd.disable_delete
      assert_equal false, Syd.enabled_delete
      if state
        Syd.enable_delete
      else
        Syd.disable_delete
      end

      state = Syd.enabled_rename
      assert_equal true, Syd.enable_rename
      assert_equal true, Syd.enabled_rename
      assert_equal true, Syd.disable_rename
      assert_equal false, Syd.enabled_rename
      if state
        Syd.enable_rename
      else
        Syd.disable_rename
      end

      state = Syd.enabled_symlink
      assert_equal true, Syd.enable_symlink
      assert_equal true, Syd.enabled_symlink
      assert_equal true, Syd.disable_symlink
      assert_equal false, Syd.enabled_symlink
      if state
        Syd.enable_symlink
      else
        Syd.disable_symlink
      end

      state = Syd.enabled_truncate
      assert_equal true, Syd.enable_truncate
      assert_equal true, Syd.enabled_truncate
      assert_equal true, Syd.disable_truncate
      assert_equal false, Syd.enabled_truncate
      if state
        Syd.enable_truncate
      else
        Syd.disable_truncate
      end

      state = Syd.enabled_chdir
      assert_equal true, Syd.enable_chdir
      assert_equal true, Syd.enabled_chdir
      assert_equal true, Syd.disable_chdir
      assert_equal false, Syd.enabled_chdir
      if state
        Syd.enable_chdir
      else
        Syd.disable_chdir
      end

      state = Syd.enabled_readdir
      assert_equal true, Syd.enable_readdir
      assert_equal true, Syd.enabled_readdir
      assert_equal true, Syd.disable_readdir
      assert_equal false, Syd.enabled_readdir
      if state
        Syd.enable_readdir
      else
        Syd.disable_readdir
      end

      state = Syd.enabled_mkdir
      assert_equal true, Syd.enable_mkdir
      assert_equal true, Syd.enabled_mkdir
      assert_equal true, Syd.disable_mkdir
      assert_equal false, Syd.enabled_mkdir
      if state
        Syd.enable_mkdir
      else
        Syd.disable_mkdir
      end

      state = Syd.enabled_chown
      assert_equal true, Syd.enable_chown
      assert_equal true, Syd.enabled_chown
      assert_equal true, Syd.disable_chown
      assert_equal false, Syd.enabled_chown
      if state
        Syd.enable_chown
      else
        Syd.disable_chown
      end

      state = Syd.enabled_chgrp
      assert_equal true, Syd.enable_chgrp
      assert_equal true, Syd.enabled_chgrp
      assert_equal true, Syd.disable_chgrp
      assert_equal false, Syd.enabled_chgrp
      if state
        Syd.enable_chgrp
      else
        Syd.disable_chgrp
      end

      state = Syd.enabled_chmod
      assert_equal true, Syd.enable_chmod
      assert_equal true, Syd.enabled_chmod
      assert_equal true, Syd.disable_chmod
      assert_equal false, Syd.enabled_chmod
      if state
        Syd.enable_chmod
      else
        Syd.disable_chmod
      end

      state = Syd.enabled_chattr
      assert_equal true, Syd.enable_chattr
      assert_equal true, Syd.enabled_chattr
      assert_equal true, Syd.disable_chattr
      assert_equal false, Syd.enabled_chattr
      if state
        Syd.enable_chattr
      else
        Syd.disable_chattr
      end

      # Chroot is startup only since 3.32.4
      #      state = Syd.enabled_chroot
      #      assert_equal true, Syd.enable_chroot
      #      assert_equal true, Syd.enabled_chroot
      #      assert_equal true, Syd.disable_chroot
      #      assert_equal false, Syd.enabled_chroot
      #      if state
      #        Syd.enable_chroot
      #      else
      #        Syd.disable_chroot
      #      end

      state = Syd.enabled_utime
      assert_equal true, Syd.enable_utime
      assert_equal true, Syd.enabled_utime
      assert_equal true, Syd.disable_utime
      assert_equal false, Syd.enabled_utime
      if state
        Syd.enable_utime
      else
        Syd.disable_utime
      end

      state = Syd.enabled_mkdev
      assert_equal true, Syd.enable_mkdev
      assert_equal true, Syd.enabled_mkdev
      assert_equal true, Syd.disable_mkdev
      assert_equal false, Syd.enabled_mkdev
      if state
        Syd.enable_mkdev
      else
        Syd.disable_mkdev
      end

      state = Syd.enabled_mkfifo
      assert_equal true, Syd.enable_mkfifo
      assert_equal true, Syd.enabled_mkfifo
      assert_equal true, Syd.disable_mkfifo
      assert_equal false, Syd.enabled_mkfifo
      if state
        Syd.enable_mkfifo
      else
        Syd.disable_mkfifo
      end

      state = Syd.enabled_mktemp
      assert_equal true, Syd.enable_mktemp
      assert_equal true, Syd.enabled_mktemp
      assert_equal true, Syd.disable_mktemp
      assert_equal false, Syd.enabled_mktemp
      if state
        Syd.enable_mktemp
      else
        Syd.disable_mktemp
      end

      state = Syd.enabled_net
      assert_equal true, Syd.enable_net
      assert_equal true, Syd.enabled_net
      assert_equal true, Syd.disable_net
      assert_equal false, Syd.enabled_net
      if state
        Syd.enable_net
      else
        Syd.disable_net
      end

      assert_equal false, Syd.enabled_lock
      assert_equal false, Syd.enabled_crypt
      assert_equal false, Syd.enabled_proxy

      state = Syd.enabled_mem
      assert_equal true, Syd.enable_mem
      assert_equal true, Syd.enabled_mem
      assert_equal true, Syd.disable_mem
      assert_equal false, Syd.enabled_mem
      if state
        Syd.enable_mem
      else
        Syd.disable_mem
      end

      state = Syd.enabled_pid
      assert_equal true, Syd.enable_pid
      assert_equal true, Syd.enabled_pid
      assert_equal true, Syd.disable_pid
      assert_equal false, Syd.enabled_pid
      if state
        Syd.enable_pid
      else
        Syd.disable_pid
      end

      state = Syd.enabled_force
      assert_equal true, Syd.enable_force
      assert_equal true, Syd.enabled_force
      assert_equal true, Syd.disable_force
      assert_equal false, Syd.enabled_force
      if state
        Syd.enable_force
      else
        Syd.disable_force
      end

      state = Syd.enabled_tpe
      assert_equal true, Syd.enable_tpe
      assert_equal true, Syd.enabled_tpe
      assert_equal true, Syd.disable_tpe
      assert_equal false, Syd.enabled_tpe
      if state
        Syd.enable_tpe
      else
        Syd.disable_tpe
      end

      mem_max_orig = Syd.info[:mem_max]
      mem_vm_max_orig = Syd.info[:mem_vm_max]
      pid_max_orig = Syd.info[:pid_max]

      assert_equal true, Syd.mem_max("1G")
      assert_equal 1024 * 1024 * 1024, Syd.info[:mem_max]
      assert_equal true, Syd.mem_max("10G")
      assert_equal 10 * 1024 * 1024 * 1024, Syd.info[:mem_max]
      Syd.mem_max(mem_max_orig.to_s)

      assert_equal true, Syd.mem_vm_max("1G")
      assert_equal 1024 * 1024 * 1024, Syd.info[:mem_vm_max]
      assert_equal true, Syd.mem_vm_max("10G")
      assert_equal 10 * 1024 * 1024 * 1024, Syd.info[:mem_vm_max]
      Syd.mem_vm_max(mem_vm_max_orig.to_s)

      assert_equal true, Syd.pid_max(4096)
      assert_equal 4096, Syd.info[:pid_max]
      assert_equal true, Syd.pid_max(8192)
      assert_equal 8192, Syd.info[:pid_max]
      Syd.pid_max(pid_max_orig)
    end

    def test_03_default
      action = Syd.info[:default_stat]
      assert_equal action, "Deny"
      assert Syd.default_stat(Syd::ACTION_ALLOW)
      action = Syd.info[:default_stat]
      assert_equal action, "Allow"
      assert Syd.default_stat(Syd::ACTION_WARN)
      action = Syd.info[:default_stat]
      assert_equal action, "Warn"
      assert Syd.default_stat(Syd::ACTION_FILTER)
      action = Syd.info[:default_stat]
      assert_equal action, "Filter"
      assert Syd.default_stat(Syd::ACTION_STOP)
      action = Syd.info[:default_stat]
      assert_equal action, "Stop"
      assert Syd.default_stat(Syd::ACTION_KILL)
      action = Syd.info[:default_stat]
      assert_equal action, "Kill"
      assert Syd.default_stat(Syd::ACTION_EXIT)
      action = Syd.info[:default_stat]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_stat(Syd::ACTION_DENY)
      action = Syd.info[:default_stat]
      assert_equal action, "Deny"

      action = Syd.info[:default_read]
      assert_equal action, "Deny"
      assert Syd.default_read(Syd::ACTION_ALLOW)
      action = Syd.info[:default_read]
      assert_equal action, "Allow"
      assert Syd.default_read(Syd::ACTION_WARN)
      action = Syd.info[:default_read]
      assert_equal action, "Warn"
      assert Syd.default_read(Syd::ACTION_FILTER)
      action = Syd.info[:default_read]
      assert_equal action, "Filter"
      assert Syd.default_read(Syd::ACTION_STOP)
      action = Syd.info[:default_read]
      assert_equal action, "Stop"
      assert Syd.default_read(Syd::ACTION_KILL)
      action = Syd.info[:default_read]
      assert_equal action, "Kill"
      assert Syd.default_read(Syd::ACTION_EXIT)
      action = Syd.info[:default_read]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_read(Syd::ACTION_DENY)
      action = Syd.info[:default_read]
      assert_equal action, "Deny"

      action = Syd.info[:default_write]
      assert_equal action, "Deny"
      assert Syd.default_write(Syd::ACTION_ALLOW)
      action = Syd.info[:default_write]
      assert_equal action, "Allow"
      assert Syd.default_write(Syd::ACTION_WARN)
      action = Syd.info[:default_write]
      assert_equal action, "Warn"
      assert Syd.default_write(Syd::ACTION_FILTER)
      action = Syd.info[:default_write]
      assert_equal action, "Filter"
      assert Syd.default_write(Syd::ACTION_STOP)
      action = Syd.info[:default_write]
      assert_equal action, "Stop"
      assert Syd.default_write(Syd::ACTION_KILL)
      action = Syd.info[:default_write]
      assert_equal action, "Kill"
      assert Syd.default_write(Syd::ACTION_EXIT)
      action = Syd.info[:default_write]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_write(Syd::ACTION_DENY)
      action = Syd.info[:default_write]
      assert_equal action, "Deny"

      action = Syd.info[:default_exec]
      assert_equal action, "Deny"
      assert Syd.default_exec(Syd::ACTION_ALLOW)
      action = Syd.info[:default_exec]
      assert_equal action, "Allow"
      assert Syd.default_exec(Syd::ACTION_WARN)
      action = Syd.info[:default_exec]
      assert_equal action, "Warn"
      assert Syd.default_exec(Syd::ACTION_FILTER)
      action = Syd.info[:default_exec]
      assert_equal action, "Filter"
      assert Syd.default_exec(Syd::ACTION_STOP)
      action = Syd.info[:default_exec]
      assert_equal action, "Stop"
      assert Syd.default_exec(Syd::ACTION_KILL)
      action = Syd.info[:default_exec]
      assert_equal action, "Kill"
      assert Syd.default_exec(Syd::ACTION_EXIT)
      action = Syd.info[:default_exec]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_exec(Syd::ACTION_DENY)
      action = Syd.info[:default_exec]
      assert_equal action, "Deny"

      action = Syd.info[:default_ioctl]
      assert_equal action, "Deny"
      assert Syd.default_ioctl(Syd::ACTION_ALLOW)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Allow"
      assert Syd.default_ioctl(Syd::ACTION_WARN)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Warn"
      assert Syd.default_ioctl(Syd::ACTION_FILTER)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Filter"
      assert Syd.default_ioctl(Syd::ACTION_STOP)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Stop"
      assert Syd.default_ioctl(Syd::ACTION_KILL)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Kill"
      assert Syd.default_ioctl(Syd::ACTION_EXIT)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_ioctl(Syd::ACTION_DENY)
      action = Syd.info[:default_ioctl]
      assert_equal action, "Deny"

      action = Syd.info[:default_create]
      assert_equal action, "Deny"
      assert Syd.default_create(Syd::ACTION_ALLOW)
      action = Syd.info[:default_create]
      assert_equal action, "Allow"
      assert Syd.default_create(Syd::ACTION_WARN)
      action = Syd.info[:default_create]
      assert_equal action, "Warn"
      assert Syd.default_create(Syd::ACTION_FILTER)
      action = Syd.info[:default_create]
      assert_equal action, "Filter"
      assert Syd.default_create(Syd::ACTION_STOP)
      action = Syd.info[:default_create]
      assert_equal action, "Stop"
      assert Syd.default_create(Syd::ACTION_KILL)
      action = Syd.info[:default_create]
      assert_equal action, "Kill"
      assert Syd.default_create(Syd::ACTION_EXIT)
      action = Syd.info[:default_create]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_create(Syd::ACTION_DENY)
      action = Syd.info[:default_create]
      assert_equal action, "Deny"

      action = Syd.info[:default_delete]
      assert_equal action, "Deny"
      assert Syd.default_delete(Syd::ACTION_ALLOW)
      action = Syd.info[:default_delete]
      assert_equal action, "Allow"
      assert Syd.default_delete(Syd::ACTION_WARN)
      action = Syd.info[:default_delete]
      assert_equal action, "Warn"
      assert Syd.default_delete(Syd::ACTION_FILTER)
      action = Syd.info[:default_delete]
      assert_equal action, "Filter"
      assert Syd.default_delete(Syd::ACTION_STOP)
      action = Syd.info[:default_delete]
      assert_equal action, "Stop"
      assert Syd.default_delete(Syd::ACTION_KILL)
      action = Syd.info[:default_delete]
      assert_equal action, "Kill"
      assert Syd.default_delete(Syd::ACTION_EXIT)
      action = Syd.info[:default_delete]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_delete(Syd::ACTION_DENY)
      action = Syd.info[:default_delete]
      assert_equal action, "Deny"

      action = Syd.info[:default_rename]
      assert_equal action, "Deny"
      assert Syd.default_rename(Syd::ACTION_ALLOW)
      action = Syd.info[:default_rename]
      assert_equal action, "Allow"
      assert Syd.default_rename(Syd::ACTION_WARN)
      action = Syd.info[:default_rename]
      assert_equal action, "Warn"
      assert Syd.default_rename(Syd::ACTION_FILTER)
      action = Syd.info[:default_rename]
      assert_equal action, "Filter"
      assert Syd.default_rename(Syd::ACTION_STOP)
      action = Syd.info[:default_rename]
      assert_equal action, "Stop"
      assert Syd.default_rename(Syd::ACTION_KILL)
      action = Syd.info[:default_rename]
      assert_equal action, "Kill"
      assert Syd.default_rename(Syd::ACTION_EXIT)
      action = Syd.info[:default_rename]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_rename(Syd::ACTION_DENY)
      action = Syd.info[:default_rename]
      assert_equal action, "Deny"

      action = Syd.info[:default_symlink]
      assert_equal action, "Deny"
      assert Syd.default_symlink(Syd::ACTION_ALLOW)
      action = Syd.info[:default_symlink]
      assert_equal action, "Allow"
      assert Syd.default_symlink(Syd::ACTION_WARN)
      action = Syd.info[:default_symlink]
      assert_equal action, "Warn"
      assert Syd.default_symlink(Syd::ACTION_FILTER)
      action = Syd.info[:default_symlink]
      assert_equal action, "Filter"
      assert Syd.default_symlink(Syd::ACTION_STOP)
      action = Syd.info[:default_symlink]
      assert_equal action, "Stop"
      assert Syd.default_symlink(Syd::ACTION_KILL)
      action = Syd.info[:default_symlink]
      assert_equal action, "Kill"
      assert Syd.default_symlink(Syd::ACTION_EXIT)
      action = Syd.info[:default_symlink]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_symlink(Syd::ACTION_DENY)
      action = Syd.info[:default_symlink]
      assert_equal action, "Deny"

      action = Syd.info[:default_truncate]
      assert_equal action, "Deny"
      assert Syd.default_truncate(Syd::ACTION_ALLOW)
      action = Syd.info[:default_truncate]
      assert_equal action, "Allow"
      assert Syd.default_truncate(Syd::ACTION_WARN)
      action = Syd.info[:default_truncate]
      assert_equal action, "Warn"
      assert Syd.default_truncate(Syd::ACTION_FILTER)
      action = Syd.info[:default_truncate]
      assert_equal action, "Filter"
      assert Syd.default_truncate(Syd::ACTION_STOP)
      action = Syd.info[:default_truncate]
      assert_equal action, "Stop"
      assert Syd.default_truncate(Syd::ACTION_KILL)
      action = Syd.info[:default_truncate]
      assert_equal action, "Kill"
      assert Syd.default_truncate(Syd::ACTION_EXIT)
      action = Syd.info[:default_truncate]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_truncate(Syd::ACTION_DENY)
      action = Syd.info[:default_truncate]
      assert_equal action, "Deny"

      action = Syd.info[:default_chdir]
      assert_equal action, "Deny"
      assert Syd.default_chdir(Syd::ACTION_ALLOW)
      action = Syd.info[:default_chdir]
      assert_equal action, "Allow"
      assert Syd.default_chdir(Syd::ACTION_WARN)
      action = Syd.info[:default_chdir]
      assert_equal action, "Warn"
      assert Syd.default_chdir(Syd::ACTION_FILTER)
      action = Syd.info[:default_chdir]
      assert_equal action, "Filter"
      assert Syd.default_chdir(Syd::ACTION_STOP)
      action = Syd.info[:default_chdir]
      assert_equal action, "Stop"
      assert Syd.default_chdir(Syd::ACTION_KILL)
      action = Syd.info[:default_chdir]
      assert_equal action, "Kill"
      assert Syd.default_chdir(Syd::ACTION_EXIT)
      action = Syd.info[:default_chdir]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_chdir(Syd::ACTION_DENY)
      action = Syd.info[:default_chdir]
      assert_equal action, "Deny"

      action = Syd.info[:default_readdir]
      assert_equal action, "Deny"
      assert Syd.default_readdir(Syd::ACTION_ALLOW)
      action = Syd.info[:default_readdir]
      assert_equal action, "Allow"
      assert Syd.default_readdir(Syd::ACTION_WARN)
      action = Syd.info[:default_readdir]
      assert_equal action, "Warn"
      assert Syd.default_readdir(Syd::ACTION_FILTER)
      action = Syd.info[:default_readdir]
      assert_equal action, "Filter"
      assert Syd.default_readdir(Syd::ACTION_STOP)
      action = Syd.info[:default_readdir]
      assert_equal action, "Stop"
      assert Syd.default_readdir(Syd::ACTION_KILL)
      action = Syd.info[:default_readdir]
      assert_equal action, "Kill"
      assert Syd.default_readdir(Syd::ACTION_EXIT)
      action = Syd.info[:default_readdir]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_readdir(Syd::ACTION_DENY)
      action = Syd.info[:default_readdir]
      assert_equal action, "Deny"

      action = Syd.info[:default_mkdir]
      assert_equal action, "Deny"
      assert Syd.default_mkdir(Syd::ACTION_ALLOW)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Allow"
      assert Syd.default_mkdir(Syd::ACTION_WARN)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Warn"
      assert Syd.default_mkdir(Syd::ACTION_FILTER)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Filter"
      assert Syd.default_mkdir(Syd::ACTION_STOP)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Stop"
      assert Syd.default_mkdir(Syd::ACTION_KILL)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Kill"
      assert Syd.default_mkdir(Syd::ACTION_EXIT)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_mkdir(Syd::ACTION_DENY)
      action = Syd.info[:default_mkdir]
      assert_equal action, "Deny"

      action = Syd.info[:default_chown]
      assert_equal action, "Deny"
      assert Syd.default_chown(Syd::ACTION_ALLOW)
      action = Syd.info[:default_chown]
      assert_equal action, "Allow"
      assert Syd.default_chown(Syd::ACTION_WARN)
      action = Syd.info[:default_chown]
      assert_equal action, "Warn"
      assert Syd.default_chown(Syd::ACTION_FILTER)
      action = Syd.info[:default_chown]
      assert_equal action, "Filter"
      assert Syd.default_chown(Syd::ACTION_STOP)
      action = Syd.info[:default_chown]
      assert_equal action, "Stop"
      assert Syd.default_chown(Syd::ACTION_KILL)
      action = Syd.info[:default_chown]
      assert_equal action, "Kill"
      assert Syd.default_chown(Syd::ACTION_EXIT)
      action = Syd.info[:default_chown]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_chown(Syd::ACTION_DENY)
      action = Syd.info[:default_chown]
      assert_equal action, "Deny"

      action = Syd.info[:default_chgrp]
      assert_equal action, "Deny"
      assert Syd.default_chgrp(Syd::ACTION_ALLOW)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Allow"
      assert Syd.default_chgrp(Syd::ACTION_WARN)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Warn"
      assert Syd.default_chgrp(Syd::ACTION_FILTER)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Filter"
      assert Syd.default_chgrp(Syd::ACTION_STOP)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Stop"
      assert Syd.default_chgrp(Syd::ACTION_KILL)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Kill"
      assert Syd.default_chgrp(Syd::ACTION_EXIT)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_chgrp(Syd::ACTION_DENY)
      action = Syd.info[:default_chgrp]
      assert_equal action, "Deny"

      action = Syd.info[:default_chmod]
      assert_equal action, "Deny"
      assert Syd.default_chmod(Syd::ACTION_ALLOW)
      action = Syd.info[:default_chmod]
      assert_equal action, "Allow"
      assert Syd.default_chmod(Syd::ACTION_WARN)
      action = Syd.info[:default_chmod]
      assert_equal action, "Warn"
      assert Syd.default_chmod(Syd::ACTION_FILTER)
      action = Syd.info[:default_chmod]
      assert_equal action, "Filter"
      assert Syd.default_chmod(Syd::ACTION_STOP)
      action = Syd.info[:default_chmod]
      assert_equal action, "Stop"
      assert Syd.default_chmod(Syd::ACTION_KILL)
      action = Syd.info[:default_chmod]
      assert_equal action, "Kill"
      assert Syd.default_chmod(Syd::ACTION_EXIT)
      action = Syd.info[:default_chmod]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_chmod(Syd::ACTION_DENY)
      action = Syd.info[:default_chmod]
      assert_equal action, "Deny"

      action = Syd.info[:default_chattr]
      assert_equal action, "Deny"
      assert Syd.default_chattr(Syd::ACTION_ALLOW)
      action = Syd.info[:default_chattr]
      assert_equal action, "Allow"
      assert Syd.default_chattr(Syd::ACTION_WARN)
      action = Syd.info[:default_chattr]
      assert_equal action, "Warn"
      assert Syd.default_chattr(Syd::ACTION_FILTER)
      action = Syd.info[:default_chattr]
      assert_equal action, "Filter"
      assert Syd.default_chattr(Syd::ACTION_STOP)
      action = Syd.info[:default_chattr]
      assert_equal action, "Stop"
      assert Syd.default_chattr(Syd::ACTION_KILL)
      action = Syd.info[:default_chattr]
      assert_equal action, "Kill"
      assert Syd.default_chattr(Syd::ACTION_EXIT)
      action = Syd.info[:default_chattr]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_chattr(Syd::ACTION_DENY)
      action = Syd.info[:default_chattr]
      assert_equal action, "Deny"

      action = Syd.info[:default_chroot]
      assert_equal action, "Deny"
      assert Syd.default_chroot(Syd::ACTION_ALLOW)
      action = Syd.info[:default_chroot]
      assert_equal action, "Allow"
      assert Syd.default_chroot(Syd::ACTION_WARN)
      action = Syd.info[:default_chroot]
      assert_equal action, "Warn"
      assert Syd.default_chroot(Syd::ACTION_FILTER)
      action = Syd.info[:default_chroot]
      assert_equal action, "Filter"
      assert Syd.default_chroot(Syd::ACTION_STOP)
      action = Syd.info[:default_chroot]
      assert_equal action, "Stop"
      assert Syd.default_chroot(Syd::ACTION_KILL)
      action = Syd.info[:default_chroot]
      assert_equal action, "Kill"
      assert Syd.default_chroot(Syd::ACTION_EXIT)
      action = Syd.info[:default_chroot]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_chroot(Syd::ACTION_DENY)
      action = Syd.info[:default_chroot]
      assert_equal action, "Deny"

      action = Syd.info[:default_utime]
      assert_equal action, "Deny"
      assert Syd.default_utime(Syd::ACTION_ALLOW)
      action = Syd.info[:default_utime]
      assert_equal action, "Allow"
      assert Syd.default_utime(Syd::ACTION_WARN)
      action = Syd.info[:default_utime]
      assert_equal action, "Warn"
      assert Syd.default_utime(Syd::ACTION_FILTER)
      action = Syd.info[:default_utime]
      assert_equal action, "Filter"
      assert Syd.default_utime(Syd::ACTION_STOP)
      action = Syd.info[:default_utime]
      assert_equal action, "Stop"
      assert Syd.default_utime(Syd::ACTION_KILL)
      action = Syd.info[:default_utime]
      assert_equal action, "Kill"
      assert Syd.default_utime(Syd::ACTION_EXIT)
      action = Syd.info[:default_utime]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_utime(Syd::ACTION_DENY)
      action = Syd.info[:default_utime]
      assert_equal action, "Deny"

      action = Syd.info[:default_mkdev]
      assert_equal action, "Deny"
      assert Syd.default_mkdev(Syd::ACTION_ALLOW)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Allow"
      assert Syd.default_mkdev(Syd::ACTION_WARN)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Warn"
      assert Syd.default_mkdev(Syd::ACTION_FILTER)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Filter"
      assert Syd.default_mkdev(Syd::ACTION_STOP)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Stop"
      assert Syd.default_mkdev(Syd::ACTION_KILL)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Kill"
      assert Syd.default_mkdev(Syd::ACTION_EXIT)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_mkdev(Syd::ACTION_DENY)
      action = Syd.info[:default_mkdev]
      assert_equal action, "Deny"

      action = Syd.info[:default_mkfifo]
      assert_equal action, "Deny"
      assert Syd.default_mkfifo(Syd::ACTION_ALLOW)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Allow"
      assert Syd.default_mkfifo(Syd::ACTION_WARN)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Warn"
      assert Syd.default_mkfifo(Syd::ACTION_FILTER)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Filter"
      assert Syd.default_mkfifo(Syd::ACTION_STOP)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Stop"
      assert Syd.default_mkfifo(Syd::ACTION_KILL)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Kill"
      assert Syd.default_mkfifo(Syd::ACTION_EXIT)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_mkfifo(Syd::ACTION_DENY)
      action = Syd.info[:default_mkfifo]
      assert_equal action, "Deny"

      action = Syd.info[:default_mktemp]
      assert_equal action, "Deny"
      assert Syd.default_mktemp(Syd::ACTION_ALLOW)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Allow"
      assert Syd.default_mktemp(Syd::ACTION_WARN)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Warn"
      assert Syd.default_mktemp(Syd::ACTION_FILTER)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Filter"
      assert Syd.default_mktemp(Syd::ACTION_STOP)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Stop"
      assert Syd.default_mktemp(Syd::ACTION_KILL)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Kill"
      assert Syd.default_mktemp(Syd::ACTION_EXIT)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_mktemp(Syd::ACTION_DENY)
      action = Syd.info[:default_mktemp]
      assert_equal action, "Deny"

      action = Syd.info[:default_net_bind]
      assert_equal action, "Deny"
      assert Syd.default_net(Syd::ACTION_ALLOW)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Allow"
      assert Syd.default_net(Syd::ACTION_WARN)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Warn"
      assert Syd.default_net(Syd::ACTION_FILTER)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Filter"
      assert Syd.default_net(Syd::ACTION_STOP)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Stop"
      assert Syd.default_net(Syd::ACTION_KILL)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Kill"
      assert Syd.default_net(Syd::ACTION_EXIT)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_net(Syd::ACTION_DENY)
      action = Syd.info[:default_net_bind]
      assert_equal action, "Deny"

      action = Syd.info[:default_block]
      assert_equal action, "Deny"
      assert_raises(Errno::EINVAL, "default_block_ALLOW") do
        Syd.default_block(Syd::ACTION_ALLOW)
      end
      assert Syd.default_block(Syd::ACTION_WARN)
      action = Syd.info[:default_block]
      assert_equal action, "Warn"
      assert Syd.default_block(Syd::ACTION_FILTER)
      action = Syd.info[:default_block]
      assert_equal action, "Filter"
      assert Syd.default_block(Syd::ACTION_STOP)
      action = Syd.info[:default_block]
      assert_equal action, "Stop"
      assert Syd.default_block(Syd::ACTION_KILL)
      action = Syd.info[:default_block]
      assert_equal action, "Kill"
      assert Syd.default_block(Syd::ACTION_EXIT)
      action = Syd.info[:default_block]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_block(Syd::ACTION_DENY)
      action = Syd.info[:default_block]
      assert_equal action, "Deny"

      action = Syd.info[:default_mem]
      assert_equal action, "Deny"
      assert_raises(Errno::EINVAL, "default_mem_ALLOW") do
        Syd.default_mem(Syd::ACTION_ALLOW)
      end
      assert Syd.default_mem(Syd::ACTION_WARN)
      action = Syd.info[:default_mem]
      assert_equal action, "Warn"
      assert Syd.default_mem(Syd::ACTION_FILTER)
      action = Syd.info[:default_mem]
      assert_equal action, "Filter"
      assert Syd.default_mem(Syd::ACTION_STOP)
      action = Syd.info[:default_mem]
      assert_equal action, "Stop"
      assert Syd.default_mem(Syd::ACTION_KILL)
      action = Syd.info[:default_mem]
      assert_equal action, "Kill"
      assert Syd.default_mem(Syd::ACTION_EXIT)
      action = Syd.info[:default_mem]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_mem(Syd::ACTION_DENY)
      action = Syd.info[:default_mem]
      assert_equal action, "Deny"

      action = Syd.info[:default_pid]
      assert_equal action, "Kill"
      assert_raises(Errno::EINVAL, "default_pid_ALLOW") do
        Syd.default_pid(Syd::ACTION_ALLOW)
      end
      assert Syd.default_pid(Syd::ACTION_WARN)
      action = Syd.info[:default_pid]
      assert_equal action, "Warn"
      assert Syd.default_pid(Syd::ACTION_FILTER)
      action = Syd.info[:default_pid]
      assert_equal action, "Filter"
      assert_raises(Errno::EINVAL, "default_pid_DENY") do
        Syd.default_pid(Syd::ACTION_DENY)
      end
      assert_raises(Errno::EINVAL, "default_pid_STOP") do
        Syd.default_pid(Syd::ACTION_STOP)
      end
      assert Syd.default_pid(Syd::ACTION_EXIT)
      action = Syd.info[:default_pid]
      assert_equal action, "Exit"
      # Ensure we reset to Kill last, so other tests are uneffected.
      assert Syd.default_pid(Syd::ACTION_KILL)
      action = Syd.info[:default_pid]
      assert_equal action, "Kill"

      action = Syd.info[:default_force]
      assert_equal action, "Deny"
      assert_raises(Errno::EINVAL, "default_force_ALLOW") do
        Syd.default_force(Syd::ACTION_ALLOW)
      end
      assert Syd.default_force(Syd::ACTION_WARN)
      action = Syd.info[:default_force]
      assert_equal action, "Warn"
      assert Syd.default_force(Syd::ACTION_FILTER)
      action = Syd.info[:default_force]
      assert_equal action, "Filter"
      assert Syd.default_force(Syd::ACTION_DENY)
      action = Syd.info[:default_force]
      assert_equal action, "Deny"
      assert Syd.default_force(Syd::ACTION_STOP)
      action = Syd.info[:default_force]
      assert_equal action, "Stop"
      assert Syd.default_force(Syd::ACTION_KILL)
      action = Syd.info[:default_force]
      assert_equal action, "Kill"
      assert Syd.default_force(Syd::ACTION_EXIT)
      action = Syd.info[:default_force]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_force(Syd::ACTION_DENY)
      action = Syd.info[:default_force]
      assert_equal action, "Deny"

      action = Syd.info[:default_segvguard]
      assert_equal action, "Deny"
      assert_raises(Errno::EINVAL, "default_segvguard_ALLOW") do
        Syd.default_segvguard(Syd::ACTION_ALLOW)
      end
      assert Syd.default_segvguard(Syd::ACTION_WARN)
      action = Syd.info[:default_segvguard]
      assert_equal action, "Warn"
      assert Syd.default_segvguard(Syd::ACTION_FILTER)
      action = Syd.info[:default_segvguard]
      assert_equal action, "Filter"
      assert Syd.default_segvguard(Syd::ACTION_STOP)
      action = Syd.info[:default_segvguard]
      assert_equal action, "Stop"
      assert Syd.default_segvguard(Syd::ACTION_EXIT)
      action = Syd.info[:default_segvguard]
      assert_equal action, "Exit"
      assert Syd.default_segvguard(Syd::ACTION_KILL)
      action = Syd.info[:default_segvguard]
      assert_equal action, "Kill"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_segvguard(Syd::ACTION_DENY)
      action = Syd.info[:default_segvguard]
      assert_equal action, "Deny"

      action = Syd.info[:default_tpe]
      assert_equal action, "Deny"
      assert_raises(Errno::EINVAL, "default_tpe_ALLOW") do
        Syd.default_tpe(Syd::ACTION_ALLOW)
      end
      assert Syd.default_tpe(Syd::ACTION_WARN)
      action = Syd.info[:default_tpe]
      assert_equal action, "Warn"
      assert Syd.default_tpe(Syd::ACTION_FILTER)
      action = Syd.info[:default_tpe]
      assert_equal action, "Filter"
      assert Syd.default_tpe(Syd::ACTION_STOP)
      action = Syd.info[:default_tpe]
      assert_equal action, "Stop"
      assert Syd.default_tpe(Syd::ACTION_KILL)
      action = Syd.info[:default_tpe]
      assert_equal action, "Kill"
      assert Syd.default_tpe(Syd::ACTION_EXIT)
      action = Syd.info[:default_tpe]
      assert_equal action, "Exit"
      # Ensure we reset to Deny last, so other tests are uneffected.
      assert Syd.default_tpe(Syd::ACTION_DENY)
      action = Syd.info[:default_tpe]
      assert_equal action, "Deny"
    end

    def test_04_ioctl
      assert Syd.ioctl_deny(0xdeadca11)
    end

    def test_05_glob
      path = "/tmp/rbsyd"

      rule = { act: "Allow", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_ALLOW, path) }
      assert Syd.stat_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_WARN, path) }
      assert Syd.stat_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_FILTER, path) }
      assert Syd.stat_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_DENY, path) }
      assert Syd.stat_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_STOP, path) }
      assert Syd.stat_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_KILL, path) }
      assert Syd.stat_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "stat", pat: path }
      assert Syd.stat_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.stat_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.stat_add(Syd::ACTION_EXIT, path) }
      assert Syd.stat_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_ALLOW, path) }
      assert Syd.read_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_WARN, path) }
      assert Syd.read_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_FILTER, path) }
      assert Syd.read_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_DENY, path) }
      assert Syd.read_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_STOP, path) }
      assert Syd.read_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_KILL, path) }
      assert Syd.read_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "read", pat: path }
      assert Syd.read_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.read_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.read_add(Syd::ACTION_EXIT, path) }
      assert Syd.read_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_ALLOW, path) }
      assert Syd.write_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_WARN, path) }
      assert Syd.write_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_FILTER, path) }
      assert Syd.write_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_DENY, path) }
      assert Syd.write_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_STOP, path) }
      assert Syd.write_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_KILL, path) }
      assert Syd.write_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "write", pat: path }
      assert Syd.write_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.write_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.write_add(Syd::ACTION_EXIT, path) }
      assert Syd.write_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_ALLOW, path) }
      assert Syd.exec_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_WARN, path) }
      assert Syd.exec_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_FILTER, path) }
      assert Syd.exec_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_DENY, path) }
      assert Syd.exec_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_STOP, path) }
      assert Syd.exec_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_KILL, path) }
      assert Syd.exec_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "exec", pat: path }
      assert Syd.exec_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.exec_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.exec_add(Syd::ACTION_EXIT, path) }
      assert Syd.exec_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_ALLOW, path) }
      assert Syd.ioctl_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_WARN, path) }
      assert Syd.ioctl_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_FILTER, path) }
      assert Syd.ioctl_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_DENY, path) }
      assert Syd.ioctl_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_STOP, path) }
      assert Syd.ioctl_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_KILL, path) }
      assert Syd.ioctl_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "ioctl", pat: path }
      assert Syd.ioctl_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.ioctl_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.ioctl_add(Syd::ACTION_EXIT, path) }
      assert Syd.ioctl_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_ALLOW, path) }
      assert Syd.create_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_WARN, path) }
      assert Syd.create_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_FILTER, path) }
      assert Syd.create_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_DENY, path) }
      assert Syd.create_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_STOP, path) }
      assert Syd.create_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_KILL, path) }
      assert Syd.create_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "create", pat: path }
      assert Syd.create_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.create_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.create_add(Syd::ACTION_EXIT, path) }
      assert Syd.create_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_ALLOW, path) }
      assert Syd.delete_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_WARN, path) }
      assert Syd.delete_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_FILTER, path) }
      assert Syd.delete_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_DENY, path) }
      assert Syd.delete_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_STOP, path) }
      assert Syd.delete_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_KILL, path) }
      assert Syd.delete_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "delete", pat: path }
      assert Syd.delete_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.delete_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.delete_add(Syd::ACTION_EXIT, path) }
      assert Syd.delete_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_ALLOW, path) }
      assert Syd.rename_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_WARN, path) }
      assert Syd.rename_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_FILTER, path) }
      assert Syd.rename_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_DENY, path) }
      assert Syd.rename_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_STOP, path) }
      assert Syd.rename_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_KILL, path) }
      assert Syd.rename_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "rename", pat: path }
      assert Syd.rename_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.rename_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.rename_add(Syd::ACTION_EXIT, path) }
      assert Syd.rename_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_ALLOW, path) }
      assert Syd.symlink_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_WARN, path) }
      assert Syd.symlink_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_FILTER, path) }
      assert Syd.symlink_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_DENY, path) }
      assert Syd.symlink_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_STOP, path) }
      assert Syd.symlink_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_KILL, path) }
      assert Syd.symlink_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "symlink", pat: path }
      assert Syd.symlink_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.symlink_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.symlink_add(Syd::ACTION_EXIT, path) }
      assert Syd.symlink_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_ALLOW, path) }
      assert Syd.truncate_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_WARN, path) }
      assert Syd.truncate_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_FILTER, path) }
      assert Syd.truncate_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_DENY, path) }
      assert Syd.truncate_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_STOP, path) }
      assert Syd.truncate_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_KILL, path) }
      assert Syd.truncate_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "truncate", pat: path }
      assert Syd.truncate_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.truncate_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.truncate_add(Syd::ACTION_EXIT, path) }
      assert Syd.truncate_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_ALLOW, path) }
      assert Syd.chdir_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_WARN, path) }
      assert Syd.chdir_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_FILTER, path) }
      assert Syd.chdir_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_DENY, path) }
      assert Syd.chdir_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_STOP, path) }
      assert Syd.chdir_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_KILL, path) }
      assert Syd.chdir_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "chdir", pat: path }
      assert Syd.chdir_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chdir_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chdir_add(Syd::ACTION_EXIT, path) }
      assert Syd.chdir_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_ALLOW, path) }
      assert Syd.readdir_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_WARN, path) }
      assert Syd.readdir_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_FILTER, path) }
      assert Syd.readdir_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_DENY, path) }
      assert Syd.readdir_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_STOP, path) }
      assert Syd.readdir_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_KILL, path) }
      assert Syd.readdir_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "readdir", pat: path }
      assert Syd.readdir_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.readdir_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.readdir_add(Syd::ACTION_EXIT, path) }
      assert Syd.readdir_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_ALLOW, path) }
      assert Syd.mkdir_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_WARN, path) }
      assert Syd.mkdir_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_FILTER, path) }
      assert Syd.mkdir_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_DENY, path) }
      assert Syd.mkdir_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_STOP, path) }
      assert Syd.mkdir_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_KILL, path) }
      assert Syd.mkdir_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "mkdir", pat: path }
      assert Syd.mkdir_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdir_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdir_add(Syd::ACTION_EXIT, path) }
      assert Syd.mkdir_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_ALLOW, path) }
      assert Syd.chown_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_WARN, path) }
      assert Syd.chown_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_FILTER, path) }
      assert Syd.chown_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_DENY, path) }
      assert Syd.chown_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_STOP, path) }
      assert Syd.chown_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_KILL, path) }
      assert Syd.chown_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "chown", pat: path }
      assert Syd.chown_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chown_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chown_add(Syd::ACTION_EXIT, path) }
      assert Syd.chown_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_ALLOW, path) }
      assert Syd.chgrp_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_WARN, path) }
      assert Syd.chgrp_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_FILTER, path) }
      assert Syd.chgrp_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_DENY, path) }
      assert Syd.chgrp_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_STOP, path) }
      assert Syd.chgrp_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_KILL, path) }
      assert Syd.chgrp_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "chgrp", pat: path }
      assert Syd.chgrp_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chgrp_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chgrp_add(Syd::ACTION_EXIT, path) }
      assert Syd.chgrp_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_ALLOW, path) }
      assert Syd.chmod_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_WARN, path) }
      assert Syd.chmod_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_FILTER, path) }
      assert Syd.chmod_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_DENY, path) }
      assert Syd.chmod_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_STOP, path) }
      assert Syd.chmod_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_KILL, path) }
      assert Syd.chmod_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "chmod", pat: path }
      assert Syd.chmod_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chmod_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chmod_add(Syd::ACTION_EXIT, path) }
      assert Syd.chmod_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_ALLOW, path) }
      assert Syd.chattr_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_WARN, path) }
      assert Syd.chattr_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_FILTER, path) }
      assert Syd.chattr_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_DENY, path) }
      assert Syd.chattr_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_STOP, path) }
      assert Syd.chattr_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_KILL, path) }
      assert Syd.chattr_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "chattr", pat: path }
      assert Syd.chattr_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.chattr_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.chattr_add(Syd::ACTION_EXIT, path) }
      assert Syd.chattr_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      # Chroot is startup only since 3.32.4
      #      rule = { act: "Allow", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_ALLOW, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_ALLOW, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_ALLOW, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_ALLOW, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      rule = { act: "Warn", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_WARN, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_WARN, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_WARN, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_WARN, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      rule = { act: "Filter", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_FILTER, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_FILTER, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_FILTER, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_FILTER, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      rule = { act: "Deny", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_DENY, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_DENY, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_DENY, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_DENY, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      rule = { act: "Stop", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_STOP, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_STOP, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_STOP, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_STOP, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      rule = { act: "Kill", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_KILL, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_KILL, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_KILL, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_KILL, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      rule = { act: "Exit", cap: "chroot", pat: path }
      #      assert Syd.chroot_add(Syd::ACTION_EXIT, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_equal idx, rules.length - 1
      #
      #      assert Syd.chroot_del(Syd::ACTION_EXIT, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx
      #
      #      3.times { assert Syd.chroot_add(Syd::ACTION_EXIT, path) }
      #      assert Syd.chroot_rem(Syd::ACTION_EXIT, path)
      #      rules = Syd.info[:glob_rules]
      #      idx = find(rules, rule)
      #      assert_nil idx

      rule = { act: "Allow", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_ALLOW, path) }
      assert Syd.utime_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_WARN, path) }
      assert Syd.utime_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_FILTER, path) }
      assert Syd.utime_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_DENY, path) }
      assert Syd.utime_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_STOP, path) }
      assert Syd.utime_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_KILL, path) }
      assert Syd.utime_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "utime", pat: path }
      assert Syd.utime_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.utime_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.utime_add(Syd::ACTION_EXIT, path) }
      assert Syd.utime_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_ALLOW, path) }
      assert Syd.mkdev_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_WARN, path) }
      assert Syd.mkdev_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_FILTER, path) }
      assert Syd.mkdev_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_DENY, path) }
      assert Syd.mkdev_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_STOP, path) }
      assert Syd.mkdev_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_KILL, path) }
      assert Syd.mkdev_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "mkdev", pat: path }
      assert Syd.mkdev_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkdev_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkdev_add(Syd::ACTION_EXIT, path) }
      assert Syd.mkdev_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_ALLOW, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_WARN, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_FILTER, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_DENY, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_STOP, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_KILL, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "mkfifo", pat: path }
      assert Syd.mkfifo_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mkfifo_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mkfifo_add(Syd::ACTION_EXIT, path) }
      assert Syd.mkfifo_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_ALLOW, path) }
      assert Syd.mktemp_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_WARN, path) }
      assert Syd.mktemp_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_FILTER, path) }
      assert Syd.mktemp_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_DENY, path) }
      assert Syd.mktemp_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_STOP, path) }
      assert Syd.mktemp_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_KILL, path) }
      assert Syd.mktemp_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "mktemp", pat: path }
      assert Syd.mktemp_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.mktemp_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.mktemp_add(Syd::ACTION_EXIT, path) }
      assert Syd.mktemp_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_ALLOW, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_ALLOW, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Warn", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_WARN, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_WARN, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_FILTER, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_FILTER, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_DENY, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_DENY, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Stop", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_STOP, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_STOP, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Kill", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_KILL, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_KILL, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Exit", cap: "net/sendfd", pat: path }
      assert Syd.net_sendfd_add(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_sendfd_del(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_sendfd_add(Syd::ACTION_EXIT, path) }
      assert Syd.net_sendfd_rem(Syd::ACTION_EXIT, path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx
    end

    def test_06_addr
      host = "127.3.1.4/8"
      port = 16
      addr = "#{host}!#{port}"

      rule = { act: "Allow", cap: "net/bind", pat: { addr: host, port: port } }
      assert Syd.net_bind_add(Syd::ACTION_ALLOW, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_bind_del(Syd::ACTION_ALLOW, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_bind_add(Syd::ACTION_ALLOW, addr) }
      assert Syd.net_bind_rem(Syd::ACTION_ALLOW, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "net/bind", pat: { addr: host, port: port } }
      assert Syd.net_bind_add(Syd::ACTION_DENY, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_bind_del(Syd::ACTION_DENY, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_bind_add(Syd::ACTION_DENY, addr) }
      assert Syd.net_bind_rem(Syd::ACTION_DENY, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "net/bind", pat: { addr: host, port: port } }
      assert Syd.net_bind_add(Syd::ACTION_FILTER, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_bind_del(Syd::ACTION_FILTER, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_bind_add(Syd::ACTION_FILTER, addr) }
      assert Syd.net_bind_rem(Syd::ACTION_FILTER, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "net/connect", pat: { addr: host, port: port } }
      assert Syd.net_connect_add(Syd::ACTION_ALLOW, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_connect_del(Syd::ACTION_ALLOW, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_connect_add(Syd::ACTION_ALLOW, addr) }
      assert Syd.net_connect_rem(Syd::ACTION_ALLOW, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "net/connect", pat: { addr: host, port: port } }
      assert Syd.net_connect_add(Syd::ACTION_DENY, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_connect_del(Syd::ACTION_DENY, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_connect_add(Syd::ACTION_DENY, addr) }
      assert Syd.net_connect_rem(Syd::ACTION_DENY, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "net/connect", pat: { addr: host, port: port } }
      assert Syd.net_connect_add(Syd::ACTION_FILTER, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.net_connect_del(Syd::ACTION_FILTER, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.net_connect_add(Syd::ACTION_FILTER, addr) }
      assert Syd.net_connect_rem(Syd::ACTION_FILTER, addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx
    end

    def test_07_force
      sha = "0" * 128
      path = "/tmp/rbsyd"
      rule = { act: "Kill", sha: sha, pat: path }

      # Invalid actions
      [-1, -10, -100, 10, 20, 30].each do |invalid_action|
        assert_raises(Errno::EINVAL, "force_add #{invalid_action}") do
          Syd.force_add(path, sha, invalid_action)
        end
      end

      # ALLOW is an invalid action for add but not def.
      assert_raises(Errno::EINVAL, "force_add ALLOW") do
        Syd.force_add(path, sha, Syd::ACTION_ALLOW)
      end

      assert Syd.force_add(path, sha, Syd::ACTION_KILL)
      rules = Syd.info[:force_rules]
      idx = find(rules, rule)
      refute_nil idx

      assert Syd.force_del(path)
      rules = Syd.info[:force_rules]
      idx = find(rules, rule)
      assert_nil idx

      assert Syd.force_add("#{path}_1", sha, Syd::ACTION_WARN)
      assert Syd.force_add("#{path}_2", sha, Syd::ACTION_KILL)
      assert Syd.force_clr
      assert_empty Syd.info[:force_rules]
    end

    def test_08_segvguard
      segvguard_expiry_orig = Syd.info[:segvguard_expiry]
      assert_equal true, Syd.segvguard_expiry(4096)
      assert_equal 4096, Syd.info[:segvguard_expiry]
      assert_equal true, Syd.segvguard_expiry(8192)
      assert_equal 8192, Syd.info[:segvguard_expiry]
      Syd.segvguard_expiry(segvguard_expiry_orig)

      segvguard_suspension_orig = Syd.info[:segvguard_suspension]
      assert_equal true, Syd.segvguard_suspension(4096)
      assert_equal 4096, Syd.info[:segvguard_suspension]
      assert_equal true, Syd.segvguard_suspension(8192)
      assert_equal 8192, Syd.info[:segvguard_suspension]
      Syd.segvguard_suspension(segvguard_suspension_orig)

      segvguard_maxcrashes_orig = Syd.info[:segvguard_maxcrashes]
      assert_equal true, Syd.segvguard_maxcrashes(40)
      assert_equal 40, Syd.info[:segvguard_maxcrashes]
      assert_equal true, Syd.segvguard_maxcrashes(81)
      assert_equal 81, Syd.info[:segvguard_maxcrashes]
      Syd.segvguard_maxcrashes(segvguard_maxcrashes_orig)
    end

    def test_09_exec
      # Create a temporary directory
      Dir.mktmpdir do |temp|
        path = File.join(temp, "file")

        # Prepare the command and arguments
        file = "/bin/sh"
        argv = ["-c", "echo 42 > '#{path}'"]

        # Execute the command
        assert_equal true, Syd.exec(file, argv), "exec"

        # Wait for the command to execute
        sleep 3

        # Assert the contents of the file
        contents = File.read(path).chomp
        assert_equal "42", contents, "exec contents"
      end
    end

    def test_10_load
      # Create a temporary file and write the specified content to it
      Tempfile.open do |tempfile|
        tempfile.write("pid/max:77\n")
        tempfile.rewind # Seek back to the beginning of the file

        # Load the file descriptor with Syd.load
        Syd.load(tempfile.fileno)

        # Fetch information with Syd.info
        info = Syd.info

        # Check if pid_max is equal to 77
        assert_equal 77, info[:pid_max], "Expected pid_max to be 77"
      end
    end

    def test_11_lock
      # Invalid states
      [-1, -10, -100, 10, 20, 30].each do |invalid_state|
        assert_raises(Errno::EINVAL, "lock #{invalid_state}") do
          Syd.lock(invalid_state)
        end
      end

      # This locks the sandbox in the last iteration.
      [Syd::LOCK_OFF, Syd::LOCK_EXEC, Syd::LOCK_ON].each do |valid_state|
        assert_equal true, Syd.lock(valid_state), "LOCK state set to #{valid_state}"
      end

      [Syd::LOCK_OFF, Syd::LOCK_EXEC, Syd::LOCK_ON].each do |no_state|
        # Once locked valid states will error too.
        assert_raises(Errno::ENOENT, "lock #{no_state}") do
          Syd.lock(no_state)
        end
      end
    end
  end

  private
  def find(rules, pattern)
    rules.reverse_each.with_index do |rule, idx|
      return rules.length - 1 - idx if pattern == rule
    end
    nil
  end
end
