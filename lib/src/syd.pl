#!/usr/bin/env perl
# coding: utf-8
#
# Syd: rock-solid application kernel
# lib/src/test.pl: Tests for Perl bindings of libsyd, the syd API C Library
# Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0

use strict;
use warnings;
use Test::More;
use Errno      qw(EINVAL ENOENT);
use Encode     qw( encode );
use File::Temp qw( tempdir tempfile );

BEGIN {
	use Cwd 'abs_path';
	use File::Basename 'dirname';
	unshift @INC, dirname(abs_path($0));
}
use syd qw(:all);

sub find {
	my ($rules_ref, $pattern_ref, $comparator) = @_;
	my @rules   = @{$rules_ref};
	my %pattern = %{$pattern_ref};

	for (my $i = 0; $i < @rules; $i++) {
		my %rule = %{$rules[$#rules - $i]};
		if ($comparator->(\%rule, \%pattern)) {
			return $#rules - $i;
		}
	}

	return undef;
}

eval { check() };
if ($@) {
	if ($! == ENOENT) {
		plan skip_all => 'not running under syd';
	}
	print "Unexpected error checking for syd: $!\n";
	exit 1;
}

plan tests => 1373;

my $api = api();
is($api, 3, 'syd API');

my $temp = tempdir(CLEANUP => 1);
my $path = "${temp}/file";

my $file = "/bin/sh";
my @argv = ("-c", 'echo 42 > "' . $path . '"');
ok(exec($file, \@argv), 'exec');

# Wait for syd to execute the process.
sleep 3;

# Assert the contents of the file
{
	open my $fh, '<', $path or die "Can't open file: $!";
	my $contents = do { local $/; <$fh> };
	chomp $contents;
	is($contents, '42', 'exec contents');
}

my $state = enabled_stat();
ok(enable_stat(),   "enable_stat");
ok(enabled_stat(),  "enabled_stat");
ok(disable_stat(),  "disable_stat");
ok(!enabled_stat(), "!enabled_stat");
if ($state) {
	enable_stat();
} else {
	disable_stat();
}

$state = enabled_read();
ok(enable_read(),   "enable_read");
ok(enabled_read(),  "enabled_read");
ok(disable_read(),  "disable_read");
ok(!enabled_read(), "!enabled_read");
if ($state) {
	enable_read();
} else {
	disable_read();
}

$state = enabled_write();
ok(enable_write(),   "enable_write");
ok(enabled_write(),  "enabled_write");
ok(disable_write(),  "disable_write");
ok(!enabled_write(), "!enabled_write");
if ($state) {
	enable_write();
} else {
	disable_write();
}

$state = enabled_exec();
ok(enable_exec(),   "enable_exec");
ok(enabled_exec(),  "enabled_exec");
ok(disable_exec(),  "disable_exec");
ok(!enabled_exec(), "!enabled_exec");
if ($state) {
	enable_exec();
} else {
	disable_exec();
}

$state = enabled_ioctl();
ok(enable_ioctl(),   "enable_ioctl");
ok(enabled_ioctl(),  "enabled_ioctl");
ok(disable_ioctl(),  "disable_ioctl");
ok(!enabled_ioctl(), "!enabled_ioctl");
if ($state) {
	enable_ioctl();
} else {
	disable_ioctl();
}

$state = enabled_create();
ok(enable_create(),   "enable_create");
ok(enabled_create(),  "enabled_create");
ok(disable_create(),  "disable_create");
ok(!enabled_create(), "!enabled_create");
if ($state) {
	enable_create();
} else {
	disable_create();
}

$state = enabled_delete();
ok(enable_delete(),   "enable_delete");
ok(enabled_delete(),  "enabled_delete");
ok(disable_delete(),  "disable_delete");
ok(!enabled_delete(), "!enabled_delete");
if ($state) {
	enable_delete();
} else {
	disable_delete();
}

$state = enabled_rename();
ok(enable_rename(),   "enable_rename");
ok(enabled_rename(),  "enabled_rename");
ok(disable_rename(),  "disable_rename");
ok(!enabled_rename(), "!enabled_rename");
if ($state) {
	enable_rename();
} else {
	disable_rename();
}

$state = enabled_symlink();
ok(enable_symlink(),   "enable_symlink");
ok(enabled_symlink(),  "enabled_symlink");
ok(disable_symlink(),  "disable_symlink");
ok(!enabled_symlink(), "!enabled_symlink");
if ($state) {
	enable_symlink();
} else {
	disable_symlink();
}

$state = enabled_truncate();
ok(enable_truncate(),   "enable_truncate");
ok(enabled_truncate(),  "enabled_truncate");
ok(disable_truncate(),  "disable_truncate");
ok(!enabled_truncate(), "!enabled_truncate");
if ($state) {
	enable_truncate();
} else {
	disable_truncate();
}

$state = enabled_chdir();
ok(enable_chdir(),   "enable_chdir");
ok(enabled_chdir(),  "enabled_chdir");
ok(disable_chdir(),  "disable_chdir");
ok(!enabled_chdir(), "!enabled_chdir");
if ($state) {
	enable_chdir();
} else {
	disable_chdir();
}

$state = enabled_readdir();
ok(enable_readdir(),   "enable_readdir");
ok(enabled_readdir(),  "enabled_readdir");
ok(disable_readdir(),  "disable_readdir");
ok(!enabled_readdir(), "!enabled_readdir");
if ($state) {
	enable_readdir();
} else {
	disable_readdir();
}

$state = enabled_mkdir();
ok(enable_mkdir(),   "enable_mkdir");
ok(enabled_mkdir(),  "enabled_mkdir");
ok(disable_mkdir(),  "disable_mkdir");
ok(!enabled_mkdir(), "!enabled_mkdir");
if ($state) {
	enable_mkdir();
} else {
	disable_mkdir();
}

$state = enabled_chown();
ok(enable_chown(),   "enable_chown");
ok(enabled_chown(),  "enabled_chown");
ok(disable_chown(),  "disable_chown");
ok(!enabled_chown(), "!enabled_chown");
if ($state) {
	enable_chown();
} else {
	disable_chown();
}

$state = enabled_chgrp();
ok(enable_chgrp(),   "enable_chgrp");
ok(enabled_chgrp(),  "enabled_chgrp");
ok(disable_chgrp(),  "disable_chgrp");
ok(!enabled_chgrp(), "!enabled_chgrp");
if ($state) {
	enable_chgrp();
} else {
	disable_chgrp();
}

$state = enabled_chmod();
ok(enable_chmod(),   "enable_chmod");
ok(enabled_chmod(),  "enabled_chmod");
ok(disable_chmod(),  "disable_chmod");
ok(!enabled_chmod(), "!enabled_chmod");
if ($state) {
	enable_chmod();
} else {
	disable_chmod();
}

$state = enabled_chattr();
ok(enable_chattr(),   "enable_chattr");
ok(enabled_chattr(),  "enabled_chattr");
ok(disable_chattr(),  "disable_chattr");
ok(!enabled_chattr(), "!enabled_chattr");
if ($state) {
	enable_chattr();
} else {
	disable_chattr();
}

# Chroot is startup only since 3.32.4
#$state = enabled_chroot();
#ok(enable_chroot(),   "enable_chroot");
#ok(enabled_chroot(),  "enabled_chroot");
#ok(disable_chroot(),  "disable_chroot");
#ok(!enabled_chroot(), "!enabled_chroot");
#if ($state) {
#	enable_chroot();
#} else {
#	disable_chroot();
#}

$state = enabled_utime();
ok(enable_utime(),   "enable_utime");
ok(enabled_utime(),  "enabled_utime");
ok(disable_utime(),  "disable_utime");
ok(!enabled_utime(), "!enabled_utime");
if ($state) {
	enable_utime();
} else {
	disable_utime();
}

$state = enabled_mkdev();
ok(enable_mkdev(),   "enable_mkdev");
ok(enabled_mkdev(),  "enabled_mkdev");
ok(disable_mkdev(),  "disable_mkdev");
ok(!enabled_mkdev(), "!enabled_mkdev");
if ($state) {
	enable_mkdev();
} else {
	disable_mkdev();
}

$state = enabled_mkfifo();
ok(enable_mkfifo(),   "enable_mkfifo");
ok(enabled_mkfifo(),  "enabled_mkfifo");
ok(disable_mkfifo(),  "disable_mkfifo");
ok(!enabled_mkfifo(), "!enabled_mkfifo");
if ($state) {
	enable_mkfifo();
} else {
	disable_mkfifo();
}

$state = enabled_mktemp();
ok(enable_mktemp(),   "enable_mktemp");
ok(enabled_mktemp(),  "enabled_mktemp");
ok(disable_mktemp(),  "disable_mktemp");
ok(!enabled_mktemp(), "!enabled_mktemp");
if ($state) {
	enable_mktemp();
} else {
	disable_mktemp();
}

$state = enabled_net();
ok(enable_net(),   "enable_net");
ok(enabled_net(),  "enabled_net");
ok(disable_net(),  "disable_net");
ok(!enabled_net(), "!enabled_net");
if ($state) {
	enable_net();
} else {
	disable_net();
}

ok(!enabled_lock(),  "!enabled_lock");
ok(!enabled_crypt(), "!enabled_crypt");
ok(!enabled_proxy(), "!enabled_proxy");

$state = enabled_mem();
ok(enable_mem(),   'enable_mem');
ok(enabled_mem(),  'enabled_mem');
ok(disable_mem(),  'disable_mem');
ok(!enabled_mem(), '!enabled_mem');
if ($state) {
	enable_mem();
} else {
	disable_mem();
}

$state = enabled_pid();
ok(enable_pid(),   "enable_pid");
ok(enabled_pid(),  "enabled_pid");
ok(disable_pid(),  "disable_pid");
ok(!enabled_pid(), "!enabled_pid");
if ($state) {
	enable_pid();
} else {
	disable_pid();
}

$state = enabled_force();
ok(enable_force(),   "enable_force");
ok(enabled_force(),  "enabled_force");
ok(disable_force(),  "disable_force");
ok(!enabled_force(), "!enabled_force");
if ($state) {
	enable_force();
} else {
	disable_force();
}

$state = enabled_tpe();
ok(enable_tpe(),   "enable_tpe");
ok(enabled_tpe(),  "enabled_tpe");
ok(disable_tpe(),  "disable_tpe");
ok(!enabled_tpe(), "!enabled_tpe");
if ($state) {
	enable_tpe();
} else {
	disable_tpe();
}

my $info   = info();
my $action = $info->{default_stat};
ok($action,                    "Deny");
ok(default_stat(ACTION_ALLOW), "default_stat_ALLOW");
$info   = info();
$action = $info->{default_stat};
ok($action,                   "Allow");
ok(default_stat(ACTION_WARN), "default_stat_WARN");
$info   = info();
$action = $info->{default_stat};
ok($action,                     "Warn");
ok(default_stat(ACTION_FILTER), "default_stat_FILTER");
$info   = info();
$action = $info->{default_stat};
ok($action,                   "Filter");
ok(default_stat(ACTION_STOP), "default_stat_STOP");
$info   = info();
$action = $info->{default_stat};
ok($action,                   "Stop");
ok(default_stat(ACTION_KILL), "default_stat_KILL");
$info   = info();
$action = $info->{default_stat};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_stat(ACTION_DENY), "default_stat_DENY");
$info   = info();
$action = $info->{default_stat};
ok($action, "Deny");

$info   = info();
$action = $info->{default_read};
ok($action,                    "Deny");
ok(default_read(ACTION_ALLOW), "default_read_ALLOW");
$info   = info();
$action = $info->{default_read};
ok($action,                   "Allow");
ok(default_read(ACTION_WARN), "default_read_WARN");
$info   = info();
$action = $info->{default_read};
ok($action,                     "Warn");
ok(default_read(ACTION_FILTER), "default_read_FILTER");
$info   = info();
$action = $info->{default_read};
ok($action,                   "Filter");
ok(default_read(ACTION_STOP), "default_read_STOP");
$info   = info();
$action = $info->{default_read};
ok($action,                   "Stop");
ok(default_read(ACTION_KILL), "default_read_KILL");
$info   = info();
$action = $info->{default_read};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_read(ACTION_DENY), "default_read_DENY");
$info   = info();
$action = $info->{default_read};
ok($action, "Deny");

$info   = info();
$action = $info->{default_write};
ok($action,                     "Deny");
ok(default_write(ACTION_ALLOW), "default_write_ALLOW");
$info   = info();
$action = $info->{default_write};
ok($action,                    "Allow");
ok(default_write(ACTION_WARN), "default_write_WARN");
$info   = info();
$action = $info->{default_write};
ok($action,                      "Warn");
ok(default_write(ACTION_FILTER), "default_write_FILTER");
$info   = info();
$action = $info->{default_write};
ok($action,                    "Filter");
ok(default_write(ACTION_STOP), "default_write_STOP");
$info   = info();
$action = $info->{default_write};
ok($action,                    "Stop");
ok(default_write(ACTION_KILL), "default_write_KILL");
$info   = info();
$action = $info->{default_write};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_write(ACTION_DENY), "default_write_DENY");
$info   = info();
$action = $info->{default_write};
ok($action, "Deny");

$info   = info();
$action = $info->{default_exec};
ok($action,                    "Deny");
ok(default_exec(ACTION_ALLOW), "default_exec_ALLOW");
$info   = info();
$action = $info->{default_exec};
ok($action,                   "Allow");
ok(default_exec(ACTION_WARN), "default_exec_WARN");
$info   = info();
$action = $info->{default_exec};
ok($action,                     "Warn");
ok(default_exec(ACTION_FILTER), "default_exec_FILTER");
$info   = info();
$action = $info->{default_exec};
ok($action,                   "Filter");
ok(default_exec(ACTION_STOP), "default_exec_STOP");
$info   = info();
$action = $info->{default_exec};
ok($action,                   "Stop");
ok(default_exec(ACTION_KILL), "default_exec_KILL");
$info   = info();
$action = $info->{default_exec};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_exec(ACTION_DENY), "default_exec_DENY");
$info   = info();
$action = $info->{default_exec};
ok($action, "Deny");

$info   = info();
$action = $info->{default_ioctl};
ok($action,                     "Deny");
ok(default_ioctl(ACTION_ALLOW), "default_ioctl_ALLOW");
$info   = info();
$action = $info->{default_ioctl};
ok($action,                    "Allow");
ok(default_ioctl(ACTION_WARN), "default_ioctl_WARN");
$info   = info();
$action = $info->{default_ioctl};
ok($action,                      "Warn");
ok(default_ioctl(ACTION_FILTER), "default_ioctl_FILTER");
$info   = info();
$action = $info->{default_ioctl};
ok($action,                    "Filter");
ok(default_ioctl(ACTION_STOP), "default_ioctl_STOP");
$info   = info();
$action = $info->{default_ioctl};
ok($action,                    "Stop");
ok(default_ioctl(ACTION_KILL), "default_ioctl_KILL");
$info   = info();
$action = $info->{default_ioctl};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_ioctl(ACTION_DENY), "default_ioctl_DENY");
$info   = info();
$action = $info->{default_ioctl};
ok($action, "Deny");

$info   = info();
$action = $info->{default_create};
ok($action,                      "Deny");
ok(default_create(ACTION_ALLOW), "default_create_ALLOW");
$info   = info();
$action = $info->{default_create};
ok($action,                     "Allow");
ok(default_create(ACTION_WARN), "default_create_WARN");
$info   = info();
$action = $info->{default_create};
ok($action,                       "Warn");
ok(default_create(ACTION_FILTER), "default_create_FILTER");
$info   = info();
$action = $info->{default_create};
ok($action,                     "Filter");
ok(default_create(ACTION_STOP), "default_create_STOP");
$info   = info();
$action = $info->{default_create};
ok($action,                     "Stop");
ok(default_create(ACTION_KILL), "default_create_KILL");
$info   = info();
$action = $info->{default_create};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_create(ACTION_DENY), "default_create_DENY");
$info   = info();
$action = $info->{default_create};
ok($action, "Deny");

$info   = info();
$action = $info->{default_delete};
ok($action,                      "Deny");
ok(default_delete(ACTION_ALLOW), "default_delete_ALLOW");
$info   = info();
$action = $info->{default_delete};
ok($action,                     "Allow");
ok(default_delete(ACTION_WARN), "default_delete_WARN");
$info   = info();
$action = $info->{default_delete};
ok($action,                       "Warn");
ok(default_delete(ACTION_FILTER), "default_delete_FILTER");
$info   = info();
$action = $info->{default_delete};
ok($action,                     "Filter");
ok(default_delete(ACTION_STOP), "default_delete_STOP");
$info   = info();
$action = $info->{default_delete};
ok($action,                     "Stop");
ok(default_delete(ACTION_KILL), "default_delete_KILL");
$info   = info();
$action = $info->{default_delete};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_delete(ACTION_DENY), "default_delete_DENY");
$info   = info();
$action = $info->{default_delete};
ok($action, "Deny");

$info   = info();
$action = $info->{default_rename};
ok($action,                      "Deny");
ok(default_rename(ACTION_ALLOW), "default_rename_ALLOW");
$info   = info();
$action = $info->{default_rename};
ok($action,                     "Allow");
ok(default_rename(ACTION_WARN), "default_rename_WARN");
$info   = info();
$action = $info->{default_rename};
ok($action,                       "Warn");
ok(default_rename(ACTION_FILTER), "default_rename_FILTER");
$info   = info();
$action = $info->{default_rename};
ok($action,                     "Filter");
ok(default_rename(ACTION_STOP), "default_rename_STOP");
$info   = info();
$action = $info->{default_rename};
ok($action,                     "Stop");
ok(default_rename(ACTION_KILL), "default_rename_KILL");
$info   = info();
$action = $info->{default_rename};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_rename(ACTION_DENY), "default_rename_DENY");
$info   = info();
$action = $info->{default_rename};
ok($action, "Deny");

$info   = info();
$action = $info->{default_symlink};
ok($action,                       "Deny");
ok(default_symlink(ACTION_ALLOW), "default_symlink_ALLOW");
$info   = info();
$action = $info->{default_symlink};
ok($action,                      "Allow");
ok(default_symlink(ACTION_WARN), "default_symlink_WARN");
$info   = info();
$action = $info->{default_symlink};
ok($action,                        "Warn");
ok(default_symlink(ACTION_FILTER), "default_symlink_FILTER");
$info   = info();
$action = $info->{default_symlink};
ok($action,                      "Filter");
ok(default_symlink(ACTION_STOP), "default_symlink_STOP");
$info   = info();
$action = $info->{default_symlink};
ok($action,                      "Stop");
ok(default_symlink(ACTION_KILL), "default_symlink_KILL");
$info   = info();
$action = $info->{default_symlink};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_symlink(ACTION_DENY), "default_symlink_DENY");
$info   = info();
$action = $info->{default_symlink};
ok($action, "Deny");

$info   = info();
$action = $info->{default_truncate};
ok($action,                        "Deny");
ok(default_truncate(ACTION_ALLOW), "default_truncate_ALLOW");
$info   = info();
$action = $info->{default_truncate};
ok($action,                       "Allow");
ok(default_truncate(ACTION_WARN), "default_truncate_WARN");
$info   = info();
$action = $info->{default_truncate};
ok($action,                         "Warn");
ok(default_truncate(ACTION_FILTER), "default_truncate_FILTER");
$info   = info();
$action = $info->{default_truncate};
ok($action,                       "Filter");
ok(default_truncate(ACTION_STOP), "default_truncate_STOP");
$info   = info();
$action = $info->{default_truncate};
ok($action,                       "Stop");
ok(default_truncate(ACTION_KILL), "default_truncate_KILL");
$info   = info();
$action = $info->{default_truncate};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_truncate(ACTION_DENY), "default_truncate_DENY");
$info   = info();
$action = $info->{default_truncate};
ok($action, "Deny");

$info   = info();
$action = $info->{default_chdir};
ok($action,                     "Deny");
ok(default_chdir(ACTION_ALLOW), "default_chdir_ALLOW");
$info   = info();
$action = $info->{default_chdir};
ok($action,                    "Allow");
ok(default_chdir(ACTION_WARN), "default_chdir_WARN");
$info   = info();
$action = $info->{default_chdir};
ok($action,                      "Warn");
ok(default_chdir(ACTION_FILTER), "default_chdir_FILTER");
$info   = info();
$action = $info->{default_chdir};
ok($action,                    "Filter");
ok(default_chdir(ACTION_STOP), "default_chdir_STOP");
$info   = info();
$action = $info->{default_chdir};
ok($action,                    "Stop");
ok(default_chdir(ACTION_KILL), "default_chdir_KILL");
$info   = info();
$action = $info->{default_chdir};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_chdir(ACTION_DENY), "default_chdir_DENY");
$info   = info();
$action = $info->{default_chdir};
ok($action, "Deny");

$info   = info();
$action = $info->{default_readdir};
ok($action,                       "Deny");
ok(default_readdir(ACTION_ALLOW), "default_readdir_ALLOW");
$info   = info();
$action = $info->{default_readdir};
ok($action,                      "Allow");
ok(default_readdir(ACTION_WARN), "default_readdir_WARN");
$info   = info();
$action = $info->{default_readdir};
ok($action,                        "Warn");
ok(default_readdir(ACTION_FILTER), "default_readdir_FILTER");
$info   = info();
$action = $info->{default_readdir};
ok($action,                      "Filter");
ok(default_readdir(ACTION_STOP), "default_readdir_STOP");
$info   = info();
$action = $info->{default_readdir};
ok($action,                      "Stop");
ok(default_readdir(ACTION_KILL), "default_readdir_KILL");
$info   = info();
$action = $info->{default_readdir};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_readdir(ACTION_DENY), "default_readdir_DENY");
$info   = info();
$action = $info->{default_readdir};
ok($action, "Deny");

$info   = info();
$action = $info->{default_mkdir};
ok($action,                     "Deny");
ok(default_mkdir(ACTION_ALLOW), "default_mkdir_ALLOW");
$info   = info();
$action = $info->{default_mkdir};
ok($action,                    "Allow");
ok(default_mkdir(ACTION_WARN), "default_mkdir_WARN");
$info   = info();
$action = $info->{default_mkdir};
ok($action,                      "Warn");
ok(default_mkdir(ACTION_FILTER), "default_mkdir_FILTER");
$info   = info();
$action = $info->{default_mkdir};
ok($action,                    "Filter");
ok(default_mkdir(ACTION_STOP), "default_mkdir_STOP");
$info   = info();
$action = $info->{default_mkdir};
ok($action,                    "Stop");
ok(default_mkdir(ACTION_KILL), "default_mkdir_KILL");
$info   = info();
$action = $info->{default_mkdir};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_mkdir(ACTION_DENY), "default_mkdir_DENY");
$info   = info();
$action = $info->{default_mkdir};
ok($action, "Deny");

$info   = info();
$action = $info->{default_chown};
ok($action,                     "Deny");
ok(default_chown(ACTION_ALLOW), "default_chown_ALLOW");
$info   = info();
$action = $info->{default_chown};
ok($action,                    "Allow");
ok(default_chown(ACTION_WARN), "default_chown_WARN");
$info   = info();
$action = $info->{default_chown};
ok($action,                      "Warn");
ok(default_chown(ACTION_FILTER), "default_chown_FILTER");
$info   = info();
$action = $info->{default_chown};
ok($action,                    "Filter");
ok(default_chown(ACTION_STOP), "default_chown_STOP");
$info   = info();
$action = $info->{default_chown};
ok($action,                    "Stop");
ok(default_chown(ACTION_KILL), "default_chown_KILL");
$info   = info();
$action = $info->{default_chown};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_chown(ACTION_DENY), "default_chown_DENY");
$info   = info();
$action = $info->{default_chown};
ok($action, "Deny");

$info   = info();
$action = $info->{default_chgrp};
ok($action,                     "Deny");
ok(default_chgrp(ACTION_ALLOW), "default_chgrp_ALLOW");
$info   = info();
$action = $info->{default_chgrp};
ok($action,                    "Allow");
ok(default_chgrp(ACTION_WARN), "default_chgrp_WARN");
$info   = info();
$action = $info->{default_chgrp};
ok($action,                      "Warn");
ok(default_chgrp(ACTION_FILTER), "default_chgrp_FILTER");
$info   = info();
$action = $info->{default_chgrp};
ok($action,                    "Filter");
ok(default_chgrp(ACTION_STOP), "default_chgrp_STOP");
$info   = info();
$action = $info->{default_chgrp};
ok($action,                    "Stop");
ok(default_chgrp(ACTION_KILL), "default_chgrp_KILL");
$info   = info();
$action = $info->{default_chgrp};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_chgrp(ACTION_DENY), "default_chgrp_DENY");
$info   = info();
$action = $info->{default_chgrp};
ok($action, "Deny");

$info   = info();
$action = $info->{default_chown};
ok($action,                     "Deny");
ok(default_chown(ACTION_ALLOW), "default_chown_ALLOW");
$info   = info();
$action = $info->{default_chown};
ok($action,                    "Allow");
ok(default_chown(ACTION_WARN), "default_chown_WARN");
$info   = info();
$action = $info->{default_chown};
ok($action,                      "Warn");
ok(default_chown(ACTION_FILTER), "default_chown_FILTER");
$info   = info();
$action = $info->{default_chown};
ok($action,                    "Filter");
ok(default_chown(ACTION_STOP), "default_chown_STOP");
$info   = info();
$action = $info->{default_chown};
ok($action,                    "Stop");
ok(default_chown(ACTION_KILL), "default_chown_KILL");
$info   = info();
$action = $info->{default_chown};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_chown(ACTION_DENY), "default_chown_DENY");
$info   = info();
$action = $info->{default_chown};
ok($action, "Deny");

$info   = info();
$action = $info->{default_chattr};
ok($action,                      "Deny");
ok(default_chattr(ACTION_ALLOW), "default_chattr_ALLOW");
$info   = info();
$action = $info->{default_chattr};
ok($action,                     "Allow");
ok(default_chattr(ACTION_WARN), "default_chattr_WARN");
$info   = info();
$action = $info->{default_chattr};
ok($action,                       "Warn");
ok(default_chattr(ACTION_FILTER), "default_chattr_FILTER");
$info   = info();
$action = $info->{default_chattr};
ok($action,                     "Filter");
ok(default_chattr(ACTION_STOP), "default_chattr_STOP");
$info   = info();
$action = $info->{default_chattr};
ok($action,                     "Stop");
ok(default_chattr(ACTION_KILL), "default_chattr_KILL");
$info   = info();
$action = $info->{default_chattr};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_chattr(ACTION_DENY), "default_chattr_DENY");
$info   = info();
$action = $info->{default_chattr};
ok($action, "Deny");

$info   = info();
$action = $info->{default_chroot};
ok($action,                      "Deny");
ok(default_chroot(ACTION_ALLOW), "default_chroot_ALLOW");
$info   = info();
$action = $info->{default_chroot};
ok($action,                     "Allow");
ok(default_chroot(ACTION_WARN), "default_chroot_WARN");
$info   = info();
$action = $info->{default_chroot};
ok($action,                       "Warn");
ok(default_chroot(ACTION_FILTER), "default_chroot_FILTER");
$info   = info();
$action = $info->{default_chroot};
ok($action,                     "Filter");
ok(default_chroot(ACTION_STOP), "default_chroot_STOP");
$info   = info();
$action = $info->{default_chroot};
ok($action,                     "Stop");
ok(default_chroot(ACTION_KILL), "default_chroot_KILL");
$info   = info();
$action = $info->{default_chroot};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_chroot(ACTION_DENY), "default_chroot_DENY");
$info   = info();
$action = $info->{default_chroot};
ok($action, "Deny");

$info   = info();
$action = $info->{default_utime};
ok($action,                     "Deny");
ok(default_utime(ACTION_ALLOW), "default_utime_ALLOW");
$info   = info();
$action = $info->{default_utime};
ok($action,                    "Allow");
ok(default_utime(ACTION_WARN), "default_utime_WARN");
$info   = info();
$action = $info->{default_utime};
ok($action,                      "Warn");
ok(default_utime(ACTION_FILTER), "default_utime_FILTER");
$info   = info();
$action = $info->{default_utime};
ok($action,                    "Filter");
ok(default_utime(ACTION_STOP), "default_utime_STOP");
$info   = info();
$action = $info->{default_utime};
ok($action,                    "Stop");
ok(default_utime(ACTION_KILL), "default_utime_KILL");
$info   = info();
$action = $info->{default_utime};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_utime(ACTION_DENY), "default_utime_DENY");
$info   = info();
$action = $info->{default_utime};
ok($action, "Deny");

$info   = info();
$action = $info->{default_mkdev};
ok($action,                     "Deny");
ok(default_mkdev(ACTION_ALLOW), "default_mkdev_ALLOW");
$info   = info();
$action = $info->{default_mkdev};
ok($action,                    "Allow");
ok(default_mkdev(ACTION_WARN), "default_mkdev_WARN");
$info   = info();
$action = $info->{default_mkdev};
ok($action,                      "Warn");
ok(default_mkdev(ACTION_FILTER), "default_mkdev_FILTER");
$info   = info();
$action = $info->{default_mkdev};
ok($action,                    "Filter");
ok(default_mkdev(ACTION_STOP), "default_mkdev_STOP");
$info   = info();
$action = $info->{default_mkdev};
ok($action,                    "Stop");
ok(default_mkdev(ACTION_KILL), "default_mkdev_KILL");
$info   = info();
$action = $info->{default_mkdev};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_mkdev(ACTION_DENY), "default_mkdev_DENY");
$info   = info();
$action = $info->{default_mkdev};
ok($action, "Deny");

$info   = info();
$action = $info->{default_mkfifo};
ok($action,                      "Deny");
ok(default_mkfifo(ACTION_ALLOW), "default_mkfifo_ALLOW");
$info   = info();
$action = $info->{default_mkfifo};
ok($action,                     "Allow");
ok(default_mkfifo(ACTION_WARN), "default_mkfifo_WARN");
$info   = info();
$action = $info->{default_mkfifo};
ok($action,                       "Warn");
ok(default_mkfifo(ACTION_FILTER), "default_mkfifo_FILTER");
$info   = info();
$action = $info->{default_mkfifo};
ok($action,                     "Filter");
ok(default_mkfifo(ACTION_STOP), "default_mkfifo_STOP");
$info   = info();
$action = $info->{default_mkfifo};
ok($action,                     "Stop");
ok(default_mkfifo(ACTION_KILL), "default_mkfifo_KILL");
$info   = info();
$action = $info->{default_mkfifo};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_mkfifo(ACTION_DENY), "default_mkfifo_DENY");
$info   = info();
$action = $info->{default_mkfifo};
ok($action, "Deny");

$info   = info();
$action = $info->{default_mktemp};
ok($action,                      "Deny");
ok(default_mktemp(ACTION_ALLOW), "default_mktemp_ALLOW");
$info   = info();
$action = $info->{default_mktemp};
ok($action,                     "Allow");
ok(default_mktemp(ACTION_WARN), "default_mktemp_WARN");
$info   = info();
$action = $info->{default_mktemp};
ok($action,                       "Warn");
ok(default_mktemp(ACTION_FILTER), "default_mktemp_FILTER");
$info   = info();
$action = $info->{default_mktemp};
ok($action,                     "Filter");
ok(default_mktemp(ACTION_STOP), "default_mktemp_STOP");
$info   = info();
$action = $info->{default_mktemp};
ok($action,                     "Stop");
ok(default_mktemp(ACTION_KILL), "default_mktemp_KILL");
$info   = info();
$action = $info->{default_mktemp};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_mktemp(ACTION_DENY), "default_mktemp_DENY");
$info   = info();
$action = $info->{default_mktemp};
ok($action, "Deny");

$info   = info();
$action = $info->{default_mem};
ok($action, "Deny");
eval { ok(default_mem(ACTION_ALLOW), "default_mem_ALLOW") };
ok($! == EINVAL,             "default_mem_ALLOW: $!");
ok(default_mem(ACTION_WARN), "default_mem_WARN");
$info   = info();
$action = $info->{default_mem};
ok($action,                    "Warn");
ok(default_mem(ACTION_FILTER), "default_mem_FILTER");
$info   = info();
$action = $info->{default_mem};
ok($action,                  "Filter");
ok(default_mem(ACTION_STOP), "default_mem_STOP");
$info   = info();
$action = $info->{default_mem};
ok($action,                  "Stop");
ok(default_mem(ACTION_KILL), "default_mem_KILL");
$info   = info();
$action = $info->{default_mem};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_mem(ACTION_DENY), "default_mem_DENY");
$info   = info();
$action = $info->{default_mem};
ok($action, "Deny");

$info   = info();
$action = $info->{default_pid};
ok($action, "Kill");
eval { ok(default_pid(ACTION_ALLOW), "default_pid_ALLOW") };
ok($! == EINVAL,             "default_pid_ALLOW: $!");
ok(default_pid(ACTION_WARN), "default_pid_WARN");
$info   = info();
$action = $info->{default_pid};
ok($action,                    "Warn");
ok(default_pid(ACTION_FILTER), "default_pid_FILTER");
$info   = info();
$action = $info->{default_pid};
ok($action, "Filter");
eval { ok(default_pid(ACTION_DENY), "default_pid_DENY") };
ok($! == EINVAL, "default_pid_DENY: $!");
eval { ok(default_pid(ACTION_STOP), "default_pid_STOP") };
ok($! == EINVAL, "default_pid_STOP: $!");

# Ensure we reset to Kill last, so other tests are uneffected.
ok(default_pid(ACTION_KILL), "default_pid_KILL");
$info   = info();
$action = $info->{default_pid};
ok($action, "Kill");

$info   = info();
$action = $info->{default_force};
ok($action, "Deny");
eval { ok(default_force(ACTION_ALLOW), "default_force_ALLOW") };
ok($! == EINVAL,               "default_force_ALLOW: $!");
ok(default_force(ACTION_WARN), "default_force_WARN");
$info   = info();
$action = $info->{default_force};
ok($action,                      "Warn");
ok(default_force(ACTION_FILTER), "default_force_FILTER");
$info   = info();
$action = $info->{default_force};
ok($action,                     "Filter");
ok(default_force(ACTION_PANIC), "default_force_PANIC");
$info   = info();
$action = $info->{default_force};
ok($action,                    "Panic");
ok(default_force(ACTION_STOP), "default_force_STOP");
$info   = info();
$action = $info->{default_force};
ok($action,                    "Stop");
ok(default_force(ACTION_KILL), "default_force_KILL");
$info   = info();
$action = $info->{default_force};
ok($action,                    "Kill");
ok(default_force(ACTION_EXIT), "default_force_EXIT");
$info   = info();
$action = $info->{default_force};
ok($action, "Exit");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_force(ACTION_DENY), "default_force_DENY");
$info   = info();
$action = $info->{default_force};
ok($action, "Deny");

$info   = info();
$action = $info->{default_segvguard};
ok($action, "Kill");
eval { ok(default_segvguard(ACTION_ALLOW), "default_segvguard_ALLOW") };
ok($! == EINVAL,                   "default_segvguard_ALLOW: $!");
ok(default_segvguard(ACTION_WARN), "default_segvguard_WARN");
$info   = info();
$action = $info->{default_segvguard};
ok($action,                          "Warn");
ok(default_segvguard(ACTION_FILTER), "default_segvguard_FILTER");
$info   = info();
$action = $info->{default_segvguard};
ok($action,                        "Filter");
ok(default_segvguard(ACTION_STOP), "default_segvguard_STOP");
$info   = info();
$action = $info->{default_segvguard};
ok($action,                        "Stop");
ok(default_segvguard(ACTION_KILL), "default_segvguard_KILL");
$info   = info();
$action = $info->{default_segvguard};
ok($action, "Kill");

$info   = info();
$action = $info->{default_tpe};
ok($action, "Deny");
eval { ok(default_tpe(ACTION_ALLOW), "default_tpe_ALLOW") };
ok($! == EINVAL,             "default_tpe_ALLOW: $!");
ok(default_tpe(ACTION_WARN), "default_tpe_WARN");
$info   = info();
$action = $info->{default_tpe};
ok($action,                    "Warn");
ok(default_tpe(ACTION_FILTER), "default_tpe_FILTER");
$info   = info();
$action = $info->{default_tpe};
ok($action,                  "Filter");
ok(default_tpe(ACTION_STOP), "default_tpe_STOP");
$info   = info();
$action = $info->{default_tpe};
ok($action,                  "Stop");
ok(default_tpe(ACTION_KILL), "default_tpe_KILL");
$info   = info();
$action = $info->{default_tpe};
ok($action, "Kill");

# Ensure we reset to Deny last, so other tests are uneffected.
ok(default_tpe(ACTION_DENY), "default_tpe_DENY");
$info   = info();
$action = $info->{default_tpe};
ok($action, "Deny");

$info = info();
my $mem_max_orig    = $info->{mem_max} . "";
my $mem_vm_max_orig = $info->{mem_vm_max} . "";
my $pid_max_orig    = $info->{pid_max};

ok(mem_max("1G"), "mem_max_1G");
$info = info();
is($info->{mem_max}, 1024 * 1024 * 1024, "mem_max_1G_check");
ok(mem_max("10G"), "mem_max_10G");
$info = info();
is($info->{mem_max}, 10 * 1024 * 1024 * 1024, "mem_max_10G_check");
mem_max($mem_max_orig);

ok(mem_vm_max("1G"), "mem_vm_max_1G");
$info = info();
is($info->{mem_vm_max}, 1024 * 1024 * 1024, "mem_vm_max_1G_check");
ok(mem_vm_max("10G"), "mem_vm_max_10G");
$info = info();
is($info->{mem_vm_max}, 10 * 1024 * 1024 * 1024, "mem_vm_max_10G_check");
mem_vm_max($mem_vm_max_orig);

ok(pid_max(4096), "pid_max_4096");
$info = info();
is($info->{pid_max}, 4096, "pid_max_4096_check");
ok(pid_max(8192), "pid_max_8192");
$info = info();
is($info->{pid_max}, 8192, "pid_max_8192_check");
pid_max($pid_max_orig);

$path = "/tmp/plsyd";
my %rule;
my $idx;
my $rules;
my $comp = sub {
	my ($rule_ref, $pattern_ref) = @_;

	# Check if 'act' and 'cap' fields match exactly
	return 0 unless $rule_ref->{act} eq $pattern_ref->{act} && $rule_ref->{cap} eq $pattern_ref->{cap};

	# Check if 'pat' field matches the given path
	return 0 unless $rule_ref->{pat} eq $path;

	# If all checks pass, the rule matches the pattern
	return 1;
};

%rule = (act => "Allow", cap => "stat", pat => $path);
ok(stat_add(ACTION_ALLOW, $path), "allow_stat_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_stat_add index");
ok(stat_del(ACTION_ALLOW, $path), "allow_stat_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_stat_del index");
ok(stat_add(ACTION_ALLOW, $path), "allow_stat_add_1");
ok(stat_add(ACTION_ALLOW, $path), "allow_stat_add_2");
ok(stat_add(ACTION_ALLOW, $path), "allow_stat_add_3");
ok(stat_rem(ACTION_ALLOW, $path), "allow_stat_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_stat_rem index");

%rule = (act => "Deny", cap => "stat", pat => $path);
ok(stat_add(ACTION_DENY, $path), "deny_stat_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_stat_add index");
ok(stat_del(ACTION_DENY, $path), "deny_stat_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_stat_del index");
ok(stat_add(ACTION_DENY, $path), "deny_stat_add_1");
ok(stat_add(ACTION_DENY, $path), "deny_stat_add_2");
ok(stat_add(ACTION_DENY, $path), "deny_stat_add_3");
ok(stat_rem(ACTION_DENY, $path), "deny_stat_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_stat_rem index");

%rule = (act => "Filter", cap => "stat", pat => $path);
ok(stat_add(ACTION_FILTER, $path), "filter_stat_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_stat_add index");
ok(stat_del(ACTION_FILTER, $path), "filter_stat_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_stat_del index");
ok(stat_add(ACTION_FILTER, $path), "filter_stat_add_1");
ok(stat_add(ACTION_FILTER, $path), "filter_stat_add_2");
ok(stat_add(ACTION_FILTER, $path), "filter_stat_add_3");
ok(stat_rem(ACTION_FILTER, $path), "filter_stat_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_stat_rem index");

%rule = (act => "Allow", cap => "read", pat => $path);
ok(read_add(ACTION_ALLOW, $path), "allow_read_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_read_add index");
ok(read_del(ACTION_ALLOW, $path), "allow_read_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_read_del index");
ok(read_add(ACTION_ALLOW, $path), "allow_read_add_1");
ok(read_add(ACTION_ALLOW, $path), "allow_read_add_2");
ok(read_add(ACTION_ALLOW, $path), "allow_read_add_3");
ok(read_rem(ACTION_ALLOW, $path), "allow_read_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_read_rem index");

%rule = (act => "Deny", cap => "read", pat => $path);
ok(read_add(ACTION_DENY, $path), "deny_read_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_read_add index");
ok(read_del(ACTION_DENY, $path), "deny_read_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_read_del index");
ok(read_add(ACTION_DENY, $path), "deny_read_add_1");
ok(read_add(ACTION_DENY, $path), "deny_read_add_2");
ok(read_add(ACTION_DENY, $path), "deny_read_add_3");
ok(read_rem(ACTION_DENY, $path), "deny_read_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_read_rem index");

%rule = (act => "Filter", cap => "read", pat => $path);
ok(read_add(ACTION_FILTER, $path), "filter_read_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_read_add index");
ok(read_del(ACTION_FILTER, $path), "filter_read_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_read_del index");
ok(read_add(ACTION_FILTER, $path), "filter_read_add_1");
ok(read_add(ACTION_FILTER, $path), "filter_read_add_2");
ok(read_add(ACTION_FILTER, $path), "filter_read_add_3");
ok(read_rem(ACTION_FILTER, $path), "filter_read_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_read_rem index");

%rule = (act => "Allow", cap => "write", pat => $path);
ok(write_add(ACTION_ALLOW, $path), "allow_write_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_write_add index");
ok(write_del(ACTION_ALLOW, $path), "allow_write_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_write_del index");
ok(write_add(ACTION_ALLOW, $path), "allow_write_add_1");
ok(write_add(ACTION_ALLOW, $path), "allow_write_add_2");
ok(write_add(ACTION_ALLOW, $path), "allow_write_add_3");
ok(write_rem(ACTION_ALLOW, $path), "allow_write_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_write_rem index");

%rule = (act => "Deny", cap => "write", pat => $path);
ok(write_add(ACTION_DENY, $path), "deny_write_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_write_add index");
ok(write_del(ACTION_DENY, $path), "deny_write_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_write_del index");
ok(write_add(ACTION_DENY, $path), "deny_write_add_1");
ok(write_add(ACTION_DENY, $path), "deny_write_add_2");
ok(write_add(ACTION_DENY, $path), "deny_write_add_3");
ok(write_rem(ACTION_DENY, $path), "deny_write_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_write_rem index");

%rule = (act => "Filter", cap => "write", pat => $path);
ok(write_add(ACTION_FILTER, $path), "filter_write_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_write_add index");
ok(write_del(ACTION_FILTER, $path), "filter_write_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_write_del index");
ok(write_add(ACTION_FILTER, $path), "filter_write_add_1");
ok(write_add(ACTION_FILTER, $path), "filter_write_add_2");
ok(write_add(ACTION_FILTER, $path), "filter_write_add_3");
ok(write_rem(ACTION_FILTER, $path), "filter_write_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_write_rem index");

%rule = (act => "Allow", cap => "exec", pat => $path);
ok(exec_add(ACTION_ALLOW, $path), "allow_exec_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_exec_add index");
ok(exec_del(ACTION_ALLOW, $path), "allow_exec_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_exec_del index");
ok(exec_add(ACTION_ALLOW, $path), "allow_exec_add_1");
ok(exec_add(ACTION_ALLOW, $path), "allow_exec_add_2");
ok(exec_add(ACTION_ALLOW, $path), "allow_exec_add_3");
ok(exec_rem(ACTION_ALLOW, $path), "allow_exec_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_exec_rem index");

%rule = (act => "Deny", cap => "exec", pat => $path);
ok(exec_add(ACTION_DENY, $path), "deny_exec_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_exec_add index");
ok(exec_del(ACTION_DENY, $path), "deny_exec_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_exec_del index");
ok(exec_add(ACTION_DENY, $path), "deny_exec_add_1");
ok(exec_add(ACTION_DENY, $path), "deny_exec_add_2");
ok(exec_add(ACTION_DENY, $path), "deny_exec_add_3");
ok(exec_rem(ACTION_DENY, $path), "deny_exec_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_exec_rem index");

%rule = (act => "Filter", cap => "exec", pat => $path);
ok(exec_add(ACTION_FILTER, $path), "filter_exec_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_exec_add index");
ok(exec_del(ACTION_FILTER, $path), "filter_exec_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_exec_del index");
ok(exec_add(ACTION_FILTER, $path), "filter_exec_add_1");
ok(exec_add(ACTION_FILTER, $path), "filter_exec_add_2");
ok(exec_add(ACTION_FILTER, $path), "filter_exec_add_3");
ok(exec_rem(ACTION_FILTER, $path), "filter_exec_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_exec_rem index");

%rule = (act => "Allow", cap => "ioctl", pat => $path);
ok(ioctl_add(ACTION_ALLOW, $path), "allow_ioctl_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_ioctl_add index");
ok(ioctl_del(ACTION_ALLOW, $path), "allow_ioctl_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_ioctl_del index");
ok(ioctl_add(ACTION_ALLOW, $path), "allow_ioctl_add_1");
ok(ioctl_add(ACTION_ALLOW, $path), "allow_ioctl_add_2");
ok(ioctl_add(ACTION_ALLOW, $path), "allow_ioctl_add_3");
ok(ioctl_rem(ACTION_ALLOW, $path), "allow_ioctl_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_ioctl_rem index");

%rule = (act => "Deny", cap => "ioctl", pat => $path);
ok(ioctl_add(ACTION_DENY, $path), "deny_ioctl_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_ioctl_add index");
ok(ioctl_del(ACTION_DENY, $path), "deny_ioctl_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_ioctl_del index");
ok(ioctl_add(ACTION_DENY, $path), "deny_ioctl_add_1");
ok(ioctl_add(ACTION_DENY, $path), "deny_ioctl_add_2");
ok(ioctl_add(ACTION_DENY, $path), "deny_ioctl_add_3");
ok(ioctl_rem(ACTION_DENY, $path), "deny_ioctl_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_ioctl_rem index");

%rule = (act => "Filter", cap => "ioctl", pat => $path);
ok(ioctl_add(ACTION_FILTER, $path), "filter_ioctl_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_ioctl_add index");
ok(ioctl_del(ACTION_FILTER, $path), "filter_ioctl_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_ioctl_del index");
ok(ioctl_add(ACTION_FILTER, $path), "filter_ioctl_add_1");
ok(ioctl_add(ACTION_FILTER, $path), "filter_ioctl_add_2");
ok(ioctl_add(ACTION_FILTER, $path), "filter_ioctl_add_3");
ok(ioctl_rem(ACTION_FILTER, $path), "filter_ioctl_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_ioctl_rem index");

%rule = (act => "Allow", cap => "create", pat => $path);
ok(create_add(ACTION_ALLOW, $path), "allow_create_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_create_add index");
ok(create_del(ACTION_ALLOW, $path), "allow_create_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_create_del index");
ok(create_add(ACTION_ALLOW, $path), "allow_create_add_1");
ok(create_add(ACTION_ALLOW, $path), "allow_create_add_2");
ok(create_add(ACTION_ALLOW, $path), "allow_create_add_3");
ok(create_rem(ACTION_ALLOW, $path), "allow_create_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_create_rem index");

%rule = (act => "Deny", cap => "create", pat => $path);
ok(create_add(ACTION_DENY, $path), "deny_create_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_create_add index");
ok(create_del(ACTION_DENY, $path), "deny_create_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_create_del index");
ok(create_add(ACTION_DENY, $path), "deny_create_add_1");
ok(create_add(ACTION_DENY, $path), "deny_create_add_2");
ok(create_add(ACTION_DENY, $path), "deny_create_add_3");
ok(create_rem(ACTION_DENY, $path), "deny_create_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_create_rem index");

%rule = (act => "Filter", cap => "create", pat => $path);
ok(create_add(ACTION_FILTER, $path), "filter_create_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_create_add index");
ok(create_del(ACTION_FILTER, $path), "filter_create_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_create_del index");
ok(create_add(ACTION_FILTER, $path), "filter_create_add_1");
ok(create_add(ACTION_FILTER, $path), "filter_create_add_2");
ok(create_add(ACTION_FILTER, $path), "filter_create_add_3");
ok(create_rem(ACTION_FILTER, $path), "filter_create_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_create_rem index");

%rule = (act => "Allow", cap => "delete", pat => $path);
ok(delete_add(ACTION_ALLOW, $path), "allow_delete_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_delete_add index");
ok(delete_del(ACTION_ALLOW, $path), "allow_delete_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_delete_del index");
ok(delete_add(ACTION_ALLOW, $path), "allow_delete_add_1");
ok(delete_add(ACTION_ALLOW, $path), "allow_delete_add_2");
ok(delete_add(ACTION_ALLOW, $path), "allow_delete_add_3");
ok(delete_rem(ACTION_ALLOW, $path), "allow_delete_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_delete_rem index");

%rule = (act => "Deny", cap => "delete", pat => $path);
ok(delete_add(ACTION_DENY, $path), "deny_delete_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_delete_add index");
ok(delete_del(ACTION_DENY, $path), "deny_delete_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_delete_del index");
ok(delete_add(ACTION_DENY, $path), "deny_delete_add_1");
ok(delete_add(ACTION_DENY, $path), "deny_delete_add_2");
ok(delete_add(ACTION_DENY, $path), "deny_delete_add_3");
ok(delete_rem(ACTION_DENY, $path), "deny_delete_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_delete_rem index");

%rule = (act => "Filter", cap => "delete", pat => $path);
ok(delete_add(ACTION_FILTER, $path), "filter_delete_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_delete_add index");
ok(delete_del(ACTION_FILTER, $path), "filter_delete_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_delete_del index");
ok(delete_add(ACTION_FILTER, $path), "filter_delete_add_1");
ok(delete_add(ACTION_FILTER, $path), "filter_delete_add_2");
ok(delete_add(ACTION_FILTER, $path), "filter_delete_add_3");
ok(delete_rem(ACTION_FILTER, $path), "filter_delete_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_delete_rem index");

%rule = (act => "Allow", cap => "rename", pat => $path);
ok(rename_add(ACTION_ALLOW, $path), "allow_rename_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_rename_add index");
ok(rename_del(ACTION_ALLOW, $path), "allow_rename_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_rename_del index");
ok(rename_add(ACTION_ALLOW, $path), "allow_rename_add_1");
ok(rename_add(ACTION_ALLOW, $path), "allow_rename_add_2");
ok(rename_add(ACTION_ALLOW, $path), "allow_rename_add_3");
ok(rename_rem(ACTION_ALLOW, $path), "allow_rename_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_rename_rem index");

%rule = (act => "Deny", cap => "rename", pat => $path);
ok(rename_add(ACTION_DENY, $path), "deny_rename_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_rename_add index");
ok(rename_del(ACTION_DENY, $path), "deny_rename_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_rename_del index");
ok(rename_add(ACTION_DENY, $path), "deny_rename_add_1");
ok(rename_add(ACTION_DENY, $path), "deny_rename_add_2");
ok(rename_add(ACTION_DENY, $path), "deny_rename_add_3");
ok(rename_rem(ACTION_DENY, $path), "deny_rename_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_rename_rem index");

%rule = (act => "Filter", cap => "rename", pat => $path);
ok(rename_add(ACTION_FILTER, $path), "filter_rename_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_rename_add index");
ok(rename_del(ACTION_FILTER, $path), "filter_rename_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_rename_del index");
ok(rename_add(ACTION_FILTER, $path), "filter_rename_add_1");
ok(rename_add(ACTION_FILTER, $path), "filter_rename_add_2");
ok(rename_add(ACTION_FILTER, $path), "filter_rename_add_3");
ok(rename_rem(ACTION_FILTER, $path), "filter_rename_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_rename_rem index");

%rule = (act => "Allow", cap => "symlink", pat => $path);
ok(symlink_add(ACTION_ALLOW, $path), "allow_symlink_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_symlink_add index");
ok(symlink_del(ACTION_ALLOW, $path), "allow_symlink_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_symlink_del index");
ok(symlink_add(ACTION_ALLOW, $path), "allow_symlink_add_1");
ok(symlink_add(ACTION_ALLOW, $path), "allow_symlink_add_2");
ok(symlink_add(ACTION_ALLOW, $path), "allow_symlink_add_3");
ok(symlink_rem(ACTION_ALLOW, $path), "allow_symlink_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_symlink_rem index");

%rule = (act => "Deny", cap => "symlink", pat => $path);
ok(symlink_add(ACTION_DENY, $path), "deny_symlink_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_symlink_add index");
ok(symlink_del(ACTION_DENY, $path), "deny_symlink_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_symlink_del index");
ok(symlink_add(ACTION_DENY, $path), "deny_symlink_add_1");
ok(symlink_add(ACTION_DENY, $path), "deny_symlink_add_2");
ok(symlink_add(ACTION_DENY, $path), "deny_symlink_add_3");
ok(symlink_rem(ACTION_DENY, $path), "deny_symlink_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_symlink_rem index");

%rule = (act => "Filter", cap => "symlink", pat => $path);
ok(symlink_add(ACTION_FILTER, $path), "filter_symlink_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_symlink_add index");
ok(symlink_del(ACTION_FILTER, $path), "filter_symlink_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_symlink_del index");
ok(symlink_add(ACTION_FILTER, $path), "filter_symlink_add_1");
ok(symlink_add(ACTION_FILTER, $path), "filter_symlink_add_2");
ok(symlink_add(ACTION_FILTER, $path), "filter_symlink_add_3");
ok(symlink_rem(ACTION_FILTER, $path), "filter_symlink_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_symlink_rem index");

%rule = (act => "Allow", cap => "truncate", pat => $path);
ok(truncate_add(ACTION_ALLOW, $path), "allow_truncate_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_truncate_add index");
ok(truncate_del(ACTION_ALLOW, $path), "allow_truncate_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_truncate_del index");
ok(truncate_add(ACTION_ALLOW, $path), "allow_truncate_add_1");
ok(truncate_add(ACTION_ALLOW, $path), "allow_truncate_add_2");
ok(truncate_add(ACTION_ALLOW, $path), "allow_truncate_add_3");
ok(truncate_rem(ACTION_ALLOW, $path), "allow_truncate_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_truncate_rem index");

%rule = (act => "Deny", cap => "truncate", pat => $path);
ok(truncate_add(ACTION_DENY, $path), "deny_truncate_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_truncate_add index");
ok(truncate_del(ACTION_DENY, $path), "deny_truncate_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_truncate_del index");
ok(truncate_add(ACTION_DENY, $path), "deny_truncate_add_1");
ok(truncate_add(ACTION_DENY, $path), "deny_truncate_add_2");
ok(truncate_add(ACTION_DENY, $path), "deny_truncate_add_3");
ok(truncate_rem(ACTION_DENY, $path), "deny_truncate_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_truncate_rem index");

%rule = (act => "Filter", cap => "truncate", pat => $path);
ok(truncate_add(ACTION_FILTER, $path), "filter_truncate_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_truncate_add index");
ok(truncate_del(ACTION_FILTER, $path), "filter_truncate_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_truncate_del index");
ok(truncate_add(ACTION_FILTER, $path), "filter_truncate_add_1");
ok(truncate_add(ACTION_FILTER, $path), "filter_truncate_add_2");
ok(truncate_add(ACTION_FILTER, $path), "filter_truncate_add_3");
ok(truncate_rem(ACTION_FILTER, $path), "filter_truncate_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_truncate_rem index");

%rule = (act => "Allow", cap => "chdir", pat => $path);
ok(chdir_add(ACTION_ALLOW, $path), "allow_chdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chdir_add index");
ok(chdir_del(ACTION_ALLOW, $path), "allow_chdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chdir_del index");
ok(chdir_add(ACTION_ALLOW, $path), "allow_chdir_add_1");
ok(chdir_add(ACTION_ALLOW, $path), "allow_chdir_add_2");
ok(chdir_add(ACTION_ALLOW, $path), "allow_chdir_add_3");
ok(chdir_rem(ACTION_ALLOW, $path), "allow_chdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chdir_rem index");

%rule = (act => "Deny", cap => "chdir", pat => $path);
ok(chdir_add(ACTION_DENY, $path), "deny_chdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chdir_add index");
ok(chdir_del(ACTION_DENY, $path), "deny_chdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chdir_del index");
ok(chdir_add(ACTION_DENY, $path), "deny_chdir_add_1");
ok(chdir_add(ACTION_DENY, $path), "deny_chdir_add_2");
ok(chdir_add(ACTION_DENY, $path), "deny_chdir_add_3");
ok(chdir_rem(ACTION_DENY, $path), "deny_chdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chdir_rem index");

%rule = (act => "Filter", cap => "chdir", pat => $path);
ok(chdir_add(ACTION_FILTER, $path), "filter_chdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chdir_add index");
ok(chdir_del(ACTION_FILTER, $path), "filter_chdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chdir_del index");
ok(chdir_add(ACTION_FILTER, $path), "filter_chdir_add_1");
ok(chdir_add(ACTION_FILTER, $path), "filter_chdir_add_2");
ok(chdir_add(ACTION_FILTER, $path), "filter_chdir_add_3");
ok(chdir_rem(ACTION_FILTER, $path), "filter_chdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chdir_rem index");

%rule = (act => "Allow", cap => "readdir", pat => $path);
ok(readdir_add(ACTION_ALLOW, $path), "allow_readdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_readdir_add index");
ok(readdir_del(ACTION_ALLOW, $path), "allow_readdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_readdir_del index");
ok(readdir_add(ACTION_ALLOW, $path), "allow_readdir_add_1");
ok(readdir_add(ACTION_ALLOW, $path), "allow_readdir_add_2");
ok(readdir_add(ACTION_ALLOW, $path), "allow_readdir_add_3");
ok(readdir_rem(ACTION_ALLOW, $path), "allow_readdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_readdir_rem index");

%rule = (act => "Deny", cap => "readdir", pat => $path);
ok(readdir_add(ACTION_DENY, $path), "deny_readdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_readdir_add index");
ok(readdir_del(ACTION_DENY, $path), "deny_readdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_readdir_del index");
ok(readdir_add(ACTION_DENY, $path), "deny_readdir_add_1");
ok(readdir_add(ACTION_DENY, $path), "deny_readdir_add_2");
ok(readdir_add(ACTION_DENY, $path), "deny_readdir_add_3");
ok(readdir_rem(ACTION_DENY, $path), "deny_readdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_readdir_rem index");

%rule = (act => "Filter", cap => "readdir", pat => $path);
ok(readdir_add(ACTION_FILTER, $path), "filter_readdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_readdir_add index");
ok(readdir_del(ACTION_FILTER, $path), "filter_readdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_readdir_del index");
ok(readdir_add(ACTION_FILTER, $path), "filter_readdir_add_1");
ok(readdir_add(ACTION_FILTER, $path), "filter_readdir_add_2");
ok(readdir_add(ACTION_FILTER, $path), "filter_readdir_add_3");
ok(readdir_rem(ACTION_FILTER, $path), "filter_readdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_readdir_rem index");

%rule = (act => "Allow", cap => "mkdir", pat => $path);
ok(mkdir_add(ACTION_ALLOW, $path), "allow_mkdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mkdir_add index");
ok(mkdir_del(ACTION_ALLOW, $path), "allow_mkdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkdir_del index");
ok(mkdir_add(ACTION_ALLOW, $path), "allow_mkdir_add_1");
ok(mkdir_add(ACTION_ALLOW, $path), "allow_mkdir_add_2");
ok(mkdir_add(ACTION_ALLOW, $path), "allow_mkdir_add_3");
ok(mkdir_rem(ACTION_ALLOW, $path), "allow_mkdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkdir_rem index");

%rule = (act => "Deny", cap => "mkdir", pat => $path);
ok(mkdir_add(ACTION_DENY, $path), "deny_mkdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mkdir_add index");
ok(mkdir_del(ACTION_DENY, $path), "deny_mkdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkdir_del index");
ok(mkdir_add(ACTION_DENY, $path), "deny_mkdir_add_1");
ok(mkdir_add(ACTION_DENY, $path), "deny_mkdir_add_2");
ok(mkdir_add(ACTION_DENY, $path), "deny_mkdir_add_3");
ok(mkdir_rem(ACTION_DENY, $path), "deny_mkdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkdir_rem index");

%rule = (act => "Filter", cap => "mkdir", pat => $path);
ok(mkdir_add(ACTION_FILTER, $path), "filter_mkdir_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mkdir_add index");
ok(mkdir_del(ACTION_FILTER, $path), "filter_mkdir_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkdir_del index");
ok(mkdir_add(ACTION_FILTER, $path), "filter_mkdir_add_1");
ok(mkdir_add(ACTION_FILTER, $path), "filter_mkdir_add_2");
ok(mkdir_add(ACTION_FILTER, $path), "filter_mkdir_add_3");
ok(mkdir_rem(ACTION_FILTER, $path), "filter_mkdir_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkdir_rem index");

%rule = (act => "Allow", cap => "chown", pat => $path);
ok(chown_add(ACTION_ALLOW, $path), "allow_chown_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chown_add index");
ok(chown_del(ACTION_ALLOW, $path), "allow_chown_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chown_del index");
ok(chown_add(ACTION_ALLOW, $path), "allow_chown_add_1");
ok(chown_add(ACTION_ALLOW, $path), "allow_chown_add_2");
ok(chown_add(ACTION_ALLOW, $path), "allow_chown_add_3");
ok(chown_rem(ACTION_ALLOW, $path), "allow_chown_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chown_rem index");

%rule = (act => "Deny", cap => "chown", pat => $path);
ok(chown_add(ACTION_DENY, $path), "deny_chown_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chown_add index");
ok(chown_del(ACTION_DENY, $path), "deny_chown_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chown_del index");
ok(chown_add(ACTION_DENY, $path), "deny_chown_add_1");
ok(chown_add(ACTION_DENY, $path), "deny_chown_add_2");
ok(chown_add(ACTION_DENY, $path), "deny_chown_add_3");
ok(chown_rem(ACTION_DENY, $path), "deny_chown_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chown_rem index");

%rule = (act => "Filter", cap => "chown", pat => $path);
ok(chown_add(ACTION_FILTER, $path), "filter_chown_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chown_add index");
ok(chown_del(ACTION_FILTER, $path), "filter_chown_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chown_del index");
ok(chown_add(ACTION_FILTER, $path), "filter_chown_add_1");
ok(chown_add(ACTION_FILTER, $path), "filter_chown_add_2");
ok(chown_add(ACTION_FILTER, $path), "filter_chown_add_3");
ok(chown_rem(ACTION_FILTER, $path), "filter_chown_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chown_rem index");

%rule = (act => "Allow", cap => "chgrp", pat => $path);
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chgrp_add index");
ok(chgrp_del(ACTION_ALLOW, $path), "allow_chgrp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chgrp_del index");
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add_1");
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add_2");
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add_3");
ok(chgrp_rem(ACTION_ALLOW, $path), "allow_chgrp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chgrp_rem index");

%rule = (act => "Deny", cap => "chgrp", pat => $path);
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chgrp_add index");
ok(chgrp_del(ACTION_DENY, $path), "deny_chgrp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chgrp_del index");
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add_1");
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add_2");
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add_3");
ok(chgrp_rem(ACTION_DENY, $path), "deny_chgrp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chgrp_rem index");

%rule = (act => "Filter", cap => "chgrp", pat => $path);
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chgrp_add index");
ok(chgrp_del(ACTION_FILTER, $path), "filter_chgrp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chgrp_del index");
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add_1");
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add_2");
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add_3");
ok(chgrp_rem(ACTION_FILTER, $path), "filter_chgrp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chgrp_rem index");

%rule = (act => "Allow", cap => "chgrp", pat => $path);
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chgrp_add index");
ok(chgrp_del(ACTION_ALLOW, $path), "allow_chgrp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chgrp_del index");
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add_1");
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add_2");
ok(chgrp_add(ACTION_ALLOW, $path), "allow_chgrp_add_3");
ok(chgrp_rem(ACTION_ALLOW, $path), "allow_chgrp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chgrp_rem index");

%rule = (act => "Deny", cap => "chgrp", pat => $path);
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chgrp_add index");
ok(chgrp_del(ACTION_DENY, $path), "deny_chgrp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chgrp_del index");
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add_1");
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add_2");
ok(chgrp_add(ACTION_DENY, $path), "deny_chgrp_add_3");
ok(chgrp_rem(ACTION_DENY, $path), "deny_chgrp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chgrp_rem index");

%rule = (act => "Filter", cap => "chgrp", pat => $path);
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chgrp_add index");
ok(chgrp_del(ACTION_FILTER, $path), "filter_chgrp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chgrp_del index");
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add_1");
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add_2");
ok(chgrp_add(ACTION_FILTER, $path), "filter_chgrp_add_3");
ok(chgrp_rem(ACTION_FILTER, $path), "filter_chgrp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chgrp_rem index");

%rule = (act => "Allow", cap => "chmod", pat => $path);
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chmod_add index");
ok(chmod_del(ACTION_ALLOW, $path), "allow_chmod_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chmod_del index");
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add_1");
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add_2");
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add_3");
ok(chmod_rem(ACTION_ALLOW, $path), "allow_chmod_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chmod_rem index");

%rule = (act => "Deny", cap => "chmod", pat => $path);
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chmod_add index");
ok(chmod_del(ACTION_DENY, $path), "deny_chmod_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chmod_del index");
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add_1");
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add_2");
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add_3");
ok(chmod_rem(ACTION_DENY, $path), "deny_chmod_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chmod_rem index");

%rule = (act => "Filter", cap => "chmod", pat => $path);
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chmod_add index");
ok(chmod_del(ACTION_FILTER, $path), "filter_chmod_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chmod_del index");
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add_1");
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add_2");
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add_3");
ok(chmod_rem(ACTION_FILTER, $path), "filter_chmod_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chmod_rem index");

%rule = (act => "Allow", cap => "chmod", pat => $path);
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chmod_add index");
ok(chmod_del(ACTION_ALLOW, $path), "allow_chmod_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chmod_del index");
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add_1");
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add_2");
ok(chmod_add(ACTION_ALLOW, $path), "allow_chmod_add_3");
ok(chmod_rem(ACTION_ALLOW, $path), "allow_chmod_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chmod_rem index");

%rule = (act => "Deny", cap => "chmod", pat => $path);
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chmod_add index");
ok(chmod_del(ACTION_DENY, $path), "deny_chmod_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chmod_del index");
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add_1");
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add_2");
ok(chmod_add(ACTION_DENY, $path), "deny_chmod_add_3");
ok(chmod_rem(ACTION_DENY, $path), "deny_chmod_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chmod_rem index");

%rule = (act => "Filter", cap => "chmod", pat => $path);
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chmod_add index");
ok(chmod_del(ACTION_FILTER, $path), "filter_chmod_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chmod_del index");
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add_1");
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add_2");
ok(chmod_add(ACTION_FILTER, $path), "filter_chmod_add_3");
ok(chmod_rem(ACTION_FILTER, $path), "filter_chmod_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chmod_rem index");

%rule = (act => "Allow", cap => "chattr", pat => $path);
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chattr_add index");
ok(chattr_del(ACTION_ALLOW, $path), "allow_chattr_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chattr_del index");
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add_1");
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add_2");
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add_3");
ok(chattr_rem(ACTION_ALLOW, $path), "allow_chattr_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chattr_rem index");

%rule = (act => "Deny", cap => "chattr", pat => $path);
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chattr_add index");
ok(chattr_del(ACTION_DENY, $path), "deny_chattr_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chattr_del index");
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add_1");
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add_2");
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add_3");
ok(chattr_rem(ACTION_DENY, $path), "deny_chattr_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chattr_rem index");

%rule = (act => "Filter", cap => "chattr", pat => $path);
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chattr_add index");
ok(chattr_del(ACTION_FILTER, $path), "filter_chattr_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chattr_del index");
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add_1");
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add_2");
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add_3");
ok(chattr_rem(ACTION_FILTER, $path), "filter_chattr_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chattr_rem index");

%rule = (act => "Allow", cap => "chattr", pat => $path);
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_chattr_add index");
ok(chattr_del(ACTION_ALLOW, $path), "allow_chattr_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chattr_del index");
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add_1");
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add_2");
ok(chattr_add(ACTION_ALLOW, $path), "allow_chattr_add_3");
ok(chattr_rem(ACTION_ALLOW, $path), "allow_chattr_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_chattr_rem index");

%rule = (act => "Deny", cap => "chattr", pat => $path);
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_chattr_add index");
ok(chattr_del(ACTION_DENY, $path), "deny_chattr_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chattr_del index");
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add_1");
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add_2");
ok(chattr_add(ACTION_DENY, $path), "deny_chattr_add_3");
ok(chattr_rem(ACTION_DENY, $path), "deny_chattr_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_chattr_rem index");

%rule = (act => "Filter", cap => "chattr", pat => $path);
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_chattr_add index");
ok(chattr_del(ACTION_FILTER, $path), "filter_chattr_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chattr_del index");
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add_1");
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add_2");
ok(chattr_add(ACTION_FILTER, $path), "filter_chattr_add_3");
ok(chattr_rem(ACTION_FILTER, $path), "filter_chattr_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_chattr_rem index");

# Chroot is startup only since 3.32.4
#%rule = (act => "Allow", cap => "chroot", pat => $path);
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, scalar(@$rules) - 1, "allow_chroot_add index");
#ok(chroot_del(ACTION_ALLOW, $path), "allow_chroot_del");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "allow_chroot_del index");
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add_1");
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add_2");
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add_3");
#ok(chroot_rem(ACTION_ALLOW, $path), "allow_chroot_rem");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "allow_chroot_rem index");
#
#%rule = (act => "Deny", cap => "chroot", pat => $path);
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, scalar(@$rules) - 1, "deny_chroot_add index");
#ok(chroot_del(ACTION_DENY, $path), "deny_chroot_del");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "deny_chroot_del index");
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add_1");
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add_2");
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add_3");
#ok(chroot_rem(ACTION_DENY, $path), "deny_chroot_rem");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "deny_chroot_rem index");
#
#%rule = (act => "Filter", cap => "chroot", pat => $path);
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, scalar(@$rules) - 1, "filter_chroot_add index");
#ok(chroot_del(ACTION_FILTER, $path), "filter_chroot_del");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "filter_chroot_del index");
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add_1");
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add_2");
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add_3");
#ok(chroot_rem(ACTION_FILTER, $path), "filter_chroot_rem");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "filter_chroot_rem index");
#
#%rule = (act => "Allow", cap => "chroot", pat => $path);
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, scalar(@$rules) - 1, "allow_chroot_add index");
#ok(chroot_del(ACTION_ALLOW, $path), "allow_chroot_del");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "allow_chroot_del index");
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add_1");
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add_2");
#ok(chroot_add(ACTION_ALLOW, $path), "allow_chroot_add_3");
#ok(chroot_rem(ACTION_ALLOW, $path), "allow_chroot_rem");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "allow_chroot_rem index");
#
#%rule = (act => "Deny", cap => "chroot", pat => $path);
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, scalar(@$rules) - 1, "deny_chroot_add index");
#ok(chroot_del(ACTION_DENY, $path), "deny_chroot_del");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "deny_chroot_del index");
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add_1");
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add_2");
#ok(chroot_add(ACTION_DENY, $path), "deny_chroot_add_3");
#ok(chroot_rem(ACTION_DENY, $path), "deny_chroot_rem");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "deny_chroot_rem index");
#
#%rule = (act => "Filter", cap => "chroot", pat => $path);
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, scalar(@$rules) - 1, "filter_chroot_add index");
#ok(chroot_del(ACTION_FILTER, $path), "filter_chroot_del");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "filter_chroot_del index");
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add_1");
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add_2");
#ok(chroot_add(ACTION_FILTER, $path), "filter_chroot_add_3");
#ok(chroot_rem(ACTION_FILTER, $path), "filter_chroot_rem");
#$rules = info()->{"glob_rules"};
#$idx   = find($rules, \%rule, $comp);
#is($idx, undef, "filter_chroot_rem index");

%rule = (act => "Allow", cap => "utime", pat => $path);
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_utime_add index");
ok(utime_del(ACTION_ALLOW, $path), "allow_utime_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_utime_del index");
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add_1");
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add_2");
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add_3");
ok(utime_rem(ACTION_ALLOW, $path), "allow_utime_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_utime_rem index");

%rule = (act => "Deny", cap => "utime", pat => $path);
ok(utime_add(ACTION_DENY, $path), "deny_utime_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_utime_add index");
ok(utime_del(ACTION_DENY, $path), "deny_utime_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_utime_del index");
ok(utime_add(ACTION_DENY, $path), "deny_utime_add_1");
ok(utime_add(ACTION_DENY, $path), "deny_utime_add_2");
ok(utime_add(ACTION_DENY, $path), "deny_utime_add_3");
ok(utime_rem(ACTION_DENY, $path), "deny_utime_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_utime_rem index");

%rule = (act => "Filter", cap => "utime", pat => $path);
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_utime_add index");
ok(utime_del(ACTION_FILTER, $path), "filter_utime_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_utime_del index");
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add_1");
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add_2");
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add_3");
ok(utime_rem(ACTION_FILTER, $path), "filter_utime_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_utime_rem index");

%rule = (act => "Allow", cap => "utime", pat => $path);
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_utime_add index");
ok(utime_del(ACTION_ALLOW, $path), "allow_utime_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_utime_del index");
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add_1");
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add_2");
ok(utime_add(ACTION_ALLOW, $path), "allow_utime_add_3");
ok(utime_rem(ACTION_ALLOW, $path), "allow_utime_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_utime_rem index");

%rule = (act => "Deny", cap => "utime", pat => $path);
ok(utime_add(ACTION_DENY, $path), "deny_utime_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_utime_add index");
ok(utime_del(ACTION_DENY, $path), "deny_utime_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_utime_del index");
ok(utime_add(ACTION_DENY, $path), "deny_utime_add_1");
ok(utime_add(ACTION_DENY, $path), "deny_utime_add_2");
ok(utime_add(ACTION_DENY, $path), "deny_utime_add_3");
ok(utime_rem(ACTION_DENY, $path), "deny_utime_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_utime_rem index");

%rule = (act => "Filter", cap => "utime", pat => $path);
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_utime_add index");
ok(utime_del(ACTION_FILTER, $path), "filter_utime_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_utime_del index");
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add_1");
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add_2");
ok(utime_add(ACTION_FILTER, $path), "filter_utime_add_3");
ok(utime_rem(ACTION_FILTER, $path), "filter_utime_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_utime_rem index");

%rule = (act => "Allow", cap => "mkdev", pat => $path);
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mkdev_add index");
ok(mkdev_del(ACTION_ALLOW, $path), "allow_mkdev_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkdev_del index");
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add_1");
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add_2");
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add_3");
ok(mkdev_rem(ACTION_ALLOW, $path), "allow_mkdev_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkdev_rem index");

%rule = (act => "Deny", cap => "mkdev", pat => $path);
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mkdev_add index");
ok(mkdev_del(ACTION_DENY, $path), "deny_mkdev_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkdev_del index");
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add_1");
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add_2");
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add_3");
ok(mkdev_rem(ACTION_DENY, $path), "deny_mkdev_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkdev_rem index");

%rule = (act => "Filter", cap => "mkdev", pat => $path);
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mkdev_add index");
ok(mkdev_del(ACTION_FILTER, $path), "filter_mkdev_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkdev_del index");
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add_1");
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add_2");
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add_3");
ok(mkdev_rem(ACTION_FILTER, $path), "filter_mkdev_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkdev_rem index");

%rule = (act => "Allow", cap => "mkdev", pat => $path);
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mkdev_add index");
ok(mkdev_del(ACTION_ALLOW, $path), "allow_mkdev_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkdev_del index");
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add_1");
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add_2");
ok(mkdev_add(ACTION_ALLOW, $path), "allow_mkdev_add_3");
ok(mkdev_rem(ACTION_ALLOW, $path), "allow_mkdev_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkdev_rem index");

%rule = (act => "Deny", cap => "mkdev", pat => $path);
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mkdev_add index");
ok(mkdev_del(ACTION_DENY, $path), "deny_mkdev_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkdev_del index");
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add_1");
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add_2");
ok(mkdev_add(ACTION_DENY, $path), "deny_mkdev_add_3");
ok(mkdev_rem(ACTION_DENY, $path), "deny_mkdev_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkdev_rem index");

%rule = (act => "Filter", cap => "mkdev", pat => $path);
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mkdev_add index");
ok(mkdev_del(ACTION_FILTER, $path), "filter_mkdev_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkdev_del index");
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add_1");
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add_2");
ok(mkdev_add(ACTION_FILTER, $path), "filter_mkdev_add_3");
ok(mkdev_rem(ACTION_FILTER, $path), "filter_mkdev_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkdev_rem index");

%rule = (act => "Allow", cap => "mkfifo", pat => $path);
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mkfifo_add index");
ok(mkfifo_del(ACTION_ALLOW, $path), "allow_mkfifo_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkfifo_del index");
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add_1");
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add_2");
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add_3");
ok(mkfifo_rem(ACTION_ALLOW, $path), "allow_mkfifo_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkfifo_rem index");

%rule = (act => "Deny", cap => "mkfifo", pat => $path);
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mkfifo_add index");
ok(mkfifo_del(ACTION_DENY, $path), "deny_mkfifo_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkfifo_del index");
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add_1");
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add_2");
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add_3");
ok(mkfifo_rem(ACTION_DENY, $path), "deny_mkfifo_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkfifo_rem index");

%rule = (act => "Filter", cap => "mkfifo", pat => $path);
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mkfifo_add index");
ok(mkfifo_del(ACTION_FILTER, $path), "filter_mkfifo_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkfifo_del index");
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add_1");
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add_2");
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add_3");
ok(mkfifo_rem(ACTION_FILTER, $path), "filter_mkfifo_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkfifo_rem index");

%rule = (act => "Allow", cap => "mkfifo", pat => $path);
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mkfifo_add index");
ok(mkfifo_del(ACTION_ALLOW, $path), "allow_mkfifo_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkfifo_del index");
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add_1");
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add_2");
ok(mkfifo_add(ACTION_ALLOW, $path), "allow_mkfifo_add_3");
ok(mkfifo_rem(ACTION_ALLOW, $path), "allow_mkfifo_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mkfifo_rem index");

%rule = (act => "Deny", cap => "mkfifo", pat => $path);
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mkfifo_add index");
ok(mkfifo_del(ACTION_DENY, $path), "deny_mkfifo_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkfifo_del index");
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add_1");
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add_2");
ok(mkfifo_add(ACTION_DENY, $path), "deny_mkfifo_add_3");
ok(mkfifo_rem(ACTION_DENY, $path), "deny_mkfifo_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mkfifo_rem index");

%rule = (act => "Filter", cap => "mkfifo", pat => $path);
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mkfifo_add index");
ok(mkfifo_del(ACTION_FILTER, $path), "filter_mkfifo_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkfifo_del index");
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add_1");
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add_2");
ok(mkfifo_add(ACTION_FILTER, $path), "filter_mkfifo_add_3");
ok(mkfifo_rem(ACTION_FILTER, $path), "filter_mkfifo_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mkfifo_rem index");

%rule = (act => "Allow", cap => "mktemp", pat => $path);
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mktemp_add index");
ok(mktemp_del(ACTION_ALLOW, $path), "allow_mktemp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mktemp_del index");
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add_1");
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add_2");
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add_3");
ok(mktemp_rem(ACTION_ALLOW, $path), "allow_mktemp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mktemp_rem index");

%rule = (act => "Deny", cap => "mktemp", pat => $path);
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mktemp_add index");
ok(mktemp_del(ACTION_DENY, $path), "deny_mktemp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mktemp_del index");
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add_1");
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add_2");
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add_3");
ok(mktemp_rem(ACTION_DENY, $path), "deny_mktemp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mktemp_rem index");

%rule = (act => "Filter", cap => "mktemp", pat => $path);
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mktemp_add index");
ok(mktemp_del(ACTION_FILTER, $path), "filter_mktemp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mktemp_del index");
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add_1");
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add_2");
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add_3");
ok(mktemp_rem(ACTION_FILTER, $path), "filter_mktemp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mktemp_rem index");

%rule = (act => "Allow", cap => "mktemp", pat => $path);
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_mktemp_add index");
ok(mktemp_del(ACTION_ALLOW, $path), "allow_mktemp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mktemp_del index");
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add_1");
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add_2");
ok(mktemp_add(ACTION_ALLOW, $path), "allow_mktemp_add_3");
ok(mktemp_rem(ACTION_ALLOW, $path), "allow_mktemp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_mktemp_rem index");

%rule = (act => "Deny", cap => "mktemp", pat => $path);
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_mktemp_add index");
ok(mktemp_del(ACTION_DENY, $path), "deny_mktemp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mktemp_del index");
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add_1");
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add_2");
ok(mktemp_add(ACTION_DENY, $path), "deny_mktemp_add_3");
ok(mktemp_rem(ACTION_DENY, $path), "deny_mktemp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_mktemp_rem index");

%rule = (act => "Filter", cap => "mktemp", pat => $path);
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_mktemp_add index");
ok(mktemp_del(ACTION_FILTER, $path), "filter_mktemp_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mktemp_del index");
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add_1");
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add_2");
ok(mktemp_add(ACTION_FILTER, $path), "filter_mktemp_add_3");
ok(mktemp_rem(ACTION_FILTER, $path), "filter_mktemp_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_mktemp_rem index");

%rule = (act => "Allow", cap => "net/sendfd", pat => $path);
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_net_sendfd_add index");
ok(net_sendfd_del(ACTION_ALLOW, $path), "allow_net_sendfd_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_sendfd_del index");
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add_1");
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add_2");
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add_3");
ok(net_sendfd_rem(ACTION_ALLOW, $path), "allow_net_sendfd_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_sendfd_rem index");

%rule = (act => "Deny", cap => "net/sendfd", pat => $path);
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_net_sendfd_add index");
ok(net_sendfd_del(ACTION_DENY, $path), "deny_net_sendfd_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_sendfd_del index");
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add_1");
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add_2");
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add_3");
ok(net_sendfd_rem(ACTION_DENY, $path), "deny_net_sendfd_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_sendfd_rem index");

%rule = (act => "Filter", cap => "net/sendfd", pat => $path);
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_net_sendfd_add index");
ok(net_sendfd_del(ACTION_FILTER, $path), "filter_net_sendfd_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_sendfd_del index");
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add_1");
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add_2");
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add_3");
ok(net_sendfd_rem(ACTION_FILTER, $path), "filter_net_sendfd_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_sendfd_rem index");

%rule = (act => "Allow", cap => "net/sendfd", pat => $path);
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_net_sendfd_add index");
ok(net_sendfd_del(ACTION_ALLOW, $path), "allow_net_sendfd_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_sendfd_del index");
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add_1");
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add_2");
ok(net_sendfd_add(ACTION_ALLOW, $path), "allow_net_sendfd_add_3");
ok(net_sendfd_rem(ACTION_ALLOW, $path), "allow_net_sendfd_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_sendfd_rem index");

%rule = (act => "Deny", cap => "net/sendfd", pat => $path);
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_net_sendfd_add index");
ok(net_sendfd_del(ACTION_DENY, $path), "deny_net_sendfd_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_sendfd_del index");
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add_1");
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add_2");
ok(net_sendfd_add(ACTION_DENY, $path), "deny_net_sendfd_add_3");
ok(net_sendfd_rem(ACTION_DENY, $path), "deny_net_sendfd_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_sendfd_rem index");

%rule = (act => "Filter", cap => "net/sendfd", pat => $path);
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_net_sendfd_add index");
ok(net_sendfd_del(ACTION_FILTER, $path), "filter_net_sendfd_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_sendfd_del index");
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add_1");
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add_2");
ok(net_sendfd_add(ACTION_FILTER, $path), "filter_net_sendfd_add_3");
ok(net_sendfd_rem(ACTION_FILTER, $path), "filter_net_sendfd_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_sendfd_rem index");

ok(ioctl_deny(0xdeadca11), "ioctl_deny");

my $addr = "127.3.1.4/8";
my $port = 31415;
$path = "${addr}!${port}";
%rule = (act => "Allow", cap => "net/bind", pat => {addr => $addr, port => $port});
$comp = sub {
	my ($rule_ref, $pattern_ref) = @_;

	# Check if 'act' and 'cap' fields match exactly
	return 0 unless $rule_ref->{act} eq $pattern_ref->{act} && $rule_ref->{cap} eq $pattern_ref->{cap};

	# Check if 'pat' field matches the given address.
	return 0 unless $rule_ref->{pat}->{addr} eq $addr;

	# Check if 'pat' field matches the given port.
	return 0 unless $rule_ref->{pat}->{port} eq $port;

	# If all checks pass, the rule matches the pattern
	return 1;
};

ok(net_bind_add(ACTION_ALLOW, $path), "allow_net_bind_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_net_bind_add index");
ok(net_bind_del(ACTION_ALLOW, $path), "allow_net_bind_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_bind_del index");
ok(net_bind_add(ACTION_ALLOW, $path), "allow_net_bind_add_1");
ok(net_bind_add(ACTION_ALLOW, $path), "allow_net_bind_add_2");
ok(net_bind_add(ACTION_ALLOW, $path), "allow_net_bind_add_3");
ok(net_bind_rem(ACTION_ALLOW, $path), "allow_net_bind_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_bind_rem index");

%rule = (act => "Deny", cap => "net/bind", pat => {addr => $addr, port => $port});
ok(net_bind_add(ACTION_DENY, $path), "deny_net_bind_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_net_bind_add index");
ok(net_bind_del(ACTION_DENY, $path), "deny_net_bind_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_bind_del index");
ok(net_bind_add(ACTION_DENY, $path), "deny_net_bind_add_1");
ok(net_bind_add(ACTION_DENY, $path), "deny_net_bind_add_2");
ok(net_bind_add(ACTION_DENY, $path), "deny_net_bind_add_3");
ok(net_bind_rem(ACTION_DENY, $path), "deny_net_bind_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_bind_rem index");

%rule = (act => "Filter", cap => "net/bind", pat => {addr => $addr, port => $port});
ok(net_bind_add(ACTION_FILTER, $path), "filter_net_bind_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_net_bind_add index");
ok(net_bind_del(ACTION_FILTER, $path), "filter_net_bind_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_bind_del index");
ok(net_bind_add(ACTION_FILTER, $path), "filter_net_bind_add_1");
ok(net_bind_add(ACTION_FILTER, $path), "filter_net_bind_add_2");
ok(net_bind_add(ACTION_FILTER, $path), "filter_net_bind_add_3");
ok(net_bind_rem(ACTION_FILTER, $path), "filter_net_bind_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_bind_rem index");

%rule = (act => "Allow", cap => "net/connect", pat => {addr => $addr, port => $port});
ok(net_connect_add(ACTION_ALLOW, $path), "allow_net_connect_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_net_connect_add index");
ok(net_connect_del(ACTION_ALLOW, $path), "allow_net_connect_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_connect_del index");
ok(net_connect_add(ACTION_ALLOW, $path), "allow_net_connect_add_1");
ok(net_connect_add(ACTION_ALLOW, $path), "allow_net_connect_add_2");
ok(net_connect_add(ACTION_ALLOW, $path), "allow_net_connect_add_3");
ok(net_connect_rem(ACTION_ALLOW, $path), "allow_net_connect_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_connect_rem index");

%rule = (act => "Deny", cap => "net/connect", pat => {addr => $addr, port => $port});
ok(net_connect_add(ACTION_DENY, $path), "deny_net_connect_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_net_connect_add index");
ok(net_connect_del(ACTION_DENY, $path), "deny_net_connect_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_connect_del index");
ok(net_connect_add(ACTION_DENY, $path), "deny_net_connect_add_1");
ok(net_connect_add(ACTION_DENY, $path), "deny_net_connect_add_2");
ok(net_connect_add(ACTION_DENY, $path), "deny_net_connect_add_3");
ok(net_connect_rem(ACTION_DENY, $path), "deny_net_connect_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_connect_rem index");

%rule = (act => "Filter", cap => "net/connect", pat => {addr => $addr, port => $port});
ok(net_connect_add(ACTION_FILTER, $path), "filter_net_connect_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_net_connect_add index");
ok(net_connect_del(ACTION_FILTER, $path), "filter_net_connect_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_connect_del index");
ok(net_connect_add(ACTION_FILTER, $path), "filter_net_connect_add_1");
ok(net_connect_add(ACTION_FILTER, $path), "filter_net_connect_add_2");
ok(net_connect_add(ACTION_FILTER, $path), "filter_net_connect_add_3");
ok(net_connect_rem(ACTION_FILTER, $path), "filter_net_connect_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_connect_rem index");

my $sha = "0" x 128;
$path = "/tmp/plsyd";
%rule = (act => "Kill", sha => $sha, pat => $path);
$comp = sub {
	my ($rule_ref, $pattern_ref) = @_;

	# Check if 'act' and 'cap' fields match exactly
	return 0 unless $rule_ref->{act} eq $pattern_ref->{act} && $rule_ref->{sha} eq $pattern_ref->{sha};

	# Check if 'pat' field matches the given path
	return 0 unless $rule_ref->{pat} eq $path;

	# If all checks pass, the rule matches the pattern
	return 1;
};

# Check invalid actions.
eval { force_add($path, $sha, -1) };
ok($! == EINVAL, "force_add -1: $!");
eval { force_add($path, $sha, -10) };
ok($! == EINVAL, "force_add -10: $!");
eval { force_add($path, $sha, -100) };
ok($! == EINVAL, "force_add -100: $!");
eval { force_add($path, $sha, 10) };
ok($! == EINVAL, "force_add 10: $!");
eval { force_add($path, $sha, 20) };
ok($! == EINVAL, "force_add 20: $!");
eval { force_add($path, $sha, 100) };
ok($! == EINVAL, "force_add 100: $!");

# ALLOW is invalid for add but ok for def.
eval { force_add($path, $sha, ACTION_ALLOW) };
ok($! == EINVAL, "force_add ALLOW: $!");

ok(force_add($path, $sha, ACTION_KILL), "force_add");
$rules = info()->{"force_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "force_add index");
ok(force_del($path), "force_del");
$rules = info()->{"force_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "force_del index");
ok(force_add("${path}_1", $sha, ACTION_WARN), "force_add_1");
ok(force_add("${path}_2", $sha, ACTION_KILL), "force_add_2");
ok(force_clr(), "force_clr");
$rules = info()->{"force_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "force_clr");

my $segvguard_expiry = info()->{"segvguard_expiry"};
is($segvguard_expiry, 120, "segvguard_expiry");
ok(segvguard_expiry(42), "segvguard_expiry_set");
$segvguard_expiry = info()->{"segvguard_expiry"};
is($segvguard_expiry, 42, "segvguard_expiry_get");

my $segvguard_suspension = info()->{"segvguard_suspension"};
is($segvguard_suspension, 600, "segvguard_suspension");
ok(segvguard_suspension(42), "segvguard_suspension_set");
$segvguard_suspension = info()->{"segvguard_suspension"};
is($segvguard_suspension, 42, "segvguard_suspension_get");

my $segvguard_maxcrashes = info()->{"segvguard_maxcrashes"};
is($segvguard_maxcrashes, 5, "segvguard_maxcrashes");
ok(segvguard_maxcrashes(42), "segvguard_maxcrashes_set");
$segvguard_maxcrashes = info()->{"segvguard_maxcrashes"};
is($segvguard_maxcrashes, 42, "segvguard_maxcrashes_get");

my ($fh, $filename) = tempfile();
print $fh "pid/max:77\n";
seek($fh, 0, 0);
my $fd = fileno($fh);
ok(load($fd), "syd_load_$fd");
close($fh);
is(info()->{"pid_max"}, 77, "syd_load_$fd pid_max");

eval { syd::lock(-1) };
ok($! == EINVAL, "lock -1: $!");
eval { syd::lock(-10) };
ok($! == EINVAL, "lock -10: $!");
eval { syd::lock(-100) };
ok($! == EINVAL, "lock -100: $!");
eval { syd::lock(10) };
ok($! == EINVAL, "lock 10: $!");
eval { syd::lock(20) };
ok($! == EINVAL, "lock 20: $!");
eval { syd::lock(30) };
ok($! == EINVAL, "lock 30: $!");
eval { syd::lock(0.5) };
ok($! == EINVAL, "lock 0.5: $!");
eval { syd::lock(1.5) };
ok($! == EINVAL, "lock 1.5: $!");

is(syd::lock(LOCK_OFF),  0, "LOCK_OFF");
is(syd::lock(LOCK_EXEC), 0, "LOCK_EXEC");
is(syd::lock(LOCK_ON),   0, "LOCK_ON");

eval { syd::lock(LOCK_OFF) };
ok($! == ENOENT, "locked LOCK_OFF");
eval { syd::lock(LOCK_EXEC) };
ok($! == ENOENT, "locked LOCK_EXEC");
eval { syd::lock(LOCK_ON) };
ok($! == ENOENT, "locked LOCK_ON");

1;
