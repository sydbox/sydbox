// Syd: rock-solid application kernel
//
// lib/src/syd.go: Go bindings of libsyd, the syd API C Library
//
// Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: LGPL-3.0
//
// Package syd provides Go bindings for the libsyd C library.
//
// Note: Build with CGO_LDFLAGS=-static to link libsyd statically.
package syd

/*
#cgo LDFLAGS: -lsyd
#include <stdint.h>
#include <syd.h>
*/
import "C"
import (
	"encoding/json"
	"os"
	"syscall"
	"unsafe"
)

// LockState represents the state of the sandbox lock in Go.
type LockState uint8

// An enumeration of the possible states for the sandbox lock.
const (
	// LockOff indicates that the sandbox lock is off, allowing all sandbox commands.
	// This state means there are no restrictions imposed by the sandbox.
	LockOff LockState = iota

	// LockExec indicates that the sandbox lock is on for all processes except the
	// initial process (syd exec child).
	LockExec

	// LockOn indicates that the sandbox lock is on, disallowing all sandbox commands.
	// In this state, the sandbox is in its most restrictive mode, not permitting
	// any operations that could modify its state or configuration.
	LockOn
)

// Action represents the actions for Sandboxing.
type Action uint8

// An enumeration of the possible actions for Sandboxing.
const (
	// Allow system call.
	ActionAllow Action = iota
	// Allow system call and warn.
	ActionWarn
	// Deny system call silently.
	ActionFilter
	// Deny system call and warn.
	ActionDeny
	// Deny system call, warn and panic the current Syd thread.
	ActionPanic
	// Deny system call, warn and stop offending process.
	ActionStop
	// Deny system call, warn and kill offending process.
	ActionKill
	// Warn, and exit Syd immediately with deny errno as exit value.
	ActionExit
)

type Sandbox struct {
	Flags           []string `json:"flags"`
	State           string   `json:"state"`
	Lock            string   `json:"lock"`
	Cpid            int      `json:"cpid"`
	DefaultStat     string   `json:"default_stat"`
	DefaultRead     string   `json:"default_read"`
	DefaultWrite    string   `json:"default_write"`
	DefaultExec     string   `json:"default_exec"`
	DefaultIoctl    string   `json:"default_ioctl"`
	DefaultCreate   string   `json:"default_create"`
	DefaultDelete   string   `json:"default_delete"`
	DefaultRename   string   `json:"default_rename"`
	DefaultSymlink  string   `json:"default_symlink"`
	DefaultTruncate string   `json:"default_truncate"`
	DefaultChdir    string   `json:"default_chdir"`
	DefaultReaddir  string   `json:"default_readdir"`
	DefaultMkdir    string   `json:"default_mkdir"`
	DefaultChown    string   `json:"default_chown"`
	DefaultChgrp    string   `json:"default_chgrp"`
	DefaultChmod    string   `json:"default_chmod"`
	DefaultChattr   string   `json:"default_chattr"`
	DefaultChroot   string   `json:"default_chroot"`
	DefaultUtime    string   `json:"default_utime"`
	DefaultMkdev    string   `json:"default_mkdev"`
	DefaultMkfifo   string   `json:"default_mkfifo"`
	DefaultMktemp   string   `json:"default_mktemp"`

	DefaultNetBind    string `json:"default_net_bind"`
	DefaultNetConnect string `json:"default_net_connect"`
	DefaultNetSendFd  string `json:"default_net_send_fd"`

	DefaultBlock string `json:"default_block"`

	DefaultMem string `json:"default_mem"`
	DefaultPid string `json:"default_pid"`

	DefaultForce     string `json:"default_force"`
	DefaultSegvGuard string `json:"default_segvguard"`
	DefaultTPE       string `json:"default_tpe"`

	MemMax              int64       `json:"mem_max"`
	MemVmMax            int64       `json:"mem_vm_max"`
	PidMax              int         `json:"pid_max"`
	CidrRules           []CidrRule  `json:"cidr_rules"`
	GlobRules           []GlobRule  `json:"glob_rules"`
	ForceRules          []ForceRule `json:"force_rules"`
	SegvGuardExpiry     uint64      `json:"segvguard_expiry"`
	SegvGuardSuspension uint64      `json:"segvguard_suspension"`
	SegvGuardMaxCrashes uint8       `json:"segvguard_maxcrashes"`
}

type FilterRule struct {
	Pat string `json:"pat"`
}

type CidrRule struct {
	Act string  `json:"act"`
	Cap string  `json:"cap"`
	Pat Pattern `json:"pat"`
}

type GlobRule struct {
	Act string `json:"act"`
	Cap string `json:"cap"`
	Pat string `json:"pat"`
}

type ForceRule struct {
	Act string `json:"act"`
	Sha string `json:"sha"`
	Pat string `json:"pat"`
}

type Pattern struct {
	Addr string      `json:"addr"`
	Port interface{} `json:"port"` // Port could be an int or a slice of ints
}

// Info reads the state of the syd sandbox from /dev/syd and returns it as a Sandbox struct.
//
// If there is a failure in reading the file, the error returned is the corresponding syscall.Errno.
//
// If there is a JSON decoding error, syscall.EINVAL is returned.
func Info() (*Sandbox, error) {
	data, err := os.ReadFile("/dev/syd")
	if err != nil {
		return nil, err // os.ReadFile already returns an error of type syscall.Errno
	}

	var state Sandbox
	if err = json.Unmarshal(data, &state); err != nil {
		return nil, syscall.EINVAL // Return EINVAL for JSON decoding errors
	}

	return &state, nil
}

// Check performs an lstat system call on the file "/dev/syd".
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func Check() error {
	result := C.syd_check()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Api performs a syd API check. This function should be called before
// making any other syd API calls. It's used to ensure that the syd
// environment is correctly set up and ready to handle further API requests.
//
// Returns the API number on success. If the call fails, it returns an error
// corresponding to the negated errno. The successful return value is an integer
// representing the API number, and the error, if any, is of type syscall.Errno.
func Api() (int, error) {
	result := C.syd_api()
	if result < 0 {
		return -1, syscall.Errno(-result)
	}
	// On success, return the API number.
	return int(result), nil
}

// Panic causes syd to exit immediately with code 127.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func Panic() error {
	result := C.syd_panic()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Reset causes syd to reset sandboxing to the default state.
// Allowlists, denylists and filters are going to be cleared.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func Reset() error {
	result := C.syd_reset()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Load instructs syd to read its configuration from the specified file
// descriptor. This function is used to load syd configurations dynamically
// at runtime from a file represented by the given file descriptor.
//
// The function accepts a file descriptor (fd) as an argument. This file descriptor
// should be valid and point to a file containing the desired configuration.
//
// Returns nil on success. If the call fails, it returns an error corresponding
// to the negated errno. The error is of type syscall.Errno.
func Load(fd int) error {
	result := C.syd_load(C.int(fd))
	if result != 0 {
		// Convert the negated errno to a positive value and return it as an error.
		return syscall.Errno(-result)
	}
	return nil
}

// Lock sets the state of the sandbox lock.
// Returns nil on success and a syscall.Errno on failure.
func Lock(state LockState) error {
	result := C.syd_lock(C.lock_state_t(state))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Exec executes a command outside the sandbox without applying sandboxing.
// This function is used to run a command in a non-sandboxed environment.
//
// The function accepts a string for the file to execute and a slice of strings
// representing the arguments to the command.
//
// Returns nil on success. If the call fails, it returns an error corresponding
// to the negated errno. The error is of type syscall.Errno.
func Exec(file string, argv []string) error {
	cFile := C.CString(file)
	defer C.free(unsafe.Pointer(cFile))

	cArgv := make([]*C.char, len(argv)+1)
	for i, arg := range argv {
		cArgv[i] = C.CString(arg)
		defer C.free(unsafe.Pointer(cArgv[i]))
	}
	cArgv[len(argv)] = nil // Null-terminate the argument list

	result := C.syd_exec(cFile, &cArgv[0])
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnableStat enables stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableStat() error {
	result := C.syd_enable_stat()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableStat disables stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableStat() error {
	result := C.syd_disable_stat()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledStat checks if stat sandboxing is enabled in the syd environment.
//
// It returns true if stat sandboxing is enabled, and false otherwise.
func EnabledStat() bool {
	result := C.syd_enabled_stat()
	return bool(result)
}

// EnableRead enables read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableRead() error {
	result := C.syd_enable_read()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableRead disables read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableRead() error {
	result := C.syd_disable_read()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledRead checks if read sandboxing is enabled in the syd environment.
//
// It returns true if read sandboxing is enabled, and false otherwise.
func EnabledRead() bool {
	result := C.syd_enabled_read()
	return bool(result)
}

// EnableWrite enables write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableWrite() error {
	result := C.syd_enable_write()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableWrite disables write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableWrite() error {
	result := C.syd_disable_write()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledWrite checks if write sandboxing is enabled in the syd
// environment.
//
// It returns true if write sandboxing is enabled, and false otherwise.
func EnabledWrite() bool {
	result := C.syd_enabled_write()
	return bool(result)
}

// EnableExec enables exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableExec() error {
	result := C.syd_enable_exec()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableExec disables exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableExec() error {
	result := C.syd_disable_exec()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledExec checks if exec sandboxing is enabled in the syd environment.
//
// It returns true if exec sandboxing is enabled, and false otherwise.
func EnabledExec() bool {
	result := C.syd_enabled_exec()
	return bool(result)
}

// EnableIoctl enables ioctl sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableIoctl() error {
	result := C.syd_enable_ioctl()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableIoctl disables ioctl sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableIoctl() error {
	result := C.syd_disable_ioctl()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledIoctl checks if ioctl sandboxing is enabled in the syd
// environment.
//
// It returns true if ioctl sandboxing is enabled, and false otherwise.
func EnabledIoctl() bool {
	result := C.syd_enabled_ioctl()
	return bool(result)
}

// EnableCreate enables create sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableCreate() error {
	result := C.syd_enable_create()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableCreate disables create sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableCreate() error {
	result := C.syd_disable_create()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledCreate checks if create sandboxing is enabled in the syd environment.
//
// It returns true if create sandboxing is enabled, and false otherwise.
func EnabledCreate() bool {
	result := C.syd_enabled_create()
	return bool(result)
}

// EnableDelete enables delete sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableDelete() error {
	result := C.syd_enable_delete()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableDelete disables delete sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableDelete() error {
	result := C.syd_disable_delete()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledDelete checks if delete sandboxing is enabled in the syd environment.
//
// It returns true if delete sandboxing is enabled, and false otherwise.
func EnabledDelete() bool {
	result := C.syd_enabled_delete()
	return bool(result)
}

// EnableRename enables rename sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableRename() error {
	result := C.syd_enable_rename()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableRename disables rename sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableRename() error {
	result := C.syd_disable_rename()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledRename checks if rename sandboxing is enabled in the syd environment.
//
// It returns true if rename sandboxing is enabled, and false otherwise.
func EnabledRename() bool {
	result := C.syd_enabled_rename()
	return bool(result)
}

// EnableSymlink enables symlink sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableSymlink() error {
	result := C.syd_enable_symlink()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableSymlink disables symlink sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableSymlink() error {
	result := C.syd_disable_symlink()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledSymlink checks if symlink sandboxing is enabled in the syd environment.
//
// It returns true if symlink sandboxing is enabled, and false otherwise.
func EnabledSymlink() bool {
	result := C.syd_enabled_symlink()
	return bool(result)
}

// EnableTruncate enables truncate sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableTruncate() error {
	result := C.syd_enable_truncate()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableTruncate disables truncate sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableTruncate() error {
	result := C.syd_disable_truncate()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledTruncate checks if truncate sandboxing is enabled in the syd environment.
//
// It returns true if truncate sandboxing is enabled, and false otherwise.
func EnabledTruncate() bool {
	result := C.syd_enabled_truncate()
	return bool(result)
}

// EnableChdir enables chdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableChdir() error {
	result := C.syd_enable_chdir()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableChdir disables chdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableChdir() error {
	result := C.syd_disable_chdir()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledChdir checks if chdir sandboxing is enabled in the syd environment.
//
// It returns true if chdir sandboxing is enabled, and false otherwise.
func EnabledChdir() bool {
	result := C.syd_enabled_chdir()
	return bool(result)
}

// EnableReaddir enables readdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableReaddir() error {
	result := C.syd_enable_readdir()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableReaddir disables readdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableReaddir() error {
	result := C.syd_disable_readdir()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledReaddir checks if readdir sandboxing is enabled in the syd environment.
//
// It returns true if readdir sandboxing is enabled, and false otherwise.
func EnabledReaddir() bool {
	result := C.syd_enabled_readdir()
	return bool(result)
}

// EnableMkdir enables mkdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableMkdir() error {
	result := C.syd_enable_mkdir()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableMkdir disables mkdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableMkdir() error {
	result := C.syd_disable_mkdir()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledMkdir checks if mkdir sandboxing is enabled in the syd environment.
//
// It returns true if mkdir sandboxing is enabled, and false otherwise.
func EnabledMkdir() bool {
	result := C.syd_enabled_mkdir()
	return bool(result)
}

// EnableChown enables chown sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableChown() error {
	result := C.syd_enable_chown()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableChown disables chown sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableChown() error {
	result := C.syd_disable_chown()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledChown checks if chown sandboxing is enabled in the syd environment.
//
// It returns true if chown sandboxing is enabled, and false otherwise.
func EnabledChown() bool {
	result := C.syd_enabled_chown()
	return bool(result)
}

// EnableChgrp enables chgrp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableChgrp() error {
	result := C.syd_enable_chgrp()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableChgrp disables chgrp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableChgrp() error {
	result := C.syd_disable_chgrp()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledChgrp checks if chgrp sandboxing is enabled in the syd environment.
//
// It returns true if chgrp sandboxing is enabled, and false otherwise.
func EnabledChgrp() bool {
	result := C.syd_enabled_chgrp()
	return bool(result)
}

// EnableChmod enables chmod sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableChmod() error {
	result := C.syd_enable_chmod()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableChmod disables chmod sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableChmod() error {
	result := C.syd_disable_chmod()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledChmod checks if chmod sandboxing is enabled in the syd environment.
//
// It returns true if chmod sandboxing is enabled, and false otherwise.
func EnabledChmod() bool {
	result := C.syd_enabled_chmod()
	return bool(result)
}

// EnableChattr enables chattr sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableChattr() error {
	result := C.syd_enable_chattr()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableChattr disables chattr sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableChattr() error {
	result := C.syd_disable_chattr()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledChattr checks if chattr sandboxing is enabled in the syd environment.
//
// It returns true if chattr sandboxing is enabled, and false otherwise.
func EnabledChattr() bool {
	result := C.syd_enabled_chattr()
	return bool(result)
}

// EnableChroot enables chroot sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableChroot() error {
	result := C.syd_enable_chroot()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableChroot disables chroot sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableChroot() error {
	result := C.syd_disable_chroot()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledChroot checks if chroot sandboxing is enabled in the syd environment.
//
// It returns true if chroot sandboxing is enabled, and false otherwise.
func EnabledChroot() bool {
	result := C.syd_enabled_chroot()
	return bool(result)
}

// EnableUtime enables utime sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableUtime() error {
	result := C.syd_enable_utime()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableUtime disables utime sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableUtime() error {
	result := C.syd_disable_utime()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledUtime checks if utime sandboxing is enabled in the syd environment.
//
// It returns true if utime sandboxing is enabled, and false otherwise.
func EnabledUtime() bool {
	result := C.syd_enabled_utime()
	return bool(result)
}

// EnableMkdev enables mkdev sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableMkdev() error {
	result := C.syd_enable_mkdev()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableMkdev disables mkdev sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableMkdev() error {
	result := C.syd_disable_mkdev()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledMkdev checks if mkdev sandboxing is enabled in the syd environment.
//
// It returns true if mkdev sandboxing is enabled, and false otherwise.
func EnabledMkdev() bool {
	result := C.syd_enabled_mkdev()
	return bool(result)
}

// EnableMkfifo enables mkfifo sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableMkfifo() error {
	result := C.syd_enable_mkfifo()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableMkfifo disables mkfifo sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableMkfifo() error {
	result := C.syd_disable_mkfifo()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledMkfifo checks if mkfifo sandboxing is enabled in the syd environment.
//
// It returns true if mkfifo sandboxing is enabled, and false otherwise.
func EnabledMkfifo() bool {
	result := C.syd_enabled_mkfifo()
	return bool(result)
}

// EnableMktemp enables mktemp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableMktemp() error {
	result := C.syd_enable_mktemp()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableMktemp disables mktemp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableMktemp() error {
	result := C.syd_disable_mktemp()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledMktemp checks if mktemp sandboxing is enabled in the syd environment.
//
// It returns true if mktemp sandboxing is enabled, and false otherwise.
func EnabledMktemp() bool {
	result := C.syd_enabled_mktemp()
	return bool(result)
}

// EnableNet enables network sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableNet() error {
	result := C.syd_enable_net()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableNet disables network sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableNet() error {
	result := C.syd_disable_net()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledNet checks if network sandboxing is enabled in the syd
// environment.
//
// It returns true if network sandboxing is enabled, and false otherwise.
func EnabledNet() bool {
	result := C.syd_enabled_net()
	return bool(result)
}

// EnabledLock checks if lock andboxing is enabled in the syd environment.
//
// It returns true if lock sandboxing is enabled, and false otherwise.
func EnabledLock() bool {
	result := C.syd_enabled_lock()
	return bool(result)
}

// EnabledCrypt checks if crypt sandboxing is enabled in the syd environment.
//
// It returns true if crypt sandboxing is enabled, and false otherwise.
func EnabledCrypt() bool {
	result := C.syd_enabled_crypt()
	return bool(result)
}

// EnabledProxy checks if proxy andboxing is enabled in the syd environment.
//
// It returns true if proxy sandboxing is enabled, and false otherwise.
func EnabledProxy() bool {
	result := C.syd_enabled_proxy()
	return bool(result)
}

// EnableMem enables memory sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableMem() error {
	result := C.syd_enable_mem()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableMem disables memory sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableMem() error {
	result := C.syd_disable_mem()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledMem checks if memory sandboxing is enabled in the syd environment.
//
// It returns true if memory sandboxing is enabled, and false otherwise.
func EnabledMem() bool {
	result := C.syd_enabled_mem()
	return bool(result)
}

// EnablePid enables PID sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnablePid() error {
	result := C.syd_enable_pid()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisablePid disables PID sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisablePid() error {
	result := C.syd_disable_pid()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledPid checks if PID sandboxing is enabled in the syd environment.
//
// It returns true if PID sandboxing is enabled, and false otherwise.
func EnabledPid() bool {
	result := C.syd_enabled_pid()
	return bool(result)
}

// EnableForce enables force sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableForce() error {
	result := C.syd_enable_force()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableForce disables force sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableForce() error {
	result := C.syd_disable_force()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledForce checks if force sandboxing is enabled in the syd environment.
//
// It returns true if force sandboxing is enabled, and false otherwise.
func EnabledForce() bool {
	result := C.syd_enabled_force()
	return bool(result)
}

// EnableTPE enables TPE sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableTPE() error {
	result := C.syd_enable_tpe()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableTPE disables TPE sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableTPE() error {
	result := C.syd_disable_tpe()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledTPE checks if TPE sandboxing is enabled in the syd environment.
//
// It returns true if TPE sandboxing is enabled, and false otherwise.
func EnabledTPE() bool {
	result := C.syd_enabled_tpe()
	return bool(result)
}

// Set default action for Stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultStat(action Action) error {
	result := C.syd_default_stat(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultRead(action Action) error {
	result := C.syd_default_read(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultWrite(action Action) error {
	result := C.syd_default_write(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultExec(action Action) error {
	result := C.syd_default_exec(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Ioctl sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultIoctl(action Action) error {
	result := C.syd_default_ioctl(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Create sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultCreate(action Action) error {
	result := C.syd_default_create(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Delete sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultDelete(action Action) error {
	result := C.syd_default_delete(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Rename sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultRename(action Action) error {
	result := C.syd_default_rename(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Symlink sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultSymlink(action Action) error {
	result := C.syd_default_symlink(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Truncate sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultTruncate(action Action) error {
	result := C.syd_default_truncate(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Chdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultChdir(action Action) error {
	result := C.syd_default_chdir(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Readdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultReaddir(action Action) error {
	result := C.syd_default_readdir(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Mkdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultMkdir(action Action) error {
	result := C.syd_default_mkdir(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Chown sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultChown(action Action) error {
	result := C.syd_default_chown(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Chgrp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultChgrp(action Action) error {
	result := C.syd_default_chgrp(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Chmod sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultChmod(action Action) error {
	result := C.syd_default_chmod(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Chattr sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultChattr(action Action) error {
	result := C.syd_default_chattr(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Chroot sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultChroot(action Action) error {
	result := C.syd_default_chroot(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Utime sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultUtime(action Action) error {
	result := C.syd_default_utime(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Mkdev sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultMkdev(action Action) error {
	result := C.syd_default_mkdev(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Mkfifo sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultMkfifo(action Action) error {
	result := C.syd_default_mkfifo(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Mktemp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultMktemp(action Action) error {
	result := C.syd_default_mktemp(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Network sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultNet(action Action) error {
	result := C.syd_default_net(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for IP blocklist violations.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultBlock(action Action) error {
	result := C.syd_default_block(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Memory sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultMem(action Action) error {
	result := C.syd_default_mem(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for PID sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultPid(action Action) error {
	result := C.syd_default_pid(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for Force sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultForce(action Action) error {
	result := C.syd_default_force(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for SegvGuard.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultSegvGuard(action Action) error {
	result := C.syd_default_segvguard(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Set default action for TPE sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DefaultTPE(action Action) error {
	result := C.syd_default_tpe(C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Adds a request to the _ioctl_(2) denylist.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func IoctlDeny(request uint64) error {
	result := C.syd_ioctl_deny(C.uint64_t(request))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// StatAdd adds the specified glob pattern to the given actionlist of
// Stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func StatAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_stat_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// StatDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func StatDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_stat_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// StatRem removes all matching patterns from the given actionlist of
// Stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func StatRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_stat_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ReadAdd adds the specified glob pattern to the given actionlist of
// Read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ReadAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_read_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ReadDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ReadDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_read_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ReadRem removes all matching patterns from the given actionlist of
// Read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ReadRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_read_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// WriteAdd adds the specified glob pattern to the given actionlist of
// Write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func WriteAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_write_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// WriteDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func WriteDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_write_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// WriteRem removes all matching patterns from the given actionlist of
// Write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func WriteRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_write_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ExecAdd adds the specified glob pattern to the given actionlist of
// Exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ExecAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_exec_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ExecDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ExecDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_exec_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ExecRem removes all matching patterns from the given actionlist of
// Exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ExecRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_exec_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// IoctlAdd adds the specified glob pattern to the given actionlist of
// Ioctl sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func IoctlAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_ioctl_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// IoctlDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func IoctlDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_ioctl_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// IoctlRem removes all matching patterns from the given actionlist of
// Ioctl sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func IoctlRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_ioctl_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// CreateAdd adds the specified glob pattern to the given actionlist of
// Create sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func CreateAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_create_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// CreateDel removes the first instance from the end of the given
// actionlist of create sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func CreateDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_create_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// CreateRem removes all matching patterns from the given actionlist of
// Create sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func CreateRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_create_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DeleteAdd adds the specified glob pattern to the given actionlist of
// Delete sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DeleteAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_delete_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DeleteDel removes the first instance from the end of the given
// actionlist of delete sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DeleteDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_delete_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DeleteRem removes all matching patterns from the given actionlist of
// Delete sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DeleteRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_delete_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// RenameAdd adds the specified glob pattern to the given actionlist of
// Rename sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func RenameAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_rename_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// RenameDel removes the first instance from the end of the given
// actionlist of rename sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func RenameDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_rename_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// RenameRem removes all matching patterns from the given actionlist of
// Rename sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func RenameRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_rename_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// SymlinkAdd adds the specified glob pattern to the given actionlist of
// Symlink sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func SymlinkAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_symlink_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// SymlinkDel removes the first instance from the end of the given
// actionlist of symlink sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func SymlinkDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_symlink_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// SymlinkRem removes all matching patterns from the given actionlist of
// Symlink sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func SymlinkRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_symlink_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// TruncateAdd adds the specified glob pattern to the given actionlist of
// Truncate sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func TruncateAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_truncate_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// TruncateDel removes the first instance from the end of the given
// actionlist of truncate sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func TruncateDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_truncate_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// TruncateRem removes all matching patterns from the given actionlist of
// Truncate sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func TruncateRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_truncate_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChdirAdd adds the specified glob pattern to the given actionlist of
// Chdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChdirAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chdir_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChdirDel removes the first instance from the end of the given
// actionlist of chdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChdirDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chdir_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChdirRem removes all matching patterns from the given actionlist of
// Chdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChdirRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chdir_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ReaddirAdd adds the specified glob pattern to the given actionlist of
// Readdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ReaddirAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_readdir_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ReaddirDel removes the first instance from the end of the given
// actionlist of readdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ReaddirDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_readdir_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ReaddirRem removes all matching patterns from the given actionlist of
// Readdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ReaddirRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_readdir_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkdirAdd adds the specified glob pattern to the given actionlist of
// Mkdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkdirAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkdir_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkdirDel removes the first instance from the end of the given
// actionlist of mkdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkdirDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkdir_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkdirRem removes all matching patterns from the given actionlist of
// Mkdir sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkdirRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkdir_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChownAdd adds the specified glob pattern to the given actionlist of
// Chown sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChownAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chown_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChownDel removes the first instance from the end of the given
// actionlist of chown sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChownDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chown_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChownRem removes all matching patterns from the given actionlist of
// Chown sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChownRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chown_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChgrpAdd adds the specified glob pattern to the given actionlist of
// Chgrp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChgrpAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chgrp_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChgrpDel removes the first instance from the end of the given
// actionlist of chgrp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChgrpDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chgrp_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChgrpRem removes all matching patterns from the given actionlist of
// Chgrp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChgrpRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chgrp_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChmodAdd adds the specified glob pattern to the given actionlist of
// Chmod sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChmodAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chmod_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChmodDel removes the first instance from the end of the given
// actionlist of chmod sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChmodDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chmod_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChmodRem removes all matching patterns from the given actionlist of
// Chmod sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChmodRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chmod_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChattrAdd adds the specified glob pattern to the given actionlist of
// Chattr sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChattrAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chattr_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChattrDel removes the first instance from the end of the given
// actionlist of chattr sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChattrDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chattr_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChattrRem removes all matching patterns from the given actionlist of
// Chattr sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChattrRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chattr_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChrootAdd adds the specified glob pattern to the given actionlist of
// Chroot sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChrootAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chroot_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChrootDel removes the first instance from the end of the given
// actionlist of chroot sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChrootDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chroot_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// ChrootRem removes all matching patterns from the given actionlist of
// Chroot sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ChrootRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_chroot_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// UtimeAdd adds the specified glob pattern to the given actionlist of
// Utime sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func UtimeAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_utime_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// UtimeDel removes the first instance from the end of the given
// actionlist of utime sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func UtimeDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_utime_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// UtimeRem removes all matching patterns from the given actionlist of
// Utime sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func UtimeRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_utime_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkdevAdd adds the specified glob pattern to the given actionlist of
// Mkdev sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkdevAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkdev_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkdevDel removes the first instance from the end of the given
// actionlist of mkdev sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkdevDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkdev_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkdevRem removes all matching patterns from the given actionlist of
// Mkdev sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkdevRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkdev_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkfifoAdd adds the specified glob pattern to the given actionlist of
// Mkfifo sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkfifoAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkfifo_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkfifoDel removes the first instance from the end of the given
// actionlist of mkfifo sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkfifoDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkfifo_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MkfifoRem removes all matching patterns from the given actionlist of
// Mkfifo sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MkfifoRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mkfifo_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MktempAdd adds the specified glob pattern to the given actionlist of
// Mktemp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MktempAdd(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mktemp_add(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MktempDel removes the first instance from the end of the given
// actionlist of mktemp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MktempDel(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mktemp_del(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MktempRem removes all matching patterns from the given actionlist of
// Mktemp sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MktempRem(action Action, glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_mktemp_rem(C.action_t(action), cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetBindAdd adds the specified address pattern to the given actionlist of
// Net/bind sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetBindAdd(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_bind_add(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetBindDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetBindDel(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_bind_del(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetBindRem removes all matching patterns from the given actionlist of
// Net/bind sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetBindRem(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_bind_rem(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetConnectAdd adds the specified address pattern to the given actionlist of
// Net/connect sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetConnectAdd(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_connect_add(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetConnectDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetConnectDel(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_connect_del(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetConnectRem removes all matching patterns from the given actionlist of
// Net/connect sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetConnectRem(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_connect_rem(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetSendFdAdd adds the specified address pattern to the given actionlist of
// Net/send sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetSendFdAdd(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_sendfd_add(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetSendFdDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetSendFdDel(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_sendfd_del(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetSendFdRem removes all matching patterns from the given actionlist of
// Net/send sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetSendFdRem(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_sendfd_rem(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetLinkAdd adds the specified address pattern to the given actionlist of
// Net/link sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetLinkAdd(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_link_add(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetLinkDel removes the first instance from the end of the given
// actionlist of read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetLinkDel(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_link_del(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// NetLinkRem removes all matching patterns from the given actionlist of
// Net/link sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func NetLinkRem(action Action, addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_net_link_rem(C.action_t(action), cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Adds an entry to the Integrity Force map for Force Sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ForceAdd(path string, hash string, action Action) error {
	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))
	cHash := C.CString(hash)
	defer C.free(unsafe.Pointer(cHash))

	result := C.syd_force_add(cPath, cHash, C.action_t(action))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Removes an entry from the Integrity Force map for Force Sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ForceDel(path string) error {
	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))

	result := C.syd_force_del(cPath)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Clears the Integrity Force map for Force Sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func ForceClr() error {
	result := C.syd_force_clr()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MemMax sets the syd maximum per-process memory usage limit for memory
// sandboxing.
//
// The size parameter is a string that can represent the size in
// different formats, as the parse-size crate is used to parse the
// value.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MemMax(size string) error {
	cSize := C.CString(size)
	defer C.free(unsafe.Pointer(cSize))

	result := C.syd_mem_max(cSize)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MemVmMax sets the syd maximum per-process virtual memory usage limit for
// memory sandboxing.
//
// The size parameter is a string that can represent the size in
// different formats, as the parse-size crate is used to parse the
// value.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MemVmMax(size string) error {
	cSize := C.CString(size)
	defer C.free(unsafe.Pointer(cSize))

	result := C.syd_mem_vm_max(cSize)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// PidMax sets the syd maximum process ID limit for PID sandboxing.
//
// The function takes an integer representing the maximum number of PIDs.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func PidMax(size int) error {
	if size < 0 {
		return syscall.EINVAL
	}
	result := C.syd_pid_max(C.size_t(size))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Specify SegvGuard entry expiry timeout in seconds.
// Setting this timeout to 0 effectively disables SegvGuard.
//
// The function takes an integer representing the timeout.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func SegvGuardExpiry(timeout uint64) error {
	result := C.syd_segvguard_expiry(C.uint64_t(timeout))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Specify SegvGuard entry suspension timeout in seconds.
//
// The function takes an integer representing the timeout.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func SegvGuardSuspension(timeout uint64) error {
	result := C.syd_segvguard_suspension(C.uint64_t(timeout))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Specify SegvGuard max number of crashes before suspension.
//
// The function takes an integer representing the limit.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func SegvGuardMaxCrashes(timeout uint8) error {
	result := C.syd_segvguard_maxcrashes(C.uint8_t(timeout))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}
