#!/usr/bin/env perl
# coding: utf-8
#
# Syd: rock-solid application kernel
# lib/src/syd.pl: Perl bindings of libsyd, the syd API C Library
# Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0

package syd;

=head1 NAME

plsyd - Perl Bindings for the syd API Rust Library

=head1 SYNOPSIS

`plsyd` provides Perl bindings for `libsyd`, a C library written in Rust that
implements the syd stat API. This package allows interaction with the
`/dev/syd` interface of syd, enabling runtime configuration and interaction
within the syd sandboxing environment.

=head1 DESCRIPTION

The `plsyd` library is designed to interact with the syd sandboxing
environment through Perl. It offers functionalities to check and modify the
state of the sandbox lock, perform system calls to `/dev/syd`, and execute
commands within the sandbox. This makes it easier for Perl applications to
integrate with syd's features.

=head1 REQUIREMENTS

To use `plsyd`, the shared library `libsyd.so` must be available in the
system's library search path. Ensure this shared library is properly installed
and its location is included in the environment path where system libraries are
searched for.

=head1 ATTENTION

This library is currently a work in progress. The API is subject to change and
may not be stable. Users are advised to use it with caution and to stay updated
with the latest changes.

=head1 MORE INFORMATION

For more detailed information about `libsyd` and usage instructions, refer to
the syd manual: L<https://git.sr.ht/~alip/syd>. To read `libsyd` API
documentation, see L<https://libsyd.exherbolinux.org>.

=head1 AUTHOR

Ali Polatel (alip@chesswob.org)

=head1 LICENSE

This software is licensed under the LGPL-3.0 license.

=cut

use strict;
use warnings;
use Exporter 'import';
use Carp;
use Errno qw(EINVAL ENOENT ENAMETOOLONG);
use JSON;
use Scalar::Util qw(looks_like_number);

use FFI::Platypus 2.00;
use FFI::CheckLib qw( find_lib );

our $LIBSYD_PATH;
if ($ENV{'LD_LIBRARY_PATH'}) {
	my @paths = split(':', $ENV{'LD_LIBRARY_PATH'});
	($LIBSYD_PATH) = grep { -e "$_/libsyd.so" } @paths;
	$LIBSYD_PATH .= "/libsyd.so" if defined $LIBSYD_PATH;
}
if (defined $LIBSYD_PATH) {
	warn "Loading libsyd.so via LD_LIBRARY_PATH from $LIBSYD_PATH";
} else {
	$LIBSYD_PATH = find_lib(lib => 'syd') || croak "Failed to find libsyd: $!";
}
my $syd = FFI::Platypus->new(api => 2, lib => $LIBSYD_PATH);

# Define exportable and default functions
our @EXPORT_OK = qw(
  $LIBSYD_PATH

  LOCK_OFF LOCK_EXEC LOCK_ON
  ACTION_ALLOW ACTION_WARN ACTION_FILTER ACTION_DENY ACTION_STOP ACTION_KILL ACTION_PANIC ACTION_EXIT

  info
  check api
  lock
  exec
  panic reset load

  enable_stat disable_stat enabled_stat
  enable_read disable_read enabled_read
  enable_write disable_write enabled_write
  enable_exec disable_exec enabled_exec
  enable_ioctl disable_ioctl enabled_ioctl
  enable_create disable_create enabled_create
  enable_delete disable_delete enabled_delete
  enable_rename disable_rename enabled_rename
  enable_symlink disable_symlink enabled_symlink
  enable_truncate disable_truncate enabled_truncate
  enable_chdir disable_chdir enabled_chdir
  enable_readdir disable_readdir enabled_readdir
  enable_mkdir disable_mkdir enabled_mkdir
  enable_chown disable_chown enabled_chown
  enable_chgrp disable_chgrp enabled_chgrp
  enable_chmod disable_chmod enabled_chmod
  enable_chattr disable_chattr enabled_chattr
  enable_chroot disable_chroot enabled_chroot
  enable_utime disable_utime enabled_utime
  enable_mkdev disable_mkdev enabled_mkdev
  enable_mkfifo disable_mkfifo enabled_mkfifo
  enable_mktemp disable_mktemp enabled_mktemp

  enable_net disable_net enabled_net

  enabled_lock enabled_crypt enabled_proxy

  enable_mem disable_mem enabled_mem
  enable_pid disable_pid enabled_pid

  enable_force disable_force enabled_force
  enable_tpe disable_tpe enabled_tpe

  default_stat
  default_read
  default_write
  default_exec
  default_ioctl
  default_create
  default_delete
  default_rename
  default_symlink
  default_truncate
  default_chdir
  default_readdir
  default_mkdir
  default_chown
  default_chgrp
  default_chmod
  default_chattr
  default_chroot
  default_utime
  default_mkdev
  default_mkfifo
  default_mktemp

  default_net
  default_block

  default_mem
  default_pid

  default_force
  default_segvguard
  default_tpe

  ioctl_deny

  stat_add stat_del stat_rem
  read_add read_del read_rem
  write_add write_del write_rem
  exec_add exec_del exec_rem
  ioctl_add ioctl_del ioctl_rem
  create_add create_del create_rem
  delete_add delete_del delete_rem
  rename_add rename_del rename_rem
  symlink_add symlink_del symlink_rem
  truncate_add truncate_del truncate_rem
  chdir_add chdir_del chdir_rem
  readdir_add readdir_del readdir_rem
  mkdir_add mkdir_del mkdir_rem
  chown_add chown_del chown_rem
  chgrp_add chgrp_del chgrp_rem
  chmod_add chmod_del chmod_rem
  chattr_add chattr_del chattr_rem
  chroot_add chroot_del chroot_rem
  utime_add utime_del utime_rem
  mkdev_add mkdev_del mkdev_rem
  mkfifo_add mkfifo_del mkfifo_rem
  mktemp_add mktemp_del mktemp_rem

  net_bind_add net_bind_del net_bind_rem
  net_connect_add net_connect_del net_connect_rem
  net_sendfd_add net_sendfd_del net_sendfd_rem

  net_link_add net_link_del net_link_rem

  force_add force_del force_clr

  mem_max mem_vm_max pid_max
  segvguard_expiry segvguard_suspension segvguard_maxcrashes
);
our %EXPORT_TAGS = ('all' => [@EXPORT_OK],);

use constant {
	LOCK_OFF      => 0,
	LOCK_EXEC     => 1,
	LOCK_ON       => 2,
	ACTION_ALLOW  => 0,
	ACTION_WARN   => 1,
	ACTION_FILTER => 2,
	ACTION_DENY   => 3,
	ACTION_PANIC  => 4,
	ACTION_STOP   => 5,
	ACTION_KILL   => 6,
	ACTION_EXIT   => 7,
};

sub info {
	open my $fh, '<', '/dev/syd' or croak "Cannot open /dev/syd: $!";
	my $text = do { local $/; <$fh> };
	close $fh;
	return decode_json($text);
}

=head1 FUNCTIONS

=head2 check

Performs an lstat system call on the file "/dev/syd".

=over 4

=item Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_check' => 'check'] => [] => 'int', \&check_return_void);

=head2 api

Performs a syd API check. The caller is advised to perform this check
before calling any other syd API calls.

=over 4

=item Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_api' => 'api'] => [] => 'int', \&check_return_void);

=head2 panic

Causes syd to exit immediately with code 127.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_panic' => 'panic'] => [] => 'int', \&check_return_void);

=head2 reset

Causes syd to reset sandboxing to the default state.
Allowlists, denylists and filters are going to be cleared.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_reset' => 'reset'] => [] => 'int', \&check_return_void);

=head2 load

Causes syd to read configuration from the given file descriptor.

=over 4

=item * Parameters

=over 4

=item - fd (integer)

The file descriptor to read the configuration from.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_load' => 'load'] => ['int'] => 'int', \&check_return_1);

=head2 lock

Sets the state of the sandbox lock.

=over 4

=item * Args

=over 4

=item - state

The desired state of the sandbox lock, possible values are:

=over 4

=item * LOCK_OFF

The sandbox lock is off, allowing all sandbox commands.

=item * LOCK_EXEC

The sandbox lock is set to on for all processes except the initial
process (syd exec child). This is the default state.

=item * LOCK_ON

The sandbox lock is on, disallowing all sandbox commands.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_lock' => 'lock'] => ['uint8'] => 'int',
	sub {
		my ($func, $lock) = @_;

		# Validate lock state.
		unless (defined $lock
			&& looks_like_number($lock)
			&& int($lock) == $lock
			&& $lock >= LOCK_OFF
			&& $lock <= LOCK_ON) {
			$! = EINVAL;
			croak "libsyd error: $!";
		}
		my $r = $func->($lock);
		if ($r < 0) {
			$! = -$r;
			croak "libsyd error: $!";
		}
		return $r;
	}
);

=head2 exec

Execute a command outside the sandbox without sandboxing.

=over 4

=item * Parameters

=over 4

=item - file (string)

The file path of the command to be executed, as a string.

=item - argv (Array of strings)

The arguments to the command, as a list of strings.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_exec' => 'exec'] => ['string', 'opaque[]'] => 'int',
	sub {
		my ($func, $file, $argv) = @_;

		# Cast each argument in @argv to 'opaque'
		my @ptrs = map { $syd->cast('string' => 'opaque', $_) } @$argv;

		# Add a NULL pointer at the end of the argument list
		push @ptrs, undef;

		# Call the syd_exec function
		my $r = $func->($file, \@ptrs);

		# Check for errors
		if ($r == 0) {
			return 1;
		} elsif ($r < 0) {
			$! = -$r;
			croak "libsyd error: $!";
		}
		return $r;
	}
);

=head2 enable_stat

Enables Stat sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_stat' => 'enable_stat'] => [] => 'int', \&check_return_void);

=head2 disable_stat

Disables Stat sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_stat' => 'disable_stat'] => [] => 'int', \&check_return_void);

=head2 enabled_stat

Checks if Stat sandboxing is enabled.

=over 4

=item * Returns

Non-zero if Stat sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_stat' => 'enabled_stat'] => [] => 'bool', \&check_return_bool);

=head2 enable_read

Enables read sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_read' => 'enable_read'] => [] => 'int', \&check_return_void);

=head2 disable_read

Disables read sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_read' => 'disable_read'] => [] => 'int', \&check_return_void);

=head2 enabled_read

Checks if read sandboxing is enabled.

=over 4

=item * Returns

Non-zero if read sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_read' => 'enabled_read'] => [] => 'bool', \&check_return_bool);

=head2 enable_write

Enables Write sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_write' => 'enable_write'] => [] => 'int', \&check_return_void);

=head2 disable_write

Disables Write sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_write' => 'disable_write'] => [] => 'int', \&check_return_void);

=head2 enabled_write

Checks if Write sandboxing is enabled.

=over 4

=item * Returns

Non-zero if Write sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_write' => 'enabled_write'] => [] => 'bool', \&check_return_bool);

=head2 enable_exec

Enables Exec sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_exec' => 'enable_exec'] => [] => 'int', \&check_return_void);

=head2 disable_exec

Disables Exec sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_exec' => 'disable_exec'] => [] => 'int', \&check_return_void);

=head2 enabled_exec

Checks if Exec sandboxing is enabled.

=over 4

=item * Returns

Non-zero if Exec sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_exec' => 'enabled_exec'] => [] => 'bool', \&check_return_bool);

=head2 enable_ioctl

Enables ioctl sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_ioctl' => 'enable_ioctl'] => [] => 'int', \&check_return_void);

=head2 disable_ioctl

Disables ioctl sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_ioctl' => 'disable_ioctl'] => [] => 'int', \&check_return_void);

=head2 enabled_ioctl

Checks if ioctl sandboxing is enabled.

=over 4

=item * Returns

Non-zero if ioctl sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_ioctl' => 'enabled_ioctl'] => [] => 'bool', \&check_return_bool);

=head2 enable_create

Enables create sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_create' => 'enable_create'] => [] => 'int', \&check_return_void);

=head2 disable_create

Disables create sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_create' => 'disable_create'] => [] => 'int', \&check_return_void);

=head2 enabled_create

Checks if create sandboxing is enabled.

=over 4

=item * Returns

Non-zero if create sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_create' => 'enabled_create'] => [] => 'bool', \&check_return_bool);

=head2 enable_delete

Enables delete sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_delete' => 'enable_delete'] => [] => 'int', \&check_return_void);

=head2 disable_delete

Disables delete sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_delete' => 'disable_delete'] => [] => 'int', \&check_return_void);

=head2 enabled_delete

Checks if delete sandboxing is enabled.

=over 4

=item * Returns

Non-zero if delete sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_delete' => 'enabled_delete'] => [] => 'bool', \&check_return_bool);

=head2 enable_rename

Enables rename sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_rename' => 'enable_rename'] => [] => 'int', \&check_return_void);

=head2 disable_rename

Disables rename sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_rename' => 'disable_rename'] => [] => 'int', \&check_return_void);

=head2 enabled_rename

Checks if rename sandboxing is enabled.

=over 4

=item * Returns

Non-zero if rename sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_rename' => 'enabled_rename'] => [] => 'bool', \&check_return_bool);

=head2 enable_symlink

Enables symlink sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_symlink' => 'enable_symlink'] => [] => 'int', \&check_return_void);

=head2 disable_symlink

Disables symlink sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_symlink' => 'disable_symlink'] => [] => 'int', \&check_return_void);

=head2 enabled_symlink

Checks if symlink sandboxing is enabled.

=over 4

=item * Returns

Non-zero if symlink sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_symlink' => 'enabled_symlink'] => [] => 'bool', \&check_return_bool);

=head2 enable_truncate

Enables truncate sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_truncate' => 'enable_truncate'] => [] => 'int', \&check_return_void);

=head2 disable_truncate

Disables truncate sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_truncate' => 'disable_truncate'] => [] => 'int', \&check_return_void);

=head2 enabled_truncate

Checks if truncate sandboxing is enabled.

=over 4

=item * Returns

Non-zero if truncate sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_truncate' => 'enabled_truncate'] => [] => 'bool', \&check_return_bool);

=head2 enable_chdir

Enables chdir sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_chdir' => 'enable_chdir'] => [] => 'int', \&check_return_void);

=head2 disable_chdir

Disables chdir sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_chdir' => 'disable_chdir'] => [] => 'int', \&check_return_void);

=head2 enabled_chdir

Checks if chdir sandboxing is enabled.

=over 4

=item * Returns

Non-zero if chdir sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_chdir' => 'enabled_chdir'] => [] => 'bool', \&check_return_bool);

=head2 enable_readdir

Enables readdir sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_readdir' => 'enable_readdir'] => [] => 'int', \&check_return_void);

=head2 disable_readdir

Disables readdir sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_readdir' => 'disable_readdir'] => [] => 'int', \&check_return_void);

=head2 enabled_readdir

Checks if readdir sandboxing is enabled.

=over 4

=item * Returns

Non-zero if readdir sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_readdir' => 'enabled_readdir'] => [] => 'bool', \&check_return_bool);

=head2 enable_mkdir

Enables mkdir sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_mkdir' => 'enable_mkdir'] => [] => 'int', \&check_return_void);

=head2 disable_mkdir

Disables mkdir sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_mkdir' => 'disable_mkdir'] => [] => 'int', \&check_return_void);

=head2 enabled_mkdir

Checks if mkdir sandboxing is enabled.

=over 4

=item * Returns

Non-zero if mkdir sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_mkdir' => 'enabled_mkdir'] => [] => 'bool', \&check_return_bool);

=head2 enable_chown

Enables chown sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_chown' => 'enable_chown'] => [] => 'int', \&check_return_void);

=head2 disable_chown

Disables chown sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_chown' => 'disable_chown'] => [] => 'int', \&check_return_void);

=head2 enabled_chown

Checks if chown sandboxing is enabled.

=over 4

=item * Returns

Non-zero if chown sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_chown' => 'enabled_chown'] => [] => 'bool', \&check_return_bool);

=head2 enable_chgrp

Enables chgrp sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_chgrp' => 'enable_chgrp'] => [] => 'int', \&check_return_void);

=head2 disable_chgrp

Disables chgrp sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_chgrp' => 'disable_chgrp'] => [] => 'int', \&check_return_void);

=head2 enabled_chgrp

Checks if chgrp sandboxing is enabled.

=over 4

=item * Returns

Non-zero if chgrp sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_chgrp' => 'enabled_chgrp'] => [] => 'bool', \&check_return_bool);

=head2 enable_chmod

Enables chmod sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_chmod' => 'enable_chmod'] => [] => 'int', \&check_return_void);

=head2 disable_chmod

Disables chmod sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_chmod' => 'disable_chmod'] => [] => 'int', \&check_return_void);

=head2 enabled_chmod

Checks if chmod sandboxing is enabled.

=over 4

=item * Returns

Non-zero if chmod sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_chmod' => 'enabled_chmod'] => [] => 'bool', \&check_return_bool);

=head2 enable_chattr

Enables chattr sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_chattr' => 'enable_chattr'] => [] => 'int', \&check_return_void);

=head2 disable_chattr

Disables chattr sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_chattr' => 'disable_chattr'] => [] => 'int', \&check_return_void);

=head2 enabled_chattr

Checks if chattr sandboxing is enabled.

=over 4

=item * Returns

Non-zero if chattr sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_chattr' => 'enabled_chattr'] => [] => 'bool', \&check_return_bool);

=head2 enable_chroot

Enables chroot sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_chroot' => 'enable_chroot'] => [] => 'int', \&check_return_void);

=head2 disable_chroot

Disables chroot sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_chroot' => 'disable_chroot'] => [] => 'int', \&check_return_void);

=head2 enabled_chroot

Checks if chroot sandboxing is enabled.

=over 4

=item * Returns

Non-zero if chroot sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_chroot' => 'enabled_chroot'] => [] => 'bool', \&check_return_bool);

=head2 enable_utime

Enables utime sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_utime' => 'enable_utime'] => [] => 'int', \&check_return_void);

=head2 disable_utime

Disables utime sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_utime' => 'disable_utime'] => [] => 'int', \&check_return_void);

=head2 enabled_utime

Checks if utime sandboxing is enabled.

=over 4

=item * Returns

Non-zero if utime sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_utime' => 'enabled_utime'] => [] => 'bool', \&check_return_bool);

=head2 enable_mkdev

Enables mkdev sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_mkdev' => 'enable_mkdev'] => [] => 'int', \&check_return_void);

=head2 disable_mkdev

Disables mkdev sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_mkdev' => 'disable_mkdev'] => [] => 'int', \&check_return_void);

=head2 enabled_mkdev

Checks if mkdev sandboxing is enabled.

=over 4

=item * Returns

Non-zero if mkdev sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_mkdev' => 'enabled_mkdev'] => [] => 'bool', \&check_return_bool);

=head2 enable_mkfifo

Enables mkfifo sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_mkfifo' => 'enable_mkfifo'] => [] => 'int', \&check_return_void);

=head2 disable_mkfifo

Disables mkfifo sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_mkfifo' => 'disable_mkfifo'] => [] => 'int', \&check_return_void);

=head2 enabled_mkfifo

Checks if mkfifo sandboxing is enabled.

=over 4

=item * Returns

Non-zero if mkfifo sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_mkfifo' => 'enabled_mkfifo'] => [] => 'bool', \&check_return_bool);

=head2 enable_mktemp

Enables mktemp sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_mktemp' => 'enable_mktemp'] => [] => 'int', \&check_return_void);

=head2 disable_mktemp

Disables mktemp sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_mktemp' => 'disable_mktemp'] => [] => 'int', \&check_return_void);

=head2 enabled_mktemp

Checks if mktemp sandboxing is enabled.

=over 4

=item * Returns

Non-zero if mktemp sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_mktemp' => 'enabled_mktemp'] => [] => 'bool', \&check_return_bool);

=head2 enable_net

Enables net sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_net' => 'enable_net'] => [] => 'int', \&check_return_void);

=head2 disable_net

Disables net sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_net' => 'disable_net'] => [] => 'int', \&check_return_void);

=head2 enabled_net

Checks if net sandboxing is enabled.

=over 4

=item * Returns

Non-zero if net sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_net' => 'enabled_net'] => [] => 'bool', \&check_return_bool);

=head2 enabled_lock

Checks if lock sandboxing is enabled.

=over 4

=item * Returns

Non-zero if lock sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_lock' => 'enabled_lock'] => [] => 'bool', \&check_return_bool);

=head2 enabled_crypt

Checks if crypt sandboxing is enabled.

=over 4

=item * Returns

Non-zero if crypt sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_crypt' => 'enabled_crypt'] => [] => 'bool', \&check_return_bool);

=head2 enabled_proxy

Checks if proxy sandboxing is enabled.

=over 4

=item * Returns

Non-zero if proxy sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_proxy' => 'enabled_proxy'] => [] => 'bool', \&check_return_bool);

=head2 enable_mem

Enables memory sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_mem' => 'enable_mem'] => [] => 'int', \&check_return_void);

=head2 disable_mem

Disables memory sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_mem' => 'disable_mem'] => [] => 'int', \&check_return_void);

=head2 enabled_mem

Checks if memory sandboxing is enabled.

=over 4

=item * Returns

Non-zero if memory sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_mem' => 'enabled_mem'] => [] => 'bool', \&check_return_bool);

=head2 enable_pid

Enables PID sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_pid' => 'enable_pid'] => [] => 'int', \&check_return_void);

=head2 disable_pid

Disables PID sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_pid' => 'disable_pid'] => [] => 'int', \&check_return_void);

=head2 enabled_pid

Checks if PID sandboxing is enabled.

=over 4

=item * Returns

Non-zero if PID sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_pid' => 'enabled_pid'] => [] => 'bool', \&check_return_bool);

=head2 enable_force

Enables force sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_force' => 'enable_force'] => [] => 'int', \&check_return_void);

=head2 disable_force

Disables force sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_force' => 'disable_force'] => [] => 'int', \&check_return_void);

=head2 enabled_force

Checks if force sandboxing is enabled.

=over 4

=item * Returns

Non-zero if force sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_force' => 'enabled_force'] => [] => 'bool', \&check_return_bool);

=head2 enable_tpe

Enables TPE sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_tpe' => 'enable_tpe'] => [] => 'int', \&check_return_void);

=head2 disable_tpe

Disables TPE sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_tpe' => 'disable_tpe'] => [] => 'int', \&check_return_void);

=head2 enabled_tpe

Checks if TPE sandboxing is enabled.

=over 4

=item * Returns

Non-zero if TPE sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_tpe' => 'enabled_tpe'] => [] => 'bool', \&check_return_bool);

=head2 default_stat

Set the default action for Stat sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_stat' => 'default_stat'] => ['uint8'] => 'int', \&check_default_action);

=head2 default_read

Set the default action for Read Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_read' => 'default_read'] => ['uint8'] => 'int', \&check_default_action);

=head2 default_write

Set the default action for Write sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_write' => 'default_write'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_exec

Set the default action for Exec sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_exec' => 'default_exec'] => ['uint8'] => 'int', \&check_default_action);

=head2 default_ioctl

Set the default action for _ioctl_(2) sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_ioctl' => 'default_ioctl'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_create

Set the default action for create Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_create' => 'default_create'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_delete

Set the default action for delete Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_delete' => 'default_delete'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_rename

Set the default action for rename Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_rename' => 'default_rename'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_symlink

Set the default action for symlink Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_symlink' => 'default_symlink'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_truncate

Set the default action for truncate sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_truncate' => 'default_truncate'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_chdir

Set the default action for chdir sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_chdir' => 'default_chdir'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_readdir

Set the default action for readdir sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_readdir' => 'default_readdir'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_mkdir

Set the default action for mkdir sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_mkdir' => 'default_mkdir'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_chown

Set the default action for chown Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_chown' => 'default_chown'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_chgrp

Set the default action for chgrp Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_chgrp' => 'default_chgrp'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_chmod

Set the default action for chmod Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_chmod' => 'default_chmod'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_chattr

Set the default action for chattr Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_chattr' => 'default_chattr'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_chroot

Set the default action for chroot Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_chroot' => 'default_chroot'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_utime

Set the default action for utime Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_utime' => 'default_utime'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_mkdev

Set the default action for mkdev Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_mkdev' => 'default_mkdev'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_mkfifo

Set the default action for mkfifo Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_mkfifo' => 'default_mkfifo'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_mktemp

Set the default action for mktemp Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_mktemp' => 'default_mktemp'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_net

Set the default action for Network Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_net' => 'default_net'] => ['uint8'] => 'int', \&check_default_action);

=head2 default_block

Set the default action for block Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_block' => 'default_block'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_mem

Set the default action for Memory Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_mem' => 'default_mem'] => ['uint8'] => 'int', \&check_default_action);

=head2 default_pid

Set the default action for PID Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_pid' => 'default_pid'] => ['uint8'] => 'int', \&check_default_action);

=head2 default_force

Set the default action for Force Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_force' => 'default_force'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_segvguard

Set the default action for SegvGuard.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_default_segvguard' => 'default_segvguard'] => ['uint8'] => 'int',
	\&check_default_action
);

=head2 default_tpe

Set the default action for TPE Sandboxing.

=over 4

=item - action

The default action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_default_tpe' => 'default_tpe'] => ['uint8'] => 'int', \&check_default_action);

=head2 stat_add

Adds to the given actionlist of Stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_stat_add' => 'stat_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 stat_del

Removes the first instance from the end of the given actionlist of Stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_stat_del' => 'stat_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 stat_rem

Removes all matching patterns from the given actionlist of Stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_stat_rem' => 'stat_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 read_add

Adds to the given actionlist of Read sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_read_add' => 'read_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 read_del

Removes the first instance from the end of the given actionlist of Read sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_read_del' => 'read_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 read_rem

Removes all matching patterns from the given actionlist of Read sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_read_rem' => 'read_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 write_add

Adds to the given actionlist of Write sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_write_add' => 'write_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 write_del

Removes the first instance from the end of the given actionlist of Write sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_write_del' => 'write_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 write_rem

Removes all matching patterns from the given actionlist of Write sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_write_rem' => 'write_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 exec_add

Adds to the given actionlist of Exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_exec_add' => 'exec_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 exec_del

Removes the first instance from the end of the given actionlist of Exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_exec_del' => 'exec_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 exec_rem

Removes all matching patterns from the given actionlist of Exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_exec_rem' => 'exec_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 ioctl_add

Adds to the given actionlist of _ioctl_(2) sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_ioctl_add' => 'ioctl_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 ioctl_del

Removes the first instance from the end of the given actionlist of _ioctl_(2) sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_ioctl_del' => 'ioctl_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 ioctl_rem

Removes all matching patterns from the given actionlist of _ioctl_(2) sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_ioctl_rem' => 'ioctl_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 create_add

Adds to the given actionlist of create sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_create_add' => 'create_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 create_del

Removes the first instance from the end of the given actionlist of create sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_create_del' => 'create_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 create_rem

Removes all matching patterns from the given actionlist of create sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_create_rem' => 'create_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 delete_add

Adds to the given actionlist of delete sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_delete_add' => 'delete_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 delete_del

Removes the first instance from the end of the given actionlist of delete sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_delete_del' => 'delete_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 delete_rem

Removes all matching patterns from the given actionlist of delete sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_delete_rem' => 'delete_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 rename_add

Adds to the given actionlist of rename sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_rename_add' => 'rename_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 rename_del

Removes the first instance from the end of the given actionlist of rename sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_rename_del' => 'rename_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 rename_rem

Removes all matching patterns from the given actionlist of rename sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_rename_rem' => 'rename_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 symlink_add

Adds to the given actionlist of symlink sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_symlink_add' => 'symlink_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 symlink_del

Removes the first instance from the end of the given actionlist of symlink sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_symlink_del' => 'symlink_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 symlink_rem

Removes all matching patterns from the given actionlist of symlink sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_symlink_rem' => 'symlink_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 truncate_add

Adds to the given actionlist of truncate sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_truncate_add' => 'truncate_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 truncate_del

Removes the first instance from the end of the given actionlist of truncate sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_truncate_del' => 'truncate_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 truncate_rem

Removes all matching patterns from the given actionlist of truncate sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_truncate_rem' => 'truncate_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chdir_add

Adds to the given actionlist of chdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chdir_add' => 'chdir_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chdir_del

Removes the first instance from the end of the given actionlist of chdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chdir_del' => 'chdir_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chdir_rem

Removes all matching patterns from the given actionlist of chdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chdir_rem' => 'chdir_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 readdir_add

Adds to the given actionlist of readdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_readdir_add' => 'readdir_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 readdir_del

Removes the first instance from the end of the given actionlist of readdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_readdir_del' => 'readdir_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 readdir_rem

Removes all matching patterns from the given actionlist of readdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_readdir_rem' => 'readdir_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mkdir_add

Adds to the given actionlist of mkdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mkdir_add' => 'mkdir_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mkdir_del

Removes the first instance from the end of the given actionlist of mkdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mkdir_del' => 'mkdir_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mkdir_rem

Removes all matching patterns from the given actionlist of mkdir sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mkdir_rem' => 'mkdir_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chown_add

Adds to the given actionlist of chown sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chown_add' => 'chown_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chown_del

Removes the first instance from the end of the given actionlist of chown sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chown_del' => 'chown_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chown_rem

Removes all matching patterns from the given actionlist of chown sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chown_rem' => 'chown_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chgrp_add

Adds to the given actionlist of chgrp sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chgrp_add' => 'chgrp_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chgrp_del

Removes the first instance from the end of the given actionlist of chgrp sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chgrp_del' => 'chgrp_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chgrp_rem

Removes all matching patterns from the given actionlist of chgrp sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chgrp_rem' => 'chgrp_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chmod_add

Adds to the given actionlist of chmod sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chmod_add' => 'chmod_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chmod_del

Removes the first instance from the end of the given actionlist of chmod sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chmod_del' => 'chmod_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chmod_rem

Removes all matching patterns from the given actionlist of chmod sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_chmod_rem' => 'chmod_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 chattr_add

Adds to the given actionlist of chattr sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chattr_add' => 'chattr_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chattr_del

Removes the first instance from the end of the given actionlist of chattr sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chattr_del' => 'chattr_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chattr_rem

Removes all matching patterns from the given actionlist of chattr sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chattr_rem' => 'chattr_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chroot_add

Adds to the given actionlist of chroot sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chroot_add' => 'chroot_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chroot_del

Removes the first instance from the end of the given actionlist of chroot sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chroot_del' => 'chroot_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 chroot_rem

Removes all matching patterns from the given actionlist of chroot sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_chroot_rem' => 'chroot_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 utime_add

Adds to the given actionlist of utime sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_utime_add' => 'utime_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 utime_del

Removes the first instance from the end of the given actionlist of utime sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_utime_del' => 'utime_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 utime_rem

Removes all matching patterns from the given actionlist of utime sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_utime_rem' => 'utime_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 mkdev_add

Adds to the given actionlist of mkdev sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mkdev_add' => 'mkdev_add'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 mkdev_del

Removes the first instance from the end of the given actionlist of mkdev sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mkdev_del' => 'mkdev_del'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 mkdev_rem

Removes all matching patterns from the given actionlist of mkdev sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mkdev_rem' => 'mkdev_rem'] => ['uint8', 'string'] => 'int', \&check_action_glob);

=head2 mkfifo_add

Adds to the given actionlist of mkfifo sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mkfifo_add' => 'mkfifo_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mkfifo_del

Removes the first instance from the end of the given actionlist of mkfifo sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mkfifo_del' => 'mkfifo_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mkfifo_rem

Removes all matching patterns from the given actionlist of mkfifo sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mkfifo_rem' => 'mkfifo_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mktemp_add

Adds to the given actionlist of mktemp sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mktemp_add' => 'mktemp_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mktemp_del

Removes the first instance from the end of the given actionlist of mktemp sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mktemp_del' => 'mktemp_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 mktemp_rem

Removes all matching patterns from the given actionlist of mktemp sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_mktemp_rem' => 'mktemp_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_bind_add

Adds to the given actionlist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_bind_add' => 'net_bind_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_bind_del

Removes the first instance from the end of the given actionlist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_bind_del' => 'net_bind_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_bind_rem

Removes all matching patterns from the given actionlist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_bind_rem' => 'net_bind_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_connect_add

Adds to the given actionlist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_connect_add' => 'net_connect_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_connect_del

Removes the first instance from the end of the given actionlist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_connect_del' => 'net_connect_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_connect_rem

Removes all matching patterns from the given actionlist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_connect_rem' => 'net_connect_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_sendfd_add

Adds to the given actionlist of net/sendfd sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_sendfd_add' => 'net_sendfd_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_sendfd_del

Removes the first instance from the end of the given actionlist of net/sendfd sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_sendfd_del' => 'net_sendfd_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_sendfd_rem

Removes all matching patterns from the given actionlist of net/sendfd sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_sendfd_rem' => 'net_sendfd_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_link_add

Adds to the given actionlist of net/link sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_link_add' => 'net_link_add'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_link_del

Removes the first instance from the end of the given actionlist of net/link sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_link_del' => 'net_link_del'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 net_link_rem

Removes all matching patterns from the given actionlist of net/link sandboxing.

=over 4

=item * Parameters

=over 4

=item - action

The action, possible values are:

=over 4

=item * ACTION_ALLOW

Allow system call.

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_FILTER

Deny system call silently.

=item * ACTION_DENY

Deny system call and warn.

=item * ACTION_PANIC

Deny system call, warn and panic the current Syd thread.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=item * ACTION_EXIT

Warn, and exit Syd immediately with deny errno as exit value.

=back

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_net_link_rem' => 'net_link_rem'] => ['uint8', 'string'] => 'int',
	\&check_action_glob
);

=head2 force_add

Adds an entry to the Integrity Force map for Force Sandboxing.

=over 4

=item * Parameters

=over 4

=item - path (string)

Fully-qualified file name as string.

=item - hash (string)

Checksum as hexadecimal encoded string. This is either a 32-character MD5
checksum, a 40-character SHA1 checksum, a 64-character SHA3-256 checksum,
a 96-character SHA3-384 checksum or a 128-character SHA3-512 checksum.

=item - action

The action, possible values are:

=over 4

=item * ACTION_WARN

Allow system call and warn.

=item * ACTION_STOP

Deny system call, warn and stop offending process.

=item * ACTION_KILL

Deny system call, warn and kill offending process.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_force_add' => 'force_add'] => ['string', 'string', 'uint8'] => 'int',
	sub {
		my ($func, $arg0, $arg1, $action) = @_;

		validate_action($action);
		my $r = $func->($arg0, $arg1, $action);
		if ($r == 0) {
			return 1;
		} elsif ($r < 0) {
			$! = -$r;
			croak "libsyd error: $!";
		}
		return $r;
	}
);

=head2 force_del

Removes an entry from the Integrity Force map for Force Sandboxing.

=over 4

=item * Parameters

=over 4

=item - path (string)

Fully-qualified file name as string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_force_del' => 'force_del'] => ['string'] => 'int', \&check_return_1);

=head2 force_clr

Clears the Integrity Force map for Force Sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_force_clr' => 'force_clr'] => [] => 'int', \&check_return_void);

=head2 segvguard_expiry

Specify SegvGuard entry expiry timeout in seconds.
Setting this timeout to 0 effectively disables SegvGuard.

=over 4

=item * Parameters

=over 4

=item - timeout (unsigned int)

Expiry timeout in seconds, must be greater than or equal to zero.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_segvguard_expiry' => 'segvguard_expiry'] => ['uint64'] => 'int',
	\&check_return_1
);

=head2 segvguard_suspension

Specify SegvGuard entry suspension timeout in seconds.

=over 4

=item * Parameters

=over 4

=item - timeout (unsigned int)

Suspension timeout in seconds, must be greater than or equal to zero.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_segvguard_suspension' => 'segvguard_suspension'] => ['uint64'] => 'int',
	\&check_return_1
);

=head2 segvguard_maxcrashes

Specify SegvGuard max number of crashes before suspension.

=over 4

=item * Parameters

=over 4

=item - limit (uint8)

Limit as u8, must be greater than or equal to zero.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_segvguard_maxcrashes' => 'segvguard_maxcrashes'] => ['uint8'] => 'int',
	\&check_return_1
);

=head2 ioctl_deny

Adds a request to the _ioctl_(2) denylist.

=over 4

=item * Parameters

=over 4

=item - request (integer)

_ioctl_(2) request to denylist.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_ioctl_deny' => 'ioctl_deny'] => ['uint64'] => 'int', \&check_return_1);

=head2 mem_max

Set syd maximum per-process memory usage limit for memory sandboxing.
The parse-size crate is used to parse the value so formatted strings are OK.

=over 4

=item * Parameters

=over 4

=item - size (string)

Limit size as a string, which can be formatted (e.g., '10MB', '512KB').

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mem_max' => 'mem_max'] => ['string'] => 'int', \&check_return_1);

=head2 mem_vm_max

Set syd maximum per-process virtual memory usage limit for memory sandboxing,
The parse-size crate is used to parse the value so formatted strings are OK.

=over 4

=item * Parameters

=over 4

=item - size (string)

Limit size as a string, which can be formatted (e.g., '10MB', '512KB').

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mem_vm_max' => 'mem_vm_max'] => ['string'] => 'int', \&check_return_1);

=head2 pid_max

Set syd maximum process ID limit for PID sandboxing.

=over 4

=item * Parameters

=over 4

=item - size (unsigned int)

Limit size, must be greater than or equal to zero.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_pid_max' => 'pid_max'] => ['uint64'] => 'int', \&check_return_1);

sub check_action_glob {
	my ($func, $action, $glob) = @_;

	validate_action($action);
	my $r = $func->($action, $glob);
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

sub check_default_action {
	my ($func, $action) = @_;

	validate_action($action);
	my $r = $func->($action);
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

sub validate_action {
	my $action = shift;

	# Validate action.
	unless (defined $action
		&& looks_like_number($action)
		&& int($action) == $action
		&& $action >= ACTION_ALLOW
		&& $action <= ACTION_EXIT) {
		$! = EINVAL;
		croak "libsyd error: Invalid argument - $!";
	}
}

sub check_return_void {
	my ($func) = @_;
	my $r = $func->();
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

sub check_return_bool {
	my ($func) = @_;
	return $func->() != 0;
}

sub check_return_1 {
	my ($func, $arg) = @_;
	my $r = $func->($arg);
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

sub check_return_3 {
	my ($func, $arg0, $arg1, $arg2) = @_;
	my $r = $func->($arg0, $arg1, $arg2);
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

1;    # Return true to indicate successful module loading
