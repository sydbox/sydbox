#!/usr/bin/env python3
# coding: utf-8
#
# Syd: rock-solid application kernel
# lib/src/syd.py: Python ctypes bindings of libsyd, the syd API C Library
# Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0

import os, sys, time
import enum, errno, ctypes
import json, tempfile, unittest

from typing import List, Union

"""
pysyd - Python Bindings for the syd API Rust Library

`pysyd` provides Python bindings for `libsyd`, a C library written in
Rust that implements the syd stat API. This package facilitates
interaction with the `/dev/syd` interface of syd, allowing for
runtime configuration and interaction within the syd sandboxing
environment.

Overview
--------
The `pysyd` library is designed to interact with the syd sandboxing
environment through Python. It offers functionalities to check and
modify the state of the sandbox lock, perform system calls to
`/dev/syd`, and execute commands within the sandbox. This makes it
easier for Python applications to integrate with syd's features.

Requirement
-----------
To use `pysyd`, the shared library `libsyd.so` must be available in the
system's library search path. Ensure that this shared library is
properly installed and its location is included in the environment path
where system libraries are searched for.

Attention
---------
This library is currently a work in progress. The API is subject to
change and may not be stable. Users are advised to use it with caution
and to stay updated with the latest changes.

Further Information
--------------------
For more detailed information about `libsyd` and usage instructions,
refer to the syd manual: https://git.sr.ht/~alip/syd

Author
------
Ali Polatel (alip@chesswob.org)

This Python wrapper is designed to provide a seamless and idiomatic
Python interface for interacting with the functionalities offered by
`libsyd`.
"""

__all__ = (
    "Action",
    "LockState",
    "info",
    "check",
    "api",
    "panic",
    "reset",
    "load",
    "lock",
    "exec",
    "enable_stat",
    "disable_stat",
    "enabled_stat",
    "enable_read",
    "disable_read",
    "enabled_read",
    "enable_write",
    "disable_write",
    "enabled_write",
    "enable_exec",
    "disable_exec",
    "enabled_exec",
    "enable_ioctl",
    "disable_ioctl",
    "enabled_ioctl",
    "enable_create",
    "disable_create",
    "enabled_create",
    "enable_delete",
    "disable_delete",
    "enabled_delete",
    "enable_rename",
    "disable_rename",
    "enabled_rename",
    "enable_symlink",
    "disable_symlink",
    "enabled_symlink",
    "enable_truncate",
    "disable_truncate",
    "enabled_truncate",
    "enable_chdir",
    "disable_chdir",
    "enabled_chdir",
    "enable_readdir",
    "disable_readdir",
    "enabled_readdir",
    "enable_mkdir",
    "disable_mkdir",
    "enabled_mkdir",
    "enable_chown",
    "disable_chown",
    "enabled_chown",
    "enable_chgrp",
    "disable_chgrp",
    "enabled_chgrp",
    "enable_chattr",
    "disable_chattr",
    "enabled_chattr",
    "enable_chroot",
    "disable_chroot",
    "enabled_chroot",
    "enable_utime",
    "disable_utime",
    "enabled_utime",
    "enable_mkdev",
    "disable_mkdev",
    "enabled_mkdev",
    "enable_mkfifo",
    "disable_mkfifo",
    "enabled_mkfifo",
    "enable_mktemp",
    "disable_mktemp",
    "enabled_mktemp",
    "enable_net",
    "disable_net",
    "enabled_net",
    "enabled_lock",
    "enabled_crypt",
    "enabled_proxy",
    "enable_mem",
    "disable_mem",
    "enabled_mem",
    "enable_pid",
    "disable_pid",
    "enabled_pid",
    "enable_force",
    "disable_force",
    "enabled_force",
    "enable_tpe",
    "disable_tpe",
    "enabled_tpe",
    "default_stat",
    "default_read",
    "default_write",
    "default_exec",
    "default_ioctl",
    "default_create",
    "default_delete",
    "default_rename",
    "default_symlink",
    "default_truncate",
    "default_chdir",
    "default_readdir",
    "default_mkdir",
    "default_chown",
    "default_chgrp",
    "default_chmod",
    "default_chattr",
    "default_chroot",
    "default_utime",
    "default_mkdev",
    "default_mkfifo",
    "default_mktemp",
    "default_net",
    "default_block",
    "default_mem",
    "default_pid",
    "default_force",
    "default_segvguard",
    "default_tpe",
    "ioctl_deny",
    "stat_add",
    "stat_del",
    "stat_rem",
    "read_add",
    "read_del",
    "read_rem",
    "write_add",
    "write_del",
    "write_rem",
    "exec_add",
    "exec_del",
    "exec_rem",
    "ioctl_add",
    "ioctl_del",
    "ioctl_rem",
    "create_add",
    "create_del",
    "create_rem",
    "delete_add",
    "delete_del",
    "delete_rem",
    "rename_add",
    "rename_del",
    "rename_rem",
    "symlink_add",
    "symlink_del",
    "symlink_rem",
    "truncate_add",
    "truncate_del",
    "truncate_rem",
    "chdir_add",
    "chdir_del",
    "chdir_rem",
    "readdir_add",
    "readdir_del",
    "readdir_rem",
    "mkdir_add",
    "mkdir_del",
    "mkdir_rem",
    "chown_add",
    "chown_del",
    "chown_rem",
    "chgrp_add",
    "chgrp_del",
    "chgrp_rem",
    "chmod_add",
    "chmod_del",
    "chmod_rem",
    "chattr_add",
    "chattr_del",
    "chattr_rem",
    "chroot_add",
    "chroot_del",
    "chroot_rem",
    "utime_add",
    "utime_del",
    "utime_rem",
    "mkdev_add",
    "mkdev_del",
    "mkdev_rem",
    "mkfifo_add",
    "mkfifo_del",
    "mkfifo_rem",
    "mktemp_add",
    "mktemp_del",
    "mktemp_rem",
    "net_bind_add",
    "net_bind_del",
    "net_bind_rem",
    "net_connect_add",
    "net_connect_del",
    "net_connect_rem",
    "net_sendfd_add",
    "net_sendfd_del",
    "net_sendfd_rem",
    "net_link_add",
    "net_link_del",
    "net_link_rem",
    "force_add",
    "force_del",
    "force_clr",
    "mem_max",
    "mem_vm_max",
    "pid_max",
    "segvguard_expiry",
    "segvguard_suspension",
    "segvguard_maxcrashes",
)

try:
    libsyd = ctypes.CDLL("libsyd.so")
except OSError as error:
    if error.errno == errno.ENOENT or "such file" in str(error):
        raise ImportError(f"install libsyd.so: {error}")
    raise ImportError(f"fix libsyd.so: {error}")
except error:
    raise ImportError(f"fix libsyd.so: {error}")


@enum.unique
class Action(enum.Enum):
    """
    Enum for representing actions for sandboxing:

    - ALLOW: Allow system call.
    - WARN: Allow system call and warn.
    - FILTER: Deny system call silently.
    - DENY: Deny system call and warn.
    - PANIC: Deny system call, warn and panic the current Syd thread.
    - STOP: Deny system call, warn and stop offending process.
    - KILL: Deny system call, warn and kill offending process.
    - EXIT: Warn, and exit Syd immediately with deny errno as exit value.
    """

    ACTION_ALLOW = 0
    ACTION_WARN = 1
    ACTION_FILTER = 2
    ACTION_DENY = 3
    ACTION_PANIC = 4
    ACTION_STOP = 5
    ACTION_KILL = 6
    ACTION_EXIT = 7


@enum.unique
class LockState(enum.Enum):
    """
    Enum for representing the sandbox lock states:

    - LOCK_OFF: The sandbox lock is off, allowing all sandbox commands.
    - LOCK_EXEC: The sandbox lock is set to on for all processes except
      the initial process (syd exec child). This is the default state.
    - LOCK_ON: The sandbox lock is on, disallowing all sandbox commands.
    """

    LOCK_OFF = 0
    LOCK_EXEC = 1
    LOCK_ON = 2


def check_return(negated_errno: int) -> bool:
    """
    Checks the returned negated errno from syd_kill and raises an OSError if it's an error code.

    # Parameters
    negated_errno (int): The negated errno returned by the syd_kill function.

    # Raises
    OSError: If the negated_errno is a non-zero error code.
    """
    if negated_errno != 0:
        # Convert the negated errno back to the original errno
        errno = -negated_errno
        raise OSError(errno, os.strerror(errno))
    return True


def info() -> dict:
    """
    Reads the state of the syd sandbox from /dev/syd and returns it
    as a JSON object.

    This function opens the special file /dev/syd, which contains the
    current state of the syd sandbox in JSON format, and then parses
    and returns this state as a Python dictionary.

    # Returns
    dict: The current state of the syd sandbox.

    # Raises
    - OSError: If the file /dev/syd cannot be opened.
    - JSONDecodeError: If the content of /dev/syd is not valid JSON.
    """
    with open("/dev/syd") as f:
        return json.load(f)


def api() -> int:
    """
    Performs a syd API check
    The caller is advised to perform this check before
    calling any other syd API calls.

    # Returns
    int: API number on successful operation, or raises an OSError on failure.
    """
    api = libsyd.syd_api()
    if api < 0:
        raise OSError(-api, os.strerror(-api))
    return api


def check() -> bool:
    """
    Performs an lstat system call on the file "/dev/syd".

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_check())


def panic() -> bool:
    """
    Causes syd to exit immediately with code 127.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_panic())


def reset() -> bool:
    """
    Causes syd to reset sandboxing to the default state.
    Allowlists, denylists and filters are going to be cleared.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_reset())


def load(fd: int) -> bool:
    """
    Causes syd to read configuration from the given file descriptor.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_load(fd))


def lock(state: LockState) -> bool:
    """
    Sets the state of the sandbox lock.

    # Parameters
    - state (LockState): The desired state of the sandbox lock, as
      defined by the `LockState` enum.

    # Returns
    - bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_lock(state.value))


def exec(file: bytes, argv: List[bytes]) -> bool:
    """
    Execute a command outside the sandbox without sandboxing

    # Parameters
    - file (bytes): The file path of the command to be executed, as bytes.
    - argv (List[bytes]): The arguments to the command, as a list of bytes.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    argv_array = (
        ctypes.c_char_p * (len(argv) + 1)
    )()  # Array of strings, null-terminated
    argv_array[:-1] = [arg for arg in argv]
    argv_array[-1] = None  # Null-terminate the array

    return check_return(libsyd.syd_exec(file, argv_array))


def enable_stat() -> bool:
    """
    Enables stat sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_stat())


def disable_stat() -> bool:
    """
    Disables stat sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_stat())


def enabled_stat() -> bool:
    """
    Checks if stat sandboxing is enabled.

    # Returns
    bool: True if stat sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_stat()


def enable_read() -> bool:
    """
    Enables read sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_read())


def disable_read() -> bool:
    """
    Disables read sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_read())


def enabled_read() -> bool:
    """
    Checks if read sandboxing is enabled.

    # Returns
    bool: True if read sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_read()


def enable_write() -> bool:
    """
    Enables write sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_write())


def disable_write() -> bool:
    """
    Disables write sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_write())


def enabled_write() -> bool:
    """
    Checks if write sandboxing is enabled.

    # Returns
    bool: True if write sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_write()


def enable_exec() -> bool:
    """
    Enables exec sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_exec())


def disable_exec() -> bool:
    """
    Disables exec sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_exec())


def enabled_exec() -> bool:
    """
    Checks if exec sandboxing is enabled.

    # Returns
    bool: True if exec sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_exec()


def enable_ioctl() -> bool:
    """
    Enables ioctl sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_ioctl())


def disable_ioctl() -> bool:
    """
    Disables ioctl sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_ioctl())


def enabled_ioctl() -> bool:
    """
    Checks if ioctl sandboxing is enabled.

    # Returns
    bool: True if ioctl sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_ioctl()


def enable_create() -> bool:
    """
    Enables create sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_create())


def disable_create() -> bool:
    """
    Disables create sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_create())


def enabled_create() -> bool:
    """
    Checks if create sandboxing is enabled.

    # Returns
    bool: True if create sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_create()


def enable_delete() -> bool:
    """
    Enables delete sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_delete())


def disable_delete() -> bool:
    """
    Disables delete sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_delete())


def enabled_delete() -> bool:
    """
    Checks if delete sandboxing is enabled.

    # Returns
    bool: True if delete sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_delete()


def enable_rename() -> bool:
    """
    Enables rename sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_rename())


def disable_rename() -> bool:
    """
    Disables rename sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_rename())


def enabled_rename() -> bool:
    """
    Checks if rename sandboxing is enabled.

    # Returns
    bool: True if rename sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_rename()


def enable_symlink() -> bool:
    """
    Enables symlink sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_symlink())


def disable_symlink() -> bool:
    """
    Disables symlink sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_symlink())


def enabled_symlink() -> bool:
    """
    Checks if symlink sandboxing is enabled.

    # Returns
    bool: True if symlink sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_symlink()


def enable_truncate() -> bool:
    """
    Enables truncate sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_truncate())


def disable_truncate() -> bool:
    """
    Disables truncate sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_truncate())


def enabled_truncate() -> bool:
    """
    Checks if truncate sandboxing is enabled.

    # Returns
    bool: True if truncate sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_truncate()


def enable_chdir() -> bool:
    """
    Enables chdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_chdir())


def disable_chdir() -> bool:
    """
    Disables chdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_chdir())


def enabled_chdir() -> bool:
    """
    Checks if chdir sandboxing is enabled.

    # Returns
    bool: True if chdir sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_chdir()


def enable_readdir() -> bool:
    """
    Enables readdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_readdir())


def disable_readdir() -> bool:
    """
    Disables readdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_readdir())


def enabled_readdir() -> bool:
    """
    Checks if readdir sandboxing is enabled.

    # Returns
    bool: True if readdir sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_readdir()


def enable_mkdir() -> bool:
    """
    Enables mkdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_mkdir())


def disable_mkdir() -> bool:
    """
    Disables mkdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_mkdir())


def enabled_mkdir() -> bool:
    """
    Checks if mkdir sandboxing is enabled.

    # Returns
    bool: True if mkdir sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_mkdir()


def enable_chown() -> bool:
    """
    Enables chown sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_chown())


def disable_chown() -> bool:
    """
    Disables chown sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_chown())


def enabled_chown() -> bool:
    """
    Checks if chown sandboxing is enabled.

    # Returns
    bool: True if chown sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_chown()


def enable_chgrp() -> bool:
    """
    Enables chgrp sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_chgrp())


def disable_chgrp() -> bool:
    """
    Disables chgrp sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_chgrp())


def enabled_chgrp() -> bool:
    """
    Checks if chgrp sandboxing is enabled.

    # Returns
    bool: True if chgrp sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_chgrp()


def enable_chmod() -> bool:
    """
    Enables chmod sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_chmod())


def disable_chmod() -> bool:
    """
    Disables chmod sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_chmod())


def enabled_chmod() -> bool:
    """
    Checks if chmod sandboxing is enabled.

    # Returns
    bool: True if chmod sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_chmod()


def enable_chattr() -> bool:
    """
    Enables chattr sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_chattr())


def disable_chattr() -> bool:
    """
    Disables chattr sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_chattr())


def enabled_chattr() -> bool:
    """
    Checks if chattr sandboxing is enabled.

    # Returns
    bool: True if chattr sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_chattr()


def enable_chroot() -> bool:
    """
    Enables chroot sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_chroot())


def disable_chroot() -> bool:
    """
    Disables chroot sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_chroot())


def enabled_chroot() -> bool:
    """
    Checks if chroot sandboxing is enabled.

    # Returns
    bool: True if chroot sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_chroot()


def enable_utime() -> bool:
    """
    Enables utime sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_utime())


def disable_utime() -> bool:
    """
    Disables utime sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_utime())


def enabled_utime() -> bool:
    """
    Checks if utime sandboxing is enabled.

    # Returns
    bool: True if utime sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_utime()


def enable_mkdev() -> bool:
    """
    Enables mkdev sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_mkdev())


def disable_mkdev() -> bool:
    """
    Disables mkdev sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_mkdev())


def enabled_mkdev() -> bool:
    """
    Checks if mkdev sandboxing is enabled.

    # Returns
    bool: True if mkdev sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_mkdev()


def enable_mkfifo() -> bool:
    """
    Enables mkfifo sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_mkfifo())


def disable_mkfifo() -> bool:
    """
    Disables mkfifo sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_mkfifo())


def enabled_mkfifo() -> bool:
    """
    Checks if mkfifo sandboxing is enabled.

    # Returns
    bool: True if mkfifo sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_mkfifo()


def enable_mktemp() -> bool:
    """
    Enables mktemp sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_mktemp())


def disable_mktemp() -> bool:
    """
    Disables mktemp sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_mktemp())


def enabled_mktemp() -> bool:
    """
    Checks if mktemp sandboxing is enabled.

    # Returns
    bool: True if mktemp sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_mktemp()


def enable_net() -> bool:
    """
    Enables net sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_net())


def disable_net() -> bool:
    """
    Disables net sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_net())


def enabled_net() -> bool:
    """
    Checks if net sandboxing is enabled.

    # Returns
    bool: True if net sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_net()


def enabled_lock() -> bool:
    """
    Checks if lock sandboxing is enabled.

    # Returns
    bool: True if lock sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_lock()


def enabled_crypt() -> bool:
    """
    Checks if crypt sandboxing is enabled.

    # Returns
    bool: True if crypt sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_crypt()


def enabled_proxy() -> bool:
    """
    Checks if Proxy sandboxing is enabled.

    # Returns
    bool: True if Proxy andboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_proxy()


def enable_mem() -> bool:
    """
    Enables memory sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_mem())


def disable_mem() -> bool:
    """
    Disables memory sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_mem())


def enabled_mem() -> bool:
    """
    Checks if memory sandboxing is enabled.

    # Returns
    bool: True if memory sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_mem()


def enable_pid() -> bool:
    """
    Enables PID sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_pid())


def disable_pid() -> bool:
    """
    Disables PID sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_pid())


def enabled_pid() -> bool:
    """
    Checks if PID sandboxing is enabled.

    # Returns
    bool: True if PID sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_pid()


def enable_force() -> bool:
    """
    Enables force sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_force())


def disable_force() -> bool:
    """
    Disables force sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_force())


def enabled_force() -> bool:
    """
    Checks if force sandboxing is enabled.

    # Returns
    bool: True if force sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_force()


def enable_tpe() -> bool:
    """
    Enables TPE sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_tpe())


def disable_tpe() -> bool:
    """
    Disables TPE sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_tpe())


def enabled_tpe() -> bool:
    """
    Checks if TPE sandboxing is enabled.

    # Returns
    bool: True if TPE sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_tpe()


def default_stat(action: Action) -> bool:
    """
    Set default action for Stat sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_stat(action.value))


def default_read(action: Action) -> bool:
    """
    Set default action for Read sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_read(action.value))


def default_write(action: Action) -> bool:
    """
    Set default action for Write sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_write(action.value))


def default_exec(action: Action) -> bool:
    """
    Set default action for Exec sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_exec(action.value))


def default_ioctl(action: Action) -> bool:
    """
    Set default action for Ioctl sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_ioctl(action.value))


def default_create(action: Action) -> bool:
    """
    Set default action for create sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_create(action.value))


def default_delete(action: Action) -> bool:
    """
    Set default action for delete sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_delete(action.value))


def default_rename(action: Action) -> bool:
    """
    Set default action for rename sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_rename(action.value))


def default_symlink(action: Action) -> bool:
    """
    Set default action for symlink sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_symlink(action.value))


def default_truncate(action: Action) -> bool:
    """
    Set default action for truncate sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_truncate(action.value))


def default_chdir(action: Action) -> bool:
    """
    Set default action for chdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_chdir(action.value))


def default_readdir(action: Action) -> bool:
    """
    Set default action for readdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_readdir(action.value))


def default_mkdir(action: Action) -> bool:
    """
    Set default action for mkdir sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_mkdir(action.value))


def default_chown(action: Action) -> bool:
    """
    Set default action for chown sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_chown(action.value))


def default_chgrp(action: Action) -> bool:
    """
    Set default action for chgrp sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_chgrp(action.value))


def default_chmod(action: Action) -> bool:
    """
    Set default action for chmod sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_chmod(action.value))


def default_chattr(action: Action) -> bool:
    """
    Set default action for chattr sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_chattr(action.value))


def default_chroot(action: Action) -> bool:
    """
    Set default action for chroot sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_chroot(action.value))


def default_utime(action: Action) -> bool:
    """
    Set default action for utime sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_utime(action.value))


def default_mkdev(action: Action) -> bool:
    """
    Set default action for mkdev sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_mkdev(action.value))


def default_mkfifo(action: Action) -> bool:
    """
    Set default action for mkfifo sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_mkfifo(action.value))


def default_mktemp(action: Action) -> bool:
    """
    Set default action for mktemp sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_mktemp(action.value))


def default_net(action: Action) -> bool:
    """
    Set default action for Network sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_net(action.value))


def default_block(action: Action) -> bool:
    """
    Set default action for block sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_block(action.value))


def default_mem(action: Action) -> bool:
    """
    Set default action for Memory sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_mem(action.value))


def default_pid(action: Action) -> bool:
    """
    Set default action for PID sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_pid(action.value))


def default_force(action: Action) -> bool:
    """
    Set default action for Force sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_force(action.value))


def default_segvguard(action: Action) -> bool:
    """
    Set default action for SegvGuard.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_segvguard(action.value))


def default_tpe(action: Action) -> bool:
    """
    Set default action for TPE sandboxing.

    # Returns
    bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_default_tpe(action.value))


def ioctl_deny(request: int) -> bool:
    """
    Adds a request to the _ioctl_(2) denylist.

    # Parameters
    - request (int): _ioctl_(2) request

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_ioctl_deny(request))


def stat_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of stat sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_stat_add(action.value, glob))


def stat_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    stat sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_stat_del(action.value, glob))


def stat_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of stat sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_stat_rem(action.value, glob))


def read_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of read sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_read_add(action.value, glob))


def read_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    read sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_read_del(action.value, glob))


def read_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of read sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_read_rem(action.value, glob))


def write_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of write sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_write_add(action.value, glob))


def write_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    write sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_write_del(action.value, glob))


def write_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of write sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_write_rem(action.value, glob))


def exec_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of exec sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_exec_add(action.value, glob))


def exec_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    exec sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_exec_del(action.value, glob))


def exec_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of exec sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_exec_rem(action.value, glob))


def ioctl_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of ioctl sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_ioctl_add(action.value, glob))


def ioctl_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    ioctl sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_ioctl_del(action.value, glob))


def ioctl_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of ioctl sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_ioctl_rem(action.value, glob))


def create_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of create sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_create_add(action.value, glob))


def create_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    create sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_create_del(action.value, glob))


def create_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of create sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_create_rem(action.value, glob))


def delete_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of delete sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_delete_add(action.value, glob))


def delete_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    delete sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_delete_del(action.value, glob))


def delete_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of delete sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_delete_rem(action.value, glob))


def rename_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of rename sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_rename_add(action.value, glob))


def rename_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    rename sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_rename_del(action.value, glob))


def rename_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of rename sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_rename_rem(action.value, glob))


def symlink_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of symlink sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_symlink_add(action.value, glob))


def symlink_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    symlink sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_symlink_del(action.value, glob))


def symlink_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of symlink sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_symlink_rem(action.value, glob))


def truncate_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of truncate sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_truncate_add(action.value, glob))


def truncate_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    truncate sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_truncate_del(action.value, glob))


def truncate_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of truncate sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_truncate_rem(action.value, glob))


def chdir_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of chdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chdir_add(action.value, glob))


def chdir_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    chdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chdir_del(action.value, glob))


def chdir_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of chdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chdir_rem(action.value, glob))


def readdir_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of readdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_readdir_add(action.value, glob))


def readdir_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    readdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_readdir_del(action.value, glob))


def readdir_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of readdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_readdir_rem(action.value, glob))


def mkdir_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of mkdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkdir_add(action.value, glob))


def mkdir_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    mkdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkdir_del(action.value, glob))


def mkdir_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of mkdir sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkdir_rem(action.value, glob))


def chown_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of chown sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chown_add(action.value, glob))


def chown_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    chown sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chown_del(action.value, glob))


def chown_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of chown sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chown_rem(action.value, glob))


def chgrp_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of chgrp sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chgrp_add(action.value, glob))


def chgrp_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    chgrp sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chgrp_del(action.value, glob))


def chgrp_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of chgrp sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chgrp_rem(action.value, glob))


def chmod_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of chmod sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chmod_add(action.value, glob))


def chmod_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    chmod sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chmod_del(action.value, glob))


def chmod_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of chmod sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chmod_rem(action.value, glob))


def chattr_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of chattr sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chattr_add(action.value, glob))


def chattr_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    chattr sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chattr_del(action.value, glob))


def chattr_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of chattr sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chattr_rem(action.value, glob))


def chroot_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of chroot sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chroot_add(action.value, glob))


def chroot_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    chroot sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chroot_del(action.value, glob))


def chroot_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of chroot sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_chroot_rem(action.value, glob))


def utime_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of utime sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_utime_add(action.value, glob))


def utime_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    utime sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_utime_del(action.value, glob))


def utime_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of utime sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_utime_rem(action.value, glob))


def mkdev_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of mkdev sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkdev_add(action.value, glob))


def mkdev_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    mkdev sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkdev_del(action.value, glob))


def mkdev_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of mkdev sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkdev_rem(action.value, glob))


def mkfifo_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of mkfifo sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkfifo_add(action.value, glob))


def mkfifo_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    mkfifo sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkfifo_del(action.value, glob))


def mkfifo_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of mkfifo sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mkfifo_rem(action.value, glob))


def mktemp_add(action: Action, glob: bytes) -> bool:
    """
    Adds to the given actionlist of mktemp sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mktemp_add(action.value, glob))


def mktemp_del(action: Action, glob: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    mktemp sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mktemp_del(action.value, glob))


def mktemp_rem(action: Action, glob: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of mktemp sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - glob (bytes): Glob pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_mktemp_rem(action.value, glob))


def net_bind_add(action: Action, addr: bytes) -> bool:
    """
    Adds to the given actionlist of net_bind sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_bind_add(action.value, addr))


def net_bind_del(action: Action, addr: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    net/bind sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_bind_del(action.value, addr))


def net_bind_rem(action: Action, addr: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of net/bind
    sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_bind_rem(action.value, addr))


def net_connect_add(action: Action, addr: bytes) -> bool:
    """
    Adds to the given actionlist of net_connect sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_connect_add(action.value, addr))


def net_connect_del(action: Action, addr: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    net/connect sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_connect_del(action.value, addr))


def net_connect_rem(action: Action, addr: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of net/connect
    sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_connect_rem(action.value, addr))


def net_sendfd_add(action: Action, addr: bytes) -> bool:
    """
    Adds to the given actionlist of net_sendfd sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_sendfd_add(action.value, addr))


def net_sendfd_del(action: Action, addr: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    net/sendfd sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_sendfd_del(action.value, addr))


def net_sendfd_rem(action: Action, addr: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of net/sendfd
    sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_sendfd_rem(action.value, addr))


def net_link_add(action: Action, addr: bytes) -> bool:
    """
    Adds to the given actionlist of net_link sandboxing.

    # Parameters
    - action (Action): Sandbox action
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_link_add(action.value, addr))


def net_link_del(action: Action, addr: bytes) -> bool:
    """
    Removes the first instance from the end of the given actionlist of
    net/link sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_link_del(action.value, addr))


def net_link_rem(action: Action, addr: bytes) -> bool:
    """
    Removes all matching patterns from the given actionlist of net/link
    sandboxing.

    # Parameters
    - addr (bytes): Address pattern

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_net_link_rem(action.value, addr))


def force_add(path: bytes, hash: str, action: Action) -> bool:
    """
    Adds an entry to the Integrity Force map for Force Sandboxing.

    # Parameters
    - path (bytes): Fully-qualified file name.
    - hash (str): Hexadecimal encoded checksum:
       - 32-characters: MD5
       - 40-characters: SHA1
       - 64-characters: SHA3-256
       - 96-characters: SHA3-384
       - 128-characters: SHA3-512
    - action (Action): The action to take on checksum mismatch.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_force_add(path, hash.encode("utf-8"), action.value))


def force_del(path: bytes) -> bool:
    """
    Removes an entry from the Integrity Force map for Force Sandboxing.

    # Parameters
    - path (bytes): Fully-qualified file name.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_force_del(path))


def force_clr() -> bool:
    """
    Clears the Integrity Force map for Force Sandboxing.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_force_clr())


def mem_max(size: Union[int, str, bytes]) -> bool:
    """
    Set syd maximum per-process memory usage limit for memory sandboxing,
    parse-size crate is used to parse the value so formatted strings are OK.

    # Parameters
    - size (int|str|bytes): Limit size.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    if isinstance(size, int):
        size = str(size)
    if isinstance(size, str):
        size = size.encode("utf-8")
    return check_return(libsyd.syd_mem_max(size))


def mem_vm_max(size: Union[int, str, bytes]) -> bool:
    """
    Set syd maximum per-process virtual memory usage limit for memory sandboxing,
    parse-size crate is used to parse the value so formatted strings are OK.

    # Parameters
    - size (int|str|bytes): Limit size.

    # Returns
    bool: True on successful operation, or raises an OSError on failure.
    """
    if isinstance(size, int):
        size = str(size)
    if isinstance(size, str):
        size = size.encode("utf-8")
    return check_return(libsyd.syd_mem_vm_max(size))


def pid_max(size: int) -> bool:
    """
    Set syd maximum process id limit for PID sandboxing

    # Parameters
    - size (int): Limit size, must be greater than or equal to zero.

    # Returns
    bool: True on successful operation, or raises a ValueError or an OSError on failure.
    """
    if size < 0:
        raise ValueError("Invalid limit size")
    return check_return(libsyd.syd_pid_max(size))


def segvguard_expiry(timeout: int) -> bool:
    """
    Specify SegvGuard expiry timeout in seconds, must be greater than or equal to zero.
    Setting this timeout to 0 effectively disables SegvGuard.

    # Parameters
    - timeout (int): Expiry timeout in seconds, must be greater than or equal to zero.

    # Returns
    bool: True on successful operation, or raises a ValueError or an OSError on failure.
    """
    if timeout < 0:
        raise ValueError("Invalid timeout")
    return check_return(libsyd.syd_segvguard_expiry(timeout))


def segvguard_suspension(timeout: int) -> bool:
    """
    Specify SegvGuard entry suspension timeout in seconds.

    # Parameters
    - timeout (int): Suspension timeout in seconds, must be greater than or equal to zero.

    # Returns
    bool: True on successful operation, or raises a ValueError or an OSError on failure.
    """
    if timeout < 0:
        raise ValueError("Invalid timeout")
    return check_return(libsyd.syd_segvguard_suspension(timeout))


def segvguard_maxcrashes(limit: int) -> bool:
    """
    Specify SegvGuard max number of crashes before suspension.

    # Parameters
    - limit (int): Limit, must be greater than or equal to zero.

    # Returns
    bool: True on successful operation, or raises a ValueError or an OSError on failure.
    """
    if limit < 0:
        raise ValueError("Invalid maxcrashes limit")
    return check_return(libsyd.syd_segvguard_maxcrashes(limit))


###
# Ctypes Function Definitions
###
libsyd.syd_api.restype = ctypes.c_int
libsyd.syd_check.restype = ctypes.c_int
libsyd.syd_panic.restype = ctypes.c_int
libsyd.syd_reset.restype = ctypes.c_int

libsyd.syd_load.restype = ctypes.c_int
libsyd.syd_load.argtypes = [ctypes.c_int]

libsyd.syd_lock.restype = ctypes.c_int
libsyd.syd_lock.argtypes = [ctypes.c_uint]

libsyd.syd_exec.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_char_p)]
libsyd.syd_exec.restype = ctypes.c_int

libsyd.syd_enable_stat.restype = ctypes.c_int
libsyd.syd_disable_stat.restype = ctypes.c_int
libsyd.syd_enabled_stat.restype = ctypes.c_bool

libsyd.syd_enable_read.restype = ctypes.c_int
libsyd.syd_disable_read.restype = ctypes.c_int
libsyd.syd_enabled_read.restype = ctypes.c_bool

libsyd.syd_enable_write.restype = ctypes.c_int
libsyd.syd_disable_write.restype = ctypes.c_int
libsyd.syd_enabled_write.restype = ctypes.c_bool

libsyd.syd_enable_exec.restype = ctypes.c_int
libsyd.syd_disable_exec.restype = ctypes.c_int
libsyd.syd_enabled_exec.restype = ctypes.c_bool

libsyd.syd_enable_ioctl.restype = ctypes.c_int
libsyd.syd_disable_ioctl.restype = ctypes.c_int
libsyd.syd_enabled_ioctl.restype = ctypes.c_bool

libsyd.syd_enable_create.restype = ctypes.c_int
libsyd.syd_disable_create.restype = ctypes.c_int
libsyd.syd_enabled_create.restype = ctypes.c_bool

libsyd.syd_enable_delete.restype = ctypes.c_int
libsyd.syd_disable_delete.restype = ctypes.c_int
libsyd.syd_enabled_delete.restype = ctypes.c_bool

libsyd.syd_enable_rename.restype = ctypes.c_int
libsyd.syd_disable_rename.restype = ctypes.c_int
libsyd.syd_enabled_rename.restype = ctypes.c_bool

libsyd.syd_enable_symlink.restype = ctypes.c_int
libsyd.syd_disable_symlink.restype = ctypes.c_int
libsyd.syd_enabled_symlink.restype = ctypes.c_bool

libsyd.syd_enable_truncate.restype = ctypes.c_int
libsyd.syd_disable_truncate.restype = ctypes.c_int
libsyd.syd_enabled_truncate.restype = ctypes.c_bool

libsyd.syd_enable_chdir.restype = ctypes.c_int
libsyd.syd_disable_chdir.restype = ctypes.c_int
libsyd.syd_enabled_chdir.restype = ctypes.c_bool

libsyd.syd_enable_readdir.restype = ctypes.c_int
libsyd.syd_disable_readdir.restype = ctypes.c_int
libsyd.syd_enabled_readdir.restype = ctypes.c_bool

libsyd.syd_enable_mkdir.restype = ctypes.c_int
libsyd.syd_disable_mkdir.restype = ctypes.c_int
libsyd.syd_enabled_mkdir.restype = ctypes.c_bool

libsyd.syd_enable_chown.restype = ctypes.c_int
libsyd.syd_disable_chown.restype = ctypes.c_int
libsyd.syd_enabled_chown.restype = ctypes.c_bool

libsyd.syd_enable_chgrp.restype = ctypes.c_int
libsyd.syd_disable_chgrp.restype = ctypes.c_int
libsyd.syd_enabled_chgrp.restype = ctypes.c_bool

libsyd.syd_enable_chmod.restype = ctypes.c_int
libsyd.syd_disable_chmod.restype = ctypes.c_int
libsyd.syd_enabled_chmod.restype = ctypes.c_bool

libsyd.syd_enable_chattr.restype = ctypes.c_int
libsyd.syd_disable_chattr.restype = ctypes.c_int
libsyd.syd_enabled_chattr.restype = ctypes.c_bool

libsyd.syd_enable_chroot.restype = ctypes.c_int
libsyd.syd_disable_chroot.restype = ctypes.c_int
libsyd.syd_enabled_chroot.restype = ctypes.c_bool

libsyd.syd_enable_utime.restype = ctypes.c_int
libsyd.syd_disable_utime.restype = ctypes.c_int
libsyd.syd_enabled_utime.restype = ctypes.c_bool

libsyd.syd_enable_mkdev.restype = ctypes.c_int
libsyd.syd_disable_mkdev.restype = ctypes.c_int
libsyd.syd_enabled_mkdev.restype = ctypes.c_bool

libsyd.syd_enable_mkfifo.restype = ctypes.c_int
libsyd.syd_disable_mkfifo.restype = ctypes.c_int
libsyd.syd_enabled_mkfifo.restype = ctypes.c_bool

libsyd.syd_enable_mktemp.restype = ctypes.c_int
libsyd.syd_disable_mktemp.restype = ctypes.c_int
libsyd.syd_enabled_mktemp.restype = ctypes.c_bool

libsyd.syd_enable_net.restype = ctypes.c_int
libsyd.syd_disable_net.restype = ctypes.c_int
libsyd.syd_enabled_net.restype = ctypes.c_bool

libsyd.syd_enabled_lock.restype = ctypes.c_bool
libsyd.syd_enabled_crypt.restype = ctypes.c_bool
libsyd.syd_enabled_proxy.restype = ctypes.c_bool

libsyd.syd_enable_mem.restype = ctypes.c_int
libsyd.syd_disable_mem.restype = ctypes.c_int
libsyd.syd_enabled_mem.restype = ctypes.c_bool

libsyd.syd_enable_pid.restype = ctypes.c_int
libsyd.syd_disable_pid.restype = ctypes.c_int
libsyd.syd_enabled_pid.restype = ctypes.c_bool

libsyd.syd_enable_force.restype = ctypes.c_int
libsyd.syd_disable_force.restype = ctypes.c_int
libsyd.syd_enabled_force.restype = ctypes.c_bool

libsyd.syd_enable_tpe.restype = ctypes.c_int
libsyd.syd_disable_tpe.restype = ctypes.c_int
libsyd.syd_enabled_tpe.restype = ctypes.c_bool

libsyd.syd_default_stat.argtypes = [ctypes.c_uint]
libsyd.syd_default_stat.restype = ctypes.c_int

libsyd.syd_default_read.argtypes = [ctypes.c_uint]
libsyd.syd_default_read.restype = ctypes.c_int

libsyd.syd_default_write.argtypes = [ctypes.c_uint]
libsyd.syd_default_write.restype = ctypes.c_int

libsyd.syd_default_exec.argtypes = [ctypes.c_uint]
libsyd.syd_default_exec.restype = ctypes.c_int

libsyd.syd_default_ioctl.argtypes = [ctypes.c_uint]
libsyd.syd_default_ioctl.restype = ctypes.c_int

libsyd.syd_default_create.argtypes = [ctypes.c_uint]
libsyd.syd_default_create.restype = ctypes.c_int

libsyd.syd_default_delete.argtypes = [ctypes.c_uint]
libsyd.syd_default_delete.restype = ctypes.c_int

libsyd.syd_default_rename.argtypes = [ctypes.c_uint]
libsyd.syd_default_rename.restype = ctypes.c_int

libsyd.syd_default_symlink.argtypes = [ctypes.c_uint]
libsyd.syd_default_symlink.restype = ctypes.c_int

libsyd.syd_default_truncate.argtypes = [ctypes.c_uint]
libsyd.syd_default_truncate.restype = ctypes.c_int

libsyd.syd_default_chdir.argtypes = [ctypes.c_uint]
libsyd.syd_default_chdir.restype = ctypes.c_int

libsyd.syd_default_readdir.argtypes = [ctypes.c_uint]
libsyd.syd_default_readdir.restype = ctypes.c_int

libsyd.syd_default_mkdir.argtypes = [ctypes.c_uint]
libsyd.syd_default_mkdir.restype = ctypes.c_int

libsyd.syd_default_chown.argtypes = [ctypes.c_uint]
libsyd.syd_default_chown.restype = ctypes.c_int

libsyd.syd_default_chgrp.argtypes = [ctypes.c_uint]
libsyd.syd_default_chgrp.restype = ctypes.c_int

libsyd.syd_default_chmod.argtypes = [ctypes.c_uint]
libsyd.syd_default_chmod.restype = ctypes.c_int

libsyd.syd_default_chattr.argtypes = [ctypes.c_uint]
libsyd.syd_default_chattr.restype = ctypes.c_int

libsyd.syd_default_chroot.argtypes = [ctypes.c_uint]
libsyd.syd_default_chroot.restype = ctypes.c_int

libsyd.syd_default_utime.argtypes = [ctypes.c_uint]
libsyd.syd_default_utime.restype = ctypes.c_int

libsyd.syd_default_mkdev.argtypes = [ctypes.c_uint]
libsyd.syd_default_mkdev.restype = ctypes.c_int

libsyd.syd_default_mkfifo.argtypes = [ctypes.c_uint]
libsyd.syd_default_mkfifo.restype = ctypes.c_int

libsyd.syd_default_mktemp.argtypes = [ctypes.c_uint]
libsyd.syd_default_mktemp.restype = ctypes.c_int

libsyd.syd_default_net.argtypes = [ctypes.c_uint]
libsyd.syd_default_net.restype = ctypes.c_int

libsyd.syd_default_block.argtypes = [ctypes.c_uint]
libsyd.syd_default_block.restype = ctypes.c_int

libsyd.syd_default_mem.argtypes = [ctypes.c_uint]
libsyd.syd_default_mem.restype = ctypes.c_int

libsyd.syd_default_pid.argtypes = [ctypes.c_uint]
libsyd.syd_default_pid.restype = ctypes.c_int

libsyd.syd_default_force.argtypes = [ctypes.c_uint]
libsyd.syd_default_force.restype = ctypes.c_int

libsyd.syd_default_segvguard.argtypes = [ctypes.c_uint]
libsyd.syd_default_segvguard.restype = ctypes.c_int

libsyd.syd_default_tpe.argtypes = [ctypes.c_uint]
libsyd.syd_default_tpe.restype = ctypes.c_int

libsyd.syd_ioctl_deny.argtypes = [ctypes.c_uint64]
libsyd.syd_ioctl_deny.restype = ctypes.c_int

libsyd.syd_stat_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_stat_add.restype = ctypes.c_int
libsyd.syd_stat_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_stat_del.restype = ctypes.c_int
libsyd.syd_stat_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_stat_rem.restype = ctypes.c_int

libsyd.syd_read_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_read_add.restype = ctypes.c_int
libsyd.syd_read_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_read_del.restype = ctypes.c_int
libsyd.syd_read_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_read_rem.restype = ctypes.c_int

libsyd.syd_write_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_write_add.restype = ctypes.c_int
libsyd.syd_write_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_write_del.restype = ctypes.c_int
libsyd.syd_write_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_write_rem.restype = ctypes.c_int

libsyd.syd_exec_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_exec_add.restype = ctypes.c_int
libsyd.syd_exec_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_exec_del.restype = ctypes.c_int
libsyd.syd_exec_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_exec_rem.restype = ctypes.c_int

libsyd.syd_ioctl_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_ioctl_add.restype = ctypes.c_int
libsyd.syd_ioctl_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_ioctl_del.restype = ctypes.c_int
libsyd.syd_ioctl_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_ioctl_rem.restype = ctypes.c_int

libsyd.syd_create_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_create_add.restype = ctypes.c_int
libsyd.syd_create_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_create_del.restype = ctypes.c_int
libsyd.syd_create_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_create_rem.restype = ctypes.c_int

libsyd.syd_delete_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_delete_add.restype = ctypes.c_int
libsyd.syd_delete_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_delete_del.restype = ctypes.c_int
libsyd.syd_delete_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_delete_rem.restype = ctypes.c_int

libsyd.syd_rename_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_rename_add.restype = ctypes.c_int
libsyd.syd_rename_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_rename_del.restype = ctypes.c_int
libsyd.syd_rename_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_rename_rem.restype = ctypes.c_int

libsyd.syd_symlink_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_symlink_add.restype = ctypes.c_int
libsyd.syd_symlink_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_symlink_del.restype = ctypes.c_int
libsyd.syd_symlink_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_symlink_rem.restype = ctypes.c_int

libsyd.syd_truncate_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_truncate_add.restype = ctypes.c_int
libsyd.syd_truncate_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_truncate_del.restype = ctypes.c_int
libsyd.syd_truncate_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_truncate_rem.restype = ctypes.c_int

libsyd.syd_chdir_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chdir_add.restype = ctypes.c_int
libsyd.syd_chdir_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chdir_del.restype = ctypes.c_int
libsyd.syd_chdir_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chdir_rem.restype = ctypes.c_int

libsyd.syd_readdir_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_readdir_add.restype = ctypes.c_int
libsyd.syd_readdir_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_readdir_del.restype = ctypes.c_int
libsyd.syd_readdir_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_readdir_rem.restype = ctypes.c_int

libsyd.syd_mkdir_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkdir_add.restype = ctypes.c_int
libsyd.syd_mkdir_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkdir_del.restype = ctypes.c_int
libsyd.syd_mkdir_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkdir_rem.restype = ctypes.c_int

libsyd.syd_chown_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chown_add.restype = ctypes.c_int
libsyd.syd_chown_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chown_del.restype = ctypes.c_int
libsyd.syd_chown_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chown_rem.restype = ctypes.c_int

libsyd.syd_chgrp_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chgrp_add.restype = ctypes.c_int
libsyd.syd_chgrp_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chgrp_del.restype = ctypes.c_int
libsyd.syd_chgrp_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chgrp_rem.restype = ctypes.c_int

libsyd.syd_chmod_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chmod_add.restype = ctypes.c_int
libsyd.syd_chmod_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chmod_del.restype = ctypes.c_int
libsyd.syd_chmod_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chmod_rem.restype = ctypes.c_int

libsyd.syd_chattr_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chattr_add.restype = ctypes.c_int
libsyd.syd_chattr_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chattr_del.restype = ctypes.c_int
libsyd.syd_chattr_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chattr_rem.restype = ctypes.c_int

libsyd.syd_chroot_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chroot_add.restype = ctypes.c_int
libsyd.syd_chroot_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chroot_del.restype = ctypes.c_int
libsyd.syd_chroot_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_chroot_rem.restype = ctypes.c_int

libsyd.syd_utime_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_utime_add.restype = ctypes.c_int
libsyd.syd_utime_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_utime_del.restype = ctypes.c_int
libsyd.syd_utime_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_utime_rem.restype = ctypes.c_int

libsyd.syd_mkdev_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkdev_add.restype = ctypes.c_int
libsyd.syd_mkdev_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkdev_del.restype = ctypes.c_int
libsyd.syd_mkdev_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkdev_rem.restype = ctypes.c_int

libsyd.syd_mkfifo_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkfifo_add.restype = ctypes.c_int
libsyd.syd_mkfifo_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkfifo_del.restype = ctypes.c_int
libsyd.syd_mkfifo_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mkfifo_rem.restype = ctypes.c_int

libsyd.syd_mktemp_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mktemp_add.restype = ctypes.c_int
libsyd.syd_mktemp_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mktemp_del.restype = ctypes.c_int
libsyd.syd_mktemp_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_mktemp_rem.restype = ctypes.c_int

libsyd.syd_net_bind_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_bind_add.restype = ctypes.c_int
libsyd.syd_net_bind_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_bind_del.restype = ctypes.c_int
libsyd.syd_net_bind_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_bind_rem.restype = ctypes.c_int

libsyd.syd_net_connect_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_connect_add.restype = ctypes.c_int
libsyd.syd_net_connect_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_connect_del.restype = ctypes.c_int
libsyd.syd_net_connect_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_connect_rem.restype = ctypes.c_int

libsyd.syd_net_sendfd_add.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_sendfd_add.restype = ctypes.c_int
libsyd.syd_net_sendfd_del.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_sendfd_del.restype = ctypes.c_int
libsyd.syd_net_sendfd_rem.argtypes = [ctypes.c_uint8, ctypes.c_char_p]
libsyd.syd_net_sendfd_rem.restype = ctypes.c_int

# TODO net_link_add...

libsyd.syd_force_add.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_uint]
libsyd.syd_force_add.restype = ctypes.c_int
libsyd.syd_force_del.argtypes = [ctypes.c_char_p]
libsyd.syd_force_del.restype = ctypes.c_int
libsyd.syd_force_clr.argtypes = []
libsyd.syd_force_clr.restype = ctypes.c_int

libsyd.syd_mem_max.argtypes = [ctypes.c_char_p]
libsyd.syd_mem_max.restype = ctypes.c_int
libsyd.syd_mem_vm_max.argtypes = [ctypes.c_char_p]
libsyd.syd_mem_vm_max.restype = ctypes.c_int
libsyd.syd_pid_max.argtypes = [ctypes.c_size_t]
libsyd.syd_pid_max.restype = ctypes.c_int

libsyd.syd_segvguard_expiry.argtypes = [ctypes.c_uint64]
libsyd.syd_segvguard_expiry.restype = ctypes.c_int
libsyd.syd_segvguard_suspension.argtypes = [ctypes.c_uint64]
libsyd.syd_segvguard_suspension.restype = ctypes.c_int
libsyd.syd_segvguard_maxcrashes.argtypes = [ctypes.c_uint8]
libsyd.syd_segvguard_maxcrashes.restype = ctypes.c_int

if __name__ == "__main__":

    class test(unittest.TestCase):
        @classmethod
        def setUpClass(cls):
            # Global precheck with syd_check()
            try:
                check()
            except OSError:
                raise unittest.SkipTest("check() raised OsError, skipping tests.")

        @staticmethod
        def find(rules, pattern):
            for idx, rule in enumerate(reversed(rules)):
                if pattern == rule:
                    return len(rules) - 1 - idx
            return None

        # This must be the first test!
        def test_01_api(self):
            self.assertEqual(api(), 3)

        def test_02_stat(self):
            state = enabled_force()
            self.assertTrue(enable_force())
            self.assertTrue(enabled_force())
            self.assertTrue(disable_force())
            self.assertFalse(enabled_force())
            if state:
                enable_force()
            else:
                disable_force()

            state = enabled_stat()
            self.assertTrue(enable_stat())
            self.assertTrue(enabled_stat())
            self.assertTrue(disable_stat())
            self.assertFalse(enabled_stat())
            if state:
                enable_stat()
            else:
                disable_stat()

            state = enabled_read()
            self.assertTrue(enable_read())
            self.assertTrue(enabled_read())
            self.assertTrue(disable_read())
            self.assertFalse(enabled_read())
            if state:
                enable_read()
            else:
                disable_read()

            state = enabled_write()
            self.assertTrue(enable_write())
            self.assertTrue(enabled_write())
            self.assertTrue(disable_write())
            self.assertFalse(enabled_write())
            if state:
                enable_write()
            else:
                disable_write()

            state = enabled_exec()
            self.assertTrue(enable_exec())
            self.assertTrue(enabled_exec())
            self.assertTrue(disable_exec())
            self.assertFalse(enabled_exec())
            if state:
                enable_exec()
            else:
                disable_exec()

            state = enabled_ioctl()
            self.assertTrue(enable_ioctl())
            self.assertTrue(enabled_ioctl())
            self.assertTrue(disable_ioctl())
            self.assertFalse(enabled_ioctl())
            if state:
                enable_ioctl()
            else:
                disable_ioctl()

            state = enabled_create()
            self.assertTrue(enable_create())
            self.assertTrue(enabled_create())
            self.assertTrue(disable_create())
            self.assertFalse(enabled_create())
            if state:
                enable_create()
            else:
                disable_create()

            state = enabled_delete()
            self.assertTrue(enable_delete())
            self.assertTrue(enabled_delete())
            self.assertTrue(disable_delete())
            self.assertFalse(enabled_delete())
            if state:
                enable_delete()
            else:
                disable_delete()

            state = enabled_rename()
            self.assertTrue(enable_rename())
            self.assertTrue(enabled_rename())
            self.assertTrue(disable_rename())
            self.assertFalse(enabled_rename())
            if state:
                enable_rename()
            else:
                disable_rename()

            state = enabled_symlink()
            self.assertTrue(enable_symlink())
            self.assertTrue(enabled_symlink())
            self.assertTrue(disable_symlink())
            self.assertFalse(enabled_symlink())
            if state:
                enable_symlink()
            else:
                disable_symlink()

            state = enabled_truncate()
            self.assertTrue(enable_truncate())
            self.assertTrue(enabled_truncate())
            self.assertTrue(disable_truncate())
            self.assertFalse(enabled_truncate())
            if state:
                enable_truncate()
            else:
                disable_truncate()

            state = enabled_chdir()
            self.assertTrue(enable_chdir())
            self.assertTrue(enabled_chdir())
            self.assertTrue(disable_chdir())
            self.assertFalse(enabled_chdir())
            if state:
                enable_chdir()
            else:
                disable_chdir()

            state = enabled_readdir()
            self.assertTrue(enable_readdir())
            self.assertTrue(enabled_readdir())
            self.assertTrue(disable_readdir())
            self.assertFalse(enabled_readdir())
            if state:
                enable_readdir()
            else:
                disable_readdir()

            state = enabled_mkdir()
            self.assertTrue(enable_mkdir())
            self.assertTrue(enabled_mkdir())
            self.assertTrue(disable_mkdir())
            self.assertFalse(enabled_mkdir())
            if state:
                enable_mkdir()
            else:
                disable_mkdir()

            state = enabled_chown()
            self.assertTrue(enable_chown())
            self.assertTrue(enabled_chown())
            self.assertTrue(disable_chown())
            self.assertFalse(enabled_chown())
            if state:
                enable_chown()
            else:
                disable_chown()

            state = enabled_chgrp()
            self.assertTrue(enable_chgrp())
            self.assertTrue(enabled_chgrp())
            self.assertTrue(disable_chgrp())
            self.assertFalse(enabled_chgrp())
            if state:
                enable_chgrp()
            else:
                disable_chgrp()

            state = enabled_chmod()
            self.assertTrue(enable_chmod())
            self.assertTrue(enabled_chmod())
            self.assertTrue(disable_chmod())
            self.assertFalse(enabled_chmod())
            if state:
                enable_chmod()
            else:
                disable_chmod()

            state = enabled_chattr()
            self.assertTrue(enable_chattr())
            self.assertTrue(enabled_chattr())
            self.assertTrue(disable_chattr())
            self.assertFalse(enabled_chattr())
            if state:
                enable_chattr()
            else:
                disable_chattr()

            # Chroot is startup only since 3.32.4
            #            state = enabled_chroot()
            #            self.assertTrue(enable_chroot())
            #            self.assertTrue(enabled_chroot())
            #            self.assertTrue(disable_chroot())
            #            self.assertFalse(enabled_chroot())
            #            if state:
            #                enable_chroot()
            #            else:
            #                disable_chroot()

            state = enabled_utime()
            self.assertTrue(enable_utime())
            self.assertTrue(enabled_utime())
            self.assertTrue(disable_utime())
            self.assertFalse(enabled_utime())
            if state:
                enable_utime()
            else:
                disable_utime()

            state = enabled_mkdev()
            self.assertTrue(enable_mkdev())
            self.assertTrue(enabled_mkdev())
            self.assertTrue(disable_mkdev())
            self.assertFalse(enabled_mkdev())
            if state:
                enable_mkdev()
            else:
                disable_mkdev()

            state = enabled_mkfifo()
            self.assertTrue(enable_mkfifo())
            self.assertTrue(enabled_mkfifo())
            self.assertTrue(disable_mkfifo())
            self.assertFalse(enabled_mkfifo())
            if state:
                enable_mkfifo()
            else:
                disable_mkfifo()

            state = enabled_mktemp()
            self.assertTrue(enable_mktemp())
            self.assertTrue(enabled_mktemp())
            self.assertTrue(disable_mktemp())
            self.assertFalse(enabled_mktemp())
            if state:
                enable_mktemp()
            else:
                disable_mktemp()

            state = enabled_net()
            self.assertTrue(enable_net())
            self.assertTrue(enabled_net())
            self.assertTrue(disable_net())
            self.assertFalse(enabled_net())
            if state:
                enable_net()
            else:
                disable_net()

            state = enabled_mem()
            self.assertTrue(enable_mem())
            self.assertTrue(enabled_mem())
            self.assertTrue(disable_mem())
            self.assertFalse(enabled_mem())
            if state:
                enable_mem()
            else:
                disable_mem()

            state = enabled_pid()
            self.assertTrue(enable_pid())
            self.assertTrue(enabled_pid())
            self.assertTrue(disable_pid())
            self.assertFalse(enabled_pid())
            if state:
                enable_pid()
            else:
                disable_pid()

            state = enabled_force()
            self.assertTrue(enable_force())
            self.assertTrue(enabled_force())
            self.assertTrue(disable_force())
            self.assertFalse(enabled_force())
            if state:
                enable_force()
            else:
                disable_force()

            state = enabled_tpe()
            self.assertTrue(enable_tpe())
            self.assertTrue(enabled_tpe())
            self.assertTrue(disable_tpe())
            self.assertFalse(enabled_tpe())
            if state:
                enable_tpe()
            else:
                disable_tpe()

            mem_max_orig = str(info()["mem_max"]).encode("utf-8")
            mem_vm_max_orig = str(info()["mem_vm_max"]).encode("utf-8")
            pid_max_orig = info()["pid_max"]

            self.assertTrue(mem_max("1G".encode("utf-8")))
            self.assertEqual(info()["mem_max"], 1024 * 1024 * 1024)
            self.assertTrue(mem_max("10G".encode("utf-8")))
            self.assertEqual(info()["mem_max"], 10 * 1024 * 1024 * 1024)
            mem_max(mem_max_orig)

            self.assertTrue(mem_vm_max("1G".encode("utf-8")))
            self.assertEqual(info()["mem_vm_max"], 1024 * 1024 * 1024)
            self.assertTrue(mem_vm_max("10G".encode("utf-8")))
            self.assertEqual(info()["mem_vm_max"], 10 * 1024 * 1024 * 1024)
            mem_vm_max(mem_vm_max_orig)

            with self.assertRaises(ValueError):
                pid_max(-1)
            self.assertTrue(pid_max(4096))
            self.assertEqual(info()["pid_max"], 4096)
            self.assertTrue(pid_max(8192))
            self.assertEqual(info()["pid_max"], 8192)
            pid_max(pid_max_orig)

        def test_03_default(self):
            stat_default = info()["default_stat"]
            self.assertEqual(stat_default, "Deny")

            self.assertTrue(default_stat(Action.ACTION_ALLOW))
            stat_default = info()["default_stat"]
            self.assertEqual(stat_default, "Allow")

            self.assertTrue(default_stat(Action.ACTION_FILTER))
            stat_default = info()["default_stat"]
            self.assertEqual(stat_default, "Filter")

            self.assertTrue(default_stat(Action.ACTION_STOP))
            stat_default = info()["default_stat"]
            self.assertEqual(stat_default, "Stop")

            self.assertTrue(default_stat(Action.ACTION_KILL))
            stat_default = info()["default_stat"]
            self.assertEqual(stat_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_stat(Action.ACTION_DENY))
            stat_default = info()["default_stat"]
            self.assertEqual(stat_default, "Deny")

            read_default = info()["default_read"]
            self.assertEqual(read_default, "Deny")

            self.assertTrue(default_read(Action.ACTION_ALLOW))
            read_default = info()["default_read"]
            self.assertEqual(read_default, "Allow")

            self.assertTrue(default_read(Action.ACTION_FILTER))
            read_default = info()["default_read"]
            self.assertEqual(read_default, "Filter")

            self.assertTrue(default_read(Action.ACTION_STOP))
            read_default = info()["default_read"]
            self.assertEqual(read_default, "Stop")

            self.assertTrue(default_read(Action.ACTION_KILL))
            read_default = info()["default_read"]
            self.assertEqual(read_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_read(Action.ACTION_DENY))
            read_default = info()["default_read"]
            self.assertEqual(read_default, "Deny")

            write_default = info()["default_write"]
            self.assertEqual(write_default, "Deny")

            self.assertTrue(default_write(Action.ACTION_ALLOW))
            write_default = info()["default_write"]
            self.assertEqual(write_default, "Allow")

            self.assertTrue(default_write(Action.ACTION_FILTER))
            write_default = info()["default_write"]
            self.assertEqual(write_default, "Filter")

            self.assertTrue(default_write(Action.ACTION_STOP))
            write_default = info()["default_write"]
            self.assertEqual(write_default, "Stop")

            self.assertTrue(default_write(Action.ACTION_KILL))
            write_default = info()["default_write"]
            self.assertEqual(write_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_write(Action.ACTION_DENY))
            write_default = info()["default_write"]
            self.assertEqual(write_default, "Deny")

            exec_default = info()["default_exec"]
            self.assertEqual(exec_default, "Deny")

            self.assertTrue(default_exec(Action.ACTION_ALLOW))
            exec_default = info()["default_exec"]
            self.assertEqual(exec_default, "Allow")

            self.assertTrue(default_exec(Action.ACTION_FILTER))
            exec_default = info()["default_exec"]
            self.assertEqual(exec_default, "Filter")

            self.assertTrue(default_exec(Action.ACTION_STOP))
            exec_default = info()["default_exec"]
            self.assertEqual(exec_default, "Stop")

            self.assertTrue(default_exec(Action.ACTION_KILL))
            exec_default = info()["default_exec"]
            self.assertEqual(exec_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_exec(Action.ACTION_DENY))
            exec_default = info()["default_exec"]
            self.assertEqual(exec_default, "Deny")

            ioctl_default = info()["default_ioctl"]
            self.assertEqual(ioctl_default, "Deny")

            self.assertTrue(default_ioctl(Action.ACTION_ALLOW))
            ioctl_default = info()["default_ioctl"]
            self.assertEqual(ioctl_default, "Allow")

            self.assertTrue(default_ioctl(Action.ACTION_FILTER))
            ioctl_default = info()["default_ioctl"]
            self.assertEqual(ioctl_default, "Filter")

            self.assertTrue(default_ioctl(Action.ACTION_STOP))
            ioctl_default = info()["default_ioctl"]
            self.assertEqual(ioctl_default, "Stop")

            self.assertTrue(default_ioctl(Action.ACTION_KILL))
            ioctl_default = info()["default_ioctl"]
            self.assertEqual(ioctl_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_ioctl(Action.ACTION_DENY))
            ioctl_default = info()["default_ioctl"]
            self.assertEqual(ioctl_default, "Deny")

            create_default = info()["default_create"]
            self.assertEqual(create_default, "Deny")

            self.assertTrue(default_create(Action.ACTION_ALLOW))
            create_default = info()["default_create"]
            self.assertEqual(create_default, "Allow")

            self.assertTrue(default_create(Action.ACTION_FILTER))
            create_default = info()["default_create"]
            self.assertEqual(create_default, "Filter")

            self.assertTrue(default_create(Action.ACTION_STOP))
            create_default = info()["default_create"]
            self.assertEqual(create_default, "Stop")

            self.assertTrue(default_create(Action.ACTION_KILL))
            create_default = info()["default_create"]
            self.assertEqual(create_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_create(Action.ACTION_DENY))
            create_default = info()["default_create"]
            self.assertEqual(create_default, "Deny")

            delete_default = info()["default_delete"]
            self.assertEqual(delete_default, "Deny")

            self.assertTrue(default_delete(Action.ACTION_ALLOW))
            delete_default = info()["default_delete"]
            self.assertEqual(delete_default, "Allow")

            self.assertTrue(default_delete(Action.ACTION_FILTER))
            delete_default = info()["default_delete"]
            self.assertEqual(delete_default, "Filter")

            self.assertTrue(default_delete(Action.ACTION_STOP))
            delete_default = info()["default_delete"]
            self.assertEqual(delete_default, "Stop")

            self.assertTrue(default_delete(Action.ACTION_KILL))
            delete_default = info()["default_delete"]
            self.assertEqual(delete_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_delete(Action.ACTION_DENY))
            delete_default = info()["default_delete"]
            self.assertEqual(delete_default, "Deny")

            rename_default = info()["default_rename"]
            self.assertEqual(rename_default, "Deny")

            self.assertTrue(default_rename(Action.ACTION_ALLOW))
            rename_default = info()["default_rename"]
            self.assertEqual(rename_default, "Allow")

            self.assertTrue(default_rename(Action.ACTION_FILTER))
            rename_default = info()["default_rename"]
            self.assertEqual(rename_default, "Filter")

            self.assertTrue(default_rename(Action.ACTION_STOP))
            rename_default = info()["default_rename"]
            self.assertEqual(rename_default, "Stop")

            self.assertTrue(default_rename(Action.ACTION_KILL))
            rename_default = info()["default_rename"]
            self.assertEqual(rename_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_rename(Action.ACTION_DENY))
            rename_default = info()["default_rename"]
            self.assertEqual(rename_default, "Deny")

            symlink_default = info()["default_symlink"]
            self.assertEqual(symlink_default, "Deny")

            self.assertTrue(default_symlink(Action.ACTION_ALLOW))
            symlink_default = info()["default_symlink"]
            self.assertEqual(symlink_default, "Allow")

            self.assertTrue(default_symlink(Action.ACTION_FILTER))
            symlink_default = info()["default_symlink"]
            self.assertEqual(symlink_default, "Filter")

            self.assertTrue(default_symlink(Action.ACTION_STOP))
            symlink_default = info()["default_symlink"]
            self.assertEqual(symlink_default, "Stop")

            self.assertTrue(default_symlink(Action.ACTION_KILL))
            symlink_default = info()["default_symlink"]
            self.assertEqual(symlink_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_symlink(Action.ACTION_DENY))
            symlink_default = info()["default_symlink"]
            self.assertEqual(symlink_default, "Deny")

            truncate_default = info()["default_truncate"]
            self.assertEqual(truncate_default, "Deny")

            self.assertTrue(default_truncate(Action.ACTION_ALLOW))
            truncate_default = info()["default_truncate"]
            self.assertEqual(truncate_default, "Allow")

            self.assertTrue(default_truncate(Action.ACTION_FILTER))
            truncate_default = info()["default_truncate"]
            self.assertEqual(truncate_default, "Filter")

            self.assertTrue(default_truncate(Action.ACTION_STOP))
            truncate_default = info()["default_truncate"]
            self.assertEqual(truncate_default, "Stop")

            self.assertTrue(default_truncate(Action.ACTION_KILL))
            truncate_default = info()["default_truncate"]
            self.assertEqual(truncate_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_truncate(Action.ACTION_DENY))
            truncate_default = info()["default_truncate"]
            self.assertEqual(truncate_default, "Deny")

            chdir_default = info()["default_chdir"]
            self.assertEqual(chdir_default, "Deny")

            self.assertTrue(default_chdir(Action.ACTION_ALLOW))
            chdir_default = info()["default_chdir"]
            self.assertEqual(chdir_default, "Allow")

            self.assertTrue(default_chdir(Action.ACTION_FILTER))
            chdir_default = info()["default_chdir"]
            self.assertEqual(chdir_default, "Filter")

            self.assertTrue(default_chdir(Action.ACTION_STOP))
            chdir_default = info()["default_chdir"]
            self.assertEqual(chdir_default, "Stop")

            self.assertTrue(default_chdir(Action.ACTION_KILL))
            chdir_default = info()["default_chdir"]
            self.assertEqual(chdir_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_chdir(Action.ACTION_DENY))
            chdir_default = info()["default_chdir"]
            self.assertEqual(chdir_default, "Deny")

            readdir_default = info()["default_readdir"]
            self.assertEqual(readdir_default, "Deny")

            self.assertTrue(default_readdir(Action.ACTION_ALLOW))
            readdir_default = info()["default_readdir"]
            self.assertEqual(readdir_default, "Allow")

            self.assertTrue(default_readdir(Action.ACTION_FILTER))
            readdir_default = info()["default_readdir"]
            self.assertEqual(readdir_default, "Filter")

            self.assertTrue(default_readdir(Action.ACTION_STOP))
            readdir_default = info()["default_readdir"]
            self.assertEqual(readdir_default, "Stop")

            self.assertTrue(default_readdir(Action.ACTION_KILL))
            readdir_default = info()["default_readdir"]
            self.assertEqual(readdir_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_readdir(Action.ACTION_DENY))
            readdir_default = info()["default_readdir"]
            self.assertEqual(readdir_default, "Deny")

            mkdir_default = info()["default_mkdir"]
            self.assertEqual(mkdir_default, "Deny")

            self.assertTrue(default_mkdir(Action.ACTION_ALLOW))
            mkdir_default = info()["default_mkdir"]
            self.assertEqual(mkdir_default, "Allow")

            self.assertTrue(default_mkdir(Action.ACTION_FILTER))
            mkdir_default = info()["default_mkdir"]
            self.assertEqual(mkdir_default, "Filter")

            self.assertTrue(default_mkdir(Action.ACTION_STOP))
            mkdir_default = info()["default_mkdir"]
            self.assertEqual(mkdir_default, "Stop")

            self.assertTrue(default_mkdir(Action.ACTION_KILL))
            mkdir_default = info()["default_mkdir"]
            self.assertEqual(mkdir_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_mkdir(Action.ACTION_DENY))
            mkdir_default = info()["default_mkdir"]
            self.assertEqual(mkdir_default, "Deny")

            chown_default = info()["default_chown"]
            self.assertEqual(chown_default, "Deny")

            self.assertTrue(default_chown(Action.ACTION_ALLOW))
            chown_default = info()["default_chown"]
            self.assertEqual(chown_default, "Allow")

            self.assertTrue(default_chown(Action.ACTION_FILTER))
            chown_default = info()["default_chown"]
            self.assertEqual(chown_default, "Filter")

            self.assertTrue(default_chown(Action.ACTION_STOP))
            chown_default = info()["default_chown"]
            self.assertEqual(chown_default, "Stop")

            self.assertTrue(default_chown(Action.ACTION_KILL))
            chown_default = info()["default_chown"]
            self.assertEqual(chown_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_chown(Action.ACTION_DENY))
            chown_default = info()["default_chown"]
            self.assertEqual(chown_default, "Deny")

            chgrp_default = info()["default_chgrp"]
            self.assertEqual(chgrp_default, "Deny")

            self.assertTrue(default_chgrp(Action.ACTION_ALLOW))
            chgrp_default = info()["default_chgrp"]
            self.assertEqual(chgrp_default, "Allow")

            self.assertTrue(default_chgrp(Action.ACTION_FILTER))
            chgrp_default = info()["default_chgrp"]
            self.assertEqual(chgrp_default, "Filter")

            self.assertTrue(default_chgrp(Action.ACTION_STOP))
            chgrp_default = info()["default_chgrp"]
            self.assertEqual(chgrp_default, "Stop")

            self.assertTrue(default_chgrp(Action.ACTION_KILL))
            chgrp_default = info()["default_chgrp"]
            self.assertEqual(chgrp_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_chgrp(Action.ACTION_DENY))
            chgrp_default = info()["default_chgrp"]
            self.assertEqual(chgrp_default, "Deny")

            chmod_default = info()["default_chmod"]
            self.assertEqual(chmod_default, "Deny")

            self.assertTrue(default_chmod(Action.ACTION_ALLOW))
            chmod_default = info()["default_chmod"]
            self.assertEqual(chmod_default, "Allow")

            self.assertTrue(default_chmod(Action.ACTION_FILTER))
            chmod_default = info()["default_chmod"]
            self.assertEqual(chmod_default, "Filter")

            self.assertTrue(default_chmod(Action.ACTION_STOP))
            chmod_default = info()["default_chmod"]
            self.assertEqual(chmod_default, "Stop")

            self.assertTrue(default_chmod(Action.ACTION_KILL))
            chmod_default = info()["default_chmod"]
            self.assertEqual(chmod_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_chmod(Action.ACTION_DENY))
            chmod_default = info()["default_chmod"]
            self.assertEqual(chmod_default, "Deny")

            chattr_default = info()["default_chattr"]
            self.assertEqual(chattr_default, "Deny")

            self.assertTrue(default_chattr(Action.ACTION_ALLOW))
            chattr_default = info()["default_chattr"]
            self.assertEqual(chattr_default, "Allow")

            self.assertTrue(default_chattr(Action.ACTION_FILTER))
            chattr_default = info()["default_chattr"]
            self.assertEqual(chattr_default, "Filter")

            self.assertTrue(default_chattr(Action.ACTION_STOP))
            chattr_default = info()["default_chattr"]
            self.assertEqual(chattr_default, "Stop")

            self.assertTrue(default_chattr(Action.ACTION_KILL))
            chattr_default = info()["default_chattr"]
            self.assertEqual(chattr_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_chattr(Action.ACTION_DENY))
            chattr_default = info()["default_chattr"]
            self.assertEqual(chattr_default, "Deny")

            chroot_default = info()["default_chroot"]
            self.assertEqual(chroot_default, "Deny")

            self.assertTrue(default_chroot(Action.ACTION_ALLOW))
            chroot_default = info()["default_chroot"]
            self.assertEqual(chroot_default, "Allow")

            self.assertTrue(default_chroot(Action.ACTION_FILTER))
            chroot_default = info()["default_chroot"]
            self.assertEqual(chroot_default, "Filter")

            self.assertTrue(default_chroot(Action.ACTION_STOP))
            chroot_default = info()["default_chroot"]
            self.assertEqual(chroot_default, "Stop")

            self.assertTrue(default_chroot(Action.ACTION_KILL))
            chroot_default = info()["default_chroot"]
            self.assertEqual(chroot_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_chroot(Action.ACTION_DENY))
            chroot_default = info()["default_chroot"]
            self.assertEqual(chroot_default, "Deny")

            utime_default = info()["default_utime"]
            self.assertEqual(utime_default, "Deny")

            self.assertTrue(default_utime(Action.ACTION_ALLOW))
            utime_default = info()["default_utime"]
            self.assertEqual(utime_default, "Allow")

            self.assertTrue(default_utime(Action.ACTION_FILTER))
            utime_default = info()["default_utime"]
            self.assertEqual(utime_default, "Filter")

            self.assertTrue(default_utime(Action.ACTION_STOP))
            utime_default = info()["default_utime"]
            self.assertEqual(utime_default, "Stop")

            self.assertTrue(default_utime(Action.ACTION_KILL))
            utime_default = info()["default_utime"]
            self.assertEqual(utime_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_utime(Action.ACTION_DENY))
            utime_default = info()["default_utime"]
            self.assertEqual(utime_default, "Deny")

            mkdev_default = info()["default_mkdev"]
            self.assertEqual(mkdev_default, "Deny")

            self.assertTrue(default_mkdev(Action.ACTION_ALLOW))
            mkdev_default = info()["default_mkdev"]
            self.assertEqual(mkdev_default, "Allow")

            self.assertTrue(default_mkdev(Action.ACTION_FILTER))
            mkdev_default = info()["default_mkdev"]
            self.assertEqual(mkdev_default, "Filter")

            self.assertTrue(default_mkdev(Action.ACTION_STOP))
            mkdev_default = info()["default_mkdev"]
            self.assertEqual(mkdev_default, "Stop")

            self.assertTrue(default_mkdev(Action.ACTION_KILL))
            mkdev_default = info()["default_mkdev"]
            self.assertEqual(mkdev_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_mkdev(Action.ACTION_DENY))
            mkdev_default = info()["default_mkdev"]
            self.assertEqual(mkdev_default, "Deny")

            mkfifo_default = info()["default_mkfifo"]
            self.assertEqual(mkfifo_default, "Deny")

            self.assertTrue(default_mkfifo(Action.ACTION_ALLOW))
            mkfifo_default = info()["default_mkfifo"]
            self.assertEqual(mkfifo_default, "Allow")

            self.assertTrue(default_mkfifo(Action.ACTION_FILTER))
            mkfifo_default = info()["default_mkfifo"]
            self.assertEqual(mkfifo_default, "Filter")

            self.assertTrue(default_mkfifo(Action.ACTION_STOP))
            mkfifo_default = info()["default_mkfifo"]
            self.assertEqual(mkfifo_default, "Stop")

            self.assertTrue(default_mkfifo(Action.ACTION_KILL))
            mkfifo_default = info()["default_mkfifo"]
            self.assertEqual(mkfifo_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_mkfifo(Action.ACTION_DENY))
            mkfifo_default = info()["default_mkfifo"]
            self.assertEqual(mkfifo_default, "Deny")

            mktemp_default = info()["default_mktemp"]
            self.assertEqual(mktemp_default, "Deny")

            self.assertTrue(default_mktemp(Action.ACTION_ALLOW))
            mktemp_default = info()["default_mktemp"]
            self.assertEqual(mktemp_default, "Allow")

            self.assertTrue(default_mktemp(Action.ACTION_FILTER))
            mktemp_default = info()["default_mktemp"]
            self.assertEqual(mktemp_default, "Filter")

            self.assertTrue(default_mktemp(Action.ACTION_STOP))
            mktemp_default = info()["default_mktemp"]
            self.assertEqual(mktemp_default, "Stop")

            self.assertTrue(default_mktemp(Action.ACTION_KILL))
            mktemp_default = info()["default_mktemp"]
            self.assertEqual(mktemp_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_mktemp(Action.ACTION_DENY))
            mktemp_default = info()["default_mktemp"]
            self.assertEqual(mktemp_default, "Deny")

            # TODO: Split net_default into net_bind, net_connect and net_sendfd.
            #            net_default = info()["default_net"]
            #            self.assertEqual(net_default, "Deny")
            #
            #            self.assertTrue(default_net(Action.ACTION_ALLOW))
            #            net_default = info()["default_net"]
            #            self.assertEqual(net_default, "Allow")
            #
            #            self.assertTrue(default_net(Action.ACTION_FILTER))
            #            net_default = info()["default_net"]
            #            self.assertEqual(net_default, "Filter")
            #
            #            self.assertTrue(default_net(Action.ACTION_STOP))
            #            net_default = info()["default_net"]
            #            self.assertEqual(net_default, "Stop")
            #
            #            self.assertTrue(default_net(Action.ACTION_KILL))
            #            net_default = info()["default_net"]
            #            self.assertEqual(net_default, "Kill")
            #
            #            # Ensure we reset to Deny last, so other tests are uneffected.
            #            self.assertTrue(default_net(Action.ACTION_DENY))
            #            net_default = info()["default_net"]
            #            self.assertEqual(net_default, "Deny")

            block_default = info()["default_block"]
            self.assertEqual(block_default, "Deny")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_block(Action.ACTION_ALLOW))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            self.assertTrue(default_block(Action.ACTION_FILTER))
            block_default = info()["default_block"]
            self.assertEqual(block_default, "Filter")

            self.assertTrue(default_block(Action.ACTION_STOP))
            block_default = info()["default_block"]
            self.assertEqual(block_default, "Stop")

            self.assertTrue(default_block(Action.ACTION_KILL))
            block_default = info()["default_block"]
            self.assertEqual(block_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_block(Action.ACTION_DENY))
            block_default = info()["default_block"]
            self.assertEqual(block_default, "Deny")

            mem_default = info()["default_mem"]
            self.assertEqual(mem_default, "Deny")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_mem(Action.ACTION_ALLOW))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            self.assertTrue(default_mem(Action.ACTION_FILTER))
            mem_default = info()["default_mem"]
            self.assertEqual(mem_default, "Filter")

            self.assertTrue(default_mem(Action.ACTION_STOP))
            mem_default = info()["default_mem"]
            self.assertEqual(mem_default, "Stop")

            self.assertTrue(default_mem(Action.ACTION_KILL))
            mem_default = info()["default_mem"]
            self.assertEqual(mem_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_mem(Action.ACTION_DENY))
            mem_default = info()["default_mem"]
            self.assertEqual(mem_default, "Deny")

            pid_default = info()["default_pid"]
            self.assertEqual(pid_default, "Kill")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_pid(Action.ACTION_ALLOW))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            self.assertTrue(default_pid(Action.ACTION_FILTER))
            pid_default = info()["default_pid"]
            self.assertEqual(pid_default, "Filter")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_pid(Action.ACTION_DENY))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_pid(Action.ACTION_STOP))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            # Ensure we reset to Kill last, so other tests are uneffected.
            self.assertTrue(default_pid(Action.ACTION_KILL))
            pid_default = info()["default_pid"]
            self.assertEqual(pid_default, "Kill")

            force_default = info()["default_force"]
            self.assertEqual(force_default, "Deny")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_force(Action.ACTION_ALLOW))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            self.assertTrue(default_force(Action.ACTION_WARN))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Warn")

            self.assertTrue(default_force(Action.ACTION_FILTER))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Filter")

            self.assertTrue(default_force(Action.ACTION_PANIC))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Panic")

            self.assertTrue(default_force(Action.ACTION_STOP))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Stop")

            self.assertTrue(default_force(Action.ACTION_KILL))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Kill")

            self.assertTrue(default_force(Action.ACTION_EXIT))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Exit")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_force(Action.ACTION_DENY))
            force_default = info()["default_force"]
            self.assertEqual(force_default, "Deny")

            segvguard_default = info()["default_segvguard"]
            self.assertEqual(segvguard_default, "Deny")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_segvguard(Action.ACTION_ALLOW))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            self.assertTrue(default_segvguard(Action.ACTION_FILTER))
            segvguard_default = info()["default_segvguard"]
            self.assertEqual(segvguard_default, "Filter")

            self.assertTrue(default_segvguard(Action.ACTION_STOP))
            segvguard_default = info()["default_segvguard"]
            self.assertEqual(segvguard_default, "Stop")

            self.assertTrue(default_segvguard(Action.ACTION_KILL))
            segvguard_default = info()["default_segvguard"]
            self.assertEqual(segvguard_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_segvguard(Action.ACTION_DENY))
            segvguard_default = info()["default_segvguard"]
            self.assertEqual(segvguard_default, "Deny")

            tpe_default = info()["default_tpe"]
            self.assertEqual(tpe_default, "Deny")

            with self.assertRaises(OSError) as cm:
                self.assertTrue(default_tpe(Action.ACTION_ALLOW))
            self.assertEqual(cm.exception.errno, errno.EINVAL)

            self.assertTrue(default_tpe(Action.ACTION_FILTER))
            tpe_default = info()["default_tpe"]
            self.assertEqual(tpe_default, "Filter")

            self.assertTrue(default_tpe(Action.ACTION_STOP))
            tpe_default = info()["default_tpe"]
            self.assertEqual(tpe_default, "Stop")

            self.assertTrue(default_tpe(Action.ACTION_KILL))
            tpe_default = info()["default_tpe"]
            self.assertEqual(tpe_default, "Kill")

            # Ensure we reset to Deny last, so other tests are uneffected.
            self.assertTrue(default_tpe(Action.ACTION_DENY))
            tpe_default = info()["default_tpe"]
            self.assertEqual(tpe_default, "Deny")

        def test_04_glob(self):
            path = "/tmp/pysyd"
            parg = path.encode("utf-8")

            rule = {"act": "Allow", "cap": "stat", "pat": path}
            self.assertTrue(stat_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(stat_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(stat_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(stat_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(stat_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(stat_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "stat", "pat": path}
            self.assertTrue(stat_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(stat_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(stat_add(Action.ACTION_DENY, parg))
            self.assertTrue(stat_add(Action.ACTION_DENY, parg))
            self.assertTrue(stat_add(Action.ACTION_DENY, parg))
            self.assertTrue(stat_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "stat", "pat": path}
            self.assertTrue(stat_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(stat_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(stat_add(Action.ACTION_FILTER, parg))
            self.assertTrue(stat_add(Action.ACTION_FILTER, parg))
            self.assertTrue(stat_add(Action.ACTION_FILTER, parg))
            self.assertTrue(stat_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "read", "pat": path}
            self.assertTrue(read_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(read_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(read_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(read_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(read_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(read_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "read", "pat": path}
            self.assertTrue(read_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(read_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(read_add(Action.ACTION_DENY, parg))
            self.assertTrue(read_add(Action.ACTION_DENY, parg))
            self.assertTrue(read_add(Action.ACTION_DENY, parg))
            self.assertTrue(read_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "read", "pat": path}
            self.assertTrue(read_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(read_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(read_add(Action.ACTION_FILTER, parg))
            self.assertTrue(read_add(Action.ACTION_FILTER, parg))
            self.assertTrue(read_add(Action.ACTION_FILTER, parg))
            self.assertTrue(read_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "write", "pat": path}
            self.assertTrue(write_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(write_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(write_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(write_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(write_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(write_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "write", "pat": path}
            self.assertTrue(write_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(write_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(write_add(Action.ACTION_DENY, parg))
            self.assertTrue(write_add(Action.ACTION_DENY, parg))
            self.assertTrue(write_add(Action.ACTION_DENY, parg))
            self.assertTrue(write_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "write", "pat": path}
            self.assertTrue(write_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(write_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(write_add(Action.ACTION_FILTER, parg))
            self.assertTrue(write_add(Action.ACTION_FILTER, parg))
            self.assertTrue(write_add(Action.ACTION_FILTER, parg))
            self.assertTrue(write_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "exec", "pat": path}
            self.assertTrue(exec_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(exec_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(exec_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(exec_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(exec_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(exec_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "exec", "pat": path}
            self.assertTrue(exec_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(exec_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(exec_add(Action.ACTION_DENY, parg))
            self.assertTrue(exec_add(Action.ACTION_DENY, parg))
            self.assertTrue(exec_add(Action.ACTION_DENY, parg))
            self.assertTrue(exec_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "exec", "pat": path}
            self.assertTrue(exec_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(exec_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(exec_add(Action.ACTION_FILTER, parg))
            self.assertTrue(exec_add(Action.ACTION_FILTER, parg))
            self.assertTrue(exec_add(Action.ACTION_FILTER, parg))
            self.assertTrue(exec_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "ioctl", "pat": path}
            self.assertTrue(ioctl_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(ioctl_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(ioctl_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(ioctl_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(ioctl_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(ioctl_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "ioctl", "pat": path}
            self.assertTrue(ioctl_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(ioctl_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(ioctl_add(Action.ACTION_DENY, parg))
            self.assertTrue(ioctl_add(Action.ACTION_DENY, parg))
            self.assertTrue(ioctl_add(Action.ACTION_DENY, parg))
            self.assertTrue(ioctl_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "ioctl", "pat": path}
            self.assertTrue(ioctl_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(ioctl_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(ioctl_add(Action.ACTION_FILTER, parg))
            self.assertTrue(ioctl_add(Action.ACTION_FILTER, parg))
            self.assertTrue(ioctl_add(Action.ACTION_FILTER, parg))
            self.assertTrue(ioctl_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "create", "pat": path}
            self.assertTrue(create_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(create_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(create_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(create_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(create_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(create_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "create", "pat": path}
            self.assertTrue(create_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(create_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(create_add(Action.ACTION_DENY, parg))
            self.assertTrue(create_add(Action.ACTION_DENY, parg))
            self.assertTrue(create_add(Action.ACTION_DENY, parg))
            self.assertTrue(create_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "create", "pat": path}
            self.assertTrue(create_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(create_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(create_add(Action.ACTION_FILTER, parg))
            self.assertTrue(create_add(Action.ACTION_FILTER, parg))
            self.assertTrue(create_add(Action.ACTION_FILTER, parg))
            self.assertTrue(create_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "delete", "pat": path}
            self.assertTrue(delete_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(delete_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(delete_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(delete_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(delete_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(delete_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "delete", "pat": path}
            self.assertTrue(delete_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(delete_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(delete_add(Action.ACTION_DENY, parg))
            self.assertTrue(delete_add(Action.ACTION_DENY, parg))
            self.assertTrue(delete_add(Action.ACTION_DENY, parg))
            self.assertTrue(delete_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "delete", "pat": path}
            self.assertTrue(delete_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(delete_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(delete_add(Action.ACTION_FILTER, parg))
            self.assertTrue(delete_add(Action.ACTION_FILTER, parg))
            self.assertTrue(delete_add(Action.ACTION_FILTER, parg))
            self.assertTrue(delete_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "rename", "pat": path}
            self.assertTrue(rename_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(rename_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(rename_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(rename_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(rename_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(rename_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "rename", "pat": path}
            self.assertTrue(rename_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(rename_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(rename_add(Action.ACTION_DENY, parg))
            self.assertTrue(rename_add(Action.ACTION_DENY, parg))
            self.assertTrue(rename_add(Action.ACTION_DENY, parg))
            self.assertTrue(rename_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "rename", "pat": path}
            self.assertTrue(rename_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(rename_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(rename_add(Action.ACTION_FILTER, parg))
            self.assertTrue(rename_add(Action.ACTION_FILTER, parg))
            self.assertTrue(rename_add(Action.ACTION_FILTER, parg))
            self.assertTrue(rename_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "symlink", "pat": path}
            self.assertTrue(symlink_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(symlink_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(symlink_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(symlink_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(symlink_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(symlink_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "symlink", "pat": path}
            self.assertTrue(symlink_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(symlink_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(symlink_add(Action.ACTION_DENY, parg))
            self.assertTrue(symlink_add(Action.ACTION_DENY, parg))
            self.assertTrue(symlink_add(Action.ACTION_DENY, parg))
            self.assertTrue(symlink_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "symlink", "pat": path}
            self.assertTrue(symlink_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(symlink_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(symlink_add(Action.ACTION_FILTER, parg))
            self.assertTrue(symlink_add(Action.ACTION_FILTER, parg))
            self.assertTrue(symlink_add(Action.ACTION_FILTER, parg))
            self.assertTrue(symlink_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "truncate", "pat": path}
            self.assertTrue(truncate_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(truncate_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(truncate_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(truncate_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(truncate_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(truncate_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "truncate", "pat": path}
            self.assertTrue(truncate_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(truncate_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(truncate_add(Action.ACTION_DENY, parg))
            self.assertTrue(truncate_add(Action.ACTION_DENY, parg))
            self.assertTrue(truncate_add(Action.ACTION_DENY, parg))
            self.assertTrue(truncate_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "truncate", "pat": path}
            self.assertTrue(truncate_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(truncate_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(truncate_add(Action.ACTION_FILTER, parg))
            self.assertTrue(truncate_add(Action.ACTION_FILTER, parg))
            self.assertTrue(truncate_add(Action.ACTION_FILTER, parg))
            self.assertTrue(truncate_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "chdir", "pat": path}
            self.assertTrue(chdir_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chdir_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chdir_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "chdir", "pat": path}
            self.assertTrue(chdir_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chdir_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(chdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(chdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(chdir_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "chdir", "pat": path}
            self.assertTrue(chdir_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chdir_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chdir_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "readdir", "pat": path}
            self.assertTrue(readdir_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(readdir_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(readdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(readdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(readdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(readdir_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "readdir", "pat": path}
            self.assertTrue(readdir_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(readdir_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(readdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(readdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(readdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(readdir_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "readdir", "pat": path}
            self.assertTrue(readdir_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(readdir_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(readdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(readdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(readdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(readdir_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "mkdir", "pat": path}
            self.assertTrue(mkdir_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkdir_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkdir_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkdir_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "mkdir", "pat": path}
            self.assertTrue(mkdir_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkdir_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkdir_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkdir_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "mkdir", "pat": path}
            self.assertTrue(mkdir_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkdir_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkdir_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkdir_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "chown", "pat": path}
            self.assertTrue(chown_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chown_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chown_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chown_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chown_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chown_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "chown", "pat": path}
            self.assertTrue(chown_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chown_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chown_add(Action.ACTION_DENY, parg))
            self.assertTrue(chown_add(Action.ACTION_DENY, parg))
            self.assertTrue(chown_add(Action.ACTION_DENY, parg))
            self.assertTrue(chown_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "chown", "pat": path}
            self.assertTrue(chown_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chown_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chown_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chown_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chown_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chown_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "chgrp", "pat": path}
            self.assertTrue(chgrp_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chgrp_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chgrp_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chgrp_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chgrp_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chgrp_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "chgrp", "pat": path}
            self.assertTrue(chgrp_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chgrp_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chgrp_add(Action.ACTION_DENY, parg))
            self.assertTrue(chgrp_add(Action.ACTION_DENY, parg))
            self.assertTrue(chgrp_add(Action.ACTION_DENY, parg))
            self.assertTrue(chgrp_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "chgrp", "pat": path}
            self.assertTrue(chgrp_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chgrp_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chgrp_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chgrp_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chgrp_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chgrp_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "chmod", "pat": path}
            self.assertTrue(chmod_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chmod_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chmod_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chmod_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chmod_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chmod_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "chmod", "pat": path}
            self.assertTrue(chmod_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chmod_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chmod_add(Action.ACTION_DENY, parg))
            self.assertTrue(chmod_add(Action.ACTION_DENY, parg))
            self.assertTrue(chmod_add(Action.ACTION_DENY, parg))
            self.assertTrue(chmod_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "chmod", "pat": path}
            self.assertTrue(chmod_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chmod_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chmod_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chmod_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chmod_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chmod_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "chattr", "pat": path}
            self.assertTrue(chattr_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chattr_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chattr_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chattr_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chattr_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(chattr_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "chattr", "pat": path}
            self.assertTrue(chattr_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chattr_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chattr_add(Action.ACTION_DENY, parg))
            self.assertTrue(chattr_add(Action.ACTION_DENY, parg))
            self.assertTrue(chattr_add(Action.ACTION_DENY, parg))
            self.assertTrue(chattr_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "chattr", "pat": path}
            self.assertTrue(chattr_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(chattr_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(chattr_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chattr_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chattr_add(Action.ACTION_FILTER, parg))
            self.assertTrue(chattr_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            # Chroot is startup only since 3.32.4
            #            rule = {"act": "Allow", "cap": "chroot", "pat": path}
            #            self.assertTrue(chroot_add(Action.ACTION_ALLOW, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertEqual(idx, len(rules) - 1)
            #
            #            self.assertTrue(chroot_del(Action.ACTION_ALLOW, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertIsNone(idx)
            #
            #            self.assertTrue(chroot_add(Action.ACTION_ALLOW, parg))
            #            self.assertTrue(chroot_add(Action.ACTION_ALLOW, parg))
            #            self.assertTrue(chroot_add(Action.ACTION_ALLOW, parg))
            #            self.assertTrue(chroot_rem(Action.ACTION_ALLOW, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertIsNone(idx)
            #
            #            rule = {"act": "Deny", "cap": "chroot", "pat": path}
            #            self.assertTrue(chroot_add(Action.ACTION_DENY, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertEqual(idx, len(rules) - 1)
            #
            #            self.assertTrue(chroot_del(Action.ACTION_DENY, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertIsNone(idx)
            #
            #            self.assertTrue(chroot_add(Action.ACTION_DENY, parg))
            #            self.assertTrue(chroot_add(Action.ACTION_DENY, parg))
            #            self.assertTrue(chroot_add(Action.ACTION_DENY, parg))
            #            self.assertTrue(chroot_rem(Action.ACTION_DENY, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertIsNone(idx)
            #
            #            rule = {"act": "Filter", "cap": "chroot", "pat": path}
            #            self.assertTrue(chroot_add(Action.ACTION_FILTER, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertEqual(idx, len(rules) - 1)
            #
            #            self.assertTrue(chroot_del(Action.ACTION_FILTER, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertIsNone(idx)
            #
            #            self.assertTrue(chroot_add(Action.ACTION_FILTER, parg))
            #            self.assertTrue(chroot_add(Action.ACTION_FILTER, parg))
            #            self.assertTrue(chroot_add(Action.ACTION_FILTER, parg))
            #            self.assertTrue(chroot_rem(Action.ACTION_FILTER, parg))
            #            rules = info()["glob_rules"]
            #            idx = self.find(rules, rule)
            #            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "utime", "pat": path}
            self.assertTrue(utime_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(utime_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(utime_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(utime_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(utime_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(utime_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "utime", "pat": path}
            self.assertTrue(utime_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(utime_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(utime_add(Action.ACTION_DENY, parg))
            self.assertTrue(utime_add(Action.ACTION_DENY, parg))
            self.assertTrue(utime_add(Action.ACTION_DENY, parg))
            self.assertTrue(utime_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "utime", "pat": path}
            self.assertTrue(utime_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(utime_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(utime_add(Action.ACTION_FILTER, parg))
            self.assertTrue(utime_add(Action.ACTION_FILTER, parg))
            self.assertTrue(utime_add(Action.ACTION_FILTER, parg))
            self.assertTrue(utime_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "mkdev", "pat": path}
            self.assertTrue(mkdev_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkdev_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkdev_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkdev_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkdev_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkdev_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "mkdev", "pat": path}
            self.assertTrue(mkdev_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkdev_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkdev_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkdev_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkdev_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkdev_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "mkdev", "pat": path}
            self.assertTrue(mkdev_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkdev_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkdev_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkdev_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkdev_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkdev_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "mkfifo", "pat": path}
            self.assertTrue(mkfifo_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkfifo_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkfifo_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkfifo_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkfifo_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mkfifo_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "mkfifo", "pat": path}
            self.assertTrue(mkfifo_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkfifo_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkfifo_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkfifo_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkfifo_add(Action.ACTION_DENY, parg))
            self.assertTrue(mkfifo_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "mkfifo", "pat": path}
            self.assertTrue(mkfifo_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mkfifo_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mkfifo_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkfifo_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkfifo_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mkfifo_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "mktemp", "pat": path}
            self.assertTrue(mktemp_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mktemp_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mktemp_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mktemp_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mktemp_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(mktemp_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "mktemp", "pat": path}
            self.assertTrue(mktemp_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mktemp_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mktemp_add(Action.ACTION_DENY, parg))
            self.assertTrue(mktemp_add(Action.ACTION_DENY, parg))
            self.assertTrue(mktemp_add(Action.ACTION_DENY, parg))
            self.assertTrue(mktemp_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "mktemp", "pat": path}
            self.assertTrue(mktemp_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(mktemp_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(mktemp_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mktemp_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mktemp_add(Action.ACTION_FILTER, parg))
            self.assertTrue(mktemp_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "net/sendfd", "pat": path}
            self.assertTrue(net_sendfd_add(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_sendfd_del(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_sendfd_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(net_sendfd_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(net_sendfd_add(Action.ACTION_ALLOW, parg))
            self.assertTrue(net_sendfd_rem(Action.ACTION_ALLOW, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "net/sendfd", "pat": path}
            self.assertTrue(net_sendfd_add(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_sendfd_del(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_sendfd_add(Action.ACTION_DENY, parg))
            self.assertTrue(net_sendfd_add(Action.ACTION_DENY, parg))
            self.assertTrue(net_sendfd_add(Action.ACTION_DENY, parg))
            self.assertTrue(net_sendfd_rem(Action.ACTION_DENY, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "net/sendfd", "pat": path}
            self.assertTrue(net_sendfd_add(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_sendfd_del(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_sendfd_add(Action.ACTION_FILTER, parg))
            self.assertTrue(net_sendfd_add(Action.ACTION_FILTER, parg))
            self.assertTrue(net_sendfd_add(Action.ACTION_FILTER, parg))
            self.assertTrue(net_sendfd_rem(Action.ACTION_FILTER, parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

        def test_05_addr(self):
            host = "127.3.1.4/8"
            port = 16
            addr = f"{host}!{port}"
            aarg = addr.encode("utf-8")

            rule = {
                "act": "Allow",
                "cap": "net/bind",
                "pat": {"addr": host, "port": port},
            }
            self.assertTrue(net_bind_add(Action.ACTION_ALLOW, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_bind_del(Action.ACTION_ALLOW, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_bind_add(Action.ACTION_ALLOW, aarg))
            self.assertTrue(net_bind_add(Action.ACTION_ALLOW, aarg))
            self.assertTrue(net_bind_add(Action.ACTION_ALLOW, aarg))
            self.assertTrue(net_bind_rem(Action.ACTION_ALLOW, aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {
                "act": "Deny",
                "cap": "net/bind",
                "pat": {"addr": host, "port": port},
            }
            self.assertTrue(net_bind_add(Action.ACTION_DENY, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_bind_del(Action.ACTION_DENY, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_bind_add(Action.ACTION_DENY, aarg))
            self.assertTrue(net_bind_add(Action.ACTION_DENY, aarg))
            self.assertTrue(net_bind_add(Action.ACTION_DENY, aarg))
            self.assertTrue(net_bind_rem(Action.ACTION_DENY, aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {
                "act": "Filter",
                "cap": "net/bind",
                "pat": {"addr": host, "port": port},
            }
            self.assertTrue(net_bind_add(Action.ACTION_FILTER, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_bind_del(Action.ACTION_FILTER, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_bind_add(Action.ACTION_FILTER, aarg))
            self.assertTrue(net_bind_add(Action.ACTION_FILTER, aarg))
            self.assertTrue(net_bind_add(Action.ACTION_FILTER, aarg))
            self.assertTrue(net_bind_rem(Action.ACTION_FILTER, aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {
                "act": "Allow",
                "cap": "net/connect",
                "pat": {"addr": host, "port": port},
            }
            self.assertTrue(net_connect_add(Action.ACTION_ALLOW, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_connect_del(Action.ACTION_ALLOW, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_connect_add(Action.ACTION_ALLOW, aarg))
            self.assertTrue(net_connect_add(Action.ACTION_ALLOW, aarg))
            self.assertTrue(net_connect_add(Action.ACTION_ALLOW, aarg))
            self.assertTrue(net_connect_rem(Action.ACTION_ALLOW, aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {
                "act": "Deny",
                "cap": "net/connect",
                "pat": {"addr": host, "port": port},
            }
            self.assertTrue(net_connect_add(Action.ACTION_DENY, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_connect_del(Action.ACTION_DENY, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_connect_add(Action.ACTION_DENY, aarg))
            self.assertTrue(net_connect_add(Action.ACTION_DENY, aarg))
            self.assertTrue(net_connect_add(Action.ACTION_DENY, aarg))
            self.assertTrue(net_connect_rem(Action.ACTION_DENY, aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {
                "act": "Filter",
                "cap": "net/connect",
                "pat": {"addr": host, "port": port},
            }
            self.assertTrue(net_connect_add(Action.ACTION_FILTER, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(net_connect_del(Action.ACTION_FILTER, aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(net_connect_add(Action.ACTION_FILTER, aarg))
            self.assertTrue(net_connect_add(Action.ACTION_FILTER, aarg))
            self.assertTrue(net_connect_add(Action.ACTION_FILTER, aarg))
            self.assertTrue(net_connect_rem(Action.ACTION_FILTER, aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

        def test_06_ioctl(self):
            self.assertTrue(ioctl_deny(0xDEADCA11))

        def test_07_force(self):
            sha = "0" * 128
            path = "/tmp/pysyd"
            parg = path.encode("utf-8")
            rule = {"act": "Kill", "sha": sha, "pat": path}

            self.assertTrue(force_add(parg, sha, Action.ACTION_KILL))
            rules = info()["force_rules"]
            idx = self.find(rules, rule)
            self.assertIsNotNone(idx)

            self.assertTrue(force_del(parg))
            rules = info()["force_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(
                force_add(f"{path}_1".encode("utf-8"), sha, Action.ACTION_WARN)
            )
            self.assertTrue(
                force_add(f"{path}_2".encode("utf-8"), sha, Action.ACTION_KILL)
            )
            self.assertTrue(force_clr())
            rules = info()["force_rules"]
            self.assertFalse(rules)

        def test_08_segvguard(self):
            segvguard_expiry_orig = info()["segvguard_expiry"]
            with self.assertRaises(ValueError):
                segvguard_expiry(-1)
            self.assertTrue(segvguard_expiry(4096))
            self.assertEqual(info()["segvguard_expiry"], 4096)
            self.assertTrue(segvguard_expiry(8192))
            self.assertEqual(info()["segvguard_expiry"], 8192)
            segvguard_expiry(segvguard_expiry_orig)

            segvguard_suspension_orig = info()["segvguard_suspension"]
            with self.assertRaises(ValueError):
                segvguard_suspension(-1)
            self.assertTrue(segvguard_suspension(4096))
            self.assertEqual(info()["segvguard_suspension"], 4096)
            self.assertTrue(segvguard_suspension(8192))
            self.assertEqual(info()["segvguard_suspension"], 8192)
            segvguard_suspension(segvguard_suspension_orig)

            segvguard_maxcrashes_orig = info()["segvguard_maxcrashes"]
            with self.assertRaises(ValueError):
                segvguard_maxcrashes(-1)
            self.assertTrue(segvguard_maxcrashes(40))
            self.assertEqual(info()["segvguard_maxcrashes"], 40)
            self.assertTrue(segvguard_maxcrashes(81))
            self.assertEqual(info()["segvguard_maxcrashes"], 81)
            segvguard_maxcrashes(segvguard_maxcrashes_orig)

        def test_09_exec(self):
            with tempfile.TemporaryDirectory() as temp_dir:
                # Path to the temporary file
                temp_file = os.path.join(temp_dir, "file")

                # Prepare command and arguments
                file = b"/bin/sh"
                argv = [b"-c", b'echo 42 > "' + temp_file.encode() + b'"']

                # Call syd_exec
                self.assertTrue(exec(file, argv))

                # Wait for syd to execute the process.
                time.sleep(3)

                # Assert the contents of the file
                with open(temp_file, "r") as f:
                    contents = f.read().strip()
                    self.assertEqual(contents, "42")

        def test_10_load(self):
            with tempfile.TemporaryFile() as temp_file:
                temp_file.write(
                    b"""
pid/max:77
"""
                )
                temp_file.seek(0)
                load(temp_file.fileno())
                self.assertEqual(77, info()["pid_max"])

        # This _must_ be the final test,
        # because it locks the sandbox!!
        def test_11_lock(self):
            self.assertTrue(lock(LockState.LOCK_OFF))
            self.assertTrue(lock(LockState.LOCK_EXEC))
            self.assertTrue(lock(LockState.LOCK_ON))

            with self.assertRaises(OSError) as cm:
                lock(LockState.LOCK_OFF)
            self.assertEqual(cm.exception.errno, errno.ENOENT)

            with self.assertRaises(OSError) as cm:
                lock(LockState.LOCK_EXEC)
            self.assertEqual(cm.exception.errno, errno.ENOENT)

            with self.assertRaises(OSError) as cm:
                lock(LockState.LOCK_ON)
            self.assertEqual(cm.exception.errno, errno.ENOENT)

    unittest.main(verbosity=2)
