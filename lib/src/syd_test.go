// Syd: rock-solid application kernel
// lib/src/syd_test.go: Tests for Go bindings of libsyd, the syd API C Library
// Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: LGPL-3.0

// Package syd provides Go bindings for the libsyd C library.
package syd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"syscall"
	"testing"
	"time"
)

// Function to check if a slice contains a specific string
func contains(slice []string, value string) bool {
	for _, item := range slice {
		if item == value {
			return true
		}
	}
	return false
}

func Test_01_Api(t *testing.T) {
	api, err := Api()
	if err != nil {
		t.Fatalf("Api failed: %v", err)
	}
	if api != 3 {
		t.Errorf("Api is not 3!")
	}
}

func Test_02_Stat(t *testing.T) {
	// Stat Test
	state := EnabledStat()
	if err := EnableStat(); err != nil {
		t.Fatalf("EnableStat failed: %v", err)
	}
	if enabled := EnabledStat(); !enabled {
		t.Error("Expected Stat to be enabled")
	}
	if err := DisableStat(); err != nil {
		t.Fatalf("DisableStat failed: %v", err)
	}
	if enabled := EnabledStat(); enabled {
		t.Error("Expected Stat to be disabled")
	}
	if state {
		EnableStat()
	} else {
		DisableStat()
	}

	// Read Test
	state = EnabledRead()
	if err := EnableRead(); err != nil {
		t.Fatalf("EnableRead failed: %v", err)
	}
	if enabled := EnabledRead(); !enabled {
		t.Error("Expected Read to be enabled")
	}
	if err := DisableRead(); err != nil {
		t.Fatalf("DisableRead failed: %v", err)
	}
	if enabled := EnabledRead(); enabled {
		t.Error("Expected Read to be disabled")
	}
	if state {
		EnableRead()
	} else {
		DisableRead()
	}

	// Write Test
	state = EnabledWrite()
	if err := EnableWrite(); err != nil {
		t.Fatalf("EnableWrite failed: %v", err)
	}
	if enabled := EnabledWrite(); !enabled {
		t.Error("Expected Write to be enabled")
	}
	if err := DisableWrite(); err != nil {
		t.Fatalf("DisableWrite failed: %v", err)
	}
	if enabled := EnabledWrite(); enabled {
		t.Error("Expected Write to be disabled")
	}
	if state {
		EnableWrite()
	} else {
		DisableWrite()
	}

	// Exec Test
	state = EnabledExec()
	if err := EnableExec(); err != nil {
		t.Fatalf("EnableExec failed: %v", err)
	}
	if enabled := EnabledExec(); !enabled {
		t.Error("Expected Exec to be enabled")
	}
	if err := DisableExec(); err != nil {
		t.Fatalf("DisableExec failed: %v", err)
	}
	if enabled := EnabledExec(); enabled {
		t.Error("Expected Exec to be disabled")
	}
	if state {
		EnableExec()
	} else {
		DisableExec()
	}

	// Ioctl Test
	state = EnabledIoctl()
	if err := EnableIoctl(); err != nil {
		t.Fatalf("EnableIoctl failed: %v", err)
	}
	if enabled := EnabledIoctl(); !enabled {
		t.Error("Expected Ioctl to be enabled")
	}
	if err := DisableIoctl(); err != nil {
		t.Fatalf("DisableIoctl failed: %v", err)
	}
	if enabled := EnabledIoctl(); enabled {
		t.Error("Expected Ioctl to be disabled")
	}
	if state {
		EnableIoctl()
	} else {
		DisableIoctl()
	}

	// Create Test
	state = EnabledCreate()
	if err := EnableCreate(); err != nil {
		t.Fatalf("EnableCreate failed: %v", err)
	}
	if enabled := EnabledCreate(); !enabled {
		t.Error("Expected Create to be enabled")
	}
	if err := DisableCreate(); err != nil {
		t.Fatalf("DisableCreate failed: %v", err)
	}
	if enabled := EnabledCreate(); enabled {
		t.Error("Expected Create to be disabled")
	}
	if state {
		EnableCreate()
	} else {
		DisableCreate()
	}

	// Delete Test
	state = EnabledDelete()
	if err := EnableDelete(); err != nil {
		t.Fatalf("EnableDelete failed: %v", err)
	}
	if enabled := EnabledDelete(); !enabled {
		t.Error("Expected Delete to be enabled")
	}
	if err := DisableDelete(); err != nil {
		t.Fatalf("DisableDelete failed: %v", err)
	}
	if enabled := EnabledDelete(); enabled {
		t.Error("Expected Delete to be disabled")
	}
	if state {
		EnableDelete()
	} else {
		DisableDelete()
	}

	// Rename Test
	state = EnabledRename()
	if err := EnableRename(); err != nil {
		t.Fatalf("EnableRename failed: %v", err)
	}
	if enabled := EnabledRename(); !enabled {
		t.Error("Expected Rename to be enabled")
	}
	if err := DisableRename(); err != nil {
		t.Fatalf("DisableRename failed: %v", err)
	}
	if enabled := EnabledRename(); enabled {
		t.Error("Expected Rename to be disabled")
	}
	if state {
		EnableRename()
	} else {
		DisableRename()
	}

	// Symlink Test
	state = EnabledSymlink()
	if err := EnableSymlink(); err != nil {
		t.Fatalf("EnableSymlink failed: %v", err)
	}
	if enabled := EnabledSymlink(); !enabled {
		t.Error("Expected Symlink to be enabled")
	}
	if err := DisableSymlink(); err != nil {
		t.Fatalf("DisableSymlink failed: %v", err)
	}
	if enabled := EnabledSymlink(); enabled {
		t.Error("Expected Symlink to be disabled")
	}
	if state {
		EnableSymlink()
	} else {
		DisableSymlink()
	}

	// Truncate Test
	state = EnabledTruncate()
	if err := EnableTruncate(); err != nil {
		t.Fatalf("EnableTruncate failed: %v", err)
	}
	if enabled := EnabledTruncate(); !enabled {
		t.Error("Expected Truncate to be enabled")
	}
	if err := DisableTruncate(); err != nil {
		t.Fatalf("DisableTruncate failed: %v", err)
	}
	if enabled := EnabledTruncate(); enabled {
		t.Error("Expected Truncate to be disabled")
	}
	if state {
		EnableTruncate()
	} else {
		DisableTruncate()
	}

	// Chdir Test
	state = EnabledChdir()
	if err := EnableChdir(); err != nil {
		t.Fatalf("EnableChdir failed: %v", err)
	}
	if enabled := EnabledChdir(); !enabled {
		t.Error("Expected Chdir to be enabled")
	}
	if err := DisableChdir(); err != nil {
		t.Fatalf("DisableChdir failed: %v", err)
	}
	if enabled := EnabledChdir(); enabled {
		t.Error("Expected Chdir to be disabled")
	}
	if state {
		EnableChdir()
	} else {
		DisableChdir()
	}

	// Readdir Test
	state = EnabledReaddir()
	if err := EnableReaddir(); err != nil {
		t.Fatalf("EnableReaddir failed: %v", err)
	}
	if enabled := EnabledReaddir(); !enabled {
		t.Error("Expected Readdir to be enabled")
	}
	if err := DisableReaddir(); err != nil {
		t.Fatalf("DisableReaddir failed: %v", err)
	}
	if enabled := EnabledReaddir(); enabled {
		t.Error("Expected Readdir to be disabled")
	}
	if state {
		EnableReaddir()
	} else {
		DisableReaddir()
	}

	// Mkdir Test
	state = EnabledMkdir()
	if err := EnableMkdir(); err != nil {
		t.Fatalf("EnableMkdir failed: %v", err)
	}
	if enabled := EnabledMkdir(); !enabled {
		t.Error("Expected Mkdir to be enabled")
	}
	if err := DisableMkdir(); err != nil {
		t.Fatalf("DisableMkdir failed: %v", err)
	}
	if enabled := EnabledMkdir(); enabled {
		t.Error("Expected Mkdir to be disabled")
	}
	if state {
		EnableMkdir()
	} else {
		DisableMkdir()
	}

	// Chown Test
	state = EnabledChown()
	if err := EnableChown(); err != nil {
		t.Fatalf("EnableChown failed: %v", err)
	}
	if enabled := EnabledChown(); !enabled {
		t.Error("Expected Chown to be enabled")
	}
	if err := DisableChown(); err != nil {
		t.Fatalf("DisableChown failed: %v", err)
	}
	if enabled := EnabledChown(); enabled {
		t.Error("Expected Chown to be disabled")
	}
	if state {
		EnableChown()
	} else {
		DisableChown()
	}

	// Chgrp Test
	state = EnabledChgrp()
	if err := EnableChgrp(); err != nil {
		t.Fatalf("EnableChgrp failed: %v", err)
	}
	if enabled := EnabledChgrp(); !enabled {
		t.Error("Expected Chgrp to be enabled")
	}
	if err := DisableChgrp(); err != nil {
		t.Fatalf("DisableChgrp failed: %v", err)
	}
	if enabled := EnabledChgrp(); enabled {
		t.Error("Expected Chgrp to be disabled")
	}
	if state {
		EnableChgrp()
	} else {
		DisableChgrp()
	}

	// Chmod Test
	state = EnabledChmod()
	if err := EnableChmod(); err != nil {
		t.Fatalf("EnableChmod failed: %v", err)
	}
	if enabled := EnabledChmod(); !enabled {
		t.Error("Expected Chmod to be enabled")
	}
	if err := DisableChmod(); err != nil {
		t.Fatalf("DisableChmod failed: %v", err)
	}
	if enabled := EnabledChmod(); enabled {
		t.Error("Expected Chmod to be disabled")
	}
	if state {
		EnableChmod()
	} else {
		DisableChmod()
	}

	// Chattr Test
	state = EnabledChattr()
	if err := EnableChattr(); err != nil {
		t.Fatalf("EnableChattr failed: %v", err)
	}
	if enabled := EnabledChattr(); !enabled {
		t.Error("Expected Chattr to be enabled")
	}
	if err := DisableChattr(); err != nil {
		t.Fatalf("DisableChattr failed: %v", err)
	}
	if enabled := EnabledChattr(); enabled {
		t.Error("Expected Chattr to be disabled")
	}
	if state {
		EnableChattr()
	} else {
		DisableChattr()
	}

	/* Chroot is startup only since 3.32.4
	// Chroot Test
	state = EnabledChroot()
	if err := EnableChroot(); err != nil {
		t.Fatalf("EnableChroot failed: %v", err)
	}
	if enabled := EnabledChroot(); !enabled {
		t.Error("Expected Chroot to be enabled")
	}
	if err := DisableChroot(); err != nil {
		t.Fatalf("DisableChroot failed: %v", err)
	}
	if enabled := EnabledChroot(); enabled {
		t.Error("Expected Chroot to be disabled")
	}
	if state {
		EnableChroot()
	} else {
		DisableChroot()
	}
	*/

	// Utime Test
	state = EnabledUtime()
	if err := EnableUtime(); err != nil {
		t.Fatalf("EnableUtime failed: %v", err)
	}
	if enabled := EnabledUtime(); !enabled {
		t.Error("Expected Utime to be enabled")
	}
	if err := DisableUtime(); err != nil {
		t.Fatalf("DisableUtime failed: %v", err)
	}
	if enabled := EnabledUtime(); enabled {
		t.Error("Expected Utime to be disabled")
	}
	if state {
		EnableUtime()
	} else {
		DisableUtime()
	}

	// Mkdev Test
	state = EnabledMkdev()
	if err := EnableMkdev(); err != nil {
		t.Fatalf("EnableMkdev failed: %v", err)
	}
	if enabled := EnabledMkdev(); !enabled {
		t.Error("Expected Mkdev to be enabled")
	}
	if err := DisableMkdev(); err != nil {
		t.Fatalf("DisableMkdev failed: %v", err)
	}
	if enabled := EnabledMkdev(); enabled {
		t.Error("Expected Mkdev to be disabled")
	}
	if state {
		EnableMkdev()
	} else {
		DisableMkdev()
	}

	// Mkfifo Test
	state = EnabledMkfifo()
	if err := EnableMkfifo(); err != nil {
		t.Fatalf("EnableMkfifo failed: %v", err)
	}
	if enabled := EnabledMkfifo(); !enabled {
		t.Error("Expected Mkfifo to be enabled")
	}
	if err := DisableMkfifo(); err != nil {
		t.Fatalf("DisableMkfifo failed: %v", err)
	}
	if enabled := EnabledMkfifo(); enabled {
		t.Error("Expected Mkfifo to be disabled")
	}
	if state {
		EnableMkfifo()
	} else {
		DisableMkfifo()
	}

	// Mktemp Test
	state = EnabledMktemp()
	if err := EnableMktemp(); err != nil {
		t.Fatalf("EnableMktemp failed: %v", err)
	}
	if enabled := EnabledMktemp(); !enabled {
		t.Error("Expected Mktemp to be enabled")
	}
	if err := DisableMktemp(); err != nil {
		t.Fatalf("DisableMktemp failed: %v", err)
	}
	if enabled := EnabledMktemp(); enabled {
		t.Error("Expected Mktemp to be disabled")
	}
	if state {
		EnableMktemp()
	} else {
		DisableMktemp()
	}

	// Net Test
	state = EnabledNet()
	if err := EnableNet(); err != nil {
		t.Fatalf("EnableNet failed: %v", err)
	}
	if enabled := EnabledNet(); !enabled {
		t.Error("Expected Net to be enabled")
	}
	if err := DisableNet(); err != nil {
		t.Fatalf("DisableNet failed: %v", err)
	}
	if enabled := EnabledNet(); enabled {
		t.Error("Expected Net to be disabled")
	}
	if state {
		EnableNet()
	} else {
		DisableNet()
	}

	// Force Test
	state = EnabledForce()
	if err := EnableForce(); err != nil {
		t.Fatalf("EnableForce failed: %v", err)
	}
	if enabled := EnabledForce(); !enabled {
		t.Error("Expected Force to be enabled")
	}
	if err := DisableForce(); err != nil {
		t.Fatalf("DisableForce failed: %v", err)
	}
	if enabled := EnabledForce(); enabled {
		t.Error("Expected Force to be disabled")
	}
	if state {
		EnableForce()
	} else {
		DisableForce()
	}

	// Mem Test
	state = EnabledMem()
	if err := EnableMem(); err != nil {
		t.Fatalf("EnableMem failed: %v", err)
	}
	if enabled := EnabledMem(); !enabled {
		t.Error("Expected Mem to be enabled")
	}
	if err := DisableMem(); err != nil {
		t.Fatalf("DisableMem failed: %v", err)
	}
	if enabled := EnabledMem(); enabled {
		t.Error("Expected Mem to be disabled")
	}
	if state {
		EnableMem()
	} else {
		DisableMem()
	}

	// Pid Test
	state = EnabledPid()
	if err := EnablePid(); err != nil {
		t.Fatalf("EnablePid failed: %v", err)
	}
	if enabled := EnabledPid(); !enabled {
		t.Error("Expected Pid to be enabled")
	}
	if err := DisablePid(); err != nil {
		t.Fatalf("DisablePid failed: %v", err)
	}
	if enabled := EnabledPid(); enabled {
		t.Error("Expected Pid to be disabled")
	}
	if state {
		EnablePid()
	} else {
		DisablePid()
	}

	// TPE Test
	state = EnabledTPE()
	if err := EnableTPE(); err != nil {
		t.Fatalf("EnableTPE failed: %v", err)
	}
	if enabled := EnabledTPE(); !enabled {
		t.Error("Expected TPE to be enabled")
	}
	if err := DisableTPE(); err != nil {
		t.Fatalf("DisableTPE failed: %v", err)
	}
	if enabled := EnabledTPE(); enabled {
		t.Error("Expected TPE to be disabled")
	}
	if state {
		EnableTPE()
	} else {
		DisableTPE()
	}

	// Testing memory limits
	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	memMaxOrig := sandbox.MemMax
	memVmMaxOrig := sandbox.MemVmMax
	pidMaxOrig := sandbox.PidMax

	// Test setting MemMax
	if err := MemMax("1G"); err != nil {
		t.Fatalf("MemMax(1G) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.MemMax != 1024*1024*1024 {
		t.Errorf("Expected MemMax to be %d, got %d", 1024*1024*1024, sandbox.MemMax)
	}
	MemMax(strconv.FormatInt(memMaxOrig, 10)) // Resetting to original

	// Similar tests for MemVmMax...
	if err := MemVmMax("1G"); err != nil {
		t.Fatalf("MemVmMax(1G) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.MemVmMax != 1024*1024*1024 {
		t.Errorf("Expected MemVmMax to be %d, got %d", 1024*1024*1024, sandbox.MemVmMax)
	}
	MemVmMax(strconv.FormatInt(memVmMaxOrig, 10)) // Resetting to original

	// Test setting PidMax
	if err := PidMax(-1); err == nil {
		t.Error("Expected PidMax(-1) to fail")
	}
	if err := PidMax(4096); err != nil {
		t.Fatalf("PidMax(4096) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.PidMax != 4096 {
		t.Errorf("Expected PidMax to be 4096, got %d", sandbox.PidMax)
	}
	PidMax(pidMaxOrig) // Resetting to original

	// Test appending to IoctlDeny
	if err := IoctlDeny(0xdeadca11); err != nil {
		t.Errorf("IoctlDeny(0xdeadca11) failed: %v", err)
	}
}

func Test_03_Default(t *testing.T) {
	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Deny" {
		t.Errorf("Expected DefaultStat to be Deny, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionAllow); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Allow" {
		t.Errorf("Expected DefaultStat to be Allow, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionWarn); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Warn" {
		t.Errorf("Expected DefaultStat to be Warn, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionFilter); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Filter" {
		t.Errorf("Expected DefaultStat to be Filter, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionPanic); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Panic" {
		t.Errorf("Expected DefaultStat to be Panic, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionStop); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Stop" {
		t.Errorf("Expected DefaultStat to be Stop, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionKill); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Kill" {
		t.Errorf("Expected DefaultStat to be Kill, got %s", sandbox.DefaultStat)
	}
	if err := DefaultStat(ActionExit); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Exit" {
		t.Errorf("Expected DefaultStat to be Exit, got %s", sandbox.DefaultStat)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultStat(ActionDeny); err != nil {
		t.Errorf("DefaultStat failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultStat != "Deny" {
		t.Errorf("Expected DefaultStat to be Deny, got %s", sandbox.DefaultStat)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Deny" {
		t.Errorf("Expected DefaultRead to be Deny, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionAllow); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Allow" {
		t.Errorf("Expected DefaultRead to be Allow, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionWarn); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Warn" {
		t.Errorf("Expected DefaultRead to be Warn, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionFilter); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Filter" {
		t.Errorf("Expected DefaultRead to be Filter, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionPanic); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Panic" {
		t.Errorf("Expected DefaultRead to be Panic, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionStop); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Stop" {
		t.Errorf("Expected DefaultRead to be Stop, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionKill); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Kill" {
		t.Errorf("Expected DefaultRead to be Kill, got %s", sandbox.DefaultRead)
	}
	if err := DefaultRead(ActionExit); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Exit" {
		t.Errorf("Expected DefaultRead to be Exit, got %s", sandbox.DefaultRead)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultRead(ActionDeny); err != nil {
		t.Errorf("DefaultRead failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRead != "Deny" {
		t.Errorf("Expected DefaultRead to be Deny, got %s", sandbox.DefaultRead)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Deny" {
		t.Errorf("Expected DefaultWrite to be Deny, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionAllow); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Allow" {
		t.Errorf("Expected DefaultWrite to be Allow, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionWarn); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Warn" {
		t.Errorf("Expected DefaultWrite to be Warn, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionFilter); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Filter" {
		t.Errorf("Expected DefaultWrite to be Filter, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionPanic); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Panic" {
		t.Errorf("Expected DefaultWrite to be Panic, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionStop); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Stop" {
		t.Errorf("Expected DefaultWrite to be Stop, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionKill); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Kill" {
		t.Errorf("Expected DefaultWrite to be Kill, got %s", sandbox.DefaultWrite)
	}
	if err := DefaultWrite(ActionExit); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Exit" {
		t.Errorf("Expected DefaultWrite to be Exit, got %s", sandbox.DefaultWrite)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultWrite(ActionDeny); err != nil {
		t.Errorf("DefaultWrite failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultWrite != "Deny" {
		t.Errorf("Expected DefaultWrite to be Deny, got %s", sandbox.DefaultWrite)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Deny" {
		t.Errorf("Expected DefaultExec to be Deny, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionAllow); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Allow" {
		t.Errorf("Expected DefaultExec to be Allow, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionWarn); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Warn" {
		t.Errorf("Expected DefaultExec to be Warn, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionFilter); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Filter" {
		t.Errorf("Expected DefaultExec to be Filter, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionPanic); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Panic" {
		t.Errorf("Expected DefaultExec to be Panic, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionStop); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Stop" {
		t.Errorf("Expected DefaultExec to be Stop, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionKill); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Kill" {
		t.Errorf("Expected DefaultExec to be Kill, got %s", sandbox.DefaultExec)
	}
	if err := DefaultExec(ActionExit); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Exit" {
		t.Errorf("Expected DefaultExec to be Exit, got %s", sandbox.DefaultExec)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultExec(ActionDeny); err != nil {
		t.Errorf("DefaultExec failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultExec != "Deny" {
		t.Errorf("Expected DefaultExec to be Deny, got %s", sandbox.DefaultExec)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Deny" {
		t.Errorf("Expected DefaultIoctl to be Deny, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionAllow); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Allow" {
		t.Errorf("Expected DefaultIoctl to be Allow, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionWarn); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Warn" {
		t.Errorf("Expected DefaultIoctl to be Warn, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionFilter); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Filter" {
		t.Errorf("Expected DefaultIoctl to be Filter, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionPanic); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Panic" {
		t.Errorf("Expected DefaultIoctl to be Panic, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionStop); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Stop" {
		t.Errorf("Expected DefaultIoctl to be Stop, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionKill); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Kill" {
		t.Errorf("Expected DefaultIoctl to be Kill, got %s", sandbox.DefaultIoctl)
	}
	if err := DefaultIoctl(ActionExit); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Exit" {
		t.Errorf("Expected DefaultIoctl to be Exit, got %s", sandbox.DefaultIoctl)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultIoctl(ActionDeny); err != nil {
		t.Errorf("DefaultIoctl failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultIoctl != "Deny" {
		t.Errorf("Expected DefaultIoctl to be Deny, got %s", sandbox.DefaultIoctl)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Deny" {
		t.Errorf("Expected DefaultCreate to be Deny, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionAllow); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Allow" {
		t.Errorf("Expected DefaultCreate to be Allow, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionWarn); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Warn" {
		t.Errorf("Expected DefaultCreate to be Warn, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionFilter); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Filter" {
		t.Errorf("Expected DefaultCreate to be Filter, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionPanic); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Panic" {
		t.Errorf("Expected DefaultCreate to be Panic, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionStop); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Stop" {
		t.Errorf("Expected DefaultCreate to be Stop, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionKill); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Kill" {
		t.Errorf("Expected DefaultCreate to be Kill, got %s", sandbox.DefaultCreate)
	}
	if err := DefaultCreate(ActionExit); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Exit" {
		t.Errorf("Expected DefaultCreate to be Exit, got %s", sandbox.DefaultCreate)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultCreate(ActionDeny); err != nil {
		t.Errorf("DefaultCreate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultCreate != "Deny" {
		t.Errorf("Expected DefaultCreate to be Deny, got %s", sandbox.DefaultCreate)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Deny" {
		t.Errorf("Expected DefaultDelete to be Deny, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionAllow); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Allow" {
		t.Errorf("Expected DefaultDelete to be Allow, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionWarn); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Warn" {
		t.Errorf("Expected DefaultDelete to be Warn, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionFilter); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Filter" {
		t.Errorf("Expected DefaultDelete to be Filter, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionPanic); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Panic" {
		t.Errorf("Expected DefaultDelete to be Panic, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionStop); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Stop" {
		t.Errorf("Expected DefaultDelete to be Stop, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionKill); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Kill" {
		t.Errorf("Expected DefaultDelete to be Kill, got %s", sandbox.DefaultDelete)
	}
	if err := DefaultDelete(ActionExit); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Exit" {
		t.Errorf("Expected DefaultDelete to be Exit, got %s", sandbox.DefaultDelete)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultDelete(ActionDeny); err != nil {
		t.Errorf("DefaultDelete failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultDelete != "Deny" {
		t.Errorf("Expected DefaultDelete to be Deny, got %s", sandbox.DefaultDelete)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Deny" {
		t.Errorf("Expected DefaultRename to be Deny, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionAllow); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Allow" {
		t.Errorf("Expected DefaultRename to be Allow, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionWarn); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Warn" {
		t.Errorf("Expected DefaultRename to be Warn, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionFilter); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Filter" {
		t.Errorf("Expected DefaultRename to be Filter, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionPanic); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Panic" {
		t.Errorf("Expected DefaultRename to be Panic, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionStop); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Stop" {
		t.Errorf("Expected DefaultRename to be Stop, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionKill); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Kill" {
		t.Errorf("Expected DefaultRename to be Kill, got %s", sandbox.DefaultRename)
	}
	if err := DefaultRename(ActionExit); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Exit" {
		t.Errorf("Expected DefaultRename to be Exit, got %s", sandbox.DefaultRename)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultRename(ActionDeny); err != nil {
		t.Errorf("DefaultRename failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultRename != "Deny" {
		t.Errorf("Expected DefaultRename to be Deny, got %s", sandbox.DefaultRename)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Deny" {
		t.Errorf("Expected DefaultSymlink to be Deny, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionAllow); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Allow" {
		t.Errorf("Expected DefaultSymlink to be Allow, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionWarn); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Warn" {
		t.Errorf("Expected DefaultSymlink to be Warn, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionFilter); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Filter" {
		t.Errorf("Expected DefaultSymlink to be Filter, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionPanic); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Panic" {
		t.Errorf("Expected DefaultSymlink to be Panic, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionStop); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Stop" {
		t.Errorf("Expected DefaultSymlink to be Stop, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionKill); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Kill" {
		t.Errorf("Expected DefaultSymlink to be Kill, got %s", sandbox.DefaultSymlink)
	}
	if err := DefaultSymlink(ActionExit); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Exit" {
		t.Errorf("Expected DefaultSymlink to be Exit, got %s", sandbox.DefaultSymlink)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultSymlink(ActionDeny); err != nil {
		t.Errorf("DefaultSymlink failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultSymlink != "Deny" {
		t.Errorf("Expected DefaultSymlink to be Deny, got %s", sandbox.DefaultSymlink)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Deny" {
		t.Errorf("Expected DefaultTruncate to be Deny, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionAllow); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Allow" {
		t.Errorf("Expected DefaultTruncate to be Allow, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionWarn); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Warn" {
		t.Errorf("Expected DefaultTruncate to be Warn, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionFilter); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Filter" {
		t.Errorf("Expected DefaultTruncate to be Filter, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionPanic); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Panic" {
		t.Errorf("Expected DefaultTruncate to be Panic, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionStop); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Stop" {
		t.Errorf("Expected DefaultTruncate to be Stop, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionKill); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Kill" {
		t.Errorf("Expected DefaultTruncate to be Kill, got %s", sandbox.DefaultTruncate)
	}
	if err := DefaultTruncate(ActionExit); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Exit" {
		t.Errorf("Expected DefaultTruncate to be Exit, got %s", sandbox.DefaultTruncate)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultTruncate(ActionDeny); err != nil {
		t.Errorf("DefaultTruncate failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTruncate != "Deny" {
		t.Errorf("Expected DefaultTruncate to be Deny, got %s", sandbox.DefaultTruncate)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Deny" {
		t.Errorf("Expected DefaultChdir to be Deny, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionAllow); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Allow" {
		t.Errorf("Expected DefaultChdir to be Allow, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionWarn); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Warn" {
		t.Errorf("Expected DefaultChdir to be Warn, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionFilter); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Filter" {
		t.Errorf("Expected DefaultChdir to be Filter, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionPanic); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Panic" {
		t.Errorf("Expected DefaultChdir to be Panic, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionStop); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Stop" {
		t.Errorf("Expected DefaultChdir to be Stop, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionKill); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Kill" {
		t.Errorf("Expected DefaultChdir to be Kill, got %s", sandbox.DefaultChdir)
	}
	if err := DefaultChdir(ActionExit); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Exit" {
		t.Errorf("Expected DefaultChdir to be Exit, got %s", sandbox.DefaultChdir)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultChdir(ActionDeny); err != nil {
		t.Errorf("DefaultChdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChdir != "Deny" {
		t.Errorf("Expected DefaultChdir to be Deny, got %s", sandbox.DefaultChdir)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Deny" {
		t.Errorf("Expected DefaultReaddir to be Deny, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionAllow); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Allow" {
		t.Errorf("Expected DefaultReaddir to be Allow, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionWarn); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Warn" {
		t.Errorf("Expected DefaultReaddir to be Warn, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionFilter); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Filter" {
		t.Errorf("Expected DefaultReaddir to be Filter, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionPanic); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Panic" {
		t.Errorf("Expected DefaultReaddir to be Panic, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionStop); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Stop" {
		t.Errorf("Expected DefaultReaddir to be Stop, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionKill); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Kill" {
		t.Errorf("Expected DefaultReaddir to be Kill, got %s", sandbox.DefaultReaddir)
	}
	if err := DefaultReaddir(ActionExit); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Exit" {
		t.Errorf("Expected DefaultReaddir to be Exit, got %s", sandbox.DefaultReaddir)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultReaddir(ActionDeny); err != nil {
		t.Errorf("DefaultReaddir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultReaddir != "Deny" {
		t.Errorf("Expected DefaultReaddir to be Deny, got %s", sandbox.DefaultReaddir)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Deny" {
		t.Errorf("Expected DefaultMkdir to be Deny, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionAllow); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Allow" {
		t.Errorf("Expected DefaultMkdir to be Allow, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionWarn); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Warn" {
		t.Errorf("Expected DefaultMkdir to be Warn, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionFilter); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Filter" {
		t.Errorf("Expected DefaultMkdir to be Filter, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionPanic); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Panic" {
		t.Errorf("Expected DefaultMkdir to be Panic, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionStop); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Stop" {
		t.Errorf("Expected DefaultMkdir to be Stop, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionKill); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Kill" {
		t.Errorf("Expected DefaultMkdir to be Kill, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionExit); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Exit" {
		t.Errorf("Expected DefaultMkdir to be Exit, got %s", sandbox.DefaultMkdir)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultMkdir(ActionDeny); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Deny" {
		t.Errorf("Expected DefaultMkdir to be Deny, got %s", sandbox.DefaultMkdir)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Deny" {
		t.Errorf("Expected DefaultMkdir to be Deny, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionAllow); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Allow" {
		t.Errorf("Expected DefaultMkdir to be Allow, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionWarn); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Warn" {
		t.Errorf("Expected DefaultMkdir to be Warn, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionFilter); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Filter" {
		t.Errorf("Expected DefaultMkdir to be Filter, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionPanic); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Panic" {
		t.Errorf("Expected DefaultMkdir to be Panic, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionStop); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Stop" {
		t.Errorf("Expected DefaultMkdir to be Stop, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionKill); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Kill" {
		t.Errorf("Expected DefaultMkdir to be Kill, got %s", sandbox.DefaultMkdir)
	}
	if err := DefaultMkdir(ActionExit); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Exit" {
		t.Errorf("Expected DefaultMkdir to be Exit, got %s", sandbox.DefaultMkdir)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultMkdir(ActionDeny); err != nil {
		t.Errorf("DefaultMkdir failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdir != "Deny" {
		t.Errorf("Expected DefaultMkdir to be Deny, got %s", sandbox.DefaultMkdir)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Deny" {
		t.Errorf("Expected DefaultChown to be Deny, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionAllow); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Allow" {
		t.Errorf("Expected DefaultChown to be Allow, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionWarn); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Warn" {
		t.Errorf("Expected DefaultChown to be Warn, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionFilter); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Filter" {
		t.Errorf("Expected DefaultChown to be Filter, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionPanic); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Panic" {
		t.Errorf("Expected DefaultChown to be Panic, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionStop); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Stop" {
		t.Errorf("Expected DefaultChown to be Stop, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionKill); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Kill" {
		t.Errorf("Expected DefaultChown to be Kill, got %s", sandbox.DefaultChown)
	}
	if err := DefaultChown(ActionExit); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Exit" {
		t.Errorf("Expected DefaultChown to be Exit, got %s", sandbox.DefaultChown)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultChown(ActionDeny); err != nil {
		t.Errorf("DefaultChown failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChown != "Deny" {
		t.Errorf("Expected DefaultChown to be Deny, got %s", sandbox.DefaultChown)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Deny" {
		t.Errorf("Expected DefaultChgrp to be Deny, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionAllow); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Allow" {
		t.Errorf("Expected DefaultChgrp to be Allow, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionWarn); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Warn" {
		t.Errorf("Expected DefaultChgrp to be Warn, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionFilter); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Filter" {
		t.Errorf("Expected DefaultChgrp to be Filter, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionPanic); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Panic" {
		t.Errorf("Expected DefaultChgrp to be Panic, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionStop); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Stop" {
		t.Errorf("Expected DefaultChgrp to be Stop, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionKill); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Kill" {
		t.Errorf("Expected DefaultChgrp to be Kill, got %s", sandbox.DefaultChgrp)
	}
	if err := DefaultChgrp(ActionExit); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Exit" {
		t.Errorf("Expected DefaultChgrp to be Exit, got %s", sandbox.DefaultChgrp)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultChgrp(ActionDeny); err != nil {
		t.Errorf("DefaultChgrp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChgrp != "Deny" {
		t.Errorf("Expected DefaultChgrp to be Deny, got %s", sandbox.DefaultChgrp)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Deny" {
		t.Errorf("Expected DefaultChmod to be Deny, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionAllow); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Allow" {
		t.Errorf("Expected DefaultChmod to be Allow, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionWarn); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Warn" {
		t.Errorf("Expected DefaultChmod to be Warn, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionFilter); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Filter" {
		t.Errorf("Expected DefaultChmod to be Filter, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionPanic); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Panic" {
		t.Errorf("Expected DefaultChmod to be Panic, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionStop); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Stop" {
		t.Errorf("Expected DefaultChmod to be Stop, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionKill); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Kill" {
		t.Errorf("Expected DefaultChmod to be Kill, got %s", sandbox.DefaultChmod)
	}
	if err := DefaultChmod(ActionExit); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Exit" {
		t.Errorf("Expected DefaultChmod to be Exit, got %s", sandbox.DefaultChmod)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultChmod(ActionDeny); err != nil {
		t.Errorf("DefaultChmod failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChmod != "Deny" {
		t.Errorf("Expected DefaultChmod to be Deny, got %s", sandbox.DefaultChmod)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Deny" {
		t.Errorf("Expected DefaultChattr to be Deny, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionAllow); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Allow" {
		t.Errorf("Expected DefaultChattr to be Allow, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionWarn); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Warn" {
		t.Errorf("Expected DefaultChattr to be Warn, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionFilter); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Filter" {
		t.Errorf("Expected DefaultChattr to be Filter, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionPanic); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Panic" {
		t.Errorf("Expected DefaultChattr to be Panic, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionStop); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Stop" {
		t.Errorf("Expected DefaultChattr to be Stop, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionKill); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Kill" {
		t.Errorf("Expected DefaultChattr to be Kill, got %s", sandbox.DefaultChattr)
	}
	if err := DefaultChattr(ActionExit); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Exit" {
		t.Errorf("Expected DefaultChattr to be Exit, got %s", sandbox.DefaultChattr)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultChattr(ActionDeny); err != nil {
		t.Errorf("DefaultChattr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChattr != "Deny" {
		t.Errorf("Expected DefaultChattr to be Deny, got %s", sandbox.DefaultChattr)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Deny" {
		t.Errorf("Expected DefaultChroot to be Deny, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionAllow); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Allow" {
		t.Errorf("Expected DefaultChroot to be Allow, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionWarn); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Warn" {
		t.Errorf("Expected DefaultChroot to be Warn, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionFilter); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Filter" {
		t.Errorf("Expected DefaultChroot to be Filter, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionPanic); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Panic" {
		t.Errorf("Expected DefaultChroot to be Panic, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionStop); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Stop" {
		t.Errorf("Expected DefaultChroot to be Stop, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionKill); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Kill" {
		t.Errorf("Expected DefaultChroot to be Kill, got %s", sandbox.DefaultChroot)
	}
	if err := DefaultChroot(ActionExit); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Exit" {
		t.Errorf("Expected DefaultChroot to be Exit, got %s", sandbox.DefaultChroot)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultChroot(ActionDeny); err != nil {
		t.Errorf("DefaultChroot failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultChroot != "Deny" {
		t.Errorf("Expected DefaultChroot to be Deny, got %s", sandbox.DefaultChroot)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Deny" {
		t.Errorf("Expected DefaultUtime to be Deny, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionAllow); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Allow" {
		t.Errorf("Expected DefaultUtime to be Allow, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionWarn); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Warn" {
		t.Errorf("Expected DefaultUtime to be Warn, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionFilter); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Filter" {
		t.Errorf("Expected DefaultUtime to be Filter, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionPanic); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Panic" {
		t.Errorf("Expected DefaultUtime to be Panic, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionStop); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Stop" {
		t.Errorf("Expected DefaultUtime to be Stop, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionKill); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Kill" {
		t.Errorf("Expected DefaultUtime to be Kill, got %s", sandbox.DefaultUtime)
	}
	if err := DefaultUtime(ActionExit); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Exit" {
		t.Errorf("Expected DefaultUtime to be Exit, got %s", sandbox.DefaultUtime)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultUtime(ActionDeny); err != nil {
		t.Errorf("DefaultUtime failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultUtime != "Deny" {
		t.Errorf("Expected DefaultUtime to be Deny, got %s", sandbox.DefaultUtime)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Deny" {
		t.Errorf("Expected DefaultMkdev to be Deny, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionAllow); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Allow" {
		t.Errorf("Expected DefaultMkdev to be Allow, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionWarn); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Warn" {
		t.Errorf("Expected DefaultMkdev to be Warn, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionFilter); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Filter" {
		t.Errorf("Expected DefaultMkdev to be Filter, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionPanic); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Panic" {
		t.Errorf("Expected DefaultMkdev to be Panic, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionStop); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Stop" {
		t.Errorf("Expected DefaultMkdev to be Stop, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionKill); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Kill" {
		t.Errorf("Expected DefaultMkdev to be Kill, got %s", sandbox.DefaultMkdev)
	}
	if err := DefaultMkdev(ActionExit); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Exit" {
		t.Errorf("Expected DefaultMkdev to be Exit, got %s", sandbox.DefaultMkdev)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultMkdev(ActionDeny); err != nil {
		t.Errorf("DefaultMkdev failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkdev != "Deny" {
		t.Errorf("Expected DefaultMkdev to be Deny, got %s", sandbox.DefaultMkdev)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Deny" {
		t.Errorf("Expected DefaultMkfifo to be Deny, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionAllow); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Allow" {
		t.Errorf("Expected DefaultMkfifo to be Allow, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionWarn); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Warn" {
		t.Errorf("Expected DefaultMkfifo to be Warn, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionFilter); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Filter" {
		t.Errorf("Expected DefaultMkfifo to be Filter, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionPanic); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Panic" {
		t.Errorf("Expected DefaultMkfifo to be Panic, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionStop); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Stop" {
		t.Errorf("Expected DefaultMkfifo to be Stop, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionKill); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Kill" {
		t.Errorf("Expected DefaultMkfifo to be Kill, got %s", sandbox.DefaultMkfifo)
	}
	if err := DefaultMkfifo(ActionExit); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Exit" {
		t.Errorf("Expected DefaultMkfifo to be Exit, got %s", sandbox.DefaultMkfifo)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultMkfifo(ActionDeny); err != nil {
		t.Errorf("DefaultMkfifo failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMkfifo != "Deny" {
		t.Errorf("Expected DefaultMkfifo to be Deny, got %s", sandbox.DefaultMkfifo)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Deny" {
		t.Errorf("Expected DefaultMktemp to be Deny, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionAllow); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Allow" {
		t.Errorf("Expected DefaultMktemp to be Allow, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionWarn); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Warn" {
		t.Errorf("Expected DefaultMktemp to be Warn, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionFilter); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Filter" {
		t.Errorf("Expected DefaultMktemp to be Filter, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionPanic); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Panic" {
		t.Errorf("Expected DefaultMktemp to be Panic, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionStop); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Stop" {
		t.Errorf("Expected DefaultMktemp to be Stop, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionKill); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Kill" {
		t.Errorf("Expected DefaultMktemp to be Kill, got %s", sandbox.DefaultMktemp)
	}
	if err := DefaultMktemp(ActionExit); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Exit" {
		t.Errorf("Expected DefaultMktemp to be Exit, got %s", sandbox.DefaultMktemp)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultMktemp(ActionDeny); err != nil {
		t.Errorf("DefaultMktemp failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMktemp != "Deny" {
		t.Errorf("Expected DefaultMktemp to be Deny, got %s", sandbox.DefaultMktemp)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Deny" {
		t.Errorf("Expected DefaultForce to be Deny, got %s", sandbox.DefaultForce)
	}
	if err := DefaultForce(ActionAllow); err == nil {
		t.Errorf("DefaultForce did not fail with ActionAllow")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultForce(ActionWarn); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Warn" {
		t.Errorf("Expected DefaultForce to be Warn, got %s", sandbox.DefaultForce)
	}
	if err := DefaultForce(ActionFilter); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Filter" {
		t.Errorf("Expected DefaultForce to be Filter, got %s", sandbox.DefaultForce)
	}
	if err := DefaultForce(ActionPanic); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Panic" {
		t.Errorf("Expected DefaultForce to be Panic, got %s", sandbox.DefaultForce)
	}
	if err := DefaultForce(ActionStop); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Stop" {
		t.Errorf("Expected DefaultForce to be Stop, got %s", sandbox.DefaultForce)
	}
	if err := DefaultForce(ActionKill); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Kill" {
		t.Errorf("Expected DefaultForce to be Kill, got %s", sandbox.DefaultForce)
	}
	if err := DefaultForce(ActionExit); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Exit" {
		t.Errorf("Expected DefaultForce to be Exit, got %s", sandbox.DefaultForce)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultForce(ActionDeny); err != nil {
		t.Errorf("DefaultForce failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultForce != "Deny" {
		t.Errorf("Expected DefaultForce to be Deny, got %s", sandbox.DefaultForce)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Deny" {
		t.Errorf("Expected DefaultMem to be Deny, got %s", sandbox.DefaultMem)
	}
	if err := DefaultMem(ActionAllow); err == nil {
		t.Errorf("DefaultMem did not fail with ActionAllow")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultMem(ActionWarn); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Warn" {
		t.Errorf("Expected DefaultMem to be Warn, got %s", sandbox.DefaultMem)
	}
	if err := DefaultMem(ActionFilter); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Filter" {
		t.Errorf("Expected DefaultMem to be Filter, got %s", sandbox.DefaultMem)
	}
	if err := DefaultMem(ActionPanic); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Panic" {
		t.Errorf("Expected DefaultMem to be Panic, got %s", sandbox.DefaultMem)
	}
	if err := DefaultMem(ActionStop); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Stop" {
		t.Errorf("Expected DefaultMem to be Stop, got %s", sandbox.DefaultMem)
	}
	if err := DefaultMem(ActionKill); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Kill" {
		t.Errorf("Expected DefaultMem to be Kill, got %s", sandbox.DefaultMem)
	}
	if err := DefaultMem(ActionExit); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Exit" {
		t.Errorf("Expected DefaultMem to be Exit, got %s", sandbox.DefaultMem)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultMem(ActionDeny); err != nil {
		t.Errorf("DefaultMem failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultMem != "Deny" {
		t.Errorf("Expected DefaultMem to be Deny, got %s", sandbox.DefaultMem)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultPid != "Kill" {
		t.Errorf("Expected DefaultPid to be Kill, got %s", sandbox.DefaultPid)
	}
	if err := DefaultPid(ActionAllow); err == nil {
		t.Errorf("DefaultPid did not fail with ActionAllow")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultPid(ActionWarn); err != nil {
		t.Errorf("DefaultPid failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultPid != "Warn" {
		t.Errorf("Expected DefaultPid to be Warn, got %s", sandbox.DefaultPid)
	}
	if err := DefaultPid(ActionFilter); err != nil {
		t.Errorf("DefaultPid failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultPid != "Filter" {
		t.Errorf("Expected DefaultPid to be Filter, got %s", sandbox.DefaultPid)
	}
	if err := DefaultPid(ActionDeny); err == nil {
		t.Errorf("DefaultPid did not fail with ActionDeny")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultPid(ActionPanic); err == nil {
		t.Errorf("DefaultPid did not fail with ActionPanic")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultPid(ActionStop); err == nil {
		t.Errorf("DefaultPid did not fail with ActionStop")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultPid(ActionExit); err != nil {
		t.Errorf("DefaultPid failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultPid != "Exit" {
		t.Errorf("Expected DefaultPid to be Exit, got %s", sandbox.DefaultPid)
	}
	// Ensure we reset to Kill last, so other tests are uneffected.
	if err := DefaultPid(ActionKill); err != nil {
		t.Errorf("DefaultPid failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultPid != "Kill" {
		t.Errorf("Expected DefaultPid to be Kill, got %s", sandbox.DefaultPid)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Deny" {
		t.Errorf("Expected DefaultTPE to be Deny, got %s", sandbox.DefaultTPE)
	}
	if err := DefaultTPE(ActionAllow); err == nil {
		t.Errorf("DefaultTPE did not fail with ActionAllow")
	} else if !errors.Is(err, syscall.EINVAL) {
		t.Errorf("Expected EINVAL error, got: %v", err)
	}
	if err := DefaultTPE(ActionWarn); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Warn" {
		t.Errorf("Expected DefaultTPE to be Warn, got %s", sandbox.DefaultTPE)
	}
	if err := DefaultTPE(ActionFilter); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Filter" {
		t.Errorf("Expected DefaultTPE to be Filter, got %s", sandbox.DefaultTPE)
	}
	if err := DefaultTPE(ActionPanic); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Panic" {
		t.Errorf("Expected DefaultTPE to be Panic, got %s", sandbox.DefaultTPE)
	}
	if err := DefaultTPE(ActionStop); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Stop" {
		t.Errorf("Expected DefaultTPE to be Stop, got %s", sandbox.DefaultTPE)
	}
	if err := DefaultTPE(ActionKill); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Kill" {
		t.Errorf("Expected DefaultTPE to be Kill, got %s", sandbox.DefaultTPE)
	}
	if err := DefaultTPE(ActionExit); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Exit" {
		t.Errorf("Expected DefaultTPE to be Exit, got %s", sandbox.DefaultTPE)
	}
	// Ensure we reset to Deny last, so other tests are uneffected.
	if err := DefaultTPE(ActionDeny); err != nil {
		t.Errorf("DefaultTPE failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.DefaultTPE != "Deny" {
		t.Errorf("Expected DefaultTPE to be Deny, got %s", sandbox.DefaultTPE)
	}
}

func Test_04_Glob(t *testing.T) {
	path := "/tmp/gosyd"

	testCases := []struct {
		AddFunc     func(Action, string) error
		DelFunc     func(Action, string) error
		RemFunc     func(Action, string) error
		Act         Action
		ActStr, Cap string
	}{
		{StatAdd, StatDel, StatRem, ActionAllow, "Allow", "stat"},
		{StatAdd, StatDel, StatRem, ActionWarn, "Warn", "stat"},
		{StatAdd, StatDel, StatRem, ActionFilter, "Filter", "stat"},
		{StatAdd, StatDel, StatRem, ActionPanic, "Panic", "stat"},
		{StatAdd, StatDel, StatRem, ActionStop, "Stop", "stat"},
		{StatAdd, StatDel, StatRem, ActionKill, "Kill", "stat"},
		{StatAdd, StatDel, StatRem, ActionExit, "Exit", "stat"},
		{StatAdd, StatDel, StatRem, ActionDeny, "Deny", "stat"},

		{ReadAdd, ReadDel, ReadRem, ActionAllow, "Allow", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionWarn, "Warn", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionFilter, "Filter", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionPanic, "Panic", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionStop, "Stop", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionKill, "Kill", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionExit, "Exit", "read"},
		{ReadAdd, ReadDel, ReadRem, ActionDeny, "Deny", "read"},

		{WriteAdd, WriteDel, WriteRem, ActionAllow, "Allow", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionWarn, "Warn", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionFilter, "Filter", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionPanic, "Panic", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionStop, "Stop", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionKill, "Kill", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionExit, "Exit", "write"},
		{WriteAdd, WriteDel, WriteRem, ActionDeny, "Deny", "write"},

		{ExecAdd, ExecDel, ExecRem, ActionAllow, "Allow", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionWarn, "Warn", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionFilter, "Filter", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionPanic, "Panic", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionStop, "Stop", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionKill, "Kill", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionExit, "Exit", "exec"},
		{ExecAdd, ExecDel, ExecRem, ActionDeny, "Deny", "exec"},

		{IoctlAdd, IoctlDel, IoctlRem, ActionAllow, "Allow", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionWarn, "Warn", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionFilter, "Filter", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionPanic, "Panic", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionStop, "Stop", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionKill, "Kill", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionExit, "Exit", "ioctl"},
		{IoctlAdd, IoctlDel, IoctlRem, ActionDeny, "Deny", "ioctl"},

		{CreateAdd, CreateDel, CreateRem, ActionAllow, "Allow", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionWarn, "Warn", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionFilter, "Filter", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionPanic, "Panic", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionStop, "Stop", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionKill, "Kill", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionExit, "Exit", "create"},
		{CreateAdd, CreateDel, CreateRem, ActionDeny, "Deny", "create"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionAllow, "Allow", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionWarn, "Warn", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionFilter, "Filter", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionPanic, "Panic", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionStop, "Stop", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionKill, "Kill", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionExit, "Exit", "delete"},
		{DeleteAdd, DeleteDel, DeleteRem, ActionDeny, "Deny", "delete"},

		{RenameAdd, RenameDel, RenameRem, ActionAllow, "Allow", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionWarn, "Warn", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionFilter, "Filter", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionPanic, "Panic", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionStop, "Stop", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionKill, "Kill", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionExit, "Exit", "rename"},
		{RenameAdd, RenameDel, RenameRem, ActionDeny, "Deny", "rename"},

		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionAllow, "Allow", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionWarn, "Warn", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionFilter, "Filter", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionPanic, "Panic", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionStop, "Stop", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionKill, "Kill", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionExit, "Exit", "symlink"},
		{SymlinkAdd, SymlinkDel, SymlinkRem, ActionDeny, "Deny", "symlink"},

		{TruncateAdd, TruncateDel, TruncateRem, ActionAllow, "Allow", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionWarn, "Warn", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionFilter, "Filter", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionPanic, "Panic", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionStop, "Stop", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionKill, "Kill", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionExit, "Exit", "truncate"},
		{TruncateAdd, TruncateDel, TruncateRem, ActionDeny, "Deny", "truncate"},

		{ChdirAdd, ChdirDel, ChdirRem, ActionAllow, "Allow", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionWarn, "Warn", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionFilter, "Filter", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionPanic, "Panic", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionStop, "Stop", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionKill, "Kill", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionExit, "Exit", "chdir"},
		{ChdirAdd, ChdirDel, ChdirRem, ActionDeny, "Deny", "chdir"},

		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionAllow, "Allow", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionWarn, "Warn", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionFilter, "Filter", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionPanic, "Panic", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionStop, "Stop", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionKill, "Kill", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionExit, "Exit", "readdir"},
		{ReaddirAdd, ReaddirDel, ReaddirRem, ActionDeny, "Deny", "readdir"},

		{MkdirAdd, MkdirDel, MkdirRem, ActionAllow, "Allow", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionWarn, "Warn", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionFilter, "Filter", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionPanic, "Panic", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionStop, "Stop", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionKill, "Kill", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionExit, "Exit", "mkdir"},
		{MkdirAdd, MkdirDel, MkdirRem, ActionDeny, "Deny", "mkdir"},

		{ChownAdd, ChownDel, ChownRem, ActionAllow, "Allow", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionWarn, "Warn", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionFilter, "Filter", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionPanic, "Panic", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionStop, "Stop", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionKill, "Kill", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionExit, "Exit", "chown"},
		{ChownAdd, ChownDel, ChownRem, ActionDeny, "Deny", "chown"},

		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionAllow, "Allow", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionWarn, "Warn", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionFilter, "Filter", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionPanic, "Panic", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionStop, "Stop", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionKill, "Kill", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionExit, "Exit", "chgrp"},
		{ChgrpAdd, ChgrpDel, ChgrpRem, ActionDeny, "Deny", "chgrp"},

		{ChmodAdd, ChmodDel, ChmodRem, ActionAllow, "Allow", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionWarn, "Warn", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionFilter, "Filter", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionPanic, "Panic", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionStop, "Stop", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionKill, "Kill", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionExit, "Exit", "chmod"},
		{ChmodAdd, ChmodDel, ChmodRem, ActionDeny, "Deny", "chmod"},

		{ChattrAdd, ChattrDel, ChattrRem, ActionAllow, "Allow", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionWarn, "Warn", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionFilter, "Filter", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionPanic, "Panic", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionStop, "Stop", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionKill, "Kill", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionExit, "Exit", "chattr"},
		{ChattrAdd, ChattrDel, ChattrRem, ActionDeny, "Deny", "chattr"},

		/* Chroot is startup only since 3.32.4
		{ChrootAdd, ChrootDel, ChrootRem, ActionAllow, "Allow", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionWarn, "Warn", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionFilter, "Filter", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionPanic, "Panic", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionStop, "Stop", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionKill, "Kill", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionExit, "Exit", "chroot"},
		{ChrootAdd, ChrootDel, ChrootRem, ActionDeny, "Deny", "chroot"},
		*/

		{UtimeAdd, UtimeDel, UtimeRem, ActionAllow, "Allow", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionWarn, "Warn", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionFilter, "Filter", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionPanic, "Panic", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionStop, "Stop", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionKill, "Kill", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionExit, "Exit", "utime"},
		{UtimeAdd, UtimeDel, UtimeRem, ActionDeny, "Deny", "utime"},

		{MkdevAdd, MkdevDel, MkdevRem, ActionAllow, "Allow", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionWarn, "Warn", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionFilter, "Filter", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionPanic, "Panic", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionStop, "Stop", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionKill, "Kill", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionExit, "Exit", "mkdev"},
		{MkdevAdd, MkdevDel, MkdevRem, ActionDeny, "Deny", "mkdev"},

		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionAllow, "Allow", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionWarn, "Warn", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionFilter, "Filter", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionPanic, "Panic", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionStop, "Stop", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionKill, "Kill", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionExit, "Exit", "mkfifo"},
		{MkfifoAdd, MkfifoDel, MkfifoRem, ActionDeny, "Deny", "mkfifo"},

		{MktempAdd, MktempDel, MktempRem, ActionAllow, "Allow", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionWarn, "Warn", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionFilter, "Filter", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionPanic, "Panic", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionStop, "Stop", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionKill, "Kill", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionExit, "Exit", "mktemp"},
		{MktempAdd, MktempDel, MktempRem, ActionDeny, "Deny", "mktemp"},

		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionAllow, "Allow", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionWarn, "Warn", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionFilter, "Filter", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionPanic, "Panic", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionStop, "Stop", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionKill, "Kill", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionExit, "Exit", "net/sendfd"},
		{NetSendFdAdd, NetSendFdDel, NetSendFdRem, ActionDeny, "Deny", "net/sendfd"},
	}

	for _, tc := range testCases {
		testName := tc.ActStr + tc.Cap
		rule := GlobRule{Act: tc.ActStr, Cap: tc.Cap, Pat: path}

		// Test Add
		err := tc.AddFunc(tc.Act, path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		sandbox, err := Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx := findGlob(sandbox.GlobRules, rule)
		if idx != len(sandbox.GlobRules)-1 {
			t.Errorf("Expected %s rule to be last, got index %d", testName, idx)
		}

		// Test Del
		err = tc.DelFunc(tc.Act, path)
		if err != nil {
			t.Fatalf("%sDel failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findGlob(sandbox.GlobRules, rule)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}

		// Test Add, Add, Add, Rem
		err = tc.AddFunc(tc.Act, path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(tc.Act, path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(tc.Act, path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.RemFunc(tc.Act, path)
		if err != nil {
			t.Fatalf("%sRem failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findGlob(sandbox.GlobRules, rule)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent after %sRem, got index %d", testName, testName, idx)
		}
	}
}

func Test_05_Cidr_Port_Single(t *testing.T) {
	host := "127.3.1.4/8"
	port := 16
	addr := host + "!" + fmt.Sprint(port)
	aarg := string(addr)

	testCases := []struct {
		AddFunc     func(Action, string) error
		DelFunc     func(Action, string) error
		RemFunc     func(Action, string) error
		Act         Action
		ActStr, Cap string
	}{
		{NetBindAdd, NetBindDel, NetBindRem, ActionAllow, "Allow", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionWarn, "Warn", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionFilter, "Filter", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionDeny, "Deny", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionStop, "Stop", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionKill, "Kill", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionExit, "Exit", "net/bind"},

		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionAllow, "Allow", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionWarn, "Warn", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionFilter, "Filter", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionDeny, "Deny", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionStop, "Stop", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionKill, "Kill", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionExit, "Exit", "net/connect"},
	}

	for _, tc := range testCases {
		testName := tc.ActStr + tc.Cap
		rule := CidrRule{
			Act: tc.ActStr,
			Cap: tc.Cap,
			Pat: Pattern{Addr: host, Port: port},
		}

		// Test Add
		err := tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		sandbox, err := Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx := findCidr(sandbox.CidrRules, rule.Pat)
		if idx != len(sandbox.CidrRules)-1 {
			t.Errorf("Expected %s rule to be last, got index %d. CIDR Rules: %+v", testName, idx, sandbox.CidrRules)
		}

		// Test Del
		err = tc.DelFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sDel failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}

		// Test Add, Add, Add, Rem
		err = tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.RemFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sRem failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}
	}
}

func Test_06_Cidr_Port_Double(t *testing.T) {
	host := "127.3.1.4/8"
	port := [2]int{1024, 65535}
	addr := host + "!" + fmt.Sprint(port[0]) + "-" + fmt.Sprint(port[1])
	aarg := string(addr)

	testCases := []struct {
		AddFunc     func(Action, string) error
		DelFunc     func(Action, string) error
		RemFunc     func(Action, string) error
		Act         Action
		ActStr, Cap string
	}{
		{NetBindAdd, NetBindDel, NetBindRem, ActionAllow, "Allow", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionWarn, "Warn", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionFilter, "Filter", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionDeny, "Deny", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionStop, "Stop", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionKill, "Kill", "net/bind"},
		{NetBindAdd, NetBindDel, NetBindRem, ActionExit, "Exit", "net/bind"},

		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionAllow, "Allow", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionWarn, "Warn", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionFilter, "Filter", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionDeny, "Deny", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionStop, "Stop", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionKill, "Kill", "net/connect"},
		{NetConnectAdd, NetConnectDel, NetConnectRem, ActionExit, "Exit", "net/connect"},
	}

	for _, tc := range testCases {
		testName := tc.ActStr + tc.Cap
		rule := CidrRule{
			Act: tc.ActStr,
			Cap: tc.Cap,
			Pat: Pattern{Addr: host, Port: port},
		}

		// Test Add
		err := tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		sandbox, err := Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx := findCidr(sandbox.CidrRules, rule.Pat)
		if idx != len(sandbox.CidrRules)-1 {
			t.Errorf("Expected %s rule to be last, got index %d. CIDR Rules: %+v", testName, idx, sandbox.CidrRules)
		}

		// Test Del
		err = tc.DelFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sDel failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}

		// Test Add, Add, Add, Rem
		err = tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.RemFunc(tc.Act, aarg)
		if err != nil {
			t.Fatalf("%sRem failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}
	}
}

func Test_07_Force(t *testing.T) {
	path := "/tmp/gosyd"
	hash := strings.Repeat("0", 128)
	rule := ForceRule{Act: "Kill", Sha: hash, Pat: path}

	// Assert ForceAdd
	err := ForceAdd(path, hash, ActionKill)
	if err != nil {
		t.Fatalf("ForceAdd failed: %v", err)
	}

	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	if !findForce(sandbox.ForceRules, rule) {
		t.Errorf("Expected rule to be present")
	}

	// Assert ForceDel
	err = ForceDel(path)
	if err != nil {
		t.Fatalf("ForceDel failed: %v", err)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	if findForce(sandbox.ForceRules, rule) {
		t.Errorf("Expected rule to be absent")
	}

	// Assert ForceClr
	path_1 := "/tmp/gosyd_1"
	path_2 := "/tmp/gosyd_2"
	err = ForceAdd(path_1, hash, ActionWarn)
	if err != nil {
		t.Fatalf("ForceAdd failed: %v", err)
	}
	err = ForceAdd(path_2, hash, ActionKill)
	if err != nil {
		t.Fatalf("ForceAdd failed: %v", err)
	}
	err = ForceClr()
	if err != nil {
		t.Fatalf("ForceClr failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	num_rules := len(sandbox.ForceRules)
	if num_rules != 0 {
		t.Errorf("Expected empty list, got %d elements!", num_rules)
	}
}

func Test_08_SegvGuard(t *testing.T) {
	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	segvGuardExpiryOrig := sandbox.SegvGuardExpiry
	segvGuardSuspensionOrig := sandbox.SegvGuardSuspension
	segvGuardMaxCrashesOrig := sandbox.SegvGuardMaxCrashes

	// Test setting SegvGuardExpiry
	if err := SegvGuardExpiry(4096); err != nil {
		t.Fatalf("SegvGuardExpiry(4096) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.SegvGuardExpiry != 4096 {
		t.Errorf("Expected SegvGuardExpiry to be 4096, got %d", sandbox.SegvGuardExpiry)
	}
	SegvGuardExpiry(segvGuardExpiryOrig) // Resetting to original

	// Test setting SegvGuardSuspension
	if err := SegvGuardSuspension(4096); err != nil {
		t.Fatalf("SegvGuardSuspension(4096) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.SegvGuardSuspension != 4096 {
		t.Errorf("Expected SegvGuardSuspension to be 4096, got %d", sandbox.SegvGuardSuspension)
	}
	SegvGuardSuspension(segvGuardSuspensionOrig) // Resetting to original

	// Test setting SegvGuardMaxCrashes
	if err := SegvGuardMaxCrashes(42); err != nil {
		t.Fatalf("SegvGuardMaxCrashes(42) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.SegvGuardMaxCrashes != 42 {
		t.Errorf("Expected SegvGuardMaxCrashes to be 42, got %d", sandbox.SegvGuardMaxCrashes)
	}
	SegvGuardMaxCrashes(segvGuardMaxCrashesOrig) // Resetting to original
}

func Test_09_Exec(t *testing.T) {
	// Create a temporary directory
	tempDir, err := ioutil.TempDir("", "syd_test")
	if err != nil {
		t.Fatalf("Failed to create temporary directory: %v", err)
	}
	defer os.RemoveAll(tempDir) // Clean up

	// Path to the temporary file
	tempFile := filepath.Join(tempDir, "file")

	// Prepare command and arguments
	file := "/bin/sh"
	argv := []string{"-c", "echo 42 > " + tempFile}

	// Call Exec
	err = Exec(file, argv)
	if err != nil {
		t.Fatalf("Exec failed: %v", err)
	}

	// Wait for the command to execute
	time.Sleep(3 * time.Second)

	// Assert the contents of the file
	contents, err := ioutil.ReadFile(tempFile)
	if err != nil {
		t.Fatalf("Failed to read from temporary file: %v", err)
	}

	if strings.TrimSpace(string(contents)) != "42" {
		t.Errorf("Expected file contents to be '42', got '%s'", contents)
	}
}

func Test_10_Load(t *testing.T) {
	// Create a temporary file
	tempFile, err := ioutil.TempFile("", "syd_test")
	if err != nil {
		t.Fatalf("Failed to create temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name()) // Clean up

	// Write test data to the temporary file
	_, err = tempFile.WriteString("pid/max:77\n")
	if err != nil {
		t.Fatalf("Failed to write to temporary file: %v", err)
	}

	// Seek back to the beginning of the file
	if _, err := tempFile.Seek(0, 0); err != nil {
		t.Fatalf("Failed to seek to beginning of the file: %v", err)
	}

	// Load the configuration from the temporary file
	if err := Load(int(tempFile.Fd())); err != nil {
		t.Fatalf("Load failed: %v", err)
	}

	// Retrieve the information using Info
	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	// Assert the pid_max value
	expectedPidMax := 77
	if sandbox.PidMax != expectedPidMax {
		t.Errorf("Expected pid_max to be %d, got %d", expectedPidMax, sandbox.PidMax)
	}
}

func Test_11_Lock(t *testing.T) {
	if err := Lock(LockOff); err != nil {
		errno := err.(syscall.Errno)
		t.Errorf("Lock(LockOff): %v", errno)
	}
	if err := Lock(LockExec); err != nil {
		errno := err.(syscall.Errno)
		t.Errorf("Lock(LockExec): %v", errno)
	}

	if err := Lock(LockOff); err != nil {
		errno := err.(syscall.Errno)
		if errno != syscall.ENOENT {
			t.Errorf("Lock(LockOff): %v", errno)
		}
	} else {
		t.Errorf("Lock(LockOff): 0")
	}

	if err := Lock(LockExec); err != nil {
		errno := err.(syscall.Errno)
		if errno != syscall.ENOENT {
			t.Errorf("Lock(LockExec): %v", errno)
		}
	} else {
		t.Errorf("Lock(LockExec): 0")
	}

	if err := Lock(LockOn); err != nil {
		errno := err.(syscall.Errno)
		if errno != syscall.ENOENT {
			t.Errorf("Lock(LockOn): %v", errno)
		}
	} else {
		t.Errorf("Lock(LockOn): 0")
	}
}

// findForce searches for a rule in the list of ForceRules and returns true if it's found.
func findForce(rules []ForceRule, rule ForceRule) bool {
	for _, r := range rules {
		if reflect.DeepEqual(r, rule) {
			return true
		}
	}
	return false
}

// findFilter searches for a rule in the reversed list of FilterRules and returns its index.
func findFilter(rules []FilterRule, rule FilterRule) int {
	for idx, r := range rules {
		if reflect.DeepEqual(r, rule) {
			return idx
		}
	}
	return -1
}

// findGlob searches for a rule in the reversed list of GlobRules and returns its index.
func findGlob(rules []GlobRule, rule GlobRule) int {
	for idx, r := range rules {
		if reflect.DeepEqual(r, rule) {
			return idx
		}
	}
	return -1
}

func findCidr(rules []CidrRule, pattern Pattern) int {
	for idx, rule := range rules {
		if rule.Pat.Addr == pattern.Addr {
			fmt.Printf("rule: '%+v' == pat: '%+v'\n", rule.Pat.Addr, pattern.Addr)
		}
		if rule.Pat.Addr == pattern.Addr && comparePorts(rule.Pat.Port, pattern.Port) {
			return idx
		} else {
			fmt.Printf("port: '%+v' != pat: '%+v'\n", rule.Pat.Port, pattern.Port)
		}
	}
	return -1
}

func comparePorts(port1, port2 interface{}) bool {
	convertFloatSliceToIntSlice := func(floatSlice []interface{}) []int {
		intSlice := make([]int, len(floatSlice))
		for i, v := range floatSlice {
			if fv, ok := v.(float64); ok {
				intSlice[i] = int(fv)
			} else {
				fmt.Printf("Element in slice is not a float64: %v\n", v)
				return nil
			}
		}
		return intSlice
	}

	switch p1 := port1.(type) {
	case []interface{}:
		convertedP1 := convertFloatSliceToIntSlice(p1)
		if convertedP1 == nil {
			fmt.Printf("Failed to convert []interface{} to []int for Port1\n")
			return false
		}
		return comparePorts(convertedP1, port2)
	case float64:
		return comparePorts(int(p1), port2)
	case int:
		switch p2 := port2.(type) {
		case float64:
			return p1 == int(p2)
		case int:
			return p1 == p2
		case []int:
			return len(p2) == 2 && p1 >= p2[0] && p1 <= p2[1]
		case [2]int:
			return p1 >= p2[0] && p1 <= p2[1]
		case []interface{}:
			convertedP2 := convertFloatSliceToIntSlice(p2)
			if convertedP2 == nil {
				fmt.Printf("Failed to convert []interface{} to []int for Port2\n")
				return false
			}
			return comparePorts(p1, convertedP2)
		default:
			fmt.Printf("Pattern Port2 is of unexpected type %T\n", port2)
		}
	case []int:
		switch p2 := port2.(type) {
		case float64:
			return len(p1) == 1 && p1[0] == int(p2)
		case int:
			return len(p1) == 2 && p2 >= p1[0] && p2 <= p1[1]
		case []int:
			return reflect.DeepEqual(p1, p2)
		case [2]int:
			return reflect.DeepEqual(p1, p2[:])
		case []interface{}:
			convertedP2 := convertFloatSliceToIntSlice(p2)
			if convertedP2 == nil {
				fmt.Printf("Failed to convert []interface{} to []int for Port2\n")
				return false
			}
			return reflect.DeepEqual(p1, convertedP2)
		default:
			fmt.Printf("Pattern Port2 is of unexpected type %T\n", port2)
		}
	case [2]int:
		switch p2 := port2.(type) {
		case float64:
			return len(p1) == 1 && p1[0] == int(p2)
		case int:
			return p2 >= p1[0] && p2 <= p1[1]
		case []int:
			return reflect.DeepEqual(p1[:], p2)
		case [2]int:
			return reflect.DeepEqual(p1, p2)
		case []interface{}:
			convertedP2 := convertFloatSliceToIntSlice(p2)
			if convertedP2 == nil {
				fmt.Printf("Failed to convert []interface{} to []int for Port2\n")
				return false
			}
			return reflect.DeepEqual(p1[:], convertedP2)
		default:
			fmt.Printf("Pattern Port2 is of unexpected type %T\n", port2)
		}
	default:
		fmt.Printf("Rule Port1 is of unexpected type %T\n", port1)
	}
	return false
}
