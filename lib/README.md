libsyd is a comprehensive C library designed for interfacing with the Syd
environment. It offers functionalities for managing sandbox states, and facilitating
runtime configuration and interaction with the syd sandboxing environment. Read the
fine manuals of [Syd](https://man.exherbolinux.org/),
[libsyd](https://libsyd.exherbolinux.org/),
[gosyd](https://gosyd.exherbolinux.org/), [plsyd](https://plsyd.exherbolinux.org/),
[pysyd](https://pysyd.exherbolinux.org/), [rbsyd](https://rbsyd.exherbolinux.org/),
and [syd.el](https://sydel.exherbolinux.org/) for more information.

To install from source, clone the repository at https://git.sr.ht/~alip/syd, change
into the directory `lib` and run `make`, `make test` and `doas make install`. Rust
1.56.1 or later is required.

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
[~alip/sydbox-devel@lists.sr.ht](mailto:~alip/sydbox-devel@lists.sr.ht).
