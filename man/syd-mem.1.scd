SYD-MEM(1)

# NAME

syd-mem - calculate the memory usage of the given process or the parent process

# SYNOPSIS

*syd-mem* [-HV] _[pid]_

# DESCRIPTION

The *syd-mem* utility calculates the memory usage of the given process or the
parent process.

# OPTIONS

|[ *-H*
:< Print human-formatted size
|[ *-V*
:< Print virtual memory size

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _proc_(5)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be
submitted to https://gitlab.exherbo.org/groups/sydbox/-/issues. Discuss
in #sydbox on Libera Chat.
