SYD(1)

# NAME

syd - rock solid application kernel

# SYNOPSIS

*syd* [-acefhlmpqxEPV] [--] {command [arg...]}

*syd* [-acefhlmpqxEPV] [--] {library.so}

*syd* --el

*syd* --sh

# DESCRIPTION

Syd is a utility leveraging the _seccomp_(2) system call for sandboxing
processes on Linux systems version 5.19 or later. It enables
fine-grained control over a process's filesystem and network access
_without requiring root privileges_. Syd is designed for ease of use
across a wide array of architectures, including *x86*, *x86_64*, *x32*,
*armv7*, *aarch64*, *loongarch64*, *mips*, *mips64*, *mips64el*, *ppc*,
*ppc64*, *ppc64le*, *riscv64*, and *s390x* embodying the principle of
providing simple, flexible, and robust access control to Linux users.

The core functionality of Syd revolves around restricting a process's
resource access through several mechanisms:

- *Bind Mounts*: Utilized within a mount namespace to enforce restrictions at \
the *Virtual File System (VFS)* level, such as *read-only*, *nodev*, *noexec*, \
*nosuid*, and *nosymfollow*.
- *Landlock*: Employs read-only and read-write path restrictions at the kernel level.
- *seccomp-bpf*: Applies Secure Computing user filters for kernel-space sandboxing.
- *seccomp-notify*: Enables sandboxing in kernel space with user space \
fallback for dereferencing pointer arguments in system calls, \
including pathnames and network addresses. Access checks utilize \
UNIX shell-style patterns and CIDR notation, defaulting to denying \
system calls with *EACCES* while attempting to emulate successful \
calls to mitigate *Time-of-Check to Time-of-Use (TOCTOU)* \
attack vectors.

Prerequisites for Syd include a Linux kernel supporting _pidfd_getfd_(2) and
_pidfd_send_signal_(2) system calls, *SECCOMP_USER_NOTIF_FLAG_CONTINUE*
operation in the Secure Computing facility, and preferably the
*CONFIG_CROSS_MEMORY_ATTACH* kernel option. For syscall emulation, Syd uses the
seccomp operation *SECCOMP_IOCTL_NOTIF_ADDFD*. Moreover Syd sets the
*SECCOMP_FILTER_FLAG_WAIT_KILLABLE_RECV* flag to correctly handle interrupts
during tracing. While _Linux version 5.19 or later is required_, for Landlock
support Syd requires a kernel configured with the option *CONFIG_LSM_LANDLOCK*
supporting _Landlock ABI version 3_, with _syd-lock_(1) available as a helper
program to verify kernel support.

Syd is committed to maintaining rigorous security standards by strictly
delimiting the resource space accessible to sandboxed processes. In the
*SECURITY* section of the _syd_(7) manual page, a detailed enumeration of the
security hardening measures implemented by Syd is provided, along with optional
configurations to relax certain restrictions. This flexibility allows for the
accommodation of a diverse range of processes within the sandbox environment.

The approach to security within Syd is methodically designed to balance
robust protection with operational flexibility, ensuring that users have the
ability to fine-tune the sandboxing mechanisms to meet specific requirements. By
offering insights into the hardening techniques and customization options,
Syd empowers users to navigate the trade-offs between security and
functionality effectively.

# OPTIONS

The following options are understood:

|[ *-h*
:< Show usage and exit.
|[ *-V*
:< Show version and exit.
|[ *-c*
:< Login shell compatibility
|[
:< Causes command to be executed under a shell with the *user* profile.
|[
:< The shell to execute is */bin/sh* by default.
|[
:< Use the environment variable *SYD_SHELL* to override.
|[ *-f*
:< Login shell compatibility.
|[
:< Causes Syd to parse the *user* profile on startup.
|[ *-l*, *--login*
:< Login shell compatibility
|[
:< Causes Syd to parse the *user* profile on startup.
|[ *-q*
:< Enable quick boot mode for faster startup times.
|[
:< This must be passed as the first option or it will be ignored.
|[
:< See the explanation of the environment variable *SYD_QUICK_BOOT* for the safety of this option.
|[ *-x*
:< Enable *trace* aka "dry run" mode.
|[
:< In this mode Syd will allow system calls even if they raise access violations.
|[
:< This mode with extended logging can be used to build sandboxing profiles in an automated way.
|[
:< See _pandora_(1) which is a tool that uses Syd's trace mode to automatically generate sandbox profiles.
|[ *-m config*
:< Configure sandbox during init, may be repeated.
|[ *-p name*
:< Use a sandbox profile during init, may be repeated.
|[ *-P path*
:< Run a configuration file during init, may be repeated.
|[ *-a alias*
:< Set alias of the command. Passed as *argv[0]* to the program.
|[ *-e*
:< Use *-e var=val* to put var=val in the environment for command, may be repeated.
|[
:< Use *-e var* to remove var from the environment for command, may be repeated.
|[
:< Use *-e var=* to pass-through an unsafe environment variable, may be repeated.
|[ *-E mode*
:< Export secure computing rules with the given format to standard output and exit.
|[
:< Mode must be one of *bpf* or *pfc*:
|[
:< *bpf*, aka *Berkeley Packet Filter* is a binary, machine readable format, whereas
|[
:< *pfc*, aka *Pseudo Filter Code* is a textual, human readable format.
|[ *--el*
:< Output *syd.el* which is the Emacs Lisp implementation of Syd _stat_(2) interface.
|[
:< This file is also available via the magic path */dev/syd.el*.
|[ *--sh*
:< Output a shell script which defines the *esyd* helper function.
|[
:< This file is also available via the magic path */dev/syd.sh*.
|[
:< Works with POSIX sh, bash and zsh.
|[
:< You may use *eval "$(syd --sh)"* in your shell init file.

# INVOCATION

Syd can either execute a command with the specified arguments or load a
dynamic library and execute the function _syd_main()_ from it. In case
the first non-option argument ends with the suffix ".so", it is expected
to be the path of a dynamic library to load. The dynamic library is
loaded early at the startup in the Syd process such that even its owning
filesystem can be mounted _noexec_, using e.g. bind+/:/:noexec. This
allows to create a very restricted environment for the library function
to run inside. The function _syd_main()_ must take no arguments and
return an integer. This integer is going to be the exit value of the Syd
process. Note, loading a library requires care because its contructors
will run unsandboxed.

# ENVIRONMENT

|[ *SYD_LOG*
:< Set log level to *emerg*, *alert*, *crit*, *error*, *warn*, *notice*, *info* or *debug*.
|[ *SYD_LOG_BUF_LEN*
:< Set syslog(2) ring buffer capacity.
| 
:< By default, the ring buffer is allocated on the stack with an architecture-dependent size.
| 
:< Setting this variable makes Syd allocate the ring buffer on the heap with the user-specified size.
| 
:< Note, the value is parsed using the *parse-size* crate.
| 
:< Refer to their documentation for information on formatting.
|[ *SYD_LOG_FD*
:< Set log file descriptor, defaults to 2 aka standard error.
|[ *SYD_PID_FN*
:< Set pid filename, makes Syd write its process ID to this file at startup.
| 
:< The file must _not_ exist and is going to be created with user-only read permissions.
|[ *SYD_NPROC*
:< Set the number of core syscall handler threads, defaults to the number of CPUs.
| 
:< The number must be at least 1.
|[ *SYD_NPROC_MAX*
:< Set the number of maximum syscall handler threads, defaults to usize::MAX.
| 
:< The number must be greater than "SYD_NPROC".
|[ *SYD_NO_SYSLOG*
:< Disable logging to _syslog_(3),
| 
:  By default logs of severity *warn* and higher are logged to _syslog_(3).
|[ *SYD_SHELL*
:< Pick the shell to spawn when invoked as a login shell, defaults to "/bin/sh".
|[ *SYD_SYNC_SCMP*
:< Use synchronous mode for seccomp-notify. This may help with performance.
|[ *SYD_FORCE_TTY*
:< Force TTY output which is pretty-printed JSON.
|[ *SYD_QUIET_TTY*
:< Force quiet TTY output which is line-oriented JSON.
|[ *SYD_PROXY_HOST*
:< Override the default value of *proxy/ext/host*,
| 
:< If the value is a hostname and not an IP address,
| 
:< Syd resolves this hostname at startup and selects a response IP randomly.
|[ *SYD_PROXY_PORT*
:< Override the default value of *proxy/ext/port*.
|[ *SYD_QUICK_BOOT*
:< Enable quick boot mode, this makes Syd startup noticably faster:
| 
:  _However, quick boot removes a layer of defense against some container breaks!_
| 
:  Use this if you frequently re-execute _syd_(1) or _syd-oci_(1),
| 
:  as Exherbo Linux does during _cave-generate-metadata_(1).
|[ *SYD_NO_CROSS_MEMORY_ATTACH*
:< Disable cross memory attach and use "/proc/pid/mem".
| 
:  By default, Syd falls back to "/proc/pid/mem" automatically,
| 
:  if _process_vm_readv_(2) or _process_vm_writev_(2) fails with "ENOSYS".

# LOGGING

There're eight log levels: emerg, alert, crit, error, warn, notice,
info, and debug. Log level may be set with the "SYD_LOG" environment
variable. Logs go to standard error unless a file descriptor is
specified with the environment variable "SYD_LOG_FD". The messages of
severity warn and above are also sent to _syslog_(3) unless the
environment variable "SYD_NO_SYSLOG" is set.

Syd logs in JSON lines. Below is a list of some of the commonly used
keys and their meanings:

[[ *KEY*
:[ *DESCRIPTION*
|  *ctx*
:  Context of the log entry, e.g. *access*, *safesetid*, *segvguard* etc.
|  *cap*
:  Sandbox capability
|  *act*
:  Sandbox action: *Allow*, *Warn*, *Deny*, *Stop*, *Kill* or *Exit*
|  *pid*
:  Process ID
|  *path*
:  Path argument of the syscall
|  *addr*
:  Network address argument of the syscall, e.g. "127.0.0.1!22"
|  *unix*
:  UNIX socket address argument of the syscall
|  *ipv*
:  IP version of the network address in the *addr* field (4 or 6)
|  *abs*
:  True if the socket address in the *unix* field is an abstract UNIX socket
|  *sys*
:  Name of the syscall
|  *arch*
:  Architecture of the syscall
|  *args*
:  Arguments of the syscalll
|  *src*
:  Origin of the syscall in format "path+offset"
| 
:  Use, e.g. "objdump -D path | grep offset" to display the syscall instruction
|  *cmd*
:  Process name, or command line if log output is a TTY or "log" feature is enabled
|  *cwd*
:  Current working directory of the process
|  *uid*
:  User ID
|  *time*
:  Timestamp in ISO8601-compatible format, currently "YYYYMMDDThhmmssZ"
| 
:  Time format may change but it will always remain ISO8601-compatible
|  *err*
:  Error information
|  *msg*
:  Miscellaneous informational messages, mostly used with the "info" log level
|  *tip*
:  Informational messages on how to configure the sandbox

# EXIT CODES

Syd exits with the same exit code as the sandbox process itself. If the
sandbox process exits with a signal, Syd exits with 128 plus the value of the
signal. In case there was an error in spawning or waiting for the sandbox
process, Syd exits with *errno* indicating the error condition. E.g. *syd
true* returns *0*, *syd false* returns *1*, and *syd -- syd true* returns *16*
which stands for *EBUSY* which means *Device or resource busy* indicating there
is already a secure computing filter loaded.

# BENCHMARKS

The table below lists the benchmark runs we ran for Syd:

[[ 1: compile kernel
:[ sydbox-{1,3}
:- https://gitlab.exherbo.org/-/snippets/2534
|[ 2: compile kernel
:[ sydbox-{1,3}
:- https://gitlab.exherbo.org/-/snippets/2536
|[ 3: unpack compressed tarball
:[ sydbox-{1,3}, Gentoo sandbox
:- https://gitlab.exherbo.org/-/snippets/2537
|[ 4: compile kernel
:[ sydbox-{1,3}, Gentoo sandbox
:- https://gitlab.exherbo.org/-/snippets/2594
|[ 5: compile kernel in a Podman container
:[ syd-oci, crun, runc, youki, gvisor
:- https://gitlab.exherbo.org/-/snippets/2613
|[ 6: compile kernel in a Podman container
:[ syd-oci, crun, runc, youki, gvisor
:- https://gitlab.exherbo.org/-/snippets/2622
|[ 7: run sqlite-bench
:[ no-syd, syd, syd+crypt
:- https://gitlab.exherbo.org/-/snippets/2758

# SEE ALSO

_syd_(2), _syd_(5), _syd_(7), _syd-lock_(1), _syd-ls_(1)

- *syd* homepage: https://sydbox.exherbolinux.org/
- *libsyd* homepage: https://libsyd.exherbolinux.org/
- *pandora* homepage: https://lib.rs/pandora_box
- *paludis* homepage: http://paludis.exherbolinux.org/
- *Landlock* homepage: https://landlock.io
- *Path* wiki: https://en.wikipedia.org/wiki/Path_(computing)
- *Unix domain socket* wiki: https://en.wikipedia.org/wiki/Unix_domain_socket
- *IPv4* wiki: https://en.wikipedia.org/wiki/IPv4
- *IPv6* wiki: https://en.wikipedia.org/wiki/IPv6
- *TOCTOU* wiki: https://en.wikipedia.org/wiki/Time-of-check_to_time-of-use
- *VFS* wiki: https://en.wikipedia.org/wiki/Virtual_file_system
- *ipnetwork* documentation: https://docs.rs/ipnetwork
- *Enabling Logging*: https://docs.rs/env_logger/latest/env_logger/#enabling-logging

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be
submitted to https://gitlab.exherbo.org/groups/sydbox/-/issues. Discuss
in #sydbox on Libera Chat.
