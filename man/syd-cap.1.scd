SYD-CAP(1)

# NAME

syd-cap - print information on Linux capabilities

# SYNOPSIS

*syd-cap* _[-h]_

# DESCRIPTION

Print information on Linux capabilities.

# OPTIONS

|[ *-h*
:< Display help and exit.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _syd-aux_(1), _syd-elf_(1), _syd-ldd_(1)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be
submitted to https://gitlab.exherbo.org/groups/sydbox/-/issues. Discuss
in #sydbox on Libera Chat.
