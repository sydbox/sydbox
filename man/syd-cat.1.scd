SYD-CAT(1)

# NAME

syd-cat - tool to parse, validate and display Syd configuration

# SYNOPSIS

*syd-cat* _[-hjJmM]_ _[-p name]_ _<path>..._

# DESCRIPTION

Given a list of paths, parses and validates configuration.

Prints configuration to standard output on success.

Supported configuration file extensions are ".ipset", ".netset", and ".syd-3".

# OPTIONS

|[ *-h*
:< Display help.
|[ *-j*
:< Display Syd configuration as JSON.
|[ *-J*
:< Display Syd configuration as compact JSON.
|[ *-m* *magic*
:< Run a magic command at init, may be repeated.
|[ *-M* *magic*
:< Run a magic command at exit, may be repeated.
|[ *-p* *name*
:< Display rules of the profile with the given name. Use *list* as name to display the list of profiles.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _syd_(7)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be
submitted to https://gitlab.exherbo.org/groups/sydbox/-/issues. Discuss
in #sydbox on Libera Chat.
