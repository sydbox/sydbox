SYD-LOCK(1)

# NAME

syd-lock - Run a command under Landlock

# SYNOPSIS

*syd-lock* _[-hvAV]_ _[-r path]..._ _[-w path]..._ _[-b port]..._ _[-c port]..._ _{command [args...]}_

# DESCRIPTION

The *syd-lock* utility runs a command under Landlock. Read-only and
read-write paths should be specified using the _-r_ and _-w_ parameters.
Path arguments must be fully-qualified, relative paths are not
permitted. _bind_(2) and _connect_(2) ports may be specified using the
_-b_ and _-c_ parameters. Argument is either a single port or a closed
range in format *port1-port2*. Use _-V_ option to check for Landlock
support. The specific support level may be determined by the exit code.
Use _-A_ option to check for Landlock ABI version.

# OPTIONS

|[ *-h*
:< Display help.
|[ *-v*
:< Be verbose. Print Landlock status to standard error before running the specified command.
|[ *-A*
:< Print ABI version on standard output and exit with ABI version as exit code, rather than running a command. Use for scripting.
|[ *-V*
:< Check if Landlock is supported and print ABI version, rather than running a command.
|[ *-r* _path_
:< Specify a read-only path, may be repeated. In this context, read-only includes execute operations.
|[ *-w* _path_
:< Specify a read-write path, may be repeated. In this context, read-write includes execute and _ioctl_(2) operations.
|[ *-b* _port_
:< Specify a port for _bind_(2), may be repeated. Argument is either a single port or a closed range in format *port1-port2*.
|[ *-c* _port_
:< Specify a port for _connect_(2), may be repeated. Argument is either a single port or a closed range in format *port1-port2*.

# EXIT CODES

When running a command, *syd-lock* exits with the same exit code as the
command.

When running in check mode with _-A_, syd exits with ABI version as exit
code.

When running in check mode with _-V_, syd may exit with the following
exit codes:

|[ *0*
:< Fully enforced
|[ *1*
:< Partially enforced
|[ *2*
:< Not enforced
|[ *127*
:< Not supported

# ABI

Landlock ABI versioning makes it possible to adjust the security policy
according to the kernel capabilities.

# HISTORY

. First Landlock ABI, introduced with Linux 5.13: https://git.kernel.org/stable/c/17ae69aba89dbfa2139b7f8024b757ab3cc42f59
. Second Landlock ABI, introduced with Linux 5.19: https://git.kernel.org/stable/c/cb44e4f061e16be65b8a16505e121490c66d30d0
. Third Landlock ABI, introduced with Linux 6.2: https://git.kernel.org/stable/c/299e2b1967578b1442128ba8b3e86ed3427d3651
. Fourth Landlock ABI, introduced with Linux 6.7: https://git.kernel.org/stable/c/136cc1e1f5be75f57f1e0404b94ee1c8792cb07d
. Fifth Landlock ABI, introduced with Linux 6.10: https://git.kernel.org/stable/c/2fc0e7892c10734c1b7c613ef04836d57d4676d5
. Sixth Landlock ABI, introduced with Linux 6.12.

# EXAMPLE

```
alip@syd:~|⇒  syd-lock wget -O/dev/null chesswob.org
alip@syd:~|⇒  syd-lock -r/ wget -O/dev/null chesswob.org
/dev/null: Permission denied
alip@syd:~|⇒  syd-lock -r/ -w/dev/null wget -O/dev/null chesswob.org
--2024-11-30 16:52:51--  http://chesswob.org/
Resolving chesswob.org... 95.216.39.164, fe80::468a:5bff:fe88:2141
Connecting to chesswob.org|95.216.39.164|:80... failed: Permission denied.
Connecting to chesswob.org|fe80::468a:5bff:fe88:2141|:80... failed: Permission denied.
Retrying.

^C
alip@syd:~|⇒  syd-lock -r/ -w/dev/null -c80 -c443 wget -O/dev/null chesswob.org
--2024-11-30 16:53:00--  http://chesswob.org/
Resolving chesswob.org... 95.216.39.164, fe80::468a:5bff:fe88:2141
Connecting to chesswob.org|95.216.39.164|:80... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://www.chesswob.org/ [following]
--2024-11-30 16:53:00--  https://www.chesswob.org/
Resolving www.chesswob.org... 95.216.39.164, fe80::468a:5bff:fe88:2141
Connecting to www.chesswob.org|95.216.39.164|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 148827 (145K) [text/html]
Saving to: ‘/dev/null’

/dev/null 100%[========================================================================================>] 145.34K  --.-KB/s    in 0.1s

2024-11-30 16:53:00 (1.04 MB/s) - ‘/dev/null’ saved [148827/148827]

alip@syd:~|⇒
```

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _syd-pds_(1)

*syd* homepage: https://sydbox.exherbolinux.org/

*LandLock* homepage: https://landlock.io/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be
submitted to https://gitlab.exherbo.org/groups/sydbox/-/issues. Discuss
in #sydbox on Libera Chat.
