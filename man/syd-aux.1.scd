SYD-AUX(1)

# NAME

syd-aux - print auxiliary vector information

# SYNOPSIS

*syd-aux* _[-hrs]_

# DESCRIPTION

Print auxiliary vector information.

If -r is given print hexadecimal-encoded AT_RANDOM cookie.

If -s is given exit with success if AT_SECURE is set.

# OPTIONS

|[ *-h*
:< Display help and exit.

|[ *-r*
:< Print hexadecimal-encoded AT_RANDOM cookie.

|[ *-s*
:< Exit with success if AT_SECURE is set.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _syd-elf_(1), _syd-ldd_(1), _getauxval_(3)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://gitlab.exherbo.org/sydbox/sydbox.git and bugs/patches can be
submitted to https://gitlab.exherbo.org/groups/sydbox/-/issues. Discuss
in #sydbox on Libera Chat.
