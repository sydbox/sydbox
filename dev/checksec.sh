#!/bin/sh
# Check security features of an ELF binary.
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v3

say() {
    echo >&2 "$*"
}

bin=$1
if ! test -x "$1"; then
    say "Usage: ${0} <binary>"
    exit 1
fi

say Checking $bin...
readelf -h "$bin" | grep Type: || say oops PIE
readelf -l "$bin" | grep -A 1 GNU_STACK || say oops GNU_STACK
readelf -l "$bin" | grep GNU_RELRO || say oops GNU_RELRO
readelf -d "$bin" | grep BIND_NOW || say oops BIND_NOW
readelf -s -W "$bin" | grep '\.cfi' || say oops CFI
readelf -s "$bin" | grep __safestack_init || say oops SAFESTACK
binary-security-check --no-libc "$bin"
checksec -f "$bin"
