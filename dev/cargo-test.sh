#!/bin/bash
#
# Run Syd integration tests
#
# Copyright 2024 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: GPL-3.0

# Make sure we don't trigger TPE.
umask 077

# Enable coredumps.
ulimit -c unlimited

:>syd.log
(
n_old=`wc -l syd.log`
while true; do
    sleep 300
    n_new=`wc -l syd.log`
    if [[ ${n_old} == ${n_new} ]]; then
        echo >&2 "[*] No output from tests for 5 minutes..."
        ps aux >&2
    fi
    n_old=${n_new}
done
) &
pid=$!
trap 'kill ${pid}' INT TERM

set -ex
"${CARGO:-cargo}" test "${@}" 2>&1 | tee syd.log
r=${PIPESTATUS[0]}
kill ${pid}
exit $r
