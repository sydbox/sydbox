;;; File: emacs-format-file
;;; Stan Warford
;;; 17 May 2006
;;; https://web.archive.org/web/20100616033100/www.cslab.pepperdine.edu/warford/BatchIndentationEmacs.html

(defun emacs-format-function ()
   "Format the whole buffer."
   (indent-region (point-min) (point-max) nil)
   (untabify (point-min) (point-max))
   (save-buffer)
)
