#!/bin/bash -ex

root=$(git rev-parse --show-toplevel)
mkdir -p -m700 "$root"/dist
cd "$root"/dist
rm -f *syd*
wget -c $(hut builds artifacts $(hut builds list | grep syd | grep linux-x86-64 | head -n1 | awk '{print $1}') | awk '{print $4}')
