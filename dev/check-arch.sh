#!/bin/bash -e

WORK_DIR="$(mktemp -d --tmpdir syd-arch.XXXXX)"
finish() {
    rm -rf "${WORK_DIR}"
}
trap finish EXIT

pushd "${WORK_DIR}" &>/dev/null

syd --list hook | sort > hook
while read -r arch; do
    echo "*** $arch ***"
    for i in {1..4096}; do
        syd --arch $arch --syscall $i
    done | awk '{print $2}' | sort > $arch
    diff -u hook $arch || true
done < <(syd --arch list |  awk '{print $2}')

popd &>/dev/null
