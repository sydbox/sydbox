#!/usr/bin/env python3
# coding: utf-8
# Based on: https://dev.to/xfbs/generating-prime-numbers-with-python-and-rust-4663

def primegen(limit = 10240):
  """
  Generates prime numbers up to a given limit.

  Args:
      limit: The upper limit (exclusive) for prime number generation.

  Yields:
      Prime numbers less than the limit.
  """
  from math import sqrt
  primes = [2]  # Seed with 2, the first prime
  for num in range(3, limit, 2):
    is_prime = True
    square_root = int(sqrt(num))  # More efficient to use integer square root
    for prime in primes:
      if num % prime == 0:
        is_prime = False
        break
      if prime > square_root:
        break  # No need to check further if prime is greater than square root
    if is_prime:
      yield num

if __name__ == '__main__':
    for i in primegen():
        print(i)
