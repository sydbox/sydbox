#!/bin/bash
# https://raw.githubusercontent.com/ahgamut/ripgrep/cosmopolitan/gcc-linker-wrapper.bash

set -eu

COSMO="${COSMO:-./libcosmo}"
ARCH="${ARCH:-$(uname -m)}"

args=()
for arg; do
    case "${arg}" in
        '-lunwind') continue;;
        '-static') continue;;
        '-Wl,-Bdynamic') continue;;
        '-Wl,-Bstatic') continue;;
    esac
    args+=( "${arg}" )
done

set -x
exec "${COSMO}"/bin/"${ARCH}"-unknown-cosmo-cc "${args[@]}"
