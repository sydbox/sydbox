#!/bin/sh -ex
exec \
    env SYD_LOG=info \
        syd -ppaludis -pcontainer -mroot:/mnt/gnu \
        -m'bind+/etc:/etc:ro,nodev,noexec,nosuid' \
        -m'bind+/home:/home:ro,nodev,noexec,nosuid' \
        -m'bind+/media:/media:ro,nodev,noexec,nosuid' \
        -m'bind+/mnt:/mnt:ro,nodev,noexec,nosuid' \
        -m'bind+/opt:/opt:ro,nodev,nosuid' \
        -m'bind+/srv:/srv:ro,nodev,noexec,nosuid' \
        -m'bind+/usr:/usr:ro,nodev' \
        -m'bind+/etc/shells:/proc/kcore:ro,nodev,noexec,nosuid' \
        -m'bind+/etc/shells:/proc/keys:ro,nodev,noexec,nosuid' \
        -m'bind+/etc/shells:/proc/latency_stats:ro,nodev,noexec,nosuid' \
        -m'bind+/etc/shells:/proc/sysrq-trigger:ro,nodev,noexec,nosuid' \
        -m'bind+/etc/shells:/proc/timer_list:ro,nodev,noexec,nosuid' \
        -m'bind+/etc/shells:/proc/timer_stats:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/acpi:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/asound:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/bus:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/driver:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/dynamic_debug:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/fs:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/irq:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/pressure:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/scsi:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/sys:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/sysvipc:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/proc/tty:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/sys/dev/block:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/sys/devices/virtual/powercap:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/sys/firmware:ro,nodev,noexec,nosuid' \
        -m'bind+/var/empty:/sys/fs:ro,nodev,noexec,nosuid' \
        -m'deny/read+/proc/1/**' \
        -m'deny/stat+/proc/1/**' \
        -m'deny/write+/proc/1/***' \
        -m'allow/read+/proc/1/comm' \
        -m'allow/stat+/proc/1/comm' \
    bash "$@"
