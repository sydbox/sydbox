#!/bin/sh -e

root=/dev/shm/sql-bench
rm -rf "$root"
mkdir -m700 "$root"

syd -V
echo >&2 "Starting benchmark under ${root}"

echo >&2 'BENCHMARK 1: NOSYD'
./sqlite-bench --db="${root}/"

echo >&2 'BENCHMARK 2: SYD'
syd -pu \
    -m 'allow/read+/proc/cpuinfo' \
    -m "allow/all+${root}/***" \
    ./sqlite-bench --db="${root}/"

echo >&2 'BENCHMARK 3: SYD+CRYPT'
syd -pu \
    -m 'allow/read+/proc/cpuinfo' \
    -m "allow/all+${root}/***" \
    -m 'sandbox/crypt:on' \
    -m "crypt/key:$(syd-key)" \
    -m "crypt+${root}/***" \
    -- ./sqlite-bench --db="${root}/"
