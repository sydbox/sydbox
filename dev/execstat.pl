#!/usr/bin/env perl
# e.g: SYD_EXEC=1 timeout -sINT 60s syd -puser ./exec.sh 2>&1 | grep "verified" | awk '{print $6}' | ./dev/execstat.pl
# where exec.sh is an exec bomb:
#while true; do
#    /bin/false &
#done
#wait
use strict;
use warnings;
use List::Util qw(sum min max);
use Math::Complex; # For geometric mean

my @times;

# Signal handler for interruptions (Ctrl+C)
$SIG{INT} = sub {
	print_results();
	exit 0; # Exit gracefully
};

# Read times from stdin
while ( my $line = <> ) {
	chomp $line;
	push @times, $line if $line =~ /^\d+(\.\d+)?$/; # Validate input
}

# Function to print results
sub print_results {
	if (@times) {
		my $number_of_runs = @times;
		my $total_time = sum(@times);
		my $worst_time = max @times;
		my $best_time = min @times;
		my $mean_time = $total_time / $number_of_runs;
		my $geometric_mean = exp( sum( map { log($_) } @times ) / @times );
		my $harmonic_mean  = @times / sum( map { 1 / $_ } @times );

		print "Number of runs: $number_of_runs\n";
		print "Total time: $total_time seconds\n";
		print "Worst time: $worst_time seconds\n";
		print "Best time: $best_time seconds\n";
		print "Mean time: $mean_time seconds\n";
		print "Geometric mean: $geometric_mean seconds\n";
		print "Harmonic mean: $harmonic_mean seconds\n";
	} else {
		print "No data was provided.\n";
	}
}

# Print final results if the loop finishes normally
print_results();
