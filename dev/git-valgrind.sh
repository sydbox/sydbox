#!/bin/bash
#
# Compile git under Syd under Valgrind.
#
# Copyright 2024 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: GPL-3.0

if [[ ${#} -lt 1 ]]; then
    echo >&2 "Usage: ${0##*/} <valgrind-arguments>..."
    exit 1
fi

# Make sure we don't trigger TPE.
umask 077

# Disable coredumps.
ulimit -c 0

VALGRIND="${VALGRIND:-valgrind}"
SYD="${CARGO_BIN_EXE_syd:-syd}"

DIR="$(mktemp -d --tmpdir=/tmp syd-git.XXXXX)"
[[ -d "${DIR}" ]] || exit 2

set -ex
pushd "${DIR}"
git clone --depth 1 https://github.com/git/git.git
pushd git
exec "${VALGRIND}" "${@}" -- \
    "${SYD}" -q -puser -pMP \
        -m "allow/read,stat,write,exec,create,node,ioctl+${DIR}/***" \
        -- \
        sh -c 'autoreconf -fiv && ./configure && make -j$(nproc) && make clean'
