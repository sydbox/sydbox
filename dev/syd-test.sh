#!/bin/sh -ex

# Set to debug for debug mode.
SYD_TEST_PROFILE=${SYD_TEST_PROFILE:-release}

root=$(git rev-parse --show-toplevel)
test -d "$root"
export PATH="${root}/target/${SYD_TEST_PROFILE}:${PATH}"
exec syd-test "$@"
