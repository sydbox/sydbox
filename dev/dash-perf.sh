#!/bin/bash
#
# Compile dash under Syd under Perf.
#
# Copyright 2024 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: GPL-3.0

if [[ ${#} -lt 1 ]]; then
    echo >&2 "Usage: ${0##*/} <perf-arguments>..."
    exit 1
fi

# Make sure we don't trigger TPE.
umask 077

# Disable coredumps.
ulimit -c 0

PERF="${PERF:-perf}"
SYD="${CARGO_BIN_EXE_syd:-syd}"

DIR="$(mktemp -d --tmpdir=/tmp syd-dash.XXXXX)"
[[ -d "${DIR}" ]] || exit 2

set -ex
pushd "${DIR}"
git clone --depth 1 https://git.kernel.org/pub/scm/utils/dash/dash.git
pushd dash
exec "${PERF}" "${@}" -- \
    "${SYD}" -q -puser \
        -mtrace/allow_unsafe_nopie:1 \
        -m "allow/read,stat,write,exec,create,node,ioctl+${DIR}/***" \
        -- \
        sh -c './autogen.sh && ./configure && make -j$(nproc) && make clean'
