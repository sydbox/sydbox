#!/bin/bash
#
# Run glibc tests under Syd.
#
# Copyright 2024 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: GPL-3.0

# Make sure we don't trigger TPE.
umask 077

# Disable coredumps.
ulimit -c 0

# Set up environment.
unset LD_LIBRARY_PATH

libgcc=/lib/x86_64-linux-gnu/libgcc_s.so.1
test -e "${libgcc}" && export LD_PRELOAD="${libgcc}"

SYD="${CARGO_BIN_EXE_syd:-syd}"

set -ex
DIR="$(mktemp -d --tmpdir=/tmp syd-glib.XXXXX)"
set +ex

function finish() {
    rm -rf "${DIR}"
}

trap finish EXIT

edo() {
    echo >&2 "$*"
    "$@"
}

set -ex

pushd "${DIR}"

git clone --depth 1 https://sourceware.org/git/glibc.git
pushd glibc

mkdir build
cd build

../configure \
    --disable-sanity-checks \
    --disable-werror
make -s -j$(nproc)

"${SYD}" -ppaludis make -s -j$(nproc) check
