#!/bin/bash
#
# Run coreutils tests under Syd.
#
# Copyright 2024 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: GPL-3.0

# Make sure we don't trigger TPE.
umask 077

# Disable coredumps.
ulimit -c 0

# Set up environment.
unset LD_LIBRARY_PATH

SYD="${CARGO_BIN_EXE_syd:-syd}"

set -ex
DIR="$(mktemp -d --tmpdir=/tmp syd-coreutils.XXXXX)"
set +ex

function finish() {
    rm -rf "${DIR}"
}

trap finish EXIT

edo() {
    echo >&2 "$*"
    "$@"
}

set -ex

pushd "${DIR}"

git clone --depth 1 --recursive git://git.sv.gnu.org/coreutils
pushd coreutils
./bootstrap --skip-po

mkdir build
cd build

../configure \
    --enable-gcc-warnings=no \
    --enable-acl \
    --enable-libcap \
    --enable-xattr \
    --with-linux-crypto \
    --with-openssl=no
make -s -j$(nproc)

"${SYD}" -ppaludis make -s -j$(nproc) check
