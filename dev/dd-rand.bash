#!/bin/bash

# Simulates dd with random block sizes and count,
# with a maximum total size of 8MB.
dd_rand() {
  # Generate random size between 1 and 1024 (inclusive).
  random_size=$((RANDOM % 1024 + 1))

  # Generate random count between 1 and 8192 (adjust for desired max size)
  # This ensures total size (count * block_size) won't exceed 8MB.
  max_count=$((8 * 1024 * 1024 / random_size)) # Adjust divisor for different max size.
  random_count=$((RANDOM % max_count + 1))

  # Read from /dev/random with random size and count
  dd if=/dev/random bs=$random_size count=$random_count status=progress
}



dd_rand
