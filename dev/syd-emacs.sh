#!/bin/sh -e
# syd-emacs: Convenience script to run Emacs under Syd.
#
# Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
# SPDX-License-Identifier: GPL-3.0

# Defaults for options.
#
# Path to the Syd profile to be used instead of lib profile.
SYD_CFG_FN=${SYD_CFG_FN:-${HOME}/.emacs.d/init.syd-3}
# Path to the log file for Syd access violations.
SYD_LOG_FN=${SYD_LOG_FN:-${HOME}/.emacs.d/syd.log}

# Determine Syd command based on user configuration.
if [ -e "${SYD_CFG_FN}" ]; then
    echo >&2 "syd-emacs: Using Syd profile from \`${SYD_CFG_FN}'."
    SYD_CMD="syd -P'${SYD_CFG_FN}'"
else
    echo >&2 "syd-emacs: Syd profile \`${SYD_CFG_FN}' does not exist."
    echo >&2 "syd-emacs: Using the \`lib' profile."
    SYD_CMD='syd -plib'
fi

# Handle Syd log redirection.
echo >&2 "syd-emacs: Logging access violations to \`${SYD_LOG_FN}'."
SYD_LOG=${SYD_LOG:-warn}
SYD_LOG_FD=${SYD_LOG_FD:-64}
export SYD_LOG
export SYD_LOG_FD
eval "exec ${SYD_LOG_FD}>>'${SYD_LOG_FN}'"

exec ${SYD_CMD} -- emacs --load=/dev/syd.el "$@"
