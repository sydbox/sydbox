% vim: set filetype=tex fileencoding=utf8 et sw=2 ts=2 sts=2 tw=80 :
% © 2024, 2025 Ali Polatel <alip@hexsys.org>
% Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported Lisansı ile yayınlanmıştır.

\documentclass[aspectratio=169]{beamer}
\usepackage[english]{babel}

\usepackage{fontspec}
\usepackage{unicode-math}
\defaultfontfeatures{Ligatures=TeX}
\setmainfont[Ligatures=TeX,
Extension=.otf,
BoldFont=*-bold,
UprightFont=*-regular,
ItalicFont=*-italic,
BoldItalicFont=*-bolditalic,
SmallCapsFeatures={Letters=SmallCaps}]{texgyretermes}
\setmathfont[Ligatures=TeX]{texgyretermes-math.otf}
\setsansfont[Ligatures=TeX,
Extension=.otf,
BoldFont=*-bold,
UprightFont=*-regular,
ItalicFont=*-italic,
BoldItalicFont=*-bolditalic,
SmallCapsFeatures={Letters=SmallCaps}]{texgyreheros}

\usepackage{graphicx}
\DeclareGraphicsExtensions{.jpg,.png}

\usepackage{fontawesome5}
\usepackage{marvosym}

\usepackage{booktabs}
\usepackage{enumerate}
\usepackage{multicol}
\usepackage{pdfpages}
\usepackage{color}
\usepackage[xspace]{ellipsis}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows.meta, positioning}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum height=0.6cm, text centered, draw=black, fill=red!20]
\tikzstyle{process} = [rectangle, minimum height=0.6cm, text centered, text width=2.5cm, draw=black, fill=orange!20]
\tikzstyle{decision} = [diamond, aspect=2, minimum height=0.6cm, text centered, draw=black, fill=green!20, inner sep=0pt]
\tikzstyle{arrow} = [-{Stealth}, shorten >=1pt, thick]

\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.9}
\usepackage{listings}
\lstset{
  inputencoding=utf8,
  extendedchars=\false,
  escapeinside={\%*}{*)},
  language=Python,
  basicstyle=\scriptsize\ttfamily,
  stringstyle=\scriptsize\ttfamily,
  keywordstyle=\color{OliveGreen},
  commentstyle=\color{gray},
  numbers=left,
  numberstyle=\tiny,
  stepnumber=1,
  numbersep=5pt,
  backgroundcolor=\color{lightlightgray},
  frame=none,
  tabsize=2,
  captionpos=t,
  breaklines=true,
  breakatwhitespace=false,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  columns=flexible
}

\usetheme{Warsaw}
\usecolortheme[snowy]{owl}
%\setbeamertemplate{itemize/enumerate body begin}{\footnotesize}
%\setbeamertemplate{itemize/enumerate subbody begin}{\scriptsize}
%\setbeamertemplate{itemize/enumerate subsubbody begin}{\tiny}

\author{Ali Polatel}
\title{Syd}
\subtitle{An Introduction to Secure Application Sandboxing for Linux}
\institute{
  \noindent
  \includegraphics[height=0.2\textheight,width=0.2\textwidth]{zebrapig}
  \hspace{0.1\textwidth}
  \includegraphics[height=0.2\textheight,width=0.2\textwidth]{syd}
  \hspace{0.1\textwidth}
  \includegraphics[height=0.2\textheight,width=0.2\textwidth]{sydbox}
}
\date{FOSDEM, 2025}

\usepackage{hyperref}
\hypersetup{%
    hyperfootnotes=true,
    breaklinks=true,
    colorlinks=true,
    urlcolor=black,
    citecolor=black,
    linkcolor=black,
    pdftitle={Syd},
    pdfauthor={Ali Polatel},
    pdfsubject={Ali Polatel, Syd},
    pdflang={en},
    pdfkeywords={Linux, Sandboxing},
    pdfproducer={LuaLaTeX, BibTeX, hyperref, memoir},
    pdfpagelabels=true
    pdfborder={0 0 0},
}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Before we start...}
  \framesubtitle{The game is on! Viva la revolución!}

  \begin{itemize}
    \item CTF: \{https,ssh\}://syd.chesswob.org
      \begin{itemize}
        \item user/pass: syd
        \item rules: \texttt{/etc/user.syd-3}
        \item goal: read \texttt{/etc/CTF} \& get 200€!
      \end{itemize}
    \item GIT: https://gitlab.exherbo.org/sydbox/sydbox.git
    \item DOC: https://man.exherbolinux.org
    \item ML: https://lists.sr.ht/\~{}alip/exherbo-dev
    \item IRC: \#sydbox at Libera
    \item Matrix: \#sydbox:mailstation.de
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{History: Exherbo}
  \framesubtitle{All you touch and all you see is all your life will ever be.}

  \begin{itemize}
    \item Gentoo Linux: {\small because no penguin can swim faster!}
      \begin{itemize}
        \item Source-based, rolling-release distribution
        \item Sandboxing required to detect package build mishaps
        \item Gentoo Sandbox: \texttt{LD\_PRELOAD}, no network restrictions
      \end{itemize}
    \item Exherbo Linux: {\small when you hear hoofbeats, think of a zebrapig!}
      \begin{itemize}
        \item \texttt{s/Gentoo fork/Gentoo done right/}
        \item Recommended watch by \texttt{Bryan Østergaard}, aka \texttt{kloeri}:
          \begin{itemize}
            \item ``10 cool things about Exherbo''
            \item ``You're doing it wrong!''
          \end{itemize}
        \item Package testing by default
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{History: Syd}
  \framesubtitle{Did you exchange a walk-on part in the war for a lead role in a cage?}

  \begin{itemize}
    \item SydBox: The {\faPeace}ther S{\CircledA}ndbøx
      \begin{itemize}
        \item SydBox-1: c, \texttt{ptrace}
          \begin{itemize}
            \item Network sandboxing: Builds restricted to loopback
            \item Exec sandboxing: Metadata phase restricted to shell builtins
          \end{itemize}
        \item SydBox-2: c, \texttt{seccomp}
          \begin{itemize}
            \item Initial experiments to replace \texttt{ptrace} with \texttt{seccomp}
            \item Initial experiments to make SydBox a security boundary
            \item Read sandboxing \& Path hiding
          \end{itemize}
        \item SydBox-3, aka \texttt{Syd}: rust, \texttt{seccomp}, \texttt{landlock}, namespaces
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Overview: What?}
  \framesubtitle{Welcome my son, welcome to the machine.}

  \begin{itemize}
    \item An application kernel to sandbox applications on Linux
    \item Written in Rust, \texttt{cargo install --locked syd}, requires \texttt{libseccomp}
    \item Licensed \texttt{GPL-3.0}, forever free
    \item Requires Linux>=5.19 with \texttt{CONFIG\_SECCOMP\_FILTER}
    \item Good portability across architectures
      \begin{itemize}
        \item Tested on \texttt{arm64}, \texttt{armv7},
          \texttt{ppc64le}, \texttt{riscv64}, \texttt{s390x},
          \texttt{x86}, and \texttt{x86-64}, with \texttt{mips},
          \texttt{m68k}, \texttt{superh} and \texttt{loongarch}
          support on the way!
        \item Should work on any \texttt{libseccomp} supported architecture with
          minimal work
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Overview: How?}
  \framesubtitle{You dreamed of a big star, he played a mean guitar.}

  \begin{itemize}
    \item Make sandboxing as easy as text searching is with \texttt{grep(1)}!
    \item UNIX philosophy: Do one thing and do it well
    \item Simple interface for complex sandboxing mechanisms
    \item Secure by default with minimal overhead
    \item No extra privileges required: No \texttt{SETUID}, \texttt{EBPF}, or \texttt{LKM}
    \item Can be used as login shell
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features: Basics}
  \framesubtitle{Breathe, breathe in the air. Don't be afraid to care.}

  \begin{itemize}
    \item Path sandboxing
      \begin{itemize}
        \item Read sandboxing and Path Masking
        \item Write sandboxing and Append-only Paths
        \item Stat sandboxing and Path Hiding
        \item Ioctl sandboxing
          \begin{itemize}
            \item Contain AI/ML workloads
            \item Safe access to PTY, DRM, and KVM
          \end{itemize}
      \end{itemize}
    \item Network sandboxing
      \begin{itemize}
        \item feat. UNIX, IPv4, IPv6, Netlink, and KCAPI sockets
        \item Application level firewalls with IP blocklists
      \end{itemize}
    \item \texttt{pledge(2)} like refined sandboxing categories
      \begin{itemize}
        \item stat, read, write, exec, chdir, readdir, create, delete,
          rename, link, truncate, tmpfile, ioctl, node, attr, chown,
          chgrp, chroot, net/bind, net/connect, net/send
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features: Execution Control}
  \framesubtitle{One slip, and down the hole we fall, it seems to take no time at all.}

  \begin{itemize}
    \item Exec sandboxing
      \begin{itemize}
        \item Requires \texttt{PTRACE\_EVENT\_EXEC} to be safe
      \end{itemize}
    \item SegvGuard
      \begin{itemize}
        \item Block execution if binary is crashing repeatedly
        \item Wider range of trigger signals than Grsecurity
        \item Can also be triggered using sandbox rules
        \item \texttt{kill/read+/etc/shadow}
      \end{itemize}
    \item Force sandboxing (aka Verified Execution)
      \begin{itemize}
        \item Verify binary/library integrity at
          \texttt{exec(3)}/\texttt{mmap(2)} time
        \item like Veriexec (NetBSD), and Integriforce (HardenedBSD)
        \item \texttt{sha3-512}, \texttt{sha3-384}, \texttt{sha3-256},
          \texttt{sha1}, \texttt{md5}, \texttt{crc64} and \texttt{crc32}
      \end{itemize}
    \item Trusted Path Execution
      \begin{itemize}
        \item like Grsecurity, HardenedBSD
        \item Execution only allowed from ``Trusted directories''
          \begin{itemize}
            \item Not writable by group or others
            \item Optionally owned by root or current user
          \end{itemize}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features: Nice-to-haves}
  \framesubtitle{Each small candle lights a corner in the dark.}

  \begin{itemize}
    \item Sandbox lock and dynamic configuration
    \item AT\_SECURE set by default
    \item Executable restrictions:
      \begin{itemize}
        \item PIE and non-executable stack enforced by default
        \item Deny based on bitness: \texttt{trace/deny\_elf32}
        \item Deny based on linkage:
          \begin{itemize}
            \item \texttt{trace/deny\_elf\_static}
            \item \texttt{trace/deny\_elf\_dynamic}
          \end{itemize}
        \item Deny scripts: \texttt{trace/deny\_script}
      \end{itemize}
    \item Fake root with \texttt{root/fake}
    \item Forcing umask with \texttt{trace/force\_umask}
    \item Deny directory-traversal with \texttt{trace/deny\_dotdot}
    \item Deny access to TSC with \texttt{trace/deny\_tsc} and \texttt{libsydtime}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Features: Bonus}
  \framesubtitle{Click clack, ride on the rail track.}

  \begin{itemize}
    \item Lock sandboxing, uses \texttt{Landlock} LSM
    \item Proxy sandboxing
      \begin{itemize}
        \item SOCKS proxy forwarding with network namespace isolation
        \item Defaults to TOR
      \end{itemize}
    \item Memory \& PID sandboxing
      \begin{itemize}
        \item Simple alternatives to Control Groups
      \end{itemize}
    \item SafeSetID
      \begin{itemize}
        \item Safe user/group switching
        \item Predefined UID/GID transitions
      \end{itemize}
    \item Ghost mode
      \begin{itemize}
        \item Similar to \texttt{Seccomp} Level 1, aka Strict Mode
      \end{itemize}
    \item Namespaces, Containerization, and \texttt{syd-oci}
    \item Learning mode with \texttt{pandora}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The End}
  \framesubtitle{You'll lose your mind and play free games for May!}

  \begin{itemize}
    \item CTF: \{https,ssh\}://syd.chesswob.org
      \begin{itemize}
        \item user/pass: syd
        \item rules: \texttt{/etc/user.syd-3}
        \item goal: read \texttt{/etc/CTF} \& get 200€!
      \end{itemize}
    \item GIT: https://gitlab.exherbo.org/sydbox/sydbox.git
    \item DOC: https://man.exherbolinux.org
    \item ML: https://lists.sr.ht/\~{}alip/exherbo-dev
    \item IRC: \#sydbox at Libera
    \item Matrix: \#sydbox:mailstation.de
  \end{itemize}

\end{frame}

\end{document}
