% vim: set filetype=tex fileencoding=utf8 et sw=2 ts=2 sts=2 tw=80 :
% © 2024, 2025 Ali Polatel <alip@hexsys.org>
% Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported Lisansı ile yayınlanmıştır.

\documentclass[aspectratio=169]{beamer}
\usepackage[english]{babel}

\usepackage{fontspec}
\usepackage{tipa} % for IPA symbols
\usepackage{unicode-math}
\defaultfontfeatures{Ligatures=TeX}
\setmainfont[Ligatures=TeX,
Extension=.otf,
BoldFont=*-bold,
UprightFont=*-regular,
ItalicFont=*-italic,
BoldItalicFont=*-bolditalic,
SmallCapsFeatures={Letters=SmallCaps}]{texgyretermes}
\setmathfont[Ligatures=TeX]{texgyretermes-math.otf}
\setsansfont[Ligatures=TeX,
Extension=.otf,
BoldFont=*-bold,
UprightFont=*-regular,
ItalicFont=*-italic,
BoldItalicFont=*-bolditalic,
SmallCapsFeatures={Letters=SmallCaps}]{texgyreheros}

\usepackage{graphicx}
\DeclareGraphicsExtensions{.jpg,.png}

\usepackage{fontawesome5}
\usepackage{marvosym}

\usepackage{booktabs}
\usepackage{enumerate}
\usepackage{multicol}
\usepackage{pdfpages}
\usepackage{color}
\usepackage[xspace]{ellipsis}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows.meta, positioning}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum height=0.6cm, text centered, draw=black, fill=red!20]
\tikzstyle{process} = [rectangle, minimum height=0.6cm, text centered, text width=2.5cm, draw=black, fill=orange!20]
\tikzstyle{decision} = [diamond, aspect=2, minimum height=0.6cm, text centered, draw=black, fill=green!20, inner sep=0pt]
\tikzstyle{arrow} = [-{Stealth}, shorten >=1pt, thick]

\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.9}
\usepackage{listings}
\lstset{
  inputencoding=utf8,
  extendedchars=\false,
  escapeinside={\%*}{*)},
  language=Python,
  basicstyle=\scriptsize\ttfamily,
  stringstyle=\scriptsize\ttfamily,
  keywordstyle=\color{OliveGreen},
  commentstyle=\color{gray},
  numbers=left,
  numberstyle=\tiny,
  stepnumber=1,
  numbersep=5pt,
  backgroundcolor=\color{lightlightgray},
  frame=none,
  tabsize=2,
  captionpos=t,
  breaklines=true,
  breakatwhitespace=false,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  columns=flexible
}

\usetheme{Warsaw}
\usecolortheme[snowy]{owl}
%\setbeamertemplate{itemize/enumerate body begin}{\footnotesize}
%\setbeamertemplate{itemize/enumerate subbody begin}{\scriptsize}
%\setbeamertemplate{itemize/enumerate subsubbody begin}{\tiny}

\author{Ali Polatel}
\title{Syd+Youki=Syd-OCI}
\subtitle{Introduction to a Secure Container Runtime for Linux}
\institute{
  \noindent
  \includegraphics[height=0.2\textheight,width=0.2\textwidth]{zebrapig}
  \hspace{0.1\textwidth}
  \includegraphics[height=0.2\textheight,width=0.2\textwidth]{syd}
  \hspace{0.1\textwidth}
  \includegraphics[height=0.2\textheight,width=0.2\textwidth]{youki}
}
\date{FOSDEM, 2025}

\usepackage{hyperref}
\hypersetup{%
    hyperfootnotes=true,
    breaklinks=true,
    colorlinks=true,
    urlcolor=black,
    citecolor=black,
    linkcolor=black,
    pdftitle={Syd},
    pdfauthor={Ali Polatel},
    pdfsubject={Ali Polatel, Syd},
    pdflang={en},
    pdfkeywords={Linux, Sandboxing, Containers},
    pdfproducer={LuaLaTeX, BibTeX, hyperref, memoir},
    pdfpagelabels=true
    pdfborder={0 0 0},
}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Before we start...}
  \framesubtitle{The game is on! Viva la revolución!}

  \begin{itemize}
    \item CTF: \{https,ssh\}://syd.chesswob.org
      \begin{itemize}
        \item user/pass: syd
        \item rules: \texttt{/etc/user.syd-3}
        \item goal: read \texttt{/etc/CTF} \& get 200€!
      \end{itemize}
    \item GIT: https://gitlab.exherbo.org/sydbox/sydbox.git
    \item DOC: https://man.exherbolinux.org
    \item ML: https://lists.sr.ht/\~{}alip/exherbo-dev
    \item IRC: \#sydbox at Libera
    \item Matrix: \#sydbox:mailstation.de
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{History: Youki.git/README.md}
  \framesubtitle{All you touch and all you see is all your life will ever be.}

  \begin{itemize}
    \item Implementation of the OCI runtime-spec in Rust, similar to
      runc.
    \item \texttt{youki} is pronounced as \textipa{/jo\textupsilon ki/} or
      \texttt{yoh-key}. \texttt{youki} is named after the Japanese word
      \texttt{'youki'}, which means 'a container'. In Japanese language,
      youki also means 'cheerful', 'merry', or 'hilarious'.
    \item Here is why we are writing a new container runtime in Rust:
      \begin{itemize}
        \item Rust is one of the best languages to implement the
          oci-runtime spec. Many very nice container tools are currently
          written in Go. However, the container runtime requires the use
          of system calls, which requires a bit of special handling when
          implemented in Go. This tricky (e.g. \texttt{namespaces(7)},
          \texttt{fork(2)}); with Rust too, but it's not that tricky.
          And, unlike in C, Rust provides the benefit of memory safety.
          While Rust is not yet a major player in the container field,
          it has the potential to contribute a lot: something this
          project attempts to exemplify.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{History: Syd-OCI}
  \framesubtitle{Did you exchange a walk-on part in the war for a lead role in a cage?}

  \begin{itemize}
    \item Syd-OCI: The {\faPeace}ther Cønt{\CircledA}iner Runtïme
      \begin{itemize}
        \item Syd: Make sandboxing as easy as text searching is with
          \texttt{grep(1)}!
        \item Watch ``Syd: An Introduction to Secure Application
          Sandboxing for Linux''
        \item Youki: \texttt{libcgroups}, \texttt{libcontainer}, \texttt{liboci-cli}
        \item Syd-OCI: \texttt{s/DefaultExecutor/SydExecutor/ < youki/src/main.rs}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Overview: What?}
  \framesubtitle{Welcome my son, welcome to the machine.}

  \begin{itemize}
    \item A secure container runtime for OCI-compliant Linux containers
    \item Licensed \texttt{GPL-3.0}, forever free
    \item \texttt{cargo install --features oci --locked syd}, requires \texttt{libseccomp}
    \item \texttt{cargo install --locked pandora\_box}
    \item \texttt{docker run -it --runtime=syd-oci alpine}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Overview: How?}
  \framesubtitle{You dreamed of a big star, he played a mean guitar.}

  \begin{itemize}
    \item \texttt{SYD\_CONFIG\_DIR}: Syd-OCI configuration directory
      \begin{itemize}
        \item For system-wide containers: ``/etc/syd/oci''
        \item For rootless containers, one of the following:
          \begin{itemize}
            \item ``\$\{XDG\_CONFIG\_HOME\}/syd/oci'' where
              \texttt{XDG\_CONFIG\_HOME} is
              usually ``\~{}/.config''.
            \item ``\$\{HOME\}/.syd/oci'' if \texttt{XDG\_CONFIG\_HOME}
              is not set.
          \end{itemize}
      \end{itemize}
    \item \texttt{syd-oci} attempts to configure the \texttt{syd(1)}
      sandbox in the following order, and parses the first file or
      profile it locates and stops processing:
      \begin{itemize}
        \item If hostname and domainname is defined for the container,
          try to load
          ``\$\{SYD\_CONFIG\_DIR\}/\$\{hostname\}.\$\{domainname\}.syd-3''.
        \item If domainname is defined for the container, try to load
          ``\$\{SYD\_CONFIG\_DIR\}/\$\{domainname\}.syd-3''.
        \item If hostname is defined for the container, try to load
          ``\$\{SYD\_CONFIG\_DIR\}/\$\{hostname\}.syd-3''.
        \item Try to load ``\$\{SYD\_CONFIG\_DIR\}/default.syd-3''.
        \item Load the builtin ``oci'' profile. This profile is designed
          to be combined with \texttt{pandora(1)} and learning mode.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Comparison: Syd-OCI vs. gVisor}
  \framesubtitle{Breathe, breathe in the air. Don't be afraid to care.}

  \begin{itemize}
    \item Rust vs. Go!
      \begin{itemize}
        \item Rust has idiomatic, strict error checking
        \item Rust has no garbage collection, less side-effects in code
        \item Allows for more refined per-syd-thread seccomp-bpf filters
          \begin{itemize}
            \item \texttt{syd\_main}, \texttt{syd\_emu}, \texttt{syd\_mon},
              \texttt{syd\_int}, ...
          \end{itemize}
      \end{itemize}
    \item PIE \& ASLR, AT\_SECURE, SegvGuard, \{S,\}ROP mitigations
    \item Syd-OCI is noticably faster than gVisor.
      \begin{itemize}
        \item Less isolation, less overhead: \texttt{sydbox.git/bench/log}
        \item Layered security: Landlock Houdini, \texttt{CVE-2024-42318}
      \end{itemize}
    \item I NEED MORE FAST! GO FAST! UNROLL MY LOOPZ!
      \begin{itemize}
        \item Do not panic, take your towel!
        \item \texttt{trace/allow\_unsafe\_nice:1}
        \item \texttt{trace/allow\_unsafe\_ptrace:1}
        \item \texttt{trace/allow\_unsafe\_spec\_exec:1}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The End}
  \framesubtitle{You'll lose your mind and play free games for May!}

  \begin{itemize}
    \item CTF: \{https,ssh\}://syd.chesswob.org
      \begin{itemize}
        \item user/pass: syd
        \item rules: \texttt{/etc/user.syd-3}
        \item goal: read \texttt{/etc/CTF} \& get 200€!
      \end{itemize}
    \item GIT: https://gitlab.exherbo.org/sydbox/sydbox.git
    \item DOC: https://man.exherbolinux.org
    \item ML: https://lists.sr.ht/\~{}alip/exherbo-dev
    \item IRC: \#sydbox at Libera
    \item Matrix: \#sydbox:mailstation.de
  \end{itemize}

\end{frame}

\end{document}
