//
// Syd: rock-solid application kernel
// tests/tests.rs: Integration tests
//
// Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
// Copyright (c) 2023 Johannes Nixdorf <mixi@exherbo.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::{env, process::Command};

const SYD_EXE: &'static str = env!("CARGO_BIN_EXE_syd");
const SYD_AUX_EXE: &'static str = env!("CARGO_BIN_EXE_syd-aux");
const SYD_BIT_EXE: &'static str = env!("CARGO_BIN_EXE_syd-bit");
const SYD_CAP_EXE: &'static str = env!("CARGO_BIN_EXE_syd-cap");
const SYD_DNS_EXE: &'static str = env!("CARGO_BIN_EXE_syd-dns");
const SYD_ENV_EXE: &'static str = env!("CARGO_BIN_EXE_syd-env");
const SYD_EXEC_EXE: &'static str = env!("CARGO_BIN_EXE_syd-exec");
const SYD_HEX_EXE: &'static str = env!("CARGO_BIN_EXE_syd-hex");
const SYD_INFO_EXE: &'static str = env!("CARGO_BIN_EXE_syd-info");
const SYD_SIZE_EXE: &'static str = env!("CARGO_BIN_EXE_syd-size");
const SYD_AES_EXE: &'static str = env!("CARGO_BIN_EXE_syd-aes");
const SYD_KEY_EXE: &'static str = env!("CARGO_BIN_EXE_syd-key");
const SYD_LOCK_EXE: &'static str = env!("CARGO_BIN_EXE_syd-lock");
const SYD_PDS_EXE: &'static str = env!("CARGO_BIN_EXE_syd-pds");
const SYD_ELF_EXE: &'static str = env!("CARGO_BIN_EXE_syd-elf");
const SYD_CPU_EXE: &'static str = env!("CARGO_BIN_EXE_syd-cpu");
const SYD_TOR_EXE: &'static str = env!("CARGO_BIN_EXE_syd-tor");
const SYD_TEST_EXE: &'static str = env!("CARGO_BIN_EXE_syd-test");
const SYD_TEST_DO_EXE: &'static str = env!("CARGO_BIN_EXE_syd-test-do");

#[test]
fn syd_test() {
    env::set_var("CARGO_BIN_EXE_syd", SYD_EXE);
    env::set_var("CARGO_BIN_EXE_syd-aux", SYD_AUX_EXE);
    env::set_var("CARGO_BIN_EXE_syd-bit", SYD_BIT_EXE);
    env::set_var("CARGO_BIN_EXE_syd-cap", SYD_CAP_EXE);
    env::set_var("CARGO_BIN_EXE_syd-dns", SYD_DNS_EXE);
    env::set_var("CARGO_BIN_EXE_syd-env", SYD_ENV_EXE);
    env::set_var("CARGO_BIN_EXE_syd-exec", SYD_EXEC_EXE);
    env::set_var("CARGO_BIN_EXE_syd-hex", SYD_HEX_EXE);
    env::set_var("CARGO_BIN_EXE_syd-info", SYD_INFO_EXE);
    env::set_var("CARGO_BIN_EXE_syd-size", SYD_SIZE_EXE);
    env::set_var("CARGO_BIN_EXE_syd-aes", SYD_AES_EXE);
    env::set_var("CARGO_BIN_EXE_syd-key", SYD_KEY_EXE);
    env::set_var("CARGO_BIN_EXE_syd-lock", SYD_LOCK_EXE);
    env::set_var("CARGO_BIN_EXE_syd-pds", SYD_PDS_EXE);
    env::set_var("CARGO_BIN_EXE_syd-elf", SYD_ELF_EXE);
    env::set_var("CARGO_BIN_EXE_syd-cpu", SYD_CPU_EXE);
    env::set_var("CARGO_BIN_EXE_syd-tor", SYD_TOR_EXE);
    env::set_var("CARGO_BIN_EXE_syd-test-do", SYD_TEST_DO_EXE);
    let status = Command::new(SYD_TEST_EXE)
        .status()
        .expect("execute syd-test");
    assert!(
        status.success(),
        "{} tests failed!",
        status.code().unwrap_or(-1)
    );
}
