# Syd benchmark: linux-20241224163304

| Command | Mean [µs] | Min [µs] | Max [µs] | Relative |
|:---|---:|---:|---:|---:|
| `bash syd-bench-linux.Mj4Zd/linux-compile.sh` | 172364204.0 ± 2719592.6 | 167015605.2 | 176480332.9 | 1.00 |
| ` syd-bench-linux.Mj4Zd/linux-compile.sh` | 172470539.2 ± 4240991.9 | 165655092.3 | 179037354.8 | 1.00 ± 0.03 |
| `syd -q -ppaludis -mtrace/sync_seccomp:0 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 246262080.3 ± 4933681.1 | 238029026.0 | 254801525.4 | 1.43 ± 0.04 |
| `syd -q -ppaludis -mtrace/sync_seccomp:1 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 244111255.5 ± 3739604.0 | 236592590.1 | 248726216.0 | 1.42 ± 0.03 |
| `syd -ppaludis -mtrace/sync_seccomp:0 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 244930895.8 ± 5424789.8 | 240232780.0 | 258378300.5 | 1.42 ± 0.04 |
| `syd -ppaludis -mtrace/sync_seccomp:1 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 226065181.7 ± 712642.0 | 225356773.3 | 227645766.8 | 1.31 ± 0.02 |
| `syd -q -poci -mtrace/sync_seccomp:0 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 234477637.9 ± 965573.0 | 233286176.8 | 236357323.3 | 1.36 ± 0.02 |
| `syd -q -poci -mtrace/sync_seccomp:1 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 233037805.3 ± 1223215.4 | 231869768.6 | 235033362.5 | 1.35 ± 0.02 |
| `syd -poci -mtrace/sync_seccomp:0 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 233877200.7 ± 650564.7 | 233260718.7 | 234967944.6 | 1.36 ± 0.02 |
| `syd -poci -mtrace/sync_seccomp:1 -pP -mallow/all+/*** -mlock:on syd-bench-linux.Mj4Zd/linux-compile.sh` | 233467870.1 ± 1971660.9 | 231673625.9 | 237674247.0 | 1.35 ± 0.02 |

## Machine

```
Linux gantenbein 6.12.4 #222 SMP Tue Dec 10 21:16:12 CET 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-368-g91b279f2f (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, -oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.4 x86_64
Host (target): 6.12.4 x86_64
Target Environment: gnu
Target Persona: linux
Target Pointer Width: 64
Target CPU Count: 20 (10 physical)
Target CPU Endian: little
Target CPU Features: fxsr,sse,sse2
```

## GVisor

runsc not found!
