# Syd benchmark: git-20250101063853

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.4iR15qQVLq/git-compile.sh` | 86.988 ± 0.681 | 86.238 | 87.568 | 1.85 ± 1.97 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.4iR15qQVLq/git-compile.sh` | 587.281 ± 14.642 | 570.770 | 598.687 | 12.46 ± 13.29 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.4iR15qQVLq/git-compile.sh` | 361.779 ± 2.805 | 359.633 | 364.953 | 7.67 ± 8.19 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.4iR15qQVLq/git-compile.sh` | 47.145 ± 50.288 | 17.547 | 105.208 | 1.00 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.4iR15qQVLq/git-compile.sh` | 47.319 ± 50.828 | 17.872 | 106.010 | 1.00 ± 1.52 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.4iR15qQVLq/git-compile.sh` | 47.323 ± 49.987 | 18.401 | 105.043 | 1.00 ± 1.51 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.4iR15qQVLq/git-compile.sh` | 101.955 ± 1.256 | 101.012 | 103.380 | 2.16 ± 2.31 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.4iR15qQVLq/git-compile.sh` | 100.975 ± 0.669 | 100.382 | 101.700 | 2.14 ± 2.28 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.4iR15qQVLq/git-compile.sh` | 101.471 ± 2.591 | 98.550 | 103.490 | 2.15 ± 2.30 |

## Machine

```
Linux build 6.12.6-0-lts #1-Alpine SMP PREEMPT_DYNAMIC 2024-12-20 08:51:07 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-771-g2d18edf8 (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, -oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.6-0-lts x86_64
Host (target): 6.12.6-0-lts x86_64
Environment: musl-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: adx,aes,avx,avx2,bmi1,bmi2,cmpxchg16b,crt-static,f16c,fma,fxsr,lzcnt,movbe,pclmulqdq,popcnt,rdrand,rdseed,sha,sse,sse2,sse3,sse4.1,sse4.2,ssse3,xsave,xsavec,xsaveopt,xsaves
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
