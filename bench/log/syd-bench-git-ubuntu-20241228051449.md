# Syd benchmark: git-20241228042544

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.o03rtrkgsp/git-compile.sh` | 58.004 ± 0.471 | 57.473 | 58.371 | 1.00 |
| `sudo runsc -ignore-cgroups -platform systrap do /tmp/tmp.o03rtrkgsp/git-compile.sh` | 162.874 ± 2.187 | 160.630 | 164.999 | 2.81 ± 0.04 |
| `sudo runsc -ignore-cgroups -platform ptrace do /tmp/tmp.o03rtrkgsp/git-compile.sh` | 162.068 ± 1.451 | 160.458 | 163.275 | 2.79 ± 0.03 |
| `sudo runsc -ignore-cgroups -platform kvm do /tmp/tmp.o03rtrkgsp/git-compile.sh` | 345.359 ± 86.973 | 265.905 | 438.280 | 5.95 ± 1.50 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.o03rtrkgsp/git-compile.sh` | 108.684 ± 0.757 | 107.974 | 109.481 | 1.87 ± 0.02 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.o03rtrkgsp/git-compile.sh` | 108.662 ± 0.298 | 108.473 | 109.005 | 1.87 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.o03rtrkgsp/git-compile.sh` | 115.293 ± 1.635 | 114.158 | 117.167 | 1.99 ± 0.03 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.o03rtrkgsp/git-compile.sh` | 105.346 ± 0.788 | 104.441 | 105.880 | 1.82 ± 0.02 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.o03rtrkgsp/git-compile.sh` | 105.559 ± 0.641 | 104.985 | 106.252 | 1.82 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.o03rtrkgsp/git-compile.sh` | 112.232 ± 1.548 | 110.470 | 113.375 | 1.93 ± 0.03 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 1 hour, 56 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 135MiB / 3916MiB
```

## Syd

```
syd 3.29.4-599-g8746a433-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 4 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
