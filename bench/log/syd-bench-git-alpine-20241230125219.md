# Syd benchmark: git-20241230104657

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.WGFfnmgybh/git-compile.sh` | 87.371 ± 0.405 | 87.092 | 87.835 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.WGFfnmgybh/git-compile.sh` | 562.093 ± 1.142 | 560.800 | 562.960 | 6.43 ± 0.03 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.WGFfnmgybh/git-compile.sh` | 333.992 ± 11.968 | 323.486 | 347.020 | 3.82 ± 0.14 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WGFfnmgybh/git-compile.sh` | 117.638 ± 0.223 | 117.381 | 117.768 | 1.35 ± 0.01 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WGFfnmgybh/git-compile.sh` | 118.615 ± 0.574 | 118.276 | 119.278 | 1.36 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WGFfnmgybh/git-compile.sh` | 121.392 ± 1.835 | 119.652 | 123.309 | 1.39 ± 0.02 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WGFfnmgybh/git-compile.sh` | 117.305 ± 0.810 | 116.417 | 118.005 | 1.34 ± 0.01 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WGFfnmgybh/git-compile.sh` | 114.396 ± 1.164 | 113.288 | 115.610 | 1.31 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WGFfnmgybh/git-compile.sh` | 116.933 ± 1.035 | 116.037 | 118.066 | 1.34 ± 0.01 |

## Machine

```
Linux build 6.12.6-0-lts #1-Alpine SMP PREEMPT_DYNAMIC 2024-12-20 08:51:07 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-637-gdc1ec697 (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, -oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.6-0-lts x86_64
Host (target): 6.12.6-0-lts x86_64
Environment: musl-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: adx,aes,avx,avx2,bmi1,bmi2,cmpxchg16b,crt-static,f16c,fma,fxsr,lzcnt,movbe,pclmulqdq,popcnt,rdrand,rdseed,sha,sse,sse2,sse3,sse4.1,sse4.2,ssse3,xsave,xsavec,xsaveopt,xsaves
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
