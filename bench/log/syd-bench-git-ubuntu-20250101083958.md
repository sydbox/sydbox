# Syd benchmark: git-20250101063434

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 59.982 ± 0.576 | 59.443 | 60.588 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 164.946 ± 1.829 | 162.834 | 166.023 | 2.75 ± 0.04 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 166.563 ± 3.572 | 162.855 | 169.981 | 2.78 ± 0.07 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 400.587 ± 114.089 | 270.841 | 485.228 | 6.68 ± 1.90 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 112.286 ± 2.617 | 109.612 | 114.841 | 1.87 ± 0.05 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 113.626 ± 1.466 | 112.088 | 115.007 | 1.89 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 111.638 ± 0.641 | 111.156 | 112.366 | 1.86 ± 0.02 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 85.772 ± 0.443 | 85.264 | 86.075 | 1.43 ± 0.02 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 89.318 ± 1.099 | 88.525 | 90.572 | 1.49 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.g6UwkAOBZq/git-compile.sh` | 91.503 ± 0.726 | 90.788 | 92.240 | 1.53 ± 0.02 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 1 hour, 49 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 137MiB / 3916MiB
```

## Syd

```
syd 3.29.4-771-g2d18edf8-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 4 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
