# Syd benchmark: git-20241229200821

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.WZzozxrHT5/git-compile.sh` | 85.195 ± 0.359 | 84.785 | 85.452 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.WZzozxrHT5/git-compile.sh` | 573.190 ± 25.791 | 554.630 | 602.640 | 6.73 ± 0.30 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.WZzozxrHT5/git-compile.sh` | 311.312 ± 11.011 | 304.073 | 323.983 | 3.65 ± 0.13 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WZzozxrHT5/git-compile.sh` | 114.960 ± 1.156 | 114.174 | 116.287 | 1.35 ± 0.01 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WZzozxrHT5/git-compile.sh` | 115.128 ± 0.478 | 114.584 | 115.484 | 1.35 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WZzozxrHT5/git-compile.sh` | 118.163 ± 1.386 | 116.758 | 119.528 | 1.39 ± 0.02 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WZzozxrHT5/git-compile.sh` | 111.500 ± 0.207 | 111.278 | 111.687 | 1.31 ± 0.01 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WZzozxrHT5/git-compile.sh` | 111.853 ± 0.289 | 111.555 | 112.132 | 1.31 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.WZzozxrHT5/git-compile.sh` | 112.680 ± 0.213 | 112.434 | 112.808 | 1.32 ± 0.01 |

## Machine

```
Linux build 6.12.6-0-lts #1-Alpine SMP PREEMPT_DYNAMIC 2024-12-20 08:51:07 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-631-g28204386 (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, -oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.6-0-lts x86_64
Host (target): 6.12.6-0-lts x86_64
Environment: musl-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: adx,aes,avx,avx2,bmi1,bmi2,cmpxchg16b,crt-static,f16c,fma,fxsr,lzcnt,movbe,pclmulqdq,popcnt,rdrand,rdseed,sha,sse,sse2,sse3,sse4.1,sse4.2,ssse3,xsave,xsavec,xsaveopt,xsaves
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
