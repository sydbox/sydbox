# Syd benchmark: git-20250121173805

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 85.143 ± 0.411 | 84.792 | 85.596 | 1018.82 ± 37.43 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 581.042 ± 30.623 | 547.853 | 608.203 | 6952.75 ± 445.42 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 311.800 ± 8.078 | 306.567 | 321.103 | 3731.00 ± 166.76 |
| `syd -puser -mbind-tmpfs:/tmp -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 121.895 ± 1.385 | 120.341 | 122.998 | 1458.60 ± 55.65 |
| `syd -puser -mbind-tmpfs:/tmp -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 122.213 ± 2.088 | 120.137 | 124.313 | 1462.40 ± 58.83 |
| `env SYD_SYNC_SCMP=1 syd -puser -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 0.084 ± 0.003 | 0.078 | 0.091 | 1.00 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 100.718 ± 0.643 | 100.162 | 101.422 | 1205.20 ± 44.56 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 101.701 ± 1.158 | 100.620 | 102.923 | 1216.95 ± 46.44 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zR1m6EIvQU/git-compile.sh` | 111.192 ± 1.887 | 109.347 | 113.117 | 1330.52 ± 53.46 |

## Machine

```
Linux build 6.12.9-0-lts #1-Alpine SMP PREEMPT_DYNAMIC 2025-01-09 18:01:12 x86_64 GNU/Linux
```

## Syd

```
syd 3.30.0-d5952283 (Dreamy Merkle)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, -oci
LibSeccomp: v2.5.5 api:7
Landlock ABI 6 is fully enforced.
User namespaces are supported.
Host (build): 6.12.9-0-lts x86_64
Host (target): 6.12.9-0-lts x86_64
Environment: musl-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20250113.0
spec: 1.1.0-rc.1
```
