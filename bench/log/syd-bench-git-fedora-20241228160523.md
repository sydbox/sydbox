# Syd benchmark: git-20241228105553

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.8qIabIuceX/git-compile.sh` | 60.453 ± 1.993 | 59.195 | 62.751 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.8qIabIuceX/git-compile.sh` | 172.860 ± 1.825 | 170.759 | 174.062 | 2.86 ± 0.10 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.8qIabIuceX/git-compile.sh` | 162.707 ± 1.350 | 161.706 | 164.242 | 2.69 ± 0.09 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.8qIabIuceX/git-compile.sh` | 925.077 ± 1068.659 | 262.364 | 2157.900 | 15.30 ± 17.68 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.8qIabIuceX/git-compile.sh` | 112.189 ± 1.884 | 110.181 | 113.918 | 1.86 ± 0.07 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.8qIabIuceX/git-compile.sh` | 110.577 ± 2.689 | 108.922 | 113.680 | 1.83 ± 0.07 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.8qIabIuceX/git-compile.sh` | 115.524 ± 3.587 | 111.548 | 118.519 | 1.91 ± 0.09 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.8qIabIuceX/git-compile.sh` | 106.362 ± 1.410 | 104.867 | 107.670 | 1.76 ± 0.06 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.8qIabIuceX/git-compile.sh` | 106.981 ± 1.283 | 105.546 | 108.020 | 1.77 ± 0.06 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.8qIabIuceX/git-compile.sh` | 106.267 ± 1.338 | 105.289 | 107.792 | 1.76 ± 0.06 |

## Machine

```
Linux build 6.12.5-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Sun Dec 15 16:48:23 UTC 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-612-gb09a8ada-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.5-200.fc41.x86_64 x86_64
Host (target): 6.12.5-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
