# Syd benchmark: git-20241228104945

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 59.768 ± 0.564 | 59.152 | 60.260 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 162.755 ± 5.363 | 159.287 | 168.932 | 2.72 ± 0.09 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 160.875 ± 1.890 | 159.639 | 163.050 | 2.69 ± 0.04 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 769.948 ± 523.037 | 364.538 | 1360.338 | 12.88 ± 8.75 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 106.887 ± 0.362 | 106.613 | 107.298 | 1.79 ± 0.02 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 109.801 ± 0.268 | 109.527 | 110.063 | 1.84 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 118.488 ± 2.446 | 115.735 | 120.412 | 1.98 ± 0.04 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 109.609 ± 2.131 | 108.004 | 112.027 | 1.83 ± 0.04 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 105.986 ± 1.555 | 104.506 | 107.606 | 1.77 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.SXUQKcBQ8j/git-compile.sh` | 109.443 ± 0.851 | 108.550 | 110.244 | 1.83 ± 0.02 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 2 hours, 8 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 120MiB / 3916MiB
```

## Syd

```
syd 3.29.4-612-gb09a8ada-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 4 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
