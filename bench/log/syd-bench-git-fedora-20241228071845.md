# Syd benchmark: git-20241228051317

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.1fX8hicv4e/git-compile.sh` | 56.953 ± 0.033 | 56.920 | 56.987 | 1.00 |
| `sudo runsc -ignore-cgroups -platform systrap do /tmp/tmp.1fX8hicv4e/git-compile.sh` | 168.956 ± 7.634 | 160.142 | 173.434 | 2.97 ± 0.13 |
| `sudo runsc -ignore-cgroups -platform ptrace do /tmp/tmp.1fX8hicv4e/git-compile.sh` | 153.233 ± 2.527 | 150.315 | 154.716 | 2.69 ± 0.04 |
| `sudo runsc -ignore-cgroups -platform kvm do /tmp/tmp.1fX8hicv4e/git-compile.sh` | 1316.756 ± 1430.213 | 257.040 | 2943.546 | 23.12 ± 25.11 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1fX8hicv4e/git-compile.sh` | 110.707 ± 1.282 | 109.505 | 112.057 | 1.94 ± 0.02 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1fX8hicv4e/git-compile.sh` | 108.934 ± 0.699 | 108.263 | 109.659 | 1.91 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1fX8hicv4e/git-compile.sh` | 110.714 ± 1.304 | 109.492 | 112.087 | 1.94 ± 0.02 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1fX8hicv4e/git-compile.sh` | 102.109 ± 2.235 | 100.478 | 104.657 | 1.79 ± 0.04 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1fX8hicv4e/git-compile.sh` | 104.201 ± 1.261 | 102.897 | 105.414 | 1.83 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1fX8hicv4e/git-compile.sh` | 106.037 ± 1.563 | 104.244 | 107.115 | 1.86 ± 0.03 |

## Machine

```
Linux build 6.12.5-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Sun Dec 15 16:48:23 UTC 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-599-g8746a433-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.5-200.fc41.x86_64 x86_64
Host (target): 6.12.5-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
