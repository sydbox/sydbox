# Syd benchmark: git-20241229200313

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 57.908 ± 0.227 | 57.677 | 58.132 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 119.890 ± 91.599 | 14.283 | 177.756 | 2.07 ± 1.58 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 156.676 ± 3.344 | 153.274 | 159.959 | 2.71 ± 0.06 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 856.977 ± 306.549 | 611.945 | 1200.721 | 14.80 ± 5.29 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 106.968 ± 2.350 | 105.465 | 109.677 | 1.85 ± 0.04 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 105.618 ± 0.697 | 104.986 | 106.365 | 1.82 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 108.097 ± 1.333 | 107.301 | 109.636 | 1.87 ± 0.02 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 103.708 ± 1.537 | 101.979 | 104.918 | 1.79 ± 0.03 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 102.531 ± 0.218 | 102.343 | 102.771 | 1.77 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.1MmR1uMhi1/git-compile.sh` | 104.280 ± 0.202 | 104.080 | 104.484 | 1.80 ± 0.01 |

## Machine

```
Linux build 6.12.5-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Sun Dec 15 16:48:23 UTC 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-631-g28204386-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.5-200.fc41.x86_64 x86_64
Host (target): 6.12.5-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
