# Syd benchmark: git-20241230144437

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 58.615 ± 0.245 | 58.347 | 58.830 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 162.100 ± 6.254 | 158.424 | 169.321 | 2.77 ± 0.11 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 170.915 ± 0.822 | 170.008 | 171.608 | 2.92 ± 0.02 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 378.063 ± 181.050 | 270.790 | 587.097 | 6.45 ± 3.09 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 108.262 ± 0.717 | 107.825 | 109.089 | 1.85 ± 0.01 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 111.595 ± 0.465 | 111.121 | 112.051 | 1.90 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 116.980 ± 2.274 | 114.354 | 118.313 | 2.00 ± 0.04 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 107.355 ± 1.640 | 105.597 | 108.844 | 1.83 ± 0.03 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 107.827 ± 1.648 | 106.563 | 109.691 | 1.84 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.CKrYMQvXFo/git-compile.sh` | 116.519 ± 0.653 | 115.765 | 116.905 | 1.99 ± 0.01 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 1 hour, 49 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 118MiB / 3916MiB
```

## Syd

```
syd 3.29.4-641-ga0ece83d-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 4 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
