# Syd benchmark: git-20250101064545

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.ywdex5A8yI/git-compile.sh` | 58.844 ± 0.641 | 58.115 | 59.321 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.ywdex5A8yI/git-compile.sh` | 174.719 ± 1.522 | 173.711 | 176.469 | 2.97 ± 0.04 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.ywdex5A8yI/git-compile.sh` | 113.074 ± 88.687 | 11.123 | 172.412 | 1.92 ± 1.51 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.ywdex5A8yI/git-compile.sh` | 814.294 ± 615.370 | 309.512 | 1499.786 | 13.84 ± 10.46 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.ywdex5A8yI/git-compile.sh` | 113.510 ± 0.994 | 112.603 | 114.572 | 1.93 ± 0.03 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.ywdex5A8yI/git-compile.sh` | 112.688 ± 1.537 | 111.002 | 114.011 | 1.92 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.ywdex5A8yI/git-compile.sh` | 115.136 ± 0.862 | 114.508 | 116.119 | 1.96 ± 0.03 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.ywdex5A8yI/git-compile.sh` | 87.211 ± 1.336 | 86.279 | 88.742 | 1.48 ± 0.03 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.ywdex5A8yI/git-compile.sh` | 86.805 ± 0.866 | 85.966 | 87.695 | 1.48 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.ywdex5A8yI/git-compile.sh` | 87.846 ± 2.273 | 85.607 | 90.151 | 1.49 ± 0.04 |

## Machine

```
Linux build 6.12.6-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Thu Dec 19 21:06:34 UTC 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-771-g2d18edf8-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.6-200.fc41.x86_64 x86_64
Host (target): 6.12.6-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
