# Syd benchmark: git-20241230105234

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.oMnrY31mHb/git-compile.sh` | 59.161 ± 0.655 | 58.584 | 59.873 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.oMnrY31mHb/git-compile.sh` | 164.733 ± 6.180 | 158.418 | 170.769 | 2.78 ± 0.11 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.oMnrY31mHb/git-compile.sh` | 159.849 ± 4.082 | 156.965 | 164.520 | 2.70 ± 0.08 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.oMnrY31mHb/git-compile.sh` | 303.019 ± 47.124 | 272.382 | 357.282 | 5.12 ± 0.80 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.oMnrY31mHb/git-compile.sh` | 112.664 ± 0.481 | 112.201 | 113.162 | 1.90 ± 0.02 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.oMnrY31mHb/git-compile.sh` | 113.434 ± 0.637 | 112.729 | 113.968 | 1.92 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.oMnrY31mHb/git-compile.sh` | 117.393 ± 1.223 | 116.011 | 118.334 | 1.98 ± 0.03 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.oMnrY31mHb/git-compile.sh` | 105.822 ± 2.184 | 103.301 | 107.134 | 1.79 ± 0.04 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.oMnrY31mHb/git-compile.sh` | 108.368 ± 1.096 | 107.688 | 109.632 | 1.83 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.oMnrY31mHb/git-compile.sh` | 111.772 ± 2.556 | 109.026 | 114.081 | 1.89 ± 0.05 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 2 hours, 10 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 141MiB / 3916MiB
```

## Syd

```
syd 3.29.4-637-gdc1ec697-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 4 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
