# Syd benchmark: git-20250121165259

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 57.863 ± 0.107 | 57.774 | 57.982 | 413.45 ± 18.14 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 172.212 ± 1.359 | 171.384 | 173.780 | 1230.51 ± 54.82 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 111.137 ± 84.758 | 13.363 | 163.776 | 794.11 ± 606.62 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 283.729 ± 38.103 | 258.681 | 327.578 | 2027.33 ± 286.40 |
| `syd -puser -mbind-tmpfs:/tmp -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 127.728 ± 9.097 | 118.200 | 136.322 | 912.66 ± 76.33 |
| `syd -puser -mbind-tmpfs:/tmp -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 117.555 ± 1.712 | 115.829 | 119.253 | 839.97 ± 38.80 |
| `env SYD_SYNC_SCMP=1 syd -puser -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 0.140 ± 0.006 | 0.127 | 0.151 | 1.00 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 96.065 ± 0.393 | 95.820 | 96.518 | 686.41 ± 30.22 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 96.394 ± 0.193 | 96.246 | 96.612 | 688.77 ± 30.23 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.0PwwDW9E1c/git-compile.sh` | 106.849 ± 2.185 | 104.882 | 109.202 | 763.47 ± 36.93 |

## Machine

```
Linux build 6.12.9-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Thu Jan  9 16:05:40 UTC 2025 x86_64 GNU/Linux
```

## Syd

```
syd 3.30.0-d5952283-dirty (Dreamy Merkle)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
LibSeccomp: v2.5.5 api:7
Landlock ABI 6 is fully enforced.
User namespaces are supported.
Host (build): 6.12.9-200.fc41.x86_64 x86_64
Host (target): 6.12.9-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20250113.0
spec: 1.1.0-rc.1
```
