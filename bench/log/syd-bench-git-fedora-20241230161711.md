# Syd benchmark: git-20241230143205

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 59.232 ± 0.353 | 58.860 | 59.561 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 115.595 ± 89.012 | 12.816 | 167.700 | 1.95 ± 1.50 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 110.545 ± 85.994 | 11.344 | 163.936 | 1.87 ± 1.45 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 300.075 ± 46.247 | 265.916 | 352.703 | 5.07 ± 0.78 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 109.417 ± 0.763 | 108.648 | 110.174 | 1.85 ± 0.02 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 109.425 ± 1.698 | 107.853 | 111.226 | 1.85 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 112.687 ± 2.172 | 110.634 | 114.961 | 1.90 ± 0.04 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 104.311 ± 0.538 | 103.986 | 104.931 | 1.76 ± 0.01 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 107.890 ± 0.486 | 107.350 | 108.293 | 1.82 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.wJUVZmnGuC/git-compile.sh` | 110.353 ± 1.754 | 108.333 | 111.482 | 1.86 ± 0.03 |

## Machine

```
Linux build 6.12.6-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Thu Dec 19 21:06:34 UTC 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-641-ga0ece83d-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.6-200.fc41.x86_64 x86_64
Host (target): 6.12.6-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
