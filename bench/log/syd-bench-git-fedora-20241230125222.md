# Syd benchmark: git-20241230101732

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.guTNAvhS01/git-compile.sh` | 58.339 ± 0.178 | 58.134 | 58.457 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.guTNAvhS01/git-compile.sh` | 172.666 ± 3.794 | 168.819 | 176.406 | 2.96 ± 0.07 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.guTNAvhS01/git-compile.sh` | 163.539 ± 3.529 | 159.507 | 166.069 | 2.80 ± 0.06 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.guTNAvhS01/git-compile.sh` | 279.499 ± 35.605 | 258.302 | 320.605 | 4.79 ± 0.61 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.guTNAvhS01/git-compile.sh` | 106.801 ± 0.050 | 106.748 | 106.848 | 1.83 ± 0.01 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.guTNAvhS01/git-compile.sh` | 107.353 ± 0.095 | 107.264 | 107.454 | 1.84 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.guTNAvhS01/git-compile.sh` | 110.601 ± 1.006 | 109.890 | 111.752 | 1.90 ± 0.02 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.guTNAvhS01/git-compile.sh` | 105.716 ± 1.298 | 104.275 | 106.794 | 1.81 ± 0.02 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.guTNAvhS01/git-compile.sh` | 106.910 ± 1.595 | 105.389 | 108.570 | 1.83 ± 0.03 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.guTNAvhS01/git-compile.sh` | 106.721 ± 0.198 | 106.519 | 106.914 | 1.83 ± 0.01 |

## Machine

```
Linux build 6.12.6-200.fc41.x86_64 #1 SMP PREEMPT_DYNAMIC Thu Dec 19 21:06:34 UTC 2024 x86_64 GNU/Linux
```

## Syd

```
syd 3.29.4-637-gdc1ec697-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 6 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.12.6-200.fc41.x86_64 x86_64
Host (target): 6.12.6-200.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
