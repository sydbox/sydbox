# Syd benchmark: inkscape-20250106063312

| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 572.1 ± 17.0 | 551.0 | 594.7 | 1.58 ± 0.07 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 574.0 ± 107.0 | 480.4 | 752.8 | 1.58 ± 0.30 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 682.2 ± 43.1 | 634.2 | 723.7 | 1.88 ± 0.13 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 363.2 ± 10.7 | 346.2 | 377.7 | 1.00 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 897.3 ± 64.0 | 824.0 | 942.4 | 2.47 ± 0.19 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 1021.4 ± 57.4 | 964.5 | 1079.4 | 2.81 ± 0.18 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.5vlw57kFvV/inkscape-compile.sh` | 739.6 ± 10.7 | 729.1 | 750.6 | 2.04 ± 0.07 |

## Machine

```
krueger@kronos
--------------
OS: Fedora Linux 41 (Forty One) x86_64
Host: Z790 Steel Legend WiFi
Kernel: 6.11.11-300.fc41.x86_64
Uptime: 22 days, 19 hours, 17 mins
Packages: 4064 (rpm), 68 (flatpak), 9 (snap)
Shell: zsh 5.9
Resolution: 1920x1080
DE: Plasma 6.2.5
WM: kwin
WM Theme: Sweet-Dark
Theme: Sweet [Plasma], Sweet [GTK2/3]
Icons: [Plasma], candy-icons [GTK2/3]
Terminal: konsole
CPU: 13th Gen Intel i5-13400F (16) @ 4.600GHz
GPU: NVIDIA GeForce RTX 4070
Memory: 27174MiB / 64119MiB
```

## Syd

```
syd 3.29.4-ab38820fd-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, -oci
Landlock ABI 5 is fully enforced.
LibSeccomp: v2.5.5 api:6
Host (build): 6.11.11-300.fc41.x86_64 x86_64
Host (target): 6.11.11-300.fc41.x86_64 x86_64
Environment: gnu-linux-64
CPU: 16 (10 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version VERSION_MISSING
spec: 1.1.0
```
