# Syd benchmark: git-20250121174046

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 59.242 ± 0.354 | 59.026 | 59.651 | 457.63 ± 17.83 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 167.215 ± 1.601 | 165.370 | 168.238 | 1291.69 ± 51.24 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 161.441 ± 1.739 | 160.033 | 163.384 | 1247.09 ± 49.85 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 942.312 ± 660.705 | 267.545 | 1587.994 | 7279.11 ± 5111.46 |
| `syd -puser -mbind-tmpfs:/tmp -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 107.129 ± 0.911 | 106.318 | 108.115 | 827.55 ± 32.63 |
| `syd -puser -mbind-tmpfs:/tmp -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 105.649 ± 0.500 | 105.177 | 106.173 | 816.11 ± 31.65 |
| `env SYD_SYNC_SCMP=1 syd -puser -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 0.129 ± 0.005 | 0.122 | 0.141 | 1.00 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 93.836 ± 0.921 | 92.773 | 94.388 | 724.86 ± 28.80 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 91.398 ± 1.077 | 90.561 | 92.612 | 706.02 ± 28.42 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.zY2N8O3JKB/git-compile.sh` | 100.815 ± 1.300 | 99.982 | 102.314 | 778.77 ± 31.62 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 2 hours, 19 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 134MiB / 3916MiB
```

## Syd

```
syd 3.30.0-d5952283-dirty (Dreamy Merkle)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
LibSeccomp: v2.5.5 api:7
Landlock ABI 4 is fully enforced.
User namespaces are supported.
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20250113.0
spec: 1.1.0-rc.1
```
