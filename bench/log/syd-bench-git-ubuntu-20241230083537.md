# Syd benchmark: git-20241229194715

| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `bash /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 58.268 ± 0.235 | 58.119 | 58.539 | 1.00 |
| `sudo runsc --network=host -ignore-cgroups -platform systrap do /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 165.011 ± 0.608 | 164.614 | 165.711 | 2.83 ± 0.02 |
| `sudo runsc --network=host -ignore-cgroups -platform ptrace do /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 162.906 ± 0.915 | 161.994 | 163.825 | 2.80 ± 0.02 |
| `sudo runsc --network=host -ignore-cgroups -platform kvm do /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 290.378 ± 50.435 | 260.598 | 348.611 | 4.98 ± 0.87 |
| `syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 107.673 ± 0.360 | 107.261 | 107.926 | 1.85 ± 0.01 |
| `syd -poci -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 108.855 ± 0.761 | 107.984 | 109.387 | 1.87 ± 0.02 |
| `env SYD_SYNC_SCMP=1 syd -poci -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 113.010 ± 0.416 | 112.548 | 113.355 | 1.94 ± 0.01 |
| `syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 104.038 ± 0.943 | 103.094 | 104.981 | 1.79 ± 0.02 |
| `syd -ppaludis -plandlock -mallow/lock/write+/ -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 104.639 ± 0.696 | 104.121 | 105.430 | 1.80 ± 0.01 |
| `env SYD_SYNC_SCMP=1 syd -ppaludis -pP -mallow/all+/*** -mfilter/read+!proc/name -mlock:on /tmp/tmp.2GYs6x5B9X/git-compile.sh` | 110.883 ± 0.283 | 110.712 | 111.209 | 1.90 ± 0.01 |

## Machine

```
build@build
-----------
OS: Ubuntu 24.04.1 LTS x86_64
Host: KVM/QEMU (Standard PC (i440FX + PIIX, 1996) pc-i440fx-7.0)
Kernel: 6.8.0-51-generic
Uptime: 1 hour, 41 mins
Packages: 751 (dpkg)
Shell: sh
Resolution: 1280x800
CPU: AMD Ryzen 9 5900X (2) @ 3.693GHz
GPU: 00:02.0 Vendor 1234 Device 1111
Memory: 120MiB / 3916MiB
```

## Syd

```
syd 3.29.4-631-g28204386-dirty (Dreamy Galileo)
Author: Ali Polatel
License: GPL-3.0
Features: -debug, +oci
Landlock ABI 4 is fully enforced.
LibSeccomp: v2.5.5 api:7
Host (build): 6.8.0-51-generic x86_64
Host (target): 6.8.0-51-generic x86_64
Environment: gnu-linux-64
CPU: 2 (2 cores), little-endian
CPUFLAGS: fxsr,sse,sse2
Store Bypass Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
Indirect Branch Status: Speculation feature is enabled, mitigation is disabled (prctl can set speculation mitigation).
L1D Flush Status: Speculation feature is force-disabled, mitigation is enabled.
```

## GVisor

```
runsc version release-20241217.0
spec: 1.1.0-rc.1
```
