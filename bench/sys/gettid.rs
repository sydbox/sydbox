//
// Syd: rock-solid application kernel
// benches/sys/gettid.rs: gettid microbenchmarks
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
// Based in part upon gVisor's gettid_benchmark.cc which is:
//   Copyright 2020 The gVisor Authors.
//   SPDX-License-Identifier: Apache-2.0
//
// SPDX-License-Identifier: GPL-3.0

// A micro-benchmark which replicates the gVisor gettid
// micro-benchmarks, but without inline assembly.  Instead, we directly
// invoke the kernel via a simple `syscall!` macro.

use brunch::{benches, Bench};
use libc::SYS_gettid;
use nix::errno::Errno;
use syd::syscall;

/// Benchmark calling the `SYS_gettid` syscall via our macro.
fn bm_gettid() {
    // Just call `syscall!` in a tight loop.
    // gettid(2) never returns error.
    let _ = syscall!(SYS_gettid);
}

fn main() {
    benches!(
        inline:

        Bench::new("GetTID").run(|| {
            bm_gettid();
        }),
    );
}
