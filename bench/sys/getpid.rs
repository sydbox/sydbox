//
// Syd: rock-solid application kernel
// benches/sys/getpid.rs: getpid microbenchmarks
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
// Based in part upon gVisor's getpid_benchmark.cc which is:
//   Copyright 2020 The gVisor Authors.
//   SPDX-License-Identifier: Apache-2.0
//
// SPDX-License-Identifier: GPL-3.0

// A micro-benchmark which replicates the gVisor getpid
// micro-benchmarks, but without inline assembly.  Instead, we directly
// invoke the kernel via a simple `syscall!` macro.

use brunch::{benches, Bench};
use libc::SYS_getpid;
use nix::errno::Errno;
use syd::syscall;

/// Benchmark calling the `SYS_getpid` syscall via our macro.
fn bm_getpid() {
    // Just call `syscall!` in a tight loop.
    // getpid(2) never returns error.
    let _ = syscall!(SYS_getpid);
}

fn main() {
    benches!(
        inline:

        Bench::new("GetPID").run(|| {
            bm_getpid();
        }),
    );
}
