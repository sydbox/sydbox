//
// Syd: rock-solid application kernel
// benches/proc_fd.rs: Benchmarks for syd::proc::proc_fd()
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use nix::unistd::Pid;
use syd::proc::proc_fd;

pub fn proc_fd_benchmark(c: &mut Criterion) {
    let this = Pid::this();
    let that = Pid::from_raw(1);
    let this_path = format!("/proc/{this}/fd/0");
    let that_path = "/proc/1/fd/0";

    c.bench_function("proc_fd_this", |b| {
        b.iter(|| proc_fd(this, black_box(&this_path)))
    });

    c.bench_function("proc_fd_that", |b| {
        b.iter(|| proc_fd(that, black_box(&that_path)))
    });
}

criterion_group!(benches, proc_fd_benchmark,);
criterion_main!(benches);
