//
// Syd: rock-solid application kernel
// benches/path_unsafe.rs: Benchmarks for syd::fs::path_unsafe()
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::path::PathBuf;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use syd::fs::path_unsafe;

pub fn path_unsafe_benchmark(c: &mut Criterion) {
    let paths = vec![
        PathBuf::from("/dev/mem"),
        PathBuf::from("/home/user/safe_file"),
        PathBuf::from("/dev/cpu/0/msr"),
        PathBuf::from("/proc/1/environ"),
        PathBuf::from("/proc/1/task/1/mem"),
        PathBuf::from("/tmp"),
        PathBuf::from("/proc/kallsyms"),
    ];

    c.bench_function("path_unsafe", |b| {
        b.iter(|| {
            for path in &paths {
                let _ = path_unsafe(black_box(path), true);
            }
        })
    });
}

criterion_group!(benches, path_unsafe_benchmark,);
criterion_main!(benches);
