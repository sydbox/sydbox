//
// Syd: rock-solid application kernel
// benches/sandbox_forcemap.rs: Benchmarks for Sandbox' ForceMap
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::path::Path;

use criterion::{black_box, criterion_group, criterion_main, Criterion};

pub fn sandbox_forcemap_benchmark(c: &mut Criterion) {
    for i in &[5, 10, 50, 100] {
        c.bench_function(&format!("sandbox_forcemap {i}"), |b| {
            b.iter(|| {
                // Create sandbox
                let mut sandbox = syd::sandbox::Sandbox::default();
                sandbox.config("sandbox/force:on").unwrap();

                // Initialize sandbox
                let mut paths = vec![];
                for j in 0..*i {
                    let path = format!("/usr/bin/test-{j}");
                    let hash = "a".repeat(128);
                    let _ = sandbox.config(&format!("force+{path}:{hash}:kill"));
                    paths.push(path);
                }

                // Perform match on non-existing key.
                for _ in 0..*i {
                    let path = black_box(Path::new("/no/such/file"));
                    let _ = sandbox.check_force(black_box(&path));
                }

                // Perform match on existing key.
                for path in paths {
                    let path = black_box(&path);
                    for _ in 0..*i {
                        let _ = sandbox.check_force(black_box(&path));
                    }
                }

                // Remove paths from the map.
                for j in 0..*i {
                    let path = format!("/usr/bin/test-{j}");
                    let _ = sandbox.config(black_box(&format!("force-{path}")));
                }
            });
        });
    }
}

criterion_group!(benches, sandbox_forcemap_benchmark,);
criterion_main!(benches);
