//
// Syd: rock-solid application kernel
// benches/sandbox_forcemap.rs: Benchmarks for Sandbox' GlobSet
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::path::PathBuf;

use criterion::{black_box, criterion_group, criterion_main, Criterion};

pub fn sandbox_globset_benchmark(c: &mut Criterion) {
    let path = PathBuf::from("/etc/passwd");
    for i in &[10, 100, 1000, 10000, 100000] {
        let mut sandbox = syd::sandbox::Sandbox::default();
        for j in 0..*i {
            sandbox
                .config(if j % 2 == 0 {
                    "allow/read+/etc/***"
                } else {
                    "deny/read+/etc/***"
                })
                .unwrap();
        }
        sandbox.build_globsets().unwrap();
        c.bench_function(&format!("sandbox_match {i}"), |b| {
            b.iter(|| {
                sandbox.match_action(syd::sandbox::Capability::CAP_READ, black_box(&path));
            })
        });
        drop(sandbox);
    }
}

criterion_group!(benches, sandbox_globset_benchmark,);
criterion_main!(benches);
