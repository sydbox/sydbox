//
// Syd: rock-solid application kernel
// benches/parse_elf.rs: Benchmarks for syd::elf vs. goblin
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::{
    fs::File,
    io::{Read, Seek},
};

use criterion::{black_box, criterion_group, criterion_main, Criterion};

pub fn parse_elf_benchmark(c: &mut Criterion) {
    let mut file = File::open("/proc/self/exe").expect("open /proc/self/exe");

    c.bench_function("syd::elf::ExecutableFile::parse without link check", |b| {
        b.iter(|| {
            file.rewind().unwrap();
            syd::elf::ExecutableFile::parse(black_box(&mut file), false).unwrap();
        })
    });

    c.bench_function("syd::elf::ExecutableFile::parse with link check", |b| {
        b.iter(|| {
            file.rewind().unwrap();
            syd::elf::ExecutableFile::parse(black_box(&mut file), true).unwrap();
        })
    });

    c.bench_function("goblin::elf::Elf::parse", |b| {
        b.iter(|| {
            file.rewind().unwrap();
            let mut bytes = vec![];
            file.read_to_end(&mut bytes).unwrap();
            goblin::elf::Elf::parse(black_box(&bytes)).unwrap();
        })
    });
}

criterion_group!(benches, parse_elf_benchmark,);
criterion_main!(benches);
