" Vim syntax file
" Language:     Syd v3 profiles
" Author:       Ali Polatel
" Copyright:    Copyright (c) 2024, 2025 Ali Polatel
" Licence:      You may redistribute this under the same terms as Vim itself
"
" Syntax highlighting for Syd version 3 profiles.
"

if &compatible || v:version < 700
    finish
endif

if exists("b:current_syntax")
    finish
endif

syn match Syd3Boolean "\<\(1\|on\|t\|tr\|tru\|true\|✓\|0\|off\|f\|fa\|fal\|fals\|false\|✗\)\>" contained
hi def link Syd3Boolean Boolean

syn match Syd3Size /\v[0-9]+[kKmMgGtTpP]?[bB]?/ contained
hi def link Syd3Size Number

syn match Syd3LockState "\<\(on\|off\|exec\)\>" contained
hi def link Syd3LockState Boolean

syn keyword Syd3LockType lock contained containedin=Syd3Lock
hi def link Syd3LockType Identifier

syn keyword Syd3DisallowedCommandsType ghost load panic contained
hi def link Syd3DisallowedCommandsType Error

syn match Syd3SandboxType /\vsandbox\/(crypt|exec|force|lock|mem|pid|proxy|tpe|stat|read|write|exec|ioctl|create|delete|rename|symlink|truncate|chdir|readdir|mkdir|chown|chgrp|chmod|chattr|chroot|utime|mkdev|mkfifo|mktemp|net\/bind|net\/connect|net\/sendfd|net)(,(crypt|exec|force|lock|mem|pid|proxy|tpe|stat|read|write|exec|ioctl|create|delete|rename|symlink|t
runcate|chdir|readdir|mkdir|chown|chgrp|chmod|chattr|chroot|utime|mkdev|mkfifo|mkdev|net\/bind|net\/connect|net\/sendfd|net))*/
      \ contained containedin=Syd3Sandbox
hi link Syd3SandboxType Identifier

syn match Syd3UnshareType /\vunshare\/(mount|uts|ipc|user|pid|net|cgroup|time)(,(mount|uts|ipc|user|pid|net|cgroup|time))*/
      \ contained containedin=Syd3Unshare
hi link Syd3UnshareType Identifier

syn match SydCacheType /\vcache\/capacity\/(path|addr)/
      \ contained containedin=Syd3Cache nextgroup=Syd3OpSet
hi link Syd3CacheType Identifier

syn match Syd3CacheVal /\v[0-9]+$/
      \ contained containedin=Syd3Cache
hi link Syd3CacheVal Number

syn match Syd3ChrootType /\vroot/
      \ contained containedin=Syd3Chroot nextgroup=Syd3OpSet
hi link Syd3ChrootType Identifier

syn match Syd3RootType /\vroot\/(fake|map)/
      \ contained containedin=Syd3Root
hi link Syd3RootType Identifier

syn match Syd3NameType /\vname\/(host|domain)/
      \ contained containedin=Syd3Name
hi link Syd3NameType Identifier

syn match Syd3KillType /\v(mem|pid)\/kill/
      \ contained containedin=Syd3Kill
hi link Syd3KillType Identifier

syn match Syd3MemType /\vmem\/(max|vm_max)/
      \ contained containedin=Syd3Mem
hi link Syd3MemType Identifier

syn match Syd3PidType "pid/max"
      \ contained containedin=Syd3Pid
hi link Syd3PidType Identifier

syn match Syd3TpeType /\vtpe\/(negate|root_owned|user_owned|kill)/
      \ contained containedin=Syd3Tpe
hi link Syd3TpeType Identifier

syn match Syd3TpeGidType /\vtpe\/gid/
      \ contained containedin=Syd3TpeGid
hi link Syd3TpeGidType Identifier

syn match Syd3PidMax /\v[0-9]+$/
      \ contained containedin=Syd3Pid
hi link Syd3PidMax Number

syn keyword Syd3GidNone none contained containedin=Syd3Gid
hi def link Syd3GidNone Special

syn match Syd3Gid /\v(none|[0-9]+)$/
      \ contained contains=Syd3None containedin=Syd3TpeGid
hi link Syd3Gid Number

syn match Syd3BindType /\vbind[-+^]/
      \ contained contains=SydRuleOps nextgroup=Syd3BindSrc
hi link Syd3BindType Identifier

syn match Syd3OpSet /\v:/ contained
hi link Syd3OpSet Operator

syn match Syd3OpNext /\v,/ contained
hi link Syd3OpNext Operator

syn match Syd3OpEq /\v\=/ contained
hi link Syd3OpEq Operator

syn match Syd3OpVal /\v\=[^0-9,]+/ contained contains=Syd3OpEq
hi link Syd3OpVal String

syn match Syd3OpAdd /\v\+/ contained
hi link Syd3OpAdd Operator

syn match Syd3OpDec /\v-/ contained
hi link Syd3OpDec Operator

syn match Syd3OpXor /\v\^/ contained
hi link Syd3OpXor Operator

syn keyword Syd3BindFs contained
      \ ext4 xfs btrfs f2fs zfs
      \ nfs cifs iso9660 squashfs
      \ tmpfs vfat proc sysfs cgroup2
      \ overlay overlayfs
      \ efivarfs configfs
      \ securityfs debugfs
hi link Syd3BindFs Type

syn match Syd3BindSrc /\v[^:]+:/
      \ contained contains=Syd3OpSet,Syd3BindFs nextgroup=Syd3BindDst
hi link Syd3BindSrc String

syn match Syd3BindDst /\v\/[^:]*/
      \ contained nextgroup=Syd3BindOpts
hi link Syd3BindDst String

syn match Syd3BindOpt /\v(ro|nodev|noexec|nosuid|noatime|nodiratime|relatime)/
      \ contained
hi link Syd3BindOpt Special

syn match Syd3BindArg /\v([ug]id|mode|size|lowerdir|upperdir|workdir)\=[^,]+/
      \ contained contains=Syd3OpEq,Syd3OpNext,Syd3Size,Syd3OpVal
hi link Syd3BindArg Special

syn match Syd3BindOpts /\v:([^,]+)(,[^,]+)*$/
      \ contained contains=Syd3OpSet,Syd3BindOpt,Syd3BindArg
hi link Syd3BindOpts None

syn match Syd3CryptType /\vcrypt/ contained
hi link Syd3CryptType Identifier

syn match Syd3CryptTmpType /\vcrypt\/tmp/ contained
hi link Syd3CryptTmpType Identifier

syn match Syd3CryptKeyType /\vcrypt\/key/ contained
hi link Syd3CryptKeyType Identifier

syn match Syd3CryptKeyHex /\v:([0-9a-fA-F]{64})$/
      \ contained contains=Syd3OpSet
hi link Syd3CryptKeyHex Number

syn match Syd3ForceType /\vforce/ contained
hi link Syd3ForceType Identifier

syn match Syd3ForceAddType /\vforce\+/
      \ contained contains=Syd3OpAdd nextgroup=Syd3ForcePath
hi link Syd3ForceAddType Identifier

syn match Syd3ForcePath /\v\/[^:]*:/
      \ contained contains=Syd3OpSet nextgroup=Syd3ForceHash
hi link Syd3ForcePath String

syn match Syd3ForceHash /\v([0-9a-fA-F]{128}|[0-9a-fA-F]{96}|[0-9a-fA-F]{64}|[0-9a-fA-F]{40}|[0-9a-fA-F]{32}|[0-9a-fA-F]{16}|[0-9a-fA-F]{8}):/
      \ contained contains=Syd3OpSet nextgroup=Syd3ForceAct
hi link Syd3ForceHash Number

syn match Syd3ForceAct /\v(kill|warn)$/
      \ contained
hi link Syd3ForceAct Type

syn match Syd3SegvGuardType /\vsegvguard\/(expiry|suspension|max_crashes)/ contained
hi link Syd3SegvGuardType Identifier

syn match Syd3MaskType /\vmask[-+^]/
      \ contained contains=Syd3RuleOps nextgroup=Syd3SetPath
hi link Syd3MaskType Identifier

syn match Syd3SetIdUGid /\v[0-9]+/ contained
hi link Syd3SetIdUGid Number

syn match Syd3SetIdUGidBadDst /\v(0|root)$/ contained
hi link Syd3SetIdUGidBadDst Error

syn match Syd3SetIdAddDelDst /\v[^:]+$/
      \ contained
      \ contains=Syd3SetIdUGidBadDst,Syd3SetIdUGid
hi link Syd3SetIdAddDelDst Type

syn match Syd3SetIdAddDelSrc /\v[^:]+:/
      \ contained
      \ contains=Syd3OpSet,Syd3SetIdUGid
      \ nextgroup=Syd3SetIdAddDelDst
hi link Syd3SetIdAddDelSrc Type

syn match Syd3SetIdRemName /\v[^:]*/ contained contains=Syd3OpSet,Syd3SetIdUGid
hi link Syd3SetIdRemName Type

syn match Syd3SetIdAddDelAct /\v[-+]/ contained nextgroup=Syd3SetIdAddDelSrc
hi link Syd3SetIdAddDelAct Operator

syn match Syd3SetIdRemAct /\v\^/ contained nextgroup=Syd3SetIdRemName
hi link Syd3SetIdRemAct Operator

syn match Syd3SetIdAddDelType /\vset[ug]id/ contained nextgroup=Syd3SetIdAddDelAct
hi link Syd3SetIdAddDelType Identifier

syn match Syd3SetIdRemType /\vset[ug]id/ contained nextgroup=Syd3SetIdRemAct
hi link Syd3SetIdRemType Identifier

syn match Syd3RuleOps /\v[-+^]/ contained
hi link Syd3RuleOps Operator

syn match Syd3RulePath /\v.+$/ contained
hi link Syd3RulePath String

syn match Syd3RuleFsType /\v(allow|warn|filter|deny|panic|stop|kill|exit)\/(stat|read|write|exec|ioctl|create|delete|rename|symlink|truncate|chdir|readdir|mkdir|chown|chgrp|chmod|chattr|chroot|utime|mkdev|mkfifo|mktemp)(,stat|,read|,write|,exec|,ioctl|,create|,delete|,rename|,symlink|,truncate|,chdir|,readdir|,mkdir|,chown|,chgrp|,chmod|,chattr|,chroot|,utime|,mkdev|,mkfifo|,mktemp)*[-+^]/
      \ contained contains=Syd3RuleOps nextgroup=Syd3RulePath
syn match Syd3RuleFsType /\vallow\/lock\/(read|write)[-+^]/
      \ contained contains=Syd3RuleOps nextgroup=Syd3RulePath
hi link Syd3RuleFsType Identifier

syn match Syd3RulePort /\v[0-9]+(-[0-9]+)?/
      \ contained containedin=Syd3RuleNet
hi link Syd3RulePort Number

syn match Syd3NetOps /\v[@!]/
      \ contained nextgroup=Syd3RulePort
hi link Syd3NetOps Operator

syn match Syd3RuleUnix /\v\/.*$/ contained
hi link Syd3RuleUnix String

syn match Syd3RuleAddr /\v([A-Fa-f0-9:\.]+\/\d+|[A-Fa-f0-9:\.]+|\/.*$|[aA][nN][yY][46]?|[lL][oO][cC][aA][lL][46]?|[lL][oO][oO][pP][bB][aA][cC][kK][46]?|[lL][iI][nN][kK][lL][oO][cC][aA][lL][46]?)/
      \ contained contains=Syd3RuleAlias,Syd3RuleUnix nextgroup=Syd3NetOps
hi link Syd3RuleAddr Constant

syn match Syd3RuleAlias /\v[aA][nN][yY][46]?/
      \ contained
syn match Syd3RuleAlias /\v[lL][oO][cC][aA][lL][46]?/
      \ contained
syn match Syd3RuleAlias /\v[lL][oO][oO][pP][bB][aA][cC][kK][46]?/
      \ contained
syn match Syd3RuleAlias /\v[lL][iI][nN][kK][lL][oO][cC][aA][lL][46]?/
      \ contained
hi link Syd3RuleAlias Type

syn match Syd3RuleNetType /\v(allow|warn|filter|deny|stop|kill|exit)\/net\/(bind|connect)[-+^]/
      \ contained contains=Syd3RuleOps nextgroup=Syd3RuleAddr
hi link Syd3RuleNetType Identifier

syn match Syd3SetInt /\v:[0-9]+/ contained contains=Syd3OpSet
hi def link Syd3SetInt Number

syn match Syd3SetName /\v:.+$/ contained contains=Syd3OpSet
hi link Syd3SetName String

syn match Syd3SetPath /\v\/.*$/ contained
hi link Syd3SetPath String

syn match Syd3StatType /\vstat$/
hi link Syd3StatType Type

let s:trace_options = join([
      \ 'allow_safe_bind',
      \ 'allow_safe_kcapi',
      \ 'allow_safe_setuid',
      \ 'allow_safe_setgid',
      \ 'allow_safe_syslog',
      \ 'deny_dotdot',
      \ 'deny_elf32',
      \ 'deny_elf_dynamic',
      \ 'deny_elf_static',
      \ 'deny_script',
      \ 'deny_tsc',
      \ 'exit_wait_all',
      \ 'lock_personality',
      \ ], '|')
let s:unsafe_trace_options = join([
      \ 'allow_unsafe_bind',
      \ 'allow_unsafe_caps',
      \ 'allow_unsafe_cbpf',
      \ 'allow_unsafe_chown',
      \ 'allow_unsafe_chroot',
      \ 'allow_unsafe_cpu',
      \ 'allow_unsafe_dumpable',
      \ 'allow_unsafe_ebpf',
      \ 'allow_unsafe_env',
      \ 'allow_unsafe_exec',
      \ 'allow_unsafe_filename',
      \ 'allow_unsafe_keyring',
      \ 'allow_unsafe_libc',
      \ 'allow_unsafe_magiclinks',
      \ 'allow_unsafe_memfd',
      \ 'allow_unsafe_memory',
      \ 'allow_unsafe_msgsnd',
      \ 'allow_unsafe_nice',
      \ 'allow_unsafe_nopie',
      \ 'allow_unsafe_open_cdev',
      \ 'allow_unsafe_open_path',
      \ 'allow_unsafe_perf',
      \ 'allow_unsafe_pkey',
      \ 'allow_unsafe_prctl',
      \ 'allow_unsafe_prlimit',
      \ 'allow_unsafe_ptrace',
      \ 'allow_unsafe_sigreturn',
      \ 'allow_unsafe_socket',
      \ 'allow_unsafe_spec_exec',
      \ 'allow_unsafe_stack',
      \ 'allow_unsafe_sync',
      \ 'allow_unsafe_sysinfo',
      \ 'allow_unsafe_syslog',
      \ 'allow_unsafe_time',
      \ 'allow_unsafe_uring',
      \ 'allow_unsupp_socket',
      \ ], '|')
let s:trace_regex = '\vtrace\/(' . s:trace_options . '):'
let s:unsafe_trace_regex = '\vtrace\/(' . s:unsafe_trace_options . '):'
exec 'syn match Syd3TraceType /' . s:trace_regex . '/ contained contains=Syd3OpSet nextgroup=Syd3Boolean'
exec 'syn match Syd3TraceUnsafe /' . s:unsafe_trace_regex . '/ contained contains=Syd3OpSet nextgroup=Syd3Boolean'
hi link Syd3TraceType Identifier
hi link Syd3TraceUnsafe Error

syn match Syd3Umask /\v(-1|[0-9]+)/ contained
hi def link Syd3Umask Number

syn match Syd3ZeroOrOne /\v[01]/ contained
hi def link Syd3ZeroOrOne Number

syn match Syd3ForceUmaskType /\vtrace\/force_umask:/
      \ contained contains=Syd3OpSet nextgroup=Syd3Umask
hi link Syd3ForceUmaskType Identifier

syn match Syd3MemoryAccessType /\vtrace\/memory_access:/
      \ contained contains=Syd3OpSet nextgroup=Syd3ZeroOrOne
hi link Syd3MemoryAccessType Identifier

syn match Syd3IncludePath /\v.*$/ contained
hi link Syd3IncludePath String

syn keyword Syd3IncludeProfilePattern
      \ container immutable
      \ landlock lib
      \ noipv4 noipv6
      \ oci
      \ paludis pandora
      \ silent user
      \ contained
hi link Syd3IncludeProfilePattern Type

syn match Syd3IncludeType /\vinclude /
      \ contained nextgroup=Syd3IncludePath
hi link Syd3IncludeType Include

syn match Syd3IncludeProfileType /\vinclude_profile /
      \ contained nextgroup=Syd3IncludeProfilePattern
hi link Syd3IncludeProfileType Include

syn match Syd3EnvEnd /\v\}/ contained
hi link Syd3EnvEnd Include

syn match Syd3EnvDef /\v(:-[^\}]*)?/ contained nextgroup=Syd3EnvEnd
hi link Syd3EnvDef String

syn match Syd3EnvVar /\v\$\{[a-zA-Z0-9_-]+/ contained nextgroup=Syd3EnvDef
hi link Syd3EnvVar Include

syn region Syd3Comment start=/^#/ end=/$/
hi def link Syd3Comment Comment

syn region Syd3Env start=/\v\$\{[a-zA-Z_]/ end=/}/
      \ contains=Syd3EnvVar
syn region Syd3Lock start=/\vlock/ end=/$/
      \ contains=Syd3LockType,Syd3OpSet,Syd3LockState,Syd3EnvVar,Syd3Comment
syn region Syd3DisallowedCommands start=/\v(ghost|load|panic)/ end=/$/
      \ contains=Syd3DisallowedCommandsType,Syd3Comment
syn region Syd3Cache start=/\vcache\// end=/$/
      \ contains=Syd3CacheType,Syd3OpSet,Syd3CacheVal,Syd3EnvVar,Syd3Comment
syn region Syd3Sandbox start=/\vsandbox\// end=/$/
      \ contains=Syd3SandboxType,Syd3OpSet,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3Unshare start=/\vunshare\// end=/$/
      \ contains=Syd3UnshareType,Syd3OpSet,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3Chroot start=/\vroot:\// end=/$/
      \ contains=Syd3ChrootType,Syd3OpSet,Syd3SetPath,Syd3EnvVar,Syd3Comment
syn region Syd3Root start=/\vroot\// end=/$/
      \ contains=Syd3RootType,Syd3OpSet,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3Name start=/\vname\// end=/$/
      \ contains=Syd3NameType,Syd3OpSet,Syd3SetName,Syd3EnvVar,Syd3Comment
syn region Syd3RuleFs start=/\v((allow|warn|filter|deny|stop|kill|exit)\/[rwsie]|allow\/lock\/[rw])/ end=/$/
      \ contains=Syd3RuleFsType,Syd3EnvVar,Syd3Comment
syn region Syd3RuleNet start=/\v(allow|warn|filter|deny|stop|kill|exit)\/net\// end=/$/
      \ contains=Syd3RuleNetType,Syd3EnvVar,Syd3Comment
syn region Syd3Kill start=/\v(mem|pid)\/kill:/ end=/$/
      \ contains=Syd3KillType,Syd3OpSet,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3Mem start=/\vmem\/(max|vm_max):/ end=/$/
      \ contains=Syd3MemType,Syd3OpSet,Syd3Size,Syd3EnvVar,Syd3Comment
syn region Syd3Pid start=/\vpid\/max:/ end=/$/
      \ contains=Syd3PidType,Syd3OpSet,Syd3PidMax,Syd3EnvVar,Syd3Comment
syn region Syd3Tpe start=/\vtpe\/(negate|root_owned|user_owned|kill):/ end=/$/
      \ contains=Syd3TpeType,Syd3OpSet,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3TpeGid start=/\vtpe\/gid:/ end=/$/
      \ contains=Syd3TpeGidType,Syd3OpSet,Syd3Gid,Syd3EnvVar,Syd3Comment
syn region Syd3Bind start=/\vbind[-+^]/ end=/$/
      \ contains=Syd3BindType,Syd3EnvVar,Syd3Comment
syn region Syd3CryptRem start=/\vcrypt\^/ end=/$/
      \ contains=Syd3CryptType,Syd3OpXor,Syd3SetPath,Syd3EnvVar,Syd3Comment
syn region Syd3CryptDel start=/\vcrypt-/ end=/$/
      \ contains=Syd3CryptType,Syd3OpDec,Syd3SetPath,Syd3EnvVar,Syd3Comment
syn region Syd3CryptAdd start=/\vcrypt\+/ end=/$/
      \ contains=Syd3CryptType,Syd3OpAdd,Syd3SetPath,Syd3EnvVar,Syd3Comment
syn region Syd3CryptKey start=/\vcrypt\/key:/ end=/$/
      \ contains=Syd3CryptKeyType,Syd3OpSet,Syd3CryptKeyHex,Syd3EnvVar,Syd3Comment
syn region Syd3CryptTmp start=/\vcrypt\/tmp:/ end=/$/
      \ contains=Syd3CryptTmpType,Syd3OpSet,Syd3SetPath,Syd3EnvVar,Syd3Comment
syn region Syd3ForceRem start=/\vforce\^/ end=/$/
      \ contains=Syd3ForceType,Syd3OpXor,Syd3EnvVar,Syd3Comment
syn region Syd3ForceDel start=/\vforce-/ end=/$/
      \ contains=Syd3ForceType,Syd3OpDec,Syd3SetPath,Syd3EnvVar,Syd3Comment
syn region Syd3ForceAdd start=/\vforce\+/ end=/$/
      \ contains=Syd3ForceAddType,Syd3EnvVar,Syd3Comment
syn region Syd3SegvGuard start=/\vsegvguard\// end=/$/
      \ contains=Syd3SegvGuardType,Syd3SetInt,Syd3EnvVar,Syd3Comment
syn region Syd3Mask start=/\vmask[-+^]/ end=/$/
      \ contains=Syd3MaskType,Syd3EnvVar,Syd3Comment
syn region Syd3SetIdAddDel start=/\vset[ug]id[-+]/ end=/$/
      \ contains=Syd3SetIdAddDelType,Syd3Comment
syn region Syd3SetIdRem start=/\vset[ug]id\^/ end=/$/
      \ contains=Syd3SetIdRemType,Syd3Comment
syn region Syd3Stat start=/\vstat/ end=/$/
      \ contains=Syd3StatType,Syd3EnvVar,Syd3Comment
syn region Syd3Trace start=/\vtrace\/([^a]|allow_[^u])/ end=/$/
      \ contains=Syd3TraceType,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3Unsafe start=/\vtrace\/allow_u/ end=/$/
      \ contains=Syd3TraceUnsafe,Syd3Boolean,Syd3EnvVar,Syd3Comment
syn region Syd3ForceUmask start=/\vtrace\/force_umask/ end=/$/
      \ contains=Syd3ForceUmaskType,Syd3EnvVar,Syd3Comment
syn region Syd3MemoryAccess start=/\vtrace\/memory_access/ end=/$/
      \ contains=Syd3MemoryAccessType,Syd3EnvVar,Syd3Comment
syn region Syd3Include start=/\vinclude / end=/$/
      \ contains=Syd3IncludeType,Syd3EnvVar,Syd3Comment
syn region Syd3IncludeProfile start=/\vinclude_profile / end=/$/
      \ contains=Syd3IncludeProfileType,Syd3EnvVar,Syd3Comment

let b:current_syntax = "syd-3"
