" Vim filetype detection file
" Language:     Syd profiles
" Author:       Ali Polatel
" Copyright:    Copyright (c) 2024 Ali Polatel
" Licence:      You may redistribute this under the same terms as Vim itself
"
" Filetype detection for Syd profiles.
"

if &compatible || v:version < 700
    finish
endif

au BufNewFile,BufRead *.syd-3
    \     set filetype=syd-3
