//
// Syd: rock-solid application kernel
// fuzz/src/config.rs: Fuzz target for ELF parser
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

fn main() {
    afl::fuzz!(|data: &[u8]| {
        let _ = syd::elf::ExecutableFile::parse(std::io::Cursor::new(data), true);
    });
}
