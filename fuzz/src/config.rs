//
// Syd: rock-solid application kernel
// fuzz/src/config.rs: Fuzz target for sandbox config
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use syd::sandbox::Sandbox;

fn main() {
    afl::fuzz!(|data: &[u8]| {
        if let Ok(command) = std::str::from_utf8(data) {
            let mut sandbox = Sandbox::default();
            let _ = sandbox.config(command);
        }
    });
}
