//
// Syd: rock-solid application kernel
// fuzz/src/canonicalize.rs: Fuzz target for path canonicalization
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::{
    ffi::OsString,
    os::unix::ffi::OsStringExt,
    path::{Path, PathBuf},
};

use nix::unistd::Pid;
use syd::fs::{canonicalize, MissingHandling};

fn main() {
    afl::fuzz!(|data: &[u8]| {
        let pid = Pid::this();

        // Turn the fuzz input into an OsString.
        let path = PathBuf::from(OsString::from_vec(data.to_vec()));

        // Call the canonicalize function with various different arguments.
        let _ = canonicalize(
            pid,
            &path,
            None,
            MissingHandling::Normal,
            false,
            false,
            false,
        );
        let _ = canonicalize(
            pid,
            &path,
            Some(Path::new("/tmp")),
            MissingHandling::Normal,
            false,
            false,
            false,
        );
        let _ = canonicalize(
            pid,
            &path,
            None,
            MissingHandling::Existing,
            false,
            false,
            false,
        );
        let _ = canonicalize(
            pid,
            &path,
            Some(Path::new("/dev/..")),
            MissingHandling::Existing,
            false,
            false,
            false,
        );
        let _ = canonicalize(
            pid,
            &path,
            None,
            MissingHandling::Missing,
            false,
            false,
            false,
        );
        let _ = canonicalize(
            pid,
            &path,
            None,
            MissingHandling::Missing,
            false,
            false,
            false,
        );
    });
}
