;;; -*- lexical-binding: t -*-
;;;
;;; syd.el --- Emacs Lisp implementation of the virtual Syd stat(2) interface
;;;
;;; Syd: rock-solid application kernel
;;; src/syd.el: Emacs Lisp implementation of the virtual Syd stat(2) interface
;;;
;;; Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
;;;
;;; SPDX-License-Identifier: GPL-3.0

                                        ; Define lock states as keywords
(defconst syd-lock-off :lock-off
  "The sandbox lock is off, allowing all sandbox commands.")

(defconst syd-lock-exec :lock-exec
  "The sandbox lock is set to on for all processes except the initial process
\(syd exec child). This is the default state.")

(defconst syd-lock-on :lock-on
  "The sandbox lock is on, disallowing all sandbox commands.")

                                        ; Define sandbox actions as keywords
(defconst syd-action-allow :action-allow
  "Allow system call.")

(defconst syd-action-warn :action-warn
  "Allow system call and warn.")

(defconst syd-action-filter :action-filter
  "Deny system call silently.")

(defconst syd-action-deny :action-deny
  "Deny system call and warn.")

(defconst syd-action-panic :action-panic
  "Deny system call, warn and panic the current Syd thread.")

(defconst syd-action-stop :action-stop
  "Deny system call, warn and stop offending process.")

(defconst syd-action-kill :action-kill
  "Deny system call, warn and kill offending process.")

(defconst syd-action-exit :action-exit
  "Warn, and exit Syd immediately with deny errno as exit value.")

(defun syd-info ()
  "Reads the state of the syd sandbox from /dev/syd and returns it as an alist.
If the `json' module is not available, returns nil."
  (if (require 'json nil t)
      (condition-case nil
          (with-temp-buffer
            (insert-file-contents "/dev/syd")
            (with-no-warnings
              (let ((json-object-type 'alist)
                    (json-array-type 'list)
                    (json-key-type 'symbol)
                    (json-false nil)
                    (json-null nil))
                (json-read))))
        (file-error
         (message "Error reading /dev/syd.")
         nil)
        (json-error
         (message "JSON decoding error.")
         nil))
    (progn
      (message "JSON module not available.")
      nil)))

(defun syd-api ()
  "Performs a syd API check."
  (if (syd--stat "/dev/syd/3")
      3   ; API number on success
    nil)) ; On error, return nil

(defun syd-check ()
  "Check if '/dev/syd' is a character device."
  (syd--stat "/dev/syd"))

(defun syd-panic ()
  "Causes syd to exit immediately with code 127"
  (syd--stat "/dev/syd/panic"))

(defun syd-reset ()
  "Causes syd to reset sandboxing to the default state."
  (syd--stat "/dev/syd/reset"))

(defun syd-load (fd)
  "Causes syd to read configuration from the given file descriptor FD."
  (let ((path (concat "/dev/syd/load/" (number-to-string fd))))
    (syd--stat path)))

(defun syd-lock (state)
  "Sets the state of the sandbox lock.
STATE is one of the keywords :lock-off, :lock-exec, or :lock-on.
Returns t on success, nil on failure."
  (cond
   ((eq state syd-lock-off) (syd--stat "/dev/syd/lock:off"))
   ((eq state syd-lock-exec) (syd--stat "/dev/syd/lock:exec"))
   ((eq state syd-lock-on) (syd--stat "/dev/syd/lock:on"))
   (t nil))) ; Invalid state

(defun syd-enabled-stat ()
  "Checks if Stat sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/stat?"))

(defun syd-enable-stat ()
  "Enable Stat sandboxing."
  (syd--stat "/dev/syd/sandbox/stat:on"))

(defun syd-disable-stat ()
  "Disable Stat sandboxing."
  (syd--stat "/dev/syd/sandbox/stat:off"))

(defun syd-enabled-read ()
  "Checks if Read sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/read?"))

(defun syd-enable-read ()
  "Enable Read sandboxing."
  (syd--stat "/dev/syd/sandbox/read:on"))

(defun syd-disable-read ()
  "Disable Read sandboxing."
  (syd--stat "/dev/syd/sandbox/read:off"))

(defun syd-enabled-write ()
  "Checks if Write sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/write?"))

(defun syd-enable-write ()
  "Enable Write sandboxing."
  (syd--stat "/dev/syd/sandbox/write:on"))

(defun syd-disable-write ()
  "Disable Write sandboxing."
  (syd--stat "/dev/syd/sandbox/write:off"))

(defun syd-enabled-exec ()
  "Checks if Exec sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/exec?"))

(defun syd-enable-exec ()
  "Enable Exec sandboxing."
  (syd--stat "/dev/syd/sandbox/exec:on"))

(defun syd-disable-exec ()
  "Disable Exec sandboxing."
  (syd--stat "/dev/syd/sandbox/exec:off"))

(defun syd-enabled-ioctl ()
  "Checks if Ioctl sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/ioctl?"))

(defun syd-enable-ioctl ()
  "Enable Ioctl sandboxing."
  (syd--stat "/dev/syd/sandbox/ioctl:on"))

(defun syd-disable-ioctl ()
  "Disable Ioctl sandboxing."
  (syd--stat "/dev/syd/sandbox/ioctl:off"))

(defun syd-enabled-create ()
  "Checks if create sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/create?"))

(defun syd-enable-create ()
  "Enable create sandboxing."
  (syd--stat "/dev/syd/sandbox/create:on"))

(defun syd-disable-create ()
  "Disable create sandboxing."
  (syd--stat "/dev/syd/sandbox/create:off"))

(defun syd-enabled-delete ()
  "Checks if delete sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/delete?"))

(defun syd-enable-delete ()
  "Enable delete sandboxing."
  (syd--stat "/dev/syd/sandbox/delete:on"))

(defun syd-disable-delete ()
  "Disable delete sandboxing."
  (syd--stat "/dev/syd/sandbox/delete:off"))

(defun syd-enabled-rename ()
  "Checks if rename sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/rename?"))

(defun syd-enable-rename ()
  "Enable rename sandboxing."
  (syd--stat "/dev/syd/sandbox/rename:on"))

(defun syd-disable-rename ()
  "Disable rename sandboxing."
  (syd--stat "/dev/syd/sandbox/rename:off"))

(defun syd-enabled-symlink ()
  "Checks if symlink sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/symlink?"))

(defun syd-enable-symlink ()
  "Enable symlink sandboxing."
  (syd--stat "/dev/syd/sandbox/symlink:on"))

(defun syd-disable-symlink ()
  "Disable symlink sandboxing."
  (syd--stat "/dev/syd/sandbox/symlink:off"))

(defun syd-enabled-truncate ()
  "Checks if Truncate sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/truncate?"))

(defun syd-enable-truncate ()
  "Enable Truncate sandboxing."
  (syd--stat "/dev/syd/sandbox/truncate:on"))

(defun syd-disable-truncate ()
  "Disable Truncate sandboxing."
  (syd--stat "/dev/syd/sandbox/truncate:off"))

(defun syd-enabled-chdir ()
  "Checks if chdir sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/chdir?"))

(defun syd-enable-chdir ()
  "Enable chdir sandboxing."
  (syd--stat "/dev/syd/sandbox/chdir:on"))

(defun syd-disable-chdir ()
  "Disable chdir sandboxing."
  (syd--stat "/dev/syd/sandbox/chdir:off"))

(defun syd-enabled-readdir ()
  "Checks if readdir sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/readdir?"))

(defun syd-enable-readdir ()
  "Enable readdir sandboxing."
  (syd--stat "/dev/syd/sandbox/readdir:on"))

(defun syd-disable-readdir ()
  "Disable readdir sandboxing."
  (syd--stat "/dev/syd/sandbox/readdir:off"))

(defun syd-enabled-mkdir ()
  "Checks if mkdir sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/mkdir?"))

(defun syd-enable-mkdir ()
  "Enable mkdir sandboxing."
  (syd--stat "/dev/syd/sandbox/mkdir:on"))

(defun syd-disable-mkdir ()
  "Disable mkdir sandboxing."
  (syd--stat "/dev/syd/sandbox/mkdir:off"))

(defun syd-enabled-chown ()
  "Checks if chown sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/chown?"))

(defun syd-enable-chown ()
  "Enable chown sandboxing."
  (syd--stat "/dev/syd/sandbox/chown:on"))

(defun syd-disable-chown ()
  "Disable chown sandboxing."
  (syd--stat "/dev/syd/sandbox/chown:off"))

(defun syd-enabled-chgrp ()
  "Checks if chgrp sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/chgrp?"))

(defun syd-enable-chgrp ()
  "Enable chgrp sandboxing."
  (syd--stat "/dev/syd/sandbox/chgrp:on"))

(defun syd-disable-chgrp ()
  "Disable chgrp sandboxing."
  (syd--stat "/dev/syd/sandbox/chgrp:off"))

(defun syd-enabled-chmod ()
  "Checks if chmod sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/chmod?"))

(defun syd-enable-chmod ()
  "Enable chmod sandboxing."
  (syd--stat "/dev/syd/sandbox/chmod:on"))

(defun syd-disable-chmod ()
  "Disable chmod sandboxing."
  (syd--stat "/dev/syd/sandbox/chmod:off"))

(defun syd-enabled-chattr ()
  "Checks if chattr sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/chattr?"))

(defun syd-enable-chattr ()
  "Enable chattr sandboxing."
  (syd--stat "/dev/syd/sandbox/chattr:on"))

(defun syd-disable-chattr ()
  "Disable chattr sandboxing."
  (syd--stat "/dev/syd/sandbox/chattr:off"))

(defun syd-enabled-chroot ()
  "Checks if chroot sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/chroot?"))

(defun syd-enable-chroot ()
  "Enable chroot sandboxing."
  (syd--stat "/dev/syd/sandbox/chroot:on"))

(defun syd-disable-chroot ()
  "Disable chroot sandboxing."
  (syd--stat "/dev/syd/sandbox/chroot:off"))

(defun syd-enabled-utime ()
  "Checks if utime sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/utime?"))

(defun syd-enable-utime ()
  "Enable utime sandboxing."
  (syd--stat "/dev/syd/sandbox/utime:on"))

(defun syd-disable-utime ()
  "Disable utime sandboxing."
  (syd--stat "/dev/syd/sandbox/utime:off"))

(defun syd-enabled-mkdev ()
  "Checks if mkdev sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/mkdev?"))

(defun syd-enable-mkdev ()
  "Enable mkdev sandboxing."
  (syd--stat "/dev/syd/sandbox/mkdev:on"))

(defun syd-disable-mkdev ()
  "Disable mkdev sandboxing."
  (syd--stat "/dev/syd/sandbox/mkdev:off"))

(defun syd-enabled-mkfifo ()
  "Checks if mkfifo sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/mkfifo?"))

(defun syd-enable-mkfifo ()
  "Enable mkfifo sandboxing."
  (syd--stat "/dev/syd/sandbox/mkfifo:on"))

(defun syd-disable-mkfifo ()
  "Disable mkfifo sandboxing."
  (syd--stat "/dev/syd/sandbox/mkfifo:off"))

(defun syd-enabled-mktemp ()
  "Checks if mktemp sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/mktemp?"))

(defun syd-enable-mktemp ()
  "Enable mktemp sandboxing."
  (syd--stat "/dev/syd/sandbox/mktemp:on"))

(defun syd-disable-mktemp ()
  "Disable mktemp sandboxing."
  (syd--stat "/dev/syd/sandbox/mktemp:off"))

(defun syd-enabled-net ()
  "Checks if Network sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/net?"))

(defun syd-enable-net ()
  "Enable Network sandboxing."
  (syd--stat "/dev/syd/sandbox/net:on"))

(defun syd-disable-net ()
  "Disable Network sandboxing."
  (syd--stat "/dev/syd/sandbox/net:off"))

(defun syd-enabled-lock ()
  "Checks if lock sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/lock?"))

(defun syd-enabled-crypt ()
  "Checks if crypt sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/crypt?"))

(defun syd-enabled-proxy ()
  "Checks if proxy sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/proxy?"))

(defun syd-enabled-mem ()
  "Checks if memory sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/mem?"))

(defun syd-enable-mem ()
  "Enable memory sandboxing."
  (syd--stat "/dev/syd/sandbox/mem:on"))

(defun syd-disable-mem ()
  "Disable memory sandboxing."
  (syd--stat "/dev/syd/sandbox/mem:off"))

(defun syd-enabled-pid ()
  "Checks if PID sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/pid?"))

(defun syd-enable-pid ()
  "Enable PID sandboxing."
  (syd--stat "/dev/syd/sandbox/pid:on"))

(defun syd-disable-pid ()
  "Disable PID sandboxing."
  (syd--stat "/dev/syd/sandbox/pid:off"))

(defun syd-enabled-force ()
  "Checks if force sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/force?"))

(defun syd-enable-force ()
  "Enable force sandboxing."
  (syd--stat "/dev/syd/sandbox/force:on"))

(defun syd-disable-force ()
  "Disable force sandboxing."
  (syd--stat "/dev/syd/sandbox/force:off"))

(defun syd-enabled-tpe ()
  "Checks if TPE sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/tpe?"))

(defun syd-enable-tpe ()
  "Enable TPE sandboxing."
  (syd--stat "/dev/syd/sandbox/tpe:on"))

(defun syd-disable-tpe ()
  "Disable TPE sandboxing."
  (syd--stat "/dev/syd/sandbox/tpe:off"))

(defun syd-default-stat (action)
  "Set default action for Stat sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/stat:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-read (action)
  "Set default action for Read sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/read:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-write (action)
  "Set default action for Write sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/write:%s" action)))
        ;; Call syd--write with the command
        (syd--stat cmd)))))

(defun syd-default-exec (action)
  "Set default action for Exec sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/exec:%s" action)))
        ;; Call syd--exec with the command
        (syd--stat cmd)))))

(defun syd-default-ioctl (action)
  "Set default action for Ioctl sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/ioctl:%s" action)))
        ;; Call syd--ioctl with the command
        (syd--stat cmd)))))

(defun syd-default-create (action)
  "Set default action for Create sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/create:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-delete (action)
  "Set default action for Delete sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/delete:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-rename (action)
  "Set default action for rename sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/rename:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-symlink (action)
  "Set default action for symlink sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/symlink:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-truncate (action)
  "Set default action for Truncate sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/truncate:%s" action)))
        ;; Call syd--truncate with the command
        (syd--stat cmd)))))

(defun syd-default-chdir (action)
  "Set default action for chdir sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/chdir:%s" action)))
        ;; Call syd--chdir with the command
        (syd--stat cmd)))))

(defun syd-default-readdir (action)
  "Set default action for readdir sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/readdir:%s" action)))
        ;; Call syd--readdir with the command
        (syd--stat cmd)))))

(defun syd-default-mkdir (action)
  "Set default action for mkdir sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/mkdir:%s" action)))
        ;; Call syd--mkdir with the command
        (syd--stat cmd)))))

(defun syd-default-chown (action)
  "Set default action for Chown sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/chown:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-chgrp (action)
  "Set default action for Chgrp sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/chgrp:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-chmod (action)
  "Set default action for chmod sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/chmod:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-chattr (action)
  "Set default action for chattr sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/chattr:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-chroot (action)
  "Set default action for chroot sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/chroot:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-utime (action)
  "Set default action for utime sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/utime:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-mkdev (action)
  "Set default action for mkdev sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/mkdev:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-mkfifo (action)
  "Set default action for mkfifo sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/mkfifo:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-mktemp (action)
  "Set default action for mktemp sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/mktemp:%s" action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-default-net (action)
  "Set default action for Network sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/net:%s" action)))
        ;; Call syd--net with the command
        (syd--stat cmd)))))

;; TODO: syd-default-block!

(defun syd-default-mem (action)
  "Set default action for Memory sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/mem:%s" action)))
        ;; Call syd--net with the command
        (syd--stat cmd)))))

(defun syd-default-pid (action)
  "Set default action for PID sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/pid:%s" action)))
        ;; Call syd--net with the command
        (syd--stat cmd)))))

(defun syd-default-force (action)
  "Set default action for Force sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/force:%s" action)))
        ;; Call syd--net with the command
        (syd--stat cmd)))))

(defun syd-default-segvguard (action)
  "Set default action for SegvGuard.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/segvguard:%s" action)))
        ;; Call syd--net with the command
        (syd--stat cmd)))))

(defun syd-default-tpe (action)
  "Set default action for TPE sandboxing.
ACTION is a constant representing the sandboxing action."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      (let ((cmd (format "/dev/syd/default/tpe:%s" action)))
        ;; Call syd--net with the command
        (syd--stat cmd)))))

(defun syd-ioctl-deny (request)
  "Adds a request to the _ioctl_(2) denylist.
REQUEST is the _ioctl_(2) request number to add to the denylist."
  (unless (numberp request)
    (error "Request must be a number"))
  (let ((path (format "/dev/syd/ioctl/deny+%d" request)))
    (syd--stat path)))

(defun syd-stat-add (action glob)
  "Adds to the given actionlist of stat sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/stat" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-stat-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of stat sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/stat" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-stat-rem (action glob)
  "Removes all matching entries from the given actionlist of stat sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/stat" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-read-add (action glob)
  "Adds to the given actionlist of read sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/read" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-read-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of read sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/read" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-read-rem (action glob)
  "Removes all matching entries from the given actionlist of read sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/read" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-write-add (action glob)
  "Adds to the given actionlist of write sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/write" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-write-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of write sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/write" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-write-rem (action glob)
  "Removes all matching entries from the given actionlist of write sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/write" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-exec-add (action glob)
  "Adds to the given actionlist of exec sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/exec" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-exec-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of exec sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/exec" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-exec-rem (action glob)
  "Removes all matching entries from the given actionlist of exec sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/exec" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-ioctl-add (action glob)
  "Adds to the given actionlist of ioctl sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/ioctl" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-ioctl-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of ioctl sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/ioctl" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-ioctl-rem (action glob)
  "Removes all matching entries from the given actionlist of ioctl sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/ioctl" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-create-add (action glob)
  "Adds to the given actionlist of create sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/create" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-create-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of create sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/create" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-create-rem (action glob)
  "Removes all matching entries from the given actionlist of create sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/create" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-delete-add (action glob)
  "Adds to the given actionlist of delete sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; delete the command string
      (let ((cmd (format "%s/delete" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-delete-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of delete sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; delete the command string
      (let ((cmd (format "%s/delete" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-delete-rem (action glob)
  "Removes all matching entries from the given actionlist of delete sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; delete the command string
      (let ((cmd (format "%s/delete" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-rename-add (action glob)
  "Adds to the given actionlist of rename sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; rename the command string
      (let ((cmd (format "%s/rename" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-rename-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of rename sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; rename the command string
      (let ((cmd (format "%s/rename" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-rename-rem (action glob)
  "Removes all matching entries from the given actionlist of rename sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; rename the command string
      (let ((cmd (format "%s/rename" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-symlink-add (action glob)
  "Adds to the given actionlist of symlink sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; symlink the command string
      (let ((cmd (format "%s/symlink" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-symlink-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of symlink sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; symlink the command string
      (let ((cmd (format "%s/symlink" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-symlink-rem (action glob)
  "Removes all matching entries from the given actionlist of symlink sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; symlink the command string
      (let ((cmd (format "%s/symlink" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-truncate-add (action glob)
  "Adds to the given actionlist of truncate sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; truncate the command string
      (let ((cmd (format "%s/truncate" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-truncate-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of truncate sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; truncate the command string
      (let ((cmd (format "%s/truncate" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-truncate-rem (action glob)
  "Removes all matching entries from the given actionlist of truncate sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; truncate the command string
      (let ((cmd (format "%s/truncate" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-chdir-add (action glob)
  "Adds to the given actionlist of chdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; chdir the command string
      (let ((cmd (format "%s/chdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-chdir-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of chdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; chdir the command string
      (let ((cmd (format "%s/chdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-chdir-rem (action glob)
  "Removes all matching entries from the given actionlist of chdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; chdir the command string
      (let ((cmd (format "%s/chdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-readdir-add (action glob)
  "Adds to the given actionlist of readdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; readdir the command string
      (let ((cmd (format "%s/readdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-readdir-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of readdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; readdir the command string
      (let ((cmd (format "%s/readdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-readdir-rem (action glob)
  "Removes all matching entries from the given actionlist of readdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; readdir the command string
      (let ((cmd (format "%s/readdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-readdir-add (action glob)
  "Adds to the given actionlist of readdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; readdir the command string
      (let ((cmd (format "%s/readdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-readdir-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of readdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; readdir the command string
      (let ((cmd (format "%s/readdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-readdir-rem (action glob)
  "Removes all matching entries from the given actionlist of readdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; readdir the command string
      (let ((cmd (format "%s/readdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-mkdir-add (action glob)
  "Adds to the given actionlist of mkdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; mkdir the command string
      (let ((cmd (format "%s/mkdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-mkdir-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of mkdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; mkdir the command string
      (let ((cmd (format "%s/mkdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-mkdir-rem (action glob)
  "Removes all matching entries from the given actionlist of mkdir sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; mkdir the command string
      (let ((cmd (format "%s/mkdir" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-chown-add (action glob)
  "Adds to the given actionlist of chown sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chown" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-chown-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of chown sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chown" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-chown-rem (action glob)
  "Removes all matching entries from the given actionlist of chown sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chown" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-chgrp-add (action glob)
  "Adds to the given actionlist of chgrp sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chgrp" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-chgrp-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of chgrp sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chgrp" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-chgrp-rem (action glob)
  "Removes all matching entries from the given actionlist of chgrp sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chgrp" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-chmod-add (action glob)
  "Adds to the given actionlist of chmod sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chmod" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-chmod-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of chmod sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chmod" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-chmod-rem (action glob)
  "Removes all matching entries from the given actionlist of chmod sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chmod" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-chattr-add (action glob)
  "Adds to the given actionlist of chattr sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chattr" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-chattr-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of chattr sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chattr" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-chattr-rem (action glob)
  "Removes all matching entries from the given actionlist of chattr sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chattr" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-chroot-add (action glob)
  "Adds to the given actionlist of chroot sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chroot" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-chroot-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of chroot sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chroot" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-chroot-rem (action glob)
  "Removes all matching entries from the given actionlist of chroot sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/chroot" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-utime-add (action glob)
  "Adds to the given actionlist of utime sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/utime" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-utime-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of utime sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/utime" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-utime-rem (action glob)
  "Removes all matching entries from the given actionlist of utime sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/utime" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-mkdev-add (action glob)
  "Adds to the given actionlist of mkdev sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mkdev" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-mkdev-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of mkdev sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mkdev" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-mkdev-rem (action glob)
  "Removes all matching entries from the given actionlist of mkdev sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mkdev" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-mkfifo-add (action glob)
  "Adds to the given actionlist of mkfifo sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mkfifo" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-mkfifo-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of mkfifo sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mkfifo" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-mkfifo-rem (action glob)
  "Removes all matching entries from the given actionlist of mkfifo sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mkfifo" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-mktemp-add (action glob)
  "Adds to the given actionlist of mktemp sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mktemp" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-mktemp-del (action glob)
  "Removes the first matching entry from the end of the given actionlist
  of mktemp sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mktemp" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?-))))))

(defun syd-mktemp-rem (action glob)
  "Removes all matching entries from the given actionlist of mktemp sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/mktemp" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?^))))))

(defun syd-net-bind-add (action addr)
  "Adds to the given actionlist of net/bind sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/bind" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?+))))))

(defun syd-net-bind-del (action addr)
  "Removes the first matching entry from the end of the given actionlist
  of net/bind sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/bind" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?-))))))

(defun syd-net-bind-rem (action addr)
  "Removes all matching entries from the given actionlist of net/bind sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/bind" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?^))))))

(defun syd-net-connect-add (action addr)
  "Adds to the given actionlist of net/connect sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/connect" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?+))))))

(defun syd-net-connect-del (action addr)
  "Removes the first matching entry from the end of the given actionlist
  of net/connect sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/connect" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?-))))))

(defun syd-net-connect-rem (action addr)
  "Removes all matching entries from the given actionlist of net/connect
  sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/connect" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?^))))))

(defun syd-net-sendfd-add (action addr)
  "Adds to the given actionlist of net/sendfd sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/sendfd" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?+))))))

(defun syd-net-sendfd-del (action addr)
  "Removes the first matching entry from the end of the given actionlist
  of net/sendfd sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/sendfd" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?-))))))

(defun syd-net-sendfd-rem (action addr)
  "Removes all matching entries from the given actionlist of net/sendfd sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/sendfd" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?^))))))

(defun syd-net-link-add (action addr)
  "Adds to the given actionlist of net/link sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/link" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?+))))))

(defun syd-net-link-del (action addr)
  "Removes the first matching entry from the end of the given actionlist
  of net/link sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/link" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?-))))))

(defun syd-net-link-rem (action addr)
  "Removes all matching entries from the given actionlist of net/link sandboxing.
ACTION is a constant representing the sandboxing action.
ADDR is a string representing the address pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/net/link" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd addr ?^))))))

(defun syd-force-add (path hash action)
  "Adds an entry to the Integrity Force map for Force Sandboxing.
PATH is a fully-qualified file name.
HASH is a hexadecimal encoded checksum.
ACTION is one of :action-warn, :action-stop or :action-kill."
  (let ((action (cond ((eq action :action-warn) "warn")
                      ((eq action :action-stop) "stop")
                      ((eq action :action-kill) "kill"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "/dev/syd/force+%s:%s:%s" path hash action)))
        ;; Call syd--stat with the command
        (syd--stat cmd)))))

(defun syd-force-del (path)
  "Removes an entry from the Integrity Force map for Force Sandboxing.
PATH is a fully-qualified file name."
  ;; Create the command string
  (let ((cmd (format "/dev/syd/force-%s" path)))
    ;; Call syd--stat with the command
    (syd--stat cmd)))

(defun syd-force-clr ()
  "Clears the Integrity Force map for Force Sandboxing."
  (syd--stat "/dev/syd/force^"))

(defun syd-attr-add (action glob)
  "Adds to the given actionlist of attr sandboxing.
ACTION is a constant representing the sandboxing action.
GLOB is a string representing the glob pattern."
  (let ((action (cond 
                 ((eq action :action-allow) "allow")
                 ((eq action :action-warn) "warn")
                 ((eq action :action-filter) "filter")
                 ((eq action :action-deny) "deny")
                 ((eq action :action-panic) "panic")
                 ((eq action :action-stop) "stop")
                 ((eq action :action-kill) "kill")
                 ((eq action :action-exit) "exit"))))
    ;; Only proceed if action is not nil
    (when action
      ;; Create the command string
      (let ((cmd (format "%s/attr" action)))
        ;; Call syd--stat with the command
        (syd--stat (syd--rule cmd glob ?+))))))

(defun syd-mem-max (size)
  "Set syd maximum per-process memory usage limit.
SIZE can be an integer or a string representing the memory limit."
  (let ((size-str (cond ((integerp size) (number-to-string size))
                        ((stringp size) size)
                        (t (error "Size must be an integer or a string")))))
    (syd--stat (syd--rule "mem/max" size-str ?:))))

(defun syd-mem-vm-max (size)
  "Set syd maximum per-process virtual memory usage limit.
SIZE can be an integer or a string representing the memory limit."
  (let ((size-str (cond ((integerp size) (number-to-string size))
                        ((stringp size) size)
                        (t (error "Size must be an integer or a string")))))
    (syd--stat (syd--rule "mem/vm_max" size-str ?:))))

(defun syd-pid-max (size)
  "Set syd maximum process ID limit for PID sandboxing.
SIZE is a number representing the PID limit."
  (unless (numberp size)
    (error "Size must be a number"))
  (let ((path (format "/dev/syd/pid/max:%d" size)))
    (syd--stat path)))

(defun syd-segvguard-expiry (timeout)
  "Specify SegvGuard entry expiry timeout in seconds.
Setting this timeout to 0 effectively disables SegvGuard.
TIMEOUT is a number representing the timeout in seconds."
  (unless (numberp timeout)
    (error "Timeout must be a number"))
  (let ((path (format "/dev/syd/segvguard/expiry:%d" timeout)))
    (syd--stat path)))

(defun syd-segvguard-suspension (timeout)
  "Specify SegvGuard entry suspension timeout in seconds.
TIMEOUT is a number representing the timeout in seconds."
  (unless (numberp timeout)
    (error "Timeout must be a number"))
  (let ((path (format "/dev/syd/segvguard/suspension:%d" timeout)))
    (syd--stat path)))

(defun syd-segvguard-maxcrashes (limit)
  "Specify SegvGuard max number of crashes before suspension.
LIMIT is a number representing the crash limit."
  (unless (numberp limit)
    (error "Limit must be a number"))
  (let ((path (format "/dev/syd/segvguard/maxcrashes:%d" limit)))
    (syd--stat path)))

(defun syd-exec (file argv)
  "Execute a command outside the sandbox without sandboxing.
FILE is the file path of the command as a string.
ARGV is a list of strings representing the arguments to the command."
  (unless (stringp file)
    (error "File must be a string"))
  (let ((all-strings t))
    (dolist (arg argv)
      (unless (stringp arg)
        (setq all-strings nil)))
    (unless all-strings
      (error "All elements in ARGV must be strings")))

  (let ((cmd (mapconcat 'identity (cons file argv) "\x1F")))
    (syd--stat (concat "/dev/syd/cmd/exec!" cmd))))

(defun syd--rule (rule elem op)
  "Helper function to construct a path for syd operations.
RULE is a string representing the rule.
ELEM is a string representing the element.
OP is a character representing the operation."
  (unless (member op '(?+ ?- ?^ ?:))
    (error "Invalid operation"))
  (when (string-empty-p elem)
    (error "Element cannot be empty"))
  (concat "/dev/syd/" rule (char-to-string op) elem))

(defun syd--stat (path)
  "Check if the file at PATH exists using `file-modes'."
  (condition-case nil
      (not (null (file-modes path)))
    (error nil)))  ; On error, return nil

(provide 'syd)
;;; syd.el ends here
