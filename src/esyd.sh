#!/bin/sh
# Syd: rock-solid application kernel
# data/syd.sh: Defines 'esyd' command, the multi functional syd helper.
#
# esyd is written in portable shell.
# It should work fine with POSIX sh, Bash and Zsh.
# If you spot a problem running this with either of them,
# please report a bug at: https://todo.sr.ht/~alip/syd
#
# Copyright (c) 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
# SPDX-License-Identifier: GPL-3.0

esyd() {
    local cmd="${1}"

    shift
    case "${cmd}" in
    api)
        echo -n 3
        ;;
    check)
        test -c /dev/syd
        ;;
    panic|reset|stat)
        [ -c /dev/syd/${cmd} ]
        ;;
    load)
        if [ ${#} -ne 1 ]; then
            echo >&2 "esyd: ${cmd} takes exactly one extra argument"
            return 1
        fi
        [ -c "/dev/syd/load/${1}" ]
        ;;
    lock)
        [ -c '/dev/syd/lock:on' ]
        ;;
    unlock)
        [ -c '/dev/syd/lock:off' ]
        ;;
    exec_lock)
        [ -c '/dev/syd/lock:exec' ]
        ;;
    info)
        if [ -c /dev/syd ]; then
            if command -v jq >/dev/null 2>&1; then
                local out
                out=$(mktemp)
                if [ -n "$BASH_VERSION" ]; then
                    # We're in Bash
                    # This works with lock:exec
                    IFS=$'\n' readarray -t syd < /dev/syd
                    echo "${syd[*]}" > "${out}"
                elif [ -n "$ZSH_VERSION" ]; then
                    syd=$(</dev/syd)
                    echo "${syd}" > "${out}"
                else
                    # Fallback for POSIX sh
                    # This needs lock:off, doesn't work with lock:exec
                    cat /dev/syd > "${out}"
                fi

                jq "${@}" < "${out}"
                r=$?
                rm -f "${out}"
                return $r
            else
                if [ -n "$BASH_VERSION" ]; then
                    # We're in Bash
                    # This works with lock:exec
                    IFS=$'\n' readarray -t syd < /dev/syd
                    echo "${syd[*]}"
                elif [ -n "$ZSH_VERSION" ]; then
                    syd=$(</dev/syd)
                    echo "${syd}"
                else
                    # Fallback for POSIX sh
                    # This needs lock:off, doesn't work with lock:exec
                    cat /dev/syd
                fi
            fi
        else
            return 1
        fi
        ;;
    mem_max)
        if [ ${#} -ne 1 ]; then
            echo >&2 "esyd: ${cmd} takes exactly one extra argument"
            return 1
        fi
        [ -c "/dev/syd/mem/max:${1}" ]
        ;;
    vm_max)
        if [ ${#} -ne 1 ]; then
            echo >&2 "esyd: ${cmd} takes exactly one extra argument"
            return 1
        fi
        [ -c "/dev/syd/mem/vm_max:${1}" ]
        ;;
    kill_mem)
        [ -c '/dev/syd/mem/kill:1' ]
        ;;
    nokill_mem)
        [ -c '/dev/syd/mem/kill:0' ]
        ;;
    filter_mem)
        [ -c '/dev/syd/filter/mem:1' ]
        ;;
    unfilter_mem)
        [ -c '/dev/syd/filter/mem:0' ]
        ;;
    pid_max)
        if [ ${#} -ne 1 ]; then
            echo >&2 "esyd: ${cmd} takes exactly one extra argument"
            return 1
        fi
        [ -c "/dev/syd/pid/max:${1}" ]
        ;;
    kill_pid)
        [ -c '/dev/syd/pid/kill:1' ]
        ;;
    nokill_pid)
        [ -c '/dev/syd/pid/kill:0' ]
        ;;
    filter_pid)
        [ -c '/dev/syd/filter/pid:1' ]
        ;;
    unfilter_pid)
        [ -c '/dev/syd/filter/pid:0' ]
        ;;
    exec)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        [ -c "$(syd-exec ${@})" ]
        ;;
    force)
        if [ ${#} -ne 3 ]; then
            echo >&2 "esyd: ${cmd} takes exactly three arguments"
            return 1
        fi
        _esyd_path 'force' '+' "${1}:${2}:${3}"
        ;;
    enabled|enabled_path)
        test -c '/dev/syd/sandbox/all?'
        ;;
    enable|enable_path)
        test -c '/dev/syd/sandbox/all:on'
        ;;
    disable|disable_path)
        test -c '/dev/syd/sandbox/all:off'
        ;;
    enabled_mem)
        [ -c '/dev/syd/sandbox/mem?' ]
        ;;
    enable_mem)
        [ -c '/dev/syd/sandbox/mem:on' ]
        ;;
    disable_mem)
        [ -c '/dev/syd/sandbox/mem:off' ]
        ;;
    enabled_pid)
        [ -c '/dev/syd/sandbox/pid?' ]
        ;;
    enable_pid)
        [ -c '/dev/syd/sandbox/pid:on' ]
        ;;
    disable_pid)
        [ -c '/dev/syd/sandbox/pid:off' ]
        ;;
    enabled_stat)
        [ -c '/dev/syd/sandbox/stat?' ]
        ;;
    enable_stat)
        [ -c '/dev/syd/sandbox/stat:on' ]
        ;;
    disable_stat)
        [ -c '/dev/syd/sandbox/stat:off' ]
        ;;
    enabled_read)
        [ -c '/dev/syd/sandbox/read?' ]
        ;;
    enable_read)
        [ -c '/dev/syd/sandbox/read:on' ]
        ;;
    disable_read)
        [ -c '/dev/syd/sandbox/read:off' ]
        ;;
    enabled_write)
        [ -c '/dev/syd/sandbox/write?' ]
        ;;
    enable_write)
        [ -c '/dev/syd/sandbox/write:on' ]
        ;;
    disable_write)
        [ -c '/dev/syd/sandbox/write:off' ]
        ;;
    enabled_exec)
        [ -c '/dev/syd/sandbox/exec?' ]
        ;;
    enable_exec)
        [ -c '/dev/syd/sandbox/exec:on' ]
        ;;
    disable_exec)
        [ -c '/dev/syd/sandbox/exec:off' ]
        ;;
    enabled_ioctl)
        [ -c '/dev/syd/sandbox/ioctl?' ]
        ;;
    enable_ioctl)
        [ -c '/dev/syd/sandbox/ioctl:on' ]
        ;;
    disable_ioctl)
        [ -c '/dev/syd/sandbox/ioctl:off' ]
        ;;
    enabled_create)
        [ -c '/dev/syd/sandbox/create?' ]
        ;;
    enable_create)
        [ -c '/dev/syd/sandbox/create:on' ]
        ;;
    disable_create)
        [ -c '/dev/syd/sandbox/create:off' ]
        ;;
    enabled_delete)
        [ -c '/dev/syd/sandbox/delete?' ]
        ;;
    enable_delete)
        [ -c '/dev/syd/sandbox/delete:on' ]
        ;;
    disable_delete)
        [ -c '/dev/syd/sandbox/delete:off' ]
        ;;
    enabled_rename)
        [ -c '/dev/syd/sandbox/rename?' ]
        ;;
    enable_rename)
        [ -c '/dev/syd/sandbox/rename:on' ]
        ;;
    disable_rename)
        [ -c '/dev/syd/sandbox/rename:off' ]
        ;;
    enabled_symlink)
        [ -c '/dev/syd/sandbox/symlink?' ]
        ;;
    enable_symlink)
        [ -c '/dev/syd/sandbox/symlink:on' ]
        ;;
    disable_symlink)
        [ -c '/dev/syd/sandbox/symlink:off' ]
        ;;
    enabled_truncate)
        [ -c '/dev/syd/sandbox/truncate?' ]
        ;;
    enable_truncate)
        [ -c '/dev/syd/sandbox/truncate:on' ]
        ;;
    disable_truncate)
        [ -c '/dev/syd/sandbox/truncate:off' ]
        ;;
    enabled_chdir)
        [ -c '/dev/syd/sandbox/chdir?' ]
        ;;
    enable_chdir)
        [ -c '/dev/syd/sandbox/chdir:on' ]
        ;;
    disable_chdir)
        [ -c '/dev/syd/sandbox/chdir:off' ]
        ;;
    enabled_readdir)
        [ -c '/dev/syd/sandbox/readdir?' ]
        ;;
    enable_readdir)
        [ -c '/dev/syd/sandbox/readdir:on' ]
        ;;
    disable_readdir)
        [ -c '/dev/syd/sandbox/readdir:off' ]
        ;;
    enabled_mkdir)
        [ -c '/dev/syd/sandbox/mkdir?' ]
        ;;
    enable_mkdir)
        [ -c '/dev/syd/sandbox/mkdir:on' ]
        ;;
    disable_mkdir)
        [ -c '/dev/syd/sandbox/mkdir:off' ]
        ;;
    enabled_chown)
        [ -c '/dev/syd/sandbox/chown?' ]
        ;;
    enable_chown)
        [ -c '/dev/syd/sandbox/chown:on' ]
        ;;
    disable_chown)
        [ -c '/dev/syd/sandbox/chown:off' ]
        ;;
    enabled_chgrp)
        [ -c '/dev/syd/sandbox/chgrp?' ]
        ;;
    enable_chgrp)
        [ -c '/dev/syd/sandbox/chgrp:on' ]
        ;;
    disable_chgrp)
        [ -c '/dev/syd/sandbox/chgrp:off' ]
        ;;
    enabled_chmod)
        [ -c '/dev/syd/sandbox/chmod?' ]
        ;;
    enable_chmod)
        [ -c '/dev/syd/sandbox/chmod:on' ]
        ;;
    disable_chmod)
        [ -c '/dev/syd/sandbox/chmod:off' ]
        ;;
    enabled_chattr)
        [ -c '/dev/syd/sandbox/chattr?' ]
        ;;
    enable_chattr)
        [ -c '/dev/syd/sandbox/chattr:on' ]
        ;;
    disable_chattr)
        [ -c '/dev/syd/sandbox/chattr:off' ]
        ;;
    enabled_chroot)
        [ -c '/dev/syd/sandbox/chroot?' ]
        ;;
    enable_chroot)
        [ -c '/dev/syd/sandbox/chroot:on' ]
        ;;
    disable_chroot)
        [ -c '/dev/syd/sandbox/chroot:off' ]
        ;;
    enabled_utime)
        [ -c '/dev/syd/sandbox/utime?' ]
        ;;
    enable_utime)
        [ -c '/dev/syd/sandbox/utime:on' ]
        ;;
    disable_utime)
        [ -c '/dev/syd/sandbox/utime:off' ]
        ;;
    enabled_mkdev)
        [ -c '/dev/syd/sandbox/mkdev?' ]
        ;;
    enable_mkdev)
        [ -c '/dev/syd/sandbox/mkdev:on' ]
        ;;
    disable_mkdev)
        [ -c '/dev/syd/sandbox/mkdev:off' ]
        ;;
    enabled_mkfifo)
        [ -c '/dev/syd/sandbox/mkfifo?' ]
        ;;
    enable_mkfifo)
        [ -c '/dev/syd/sandbox/mkfifo:on' ]
        ;;
    disable_mkfifo)
        [ -c '/dev/syd/sandbox/mkfifo:off' ]
        ;;
    enabled_mktemp)
        [ -c '/dev/syd/sandbox/mktemp?' ]
        ;;
    enable_mktemp)
        [ -c '/dev/syd/sandbox/mktemp:on' ]
        ;;
    disable_mktemp)
        [ -c '/dev/syd/sandbox/mktemp:off' ]
        ;;
    enabled_net)
        [ -c '/dev/syd/sandbox/net?' ]
        ;;
    enable_net)
        [ -c '/dev/syd/sandbox/net:on' ]
        ;;
    disable_net)
        [ -c '/dev/syd/sandbox/net:off' ]
        ;;
    enabled_force)
        [ -c '/dev/syd/sandbox/force?' ]
        ;;
    enable_force)
        [ -c '/dev/syd/sandbox/force:on' ]
        ;;
    disable_force)
        [ -c '/dev/syd/sandbox/force:off' ]
        ;;
    allow|allow_path)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        # allow is shorthand for all glob capabilities.
        for capability in stat read write ioctl create delete rename symlink truncate chdir readdir mkdir chown chgrp chmod chattr chroot mkdev mkfifo mktemp; do
            _esyd_path "allow/${capability}" '+' "${@}"
        done
        ;;
    disallow|disallow_path)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        # disallow is shorthand for all glob capabilities.
        for capability in stat read write ioctl create delete rename symlink truncate chdir readdir mkdir chown chgrp chmod chattr chroot mkdev mkfifo mktemp; do
            _esyd_path "allow/${capability}" "${op}" "${@}"
        done
        ;;
    deny|deny_path)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        # deny is shorthand for all glob capabilities.
        for capability in stat read write ioctl create delete rename symlink truncate chdir readdir mkdir chown chgrp chmod chattr chroot mkdev mkfifo mktemp; do
            _esyd_path "deny/${capability}" '+' "${@}"
        done
        ;;
    nodeny|nodeny_path)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        # nodeny is shorthand for all glob capabilities.
        for capability in stat read write ioctl create delete rename symlink truncate chdir readdir mkdir chown chgrp chmod chattr chroot mkdev mkfifo mktemp; do
            _esyd_path "deny/${capability}" "${op}" "${@}"
        done
        ;;
    allow_stat)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/stat' '+' "${@}"
        ;;
    disallow_stat)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/stat' "${op}" "${@}"
        ;;
    deny_stat)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/stat' '+' "${@}"
        ;;
    nodeny_stat)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/stat' "${op}" "${@}"
        ;;
    allow_read)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/read' '+' "${@}"
        ;;
    disallow_read)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/read' "${op}" "${@}"
        ;;
    deny_read)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/read' '+' "${@}"
        ;;
    nodeny_read)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/read' "${op}" "${@}"
        ;;
    allow_write)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/write' '+' "${@}"
        ;;
    disallow_write)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/write' "${op}" "${@}"
        ;;
    deny_write)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/write' '+' "${@}"
        ;;
    nodeny_write)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/write' "${op}" "${@}"
        ;;
    allow_exec)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/exec' '+' "${@}"
        ;;
    disallow_exec)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/exec' "${op}" "${@}"
        ;;
    deny_exec)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/exec' '+' "${@}"
        ;;
    nodeny_exec)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/exec' "${op}" "${@}"
        ;;
    allow_ioctl)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/ioctl' '+' "${@}"
        ;;
    disallow_ioctl)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/ioctl' "${op}" "${@}"
        ;;
    deny_ioctl)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/ioctl' '+' "${@}"
        ;;
    nodeny_ioctl)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/ioctl' "${op}" "${@}"
        ;;
    allow_create)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/create' '+' "${@}"
        ;;
    disallow_create)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/create' "${op}" "${@}"
        ;;
    deny_create)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/create' '+' "${@}"
        ;;
    nodeny_create)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/create' "${op}" "${@}"
        ;;
    allow_delete)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/delete' '+' "${@}"
        ;;
    disallow_delete)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/delete' "${op}" "${@}"
        ;;
    deny_delete)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/delete' '+' "${@}"
        ;;
    nodeny_delete)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/delete' "${op}" "${@}"
        ;;
    allow_rename)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/rename' '+' "${@}"
        ;;
    disallow_rename)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/rename' "${op}" "${@}"
        ;;
    deny_rename)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/rename' '+' "${@}"
        ;;
    nodeny_rename)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/rename' "${op}" "${@}"
        ;;
    allow_symlink)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/symlink' '+' "${@}"
        ;;
    disallow_symlink)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/symlink' "${op}" "${@}"
        ;;
    deny_symlink)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/symlink' '+' "${@}"
        ;;
    nodeny_symlink)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/symlink' "${op}" "${@}"
        ;;
    allow_truncate)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/truncate' '+' "${@}"
        ;;
    disallow_truncate)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/truncate' "${op}" "${@}"
        ;;
    deny_truncate)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/truncate' '+' "${@}"
        ;;
    nodeny_truncate)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/truncate' "${op}" "${@}"
        ;;
    allow_chdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chdir' '+' "${@}"
        ;;
    disallow_chdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chdir' "${op}" "${@}"
        ;;
    deny_chdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chdir' '+' "${@}"
        ;;
    nodeny_chdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chdir' "${op}" "${@}"
        ;;
    allow_readdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/readdir' '+' "${@}"
        ;;
    disallow_readdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/readdir' "${op}" "${@}"
        ;;
    deny_readdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/readdir' '+' "${@}"
        ;;
    nodeny_readdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/readdir' "${op}" "${@}"
        ;;
    allow_mkdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mkdir' '+' "${@}"
        ;;
    disallow_mkdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mkdir' "${op}" "${@}"
        ;;
    deny_mkdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mkdir' '+' "${@}"
        ;;
    nodeny_mkdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mkdir' "${op}" "${@}"
        ;;
    allow_chown)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chown' '+' "${@}"
        ;;
    disallow_chown)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chown' "${op}" "${@}"
        ;;
    deny_chown)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chown' '+' "${@}"
        ;;
    nodeny_chown)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chown' "${op}" "${@}"
        ;;
    allow_chgrp)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chgrp' '+' "${@}"
        ;;
    disallow_chgrp)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chgrp' "${op}" "${@}"
        ;;
    deny_chgrp)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chgrp' '+' "${@}"
        ;;
    nodeny_chgrp)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chgrp' "${op}" "${@}"
        ;;
    allow_chmod)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chmod' '+' "${@}"
        ;;
    disallow_chmod)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chmod' "${op}" "${@}"
        ;;
    deny_chmod)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chmod' '+' "${@}"
        ;;
    nodeny_chmod)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chmod' "${op}" "${@}"
        ;;
    allow_chattr)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chattr' '+' "${@}"
        ;;
    disallow_chattr)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chattr' "${op}" "${@}"
        ;;
    deny_chattr)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chattr' '+' "${@}"
        ;;
    nodeny_chattr)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chattr' "${op}" "${@}"
        ;;
    allow_chroot)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chroot' '+' "${@}"
        ;;
    disallow_chroot)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/chroot' "${op}" "${@}"
        ;;
    deny_chroot)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chroot' '+' "${@}"
        ;;
    nodeny_chroot)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/chroot' "${op}" "${@}"
        ;;
    allow_utime)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/utime' '+' "${@}"
        ;;
    disallow_utime)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/utime' "${op}" "${@}"
        ;;
    deny_utime)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/utime' '+' "${@}"
        ;;
    nodeny_utime)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/utime' "${op}" "${@}"
        ;;
    allow_mkdev)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mkdev' '+' "${@}"
        ;;
    disallow_mkdev)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mkdev' "${op}" "${@}"
        ;;
    deny_mkdev)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mkdev' '+' "${@}"
        ;;
    nodeny_mkdev)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mkdev' "${op}" "${@}"
        ;;
    allow_mkfifo)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mkfifo' '+' "${@}"
        ;;
    disallow_mkfifo)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mkfifo' "${op}" "${@}"
        ;;
    deny_mkfifo)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mkfifo' '+' "${@}"
        ;;
    nodeny_mkfifo)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mkfifo' "${op}" "${@}"
        ;;
    allow_mktemp)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mktemp' '+' "${@}"
        ;;
    disallow_mktemp)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'allow/mktemp' "${op}" "${@}"
        ;;
    deny_mktemp)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mktemp' '+' "${@}"
        ;;
    nodeny_mktemp)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'deny/mktemp' "${op}" "${@}"
        ;;
    allow_net)
        local op='-'
        local c='allow/net/bind'
        [ "${1}" == '--all' ] && op='^' && shift
        case "${1}" in
        '--connect')
            c='allow/net/connect'
            shift;;
        '--sendfd')
            c='allow/net/sendfd'
            shift;;
        esac
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_net "${c}" '+' "${@}"
        ;;
    disallow_net)
        local op='-'
        local c='allow/net/bind'
        [ "${1}" == '--all' ] && op='^' && shift
        case "${1}" in
        '--connect')
            c='allow/net/connect'
            shift;;
        '--sendfd')
            c='allow/net/sendfd'
            shift;;
        esac
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_net "${c}" "${op}" "${@}"
        ;;
    deny_net)
        local op='-'
        local c='deny/net/bind'
        [ "${1}" == '--all' ] && op='^' && shift
        case "${1}" in
        '--connect')
            c='allow/net/connect'
            shift;;
        '--sendfd')
            c='allow/net/sendfd'
            shift;;
        esac
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_net "${c}" '+' "${@}"
        ;;
    nodeny_net)
        local op='-'
        local c='deny/net/bind'
        [ "${1}" == '--all' ] && op='^' && shift
        case "${1}" in
        '--connect')
            c='allow/net/connect'
            shift;;
        '--sendfd')
            c='allow/net/sendfd'
            shift;;
        esac
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_net "${c}" "${op}" "${@}"
        ;;
    addfilter|addfilter_path)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        for capability in stat read write ioctl create delete rename symlink truncate chdir readdir mkdir chown chgrp chmod chattr chroot mkdev mkfifo mktemp; do
            _esyd_path "filter/${capability}" '+' "${@}" || return 1
        done
        ;;
    rmfilter|rmfilter_path)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        for capability in stat read write ioctl create delete rename symlink truncate chdir readdir mkdir chown chgrp chmod chattr chroot mkdev mkfifo mktemp; do
            _esyd_path "filter/${capability}" "${op}" "${@}" || return 1
        done
        ;;
    addfilter_stat)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/stat' '+' "${@}"
        ;;
    rmfilter_stat)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/stat' "${op}" "${@}"
        ;;
    addfilter_read)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/read' '+' "${@}"
        ;;
    rmfilter_read)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/read' "${op}" "${@}"
        ;;
    addfilter_write)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/write' '+' "${@}"
        ;;
    rmfilter_write)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/write' "${op}" "${@}"
        ;;
    addfilter_exec)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/exec' '+' "${@}"
        ;;
    rmfilter_exec)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/exec' "${op}" "${@}"
        ;;
    addfilter_ioctl)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/ioctl' '+' "${@}"
        ;;
    rmfilter_ioctl)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/ioctl' "${op}" "${@}"
        ;;
    addfilter_create)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/create' '+' "${@}"
        ;;
    rmfilter_create)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/create' "${op}" "${@}"
        ;;
    addfilter_delete)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/delete' '+' "${@}"
        ;;
    rmfilter_delete)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/delete' "${op}" "${@}"
        ;;
    addfilter_rename)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/rename' '+' "${@}"
        ;;
    rmfilter_rename)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/rename' "${op}" "${@}"
        ;;
    addfilter_symlink)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/symlink' '+' "${@}"
        ;;
    rmfilter_symlink)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/symlink' "${op}" "${@}"
        ;;
    addfilter_truncate)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/truncate' '+' "${@}"
        ;;
    rmfilter_truncate)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/truncate' "${op}" "${@}"
        ;;
    addfilter_chdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chdir' '+' "${@}"
        ;;
    rmfilter_chdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chdir' "${op}" "${@}"
        ;;
    addfilter_readdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/readdir' '+' "${@}"
        ;;
    rmfilter_readdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/readdir' "${op}" "${@}"
        ;;
    addfilter_mkdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkdir' '+' "${@}"
        ;;
    rmfilter_mkdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkdir' "${op}" "${@}"
        ;;
    addfilter_mkdir)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkdir' '+' "${@}"
        ;;
    rmfilter_mkdir)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkdir' "${op}" "${@}"
        ;;
    addfilter_chown)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chown' '+' "${@}"
        ;;
    rmfilter_chown)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chown' "${op}" "${@}"
        ;;
    addfilter_chgrp)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chgrp' '+' "${@}"
        ;;
    rmfilter_chgrp)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chgrp' "${op}" "${@}"
        ;;
    addfilter_chmod)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chmod' '+' "${@}"
        ;;
    rmfilter_chmod)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chmod' "${op}" "${@}"
        ;;
    addfilter_chattr)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chattr' '+' "${@}"
        ;;
    rmfilter_chattr)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chattr' "${op}" "${@}"
        ;;
    addfilter_chroot)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chroot' '+' "${@}"
        ;;
    rmfilter_chroot)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/chroot' "${op}" "${@}"
        ;;
    addfilter_utime)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/utime' '+' "${@}"
        ;;
    rmfilter_utime)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/utime' "${op}" "${@}"
        ;;
    addfilter_mkdev)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkdev' '+' "${@}"
        ;;
    rmfilter_mkdev)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkdev' "${op}" "${@}"
        ;;
    addfilter_mkfifo)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkfifo' '+' "${@}"
        ;;
    rmfilter_mkfifo)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mkfifo' "${op}" "${@}"
        ;;
    addfilter_mktemp)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mktemp' '+' "${@}"
        ;;
    rmfilter_mktemp)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if  [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_path 'filter/mktemp' "${op}" "${@}"
        ;;
    addfilter_net)
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_net 'filter/net' '+' "${@}"
        ;;
    rmfilter_net)
        local op='-'
        [ "${1}" == '--all' ] && op='^' && shift
        if [ ${#} -lt 1 ]; then
            echo >&2 "esyd: ${cmd} takes at least one extra argument"
            return 1
        fi
        _esyd_net 'filter/net' "${op}" "${@}"
        ;;
    help|'')
        if [ -t 1 ]; then
            esyd help | ${PAGER:-less}
            return $?
        fi

        cat <<EOF
esyd -- multi functional syd helper
Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
SPDX-License-Identifier: GPL-3.0

# Subcommands
api
    Print syd API version number
check
    Return true if running under syd
lock
    Lock syd, esyd commands will no longer work
exec_lock
    Lock syd for all processes but the syd exec child
unlock
    Unlock the syd, make it available to all processes rather than just the syd exec child
info jq-args...
    Print syd sandbox state as JSON on standard output
    If "jq" is in PATH, pass the arguments and pipe the output to jq
load fd
    This command causes syd to read configuration from the given file descriptor
panic
    This command causes syd to exit immediately with code 127
reset
    This command causes syd to reset sandboxing to the default state
    Allowlists, denylists and filters are going to be cleared
stat
    Print syd sandbox state on standard error
exec cmd args..
    Execute a command outside the sandbox without sandboxing
kill glob
    Kill any attempt to execute a path matching the given glob pattern
force path hash action
    Force pre-execution verification of the given path using the given checksum
    Action must be exactly one of kill, stop or warn.
enabled, enabled_path
    Return true if path sandboxing is enabled
enable, enable_path
    Enable path sandboxing
disable, disable_path
    Disable path sandboxing
enabled_pid
    Return true if PID sandboxing is enabled
enable_pid
    Enable PID sandboxing
disable_pid
    Disable PID sandboxing
enabled_stat
    Return true if stat sandboxing is enabled
enable_stat
    Enable stat sandboxing
disable_stat
    Disable stat sandboxing
enabled_read
    Return true if read sandboxing is enabled
enable_read
    Enable read sandboxing
disable_read
    Disable read sandboxing
enabled_write
    Return true if write sandboxing is enabled
enable_write
    Enable write sandboxing
disable_write
    Disable write sandboxing
enabled_exec
    Return true if exec sandboxing is enabled
enable_exec
    Enable exec sandboxing
disable_exec
    Disable exec sandboxing
enabled_ioctl
    Return true if ioctl sandboxing is enabled
enable_ioctl
    Enable ioctl sandboxing
disable_ioctl
    Disable ioctl sandboxing
enabled_create
    Return true if create sandboxing is enabled
enable_create
    Enable create sandboxing
disable_create
    Disable create sandboxing
enabled_delete
    Return true if delete sandboxing is enabled
enable_delete
    Enable delete sandboxing
disable_delete
    Disable delete sandboxing
enabled_rename
    Return true if rename sandboxing is enabled
enable_rename
    Enable rename sandboxing
disable_rename
    Disable rename sandboxing
enabled_symlink
    Return true if symlink sandboxing is enabled
enable_symlink
    Enable symlink sandboxing
disable_symlink
    Disable symlink sandboxing
enabled_truncate
    Return true if truncate sandboxing is enabled
enable_truncate
    Enable truncate sandboxing
disable_truncate
    Disable truncate sandboxing
enabled_chdir
    Return true if chdir sandboxing is enabled
enable_chdir
    Enable chdir sandboxing
disable_chdir
    Disable chdir sandboxing
enabled_readdir
    Return true if readdir sandboxing is enabled
enable_readdir
    Enable readdir sandboxing
disable_readdir
    Disable readdir sandboxing
enabled_mkdir
    Return true if mkdir sandboxing is enabled
enable_mkdir
    Enable mkdir sandboxing
disable_mkdir
    Disable mkdir sandboxing
enabled_chown
    Return true if chown sandboxing is enabled
enable_chown
    Enable chown sandboxing
disable_chown
    Disable chown sandboxing
enabled_chgrp
    Return true if chgrp sandboxing is enabled
enable_chgrp
    Enable chgrp sandboxing
disable_chgrp
    Disable chgrp sandboxing
enabled_chmod
    Return true if chmod sandboxing is enabled
enable_chmod
    Enable chmod sandboxing
disable_chmod
    Disable chmod sandboxing
enabled_chattr
    Return true if chattr sandboxing is enabled
enable_chattr
    Enable chattr sandboxing
disable_chattr
    Disable chattr sandboxing
enabled_chroot
    Return true if chroot sandboxing is enabled
enable_chroot
    Enable chroot sandboxing
disable_chroot
    Disable chroot sandboxing
enabled_utime
    Return true if utime sandboxing is enabled
enable_utime
    Enable utime sandboxing
disable_utime
    Disable utime sandboxing
enabled_mkdev
    Return true if mkdev sandboxing is enabled
enable_mkdev
    Enable mkdev sandboxing
disable_mkdev
    Disable mkdev sandboxing
enabled_mkfifo
    Return true if mkfifo sandboxing is enabled
enable_mkfifo
    Enable mkfifo sandboxing
disable_mkfifo
    Disable mkfifo sandboxing
enabled_mktemp
    Return true if mktemp sandboxing is enabled
enable_mktemp
    Enable mktemp sandboxing
disable_mktemp
    Disable mktemp sandboxing
enabled_net
    Return true if network sandboxing is enabled
enable_net
    Enable network sandboxing
disable_net
    Disable network sandboxing
enabled_force
    Return true if force sandboxing is enabled
enable_force
    Enable force sandboxing
disable_force
    Disable force sandboxing
allow, allow_path glob
    Allow the given glob pattern for path sandboxing
disallow, disallow_path [--all] glob
    Removes the given glob pattern from the allowlist for path sandboxing
deny, deny_path glob
    Deny the given glob pattern for path sandboxing
nodeny, nodeny_path [--all] glob
    Removes the given glob pattern from the denylist for path sandboxing
allow_stat glob
    Allow the given glob pattern for stat sandboxing
disallow_stat [--all] glob
    Removes the given glob pattern from the allowlist for stat sandboxing
deny_stat glob
    Deny the given glob pattern for stat sandboxing
nodeny_stat [--all] glob
    Removes the given glob pattern from the denylist for stat sandboxing
allow_read glob
    Allow the given glob pattern for read sandboxing
disallow_read [--all] glob
    Removes the given glob pattern from the allowlist for read sandboxing
deny_read glob
    Deny the given glob pattern for read sandboxing
nodeny_read [--all] glob
    Removes the given glob pattern from the denylist for read sandboxing
allow_write glob
    Allow the given glob pattern for write sandboxing
disallow_write [--all] glob
    Removes the given glob pattern from the allowlist for write sandboxing
deny_write glob
    Deny the given glob pattern for write sandboxing
nodeny_write [--all] glob
    Removes the given glob pattern from the denylist for write sandboxing
allow_exec glob
    Allow the given glob pattern for exec sandboxing
disallow_exec [--all] glob
    Removes the given glob pattern from the allowlist for exec sandboxing
deny_exec glob
    Deny the given glob pattern for exec sandboxing
nodeny_exec [--all] glob
    Removes the given glob pattern from the denylist for exec sandboxing
allow_ioctl glob
    Allow the given glob pattern for ioctl sandboxing
disallow_ioctl [--all] glob
    Removes the given glob pattern from the allowlist for ioctl sandboxing
deny_ioctl glob
    Deny the given glob pattern for ioctl sandboxing
nodeny_ioctl [--all] glob
    Removes the given glob pattern from the denylist for ioctl sandboxing
allow_create glob
    Allow the given glob pattern for create sandboxing
disallow_create [--all] glob
    Removes the given glob pattern from the allowlist for create sandboxing
deny_create glob
    Deny the given glob pattern for create sandboxing
nodeny_create [--all] glob
    Removes the given glob pattern from the denylist for create sandboxing
allow_delete glob
    Allow the given glob pattern for delete sandboxing
disallow_delete [--all] glob
    Removes the given glob pattern from the allowlist for delete sandboxing
deny_delete glob
    Deny the given glob pattern for delete sandboxing
nodeny_delete [--all] glob
    Removes the given glob pattern from the denylist for delete sandboxing
allow_rename glob
    Allow the given glob pattern for rename sandboxing
disallow_rename [--all] glob
    Removes the given glob pattern from the allowlist for rename sandboxing
deny_rename glob
    Deny the given glob pattern for rename sandboxing
nodeny_rename [--all] glob
    Removes the given glob pattern from the denylist for rename sandboxing
allow_symlink glob
    Allow the given glob pattern for symlink sandboxing
disallow_symlink [--all] glob
    Removes the given glob pattern from the allowlist for symlink sandboxing
deny_symlink glob
    Deny the given glob pattern for symlink sandboxing
nodeny_symlink [--all] glob
    Removes the given glob pattern from the denylist for symlink sandboxing
allow_truncate glob
    Allow the given glob pattern for truncate sandboxing
disallow_truncate [--all] glob
    Removes the given glob pattern from the allowlist for truncate sandboxing
deny_truncate glob
    Deny the given glob pattern for truncate sandboxing
nodeny_truncate [--all] glob
    Removes the given glob pattern from the denylist for truncate sandboxing
allow_chdir glob
    Allow the given glob pattern for chdir sandboxing
disallow_chdir [--all] glob
    Removes the given glob pattern from the allowlist for chdir sandboxing
deny_chdir glob
    Deny the given glob pattern for chdir sandboxing
nodeny_chdir [--all] glob
    Removes the given glob pattern from the denylist for chdir sandboxing
allow_readdir glob
    Allow the given glob pattern for readdir sandboxing
disallow_readdir [--all] glob
    Removes the given glob pattern from the allowlist for readdir sandboxing
deny_readdir glob
    Deny the given glob pattern for readdir sandboxing
nodeny_readdir [--all] glob
    Removes the given glob pattern from the denylist for readdir sandboxing
allow_mkdir glob
    Allow the given glob pattern for mkdir sandboxing
disallow_mkdir [--all] glob
    Removes the given glob pattern from the allowlist for mkdir sandboxing
deny_mkdir glob
    Deny the given glob pattern for mkdir sandboxing
nodeny_mkdir [--all] glob
    Removes the given glob pattern from the denylist for mkdir sandboxing
allow_chown glob
    Allow the given glob pattern for chown sandboxing
disallow_chown [--all] glob
    Removes the given glob pattern from the allowlist for chown sandboxing
deny_chown glob
    Deny the given glob pattern for chown sandboxing
nodeny_chown [--all] glob
    Removes the given glob pattern from the denylist for chown sandboxing
allow_chgrp glob
    Allow the given glob pattern for chgrp sandboxing
disallow_chgrp [--all] glob
    Removes the given glob pattern from the allowlist for chgrp sandboxing
deny_chgrp glob
    Deny the given glob pattern for chgrp sandboxing
nodeny_chgrp [--all] glob
    Removes the given glob pattern from the denylist for chgrp sandboxing
allow_chmod glob
    Allow the given glob pattern for chmod sandboxing
disallow_chmod [--all] glob
    Removes the given glob pattern from the allowlist for chmod sandboxing
deny_chmod glob
    Deny the given glob pattern for chmod sandboxing
nodeny_chmod [--all] glob
    Removes the given glob pattern from the denylist for chmod sandboxing
allow_chattr glob
    Allow the given glob pattern for chattr sandboxing
disallow_chattr [--all] glob
    Removes the given glob pattern from the allowlist for chattr sandboxing
deny_chattr glob
    Deny the given glob pattern for chattr sandboxing
nodeny_chattr [--all] glob
    Removes the given glob pattern from the denylist for chattr sandboxing
allow_chroot glob
    Allow the given glob pattern for chroot sandboxing
disallow_chroot [--all] glob
    Removes the given glob pattern from the allowlist for chroot sandboxing
deny_chroot glob
    Deny the given glob pattern for chroot sandboxing
nodeny_chroot [--all] glob
    Removes the given glob pattern from the denylist for chroot sandboxing
allow_utime glob
    Allow the given glob pattern for utime sandboxing
disallow_utime [--all] glob
    Removes the given glob pattern from the allowlist for utime sandboxing
deny_utime glob
    Deny the given glob pattern for utime sandboxing
nodeny_utime [--all] glob
    Removes the given glob pattern from the denylist for utime sandboxing
allow_mkdev glob
    Allow the given glob pattern for mkdev sandboxing
disallow_mkdev [--all] glob
    Removes the given glob pattern from the allowlist for mkdev sandboxing
deny_mkdev glob
    Deny the given glob pattern for mkdev sandboxing
nodeny_mkdev [--all] glob
    Removes the given glob pattern from the denylist for mkdev sandboxing
allow_mkfifo glob
    Allow the given glob pattern for mkfifo sandboxing
disallow_mkfifo [--all] glob
    Removes the given glob pattern from the allowlist for mkfifo sandboxing
deny_mkfifo glob
    Deny the given glob pattern for mkfifo sandboxing
nodeny_mkfifo [--all] glob
    Removes the given glob pattern from the denylist for mkfifo sandboxing
allow_mktemp glob
    Allow the given glob pattern for mktemp sandboxing
disallow_mktemp [--all] glob
    Removes the given glob pattern from the allowlist for mktemp sandboxing
deny_mktemp glob
    Deny the given glob pattern for mktemp sandboxing
nodeny_mktemp [--all] glob
    Removes the given glob pattern from the denylist for mktemp sandboxing
allow_net [--connect|--sendfd] glob|cidr!port[-port]
    Allow the given network address for network bind or connect sandboxing
disallow_net [--all] [--connect|--sendfd] glob|cidr!port[-port]
    Removes the given network address (Ipv4,6), or the glob pattern (UNIX domain sockets)
    from the allowlist for network bind or connect sandboxing
deny_net [--connect|--sendfd] glob|cidr!port[-port]
    Deny the given network address (Ipv4,6) or the glob pattern (UNIX domain sockets)
    for network bind or connect sandboxing
nodeny_net [--all] [--connect|--sendfd] glob|cidr!port[-port]
    Removes the given network address (Ipv4,6) or the glob pattern (UNIX domain sockets)
    from the denylist for network bind or connect sandboxing
addfilter, addfilter_path glob
    Adds the given glob pattern to the list of access violation filters for path sandboxing
rmfilter, rmfilter_path [--all] glob
    Removes the given glob pattern from the list of access violation filters for path sandboxing
addfilter_stat glob
    Adds the given glob pattern to the list of access violation filters for stat sandboxing
rmfilter_stat [--all] glob
    Removes the given glob pattern from the list of access violation filters for stat sandboxing
addfilter_read glob
    Adds the given glob pattern to the list of access violation filters for read sandboxing
rmfilter_read [--all] glob
    Removes the given glob pattern from the list of access violation filters for read sandboxing
addfilter_write glob
    Adds the given glob pattern to the list of access violation filters for write sandboxing
rmfilter_write [--all] glob
    Removes the given glob pattern from the list of access violation filters for write sandboxing
addfilter_exec glob
    Adds the given glob pattern to the list of access violation filters for exec sandboxing
rmfilter_exec [--all] glob
    Removes the given glob pattern from the list of access violation filters for exec sandboxing
addfilter_ioctl glob
    Adds the given glob pattern to the list of access violation filters for ioctl sandboxing
rmfilter_ioctl [--all] glob
    Removes the given glob pattern from the list of access violation filters for ioctl sandboxing
addfilter_create glob
    Adds the given glob pattern to the list of access violation filters for create sandboxing
rmfilter_create [--all] glob
    Removes the given glob pattern from the list of access violation filters for create sandboxing
addfilter_delete glob
    Adds the given glob pattern to the list of access violation filters for delete sandboxing
rmfilter_delete [--all] glob
    Removes the given glob pattern from the list of access violation filters for delete sandboxing
addfilter_rename glob
    Adds the given glob pattern to the list of access violation filters for rename sandboxing
rmfilter_rename [--all] glob
    Removes the given glob pattern from the list of access violation filters for rename sandboxing
addfilter_symlink glob
    Adds the given glob pattern to the list of access violation filters for symlink sandboxing
rmfilter_symlink [--all] glob
    Removes the given glob pattern from the list of access violation filters for symlink sandboxing
addfilter_truncate glob
    Adds the given glob pattern to the list of access violation filters for truncate sandboxing
rmfilter_truncate [--all] glob
    Removes the given glob pattern from the list of access violation filters for truncate sandboxing
addfilter_chdir glob
    Adds the given glob pattern to the list of access violation filters for chdir sandboxing
rmfilter_chdir [--all] glob
    Removes the given glob pattern from the list of access violation filters for chdir sandboxing
addfilter_readdir glob
    Adds the given glob pattern to the list of access violation filters for readdir sandboxing
rmfilter_readdir [--all] glob
    Removes the given glob pattern from the list of access violation filters for readdir sandboxing
addfilter_mkdir glob
    Adds the given glob pattern to the list of access violation filters for mkdir sandboxing
rmfilter_mkdir [--all] glob
    Removes the given glob pattern from the list of access violation filters for mkdir sandboxing
addfilter_chown glob
    Adds the given glob pattern to the list of access violation filters for chown sandboxing
rmfilter_chown [--all] glob
    Removes the given glob pattern from the list of access violation filters for chown sandboxing
addfilter_chgrp glob
    Adds the given glob pattern to the list of access violation filters for chgrp sandboxing
rmfilter_chgrp [--all] glob
    Removes the given glob pattern from the list of access violation filters for chgrp sandboxing
addfilter_chmod glob
    Adds the given glob pattern to the list of access violation filters for chmod sandboxing
rmfilter_chmod [--all] glob
    Removes the given glob pattern from the list of access violation filters for chmod sandboxing
addfilter_chattr glob
    Adds the given glob pattern to the list of access violation filters for chattr sandboxing
rmfilter_chattr [--all] glob
    Removes the given glob pattern from the list of access violation filters for chattr sandboxing
addfilter_chroot glob
    Adds the given glob pattern to the list of access violation filters for chroot sandboxing
rmfilter_chroot [--all] glob
    Removes the given glob pattern from the list of access violation filters for chroot sandboxing
addfilter_utime glob
    Adds the given glob pattern to the list of access violation filters for utime sandboxing
rmfilter_utime [--all] glob
    Removes the given glob pattern from the list of access violation filters for utime sandboxing
addfilter_mkdev glob
    Adds the given glob pattern to the list of access violation filters for mkdev sandboxing
rmfilter_mkdev [--all] glob
    Removes the given glob pattern from the list of access violation filters for mkdev sandboxing
addfilter_mkfifo glob
    Adds the given glob pattern to the list of access violation filters for mkfifo sandboxing
rmfilter_mkfifo [--all] glob
    Removes the given glob pattern from the list of access violation filters for mkfifo sandboxing
addfilter_mktemp glob
    Adds the given glob pattern to the list of access violation filters for mktemp sandboxing
rmfilter_mktemp [--all] glob
    Removes the given glob pattern from the list of access violation filters for mktemp sandboxing
addfilter_net glob|cidr!port[-port]
    Adds the network address (Ipv4,6) or the glob pattern (UNIX domain sockets)
    to the list of access violation filters for network sandboxing
rmfilter_net [--all] glob|cidr!port[-port]
    Removes the network address (Ipv4,6) or the glob pattern (UNIX domain sockets)
    from the list of access violation filters for network sandboxing
mem_max
    Set syd maximum per-process memory usage limit for memory sandboxing
    parse-size crate is used to parse the value so formatted strings are OK
vm_max
    Set syd maximum per-process virtual memory usage limit for memory sandboxing
    parse-size crate is used to parse the value so formatted strings are OK
kill_mem
    Send SIGKILL to process on Memory access violation
nokill_mem
    Do not send SIGKILL to process on Memory access violation
    Deny system call with ENOMEM
filter_mem
    Do not report access violations for memory sandboxing
unfilter_mem
    Report access violations for memory sandboxing
pid_max
    Set syd maximum process id limit for PID sandboxing
kill_pid
    Send SIGKILL to process on PID access violation
nokill_pid
    Do not send SIGKILL to process on PID access violation
    Deny system call with EACCES
filter_pid
    Do not report access violations for PID sandboxing
unfilter_pid
    Report access violations for PID sandboxing
EOF
        ;;
    *)
        echo >&2 "esyd: subcommand '${cmd}' unrecognised!"
        echo >&2 "Use 'esyd help' for a list of supported subcommands."
        return 1
        ;;
    esac
}

_esyd_path()
{
    local cmd="${1}"
    local op="${2}"

    case "${op}" in
    '+'|'-')
        ;;
    *)
        echo >&2 "esyd_path: invalid operation character '${op}'"
        return 1
        ;;
    esac

    shift 2

    local ret=0
    local path
    for path in "${@}"; do
        case "${path}" in
        /*)
            [ -c /dev/syd/"${cmd}${op}${path}" ] || ret=$?
            ;;
        *)
            echo >&2 "esyd_path: expects absolute path, got: ${path}"
            return 1
            ;;
        esac
    done
    return $ret
}

_esyd_net()
{
    local cmd="${1}"
    local op="${2}"

    case "${op}" in
    '+'|'-')
        ;;
    *)
        echo >&2 "esyd_net: invalid operation character '${op}'"
        return 1
        ;;
    esac

    shift 2

    local ret=0
    while [ ${#} -gt 0 ] ; do
        # syd does input validation so we don't do any here.
        [ -c "/dev/syd/${cmd}${op}${1}" ] || ret=$?
        shift
    done
    return $ret
}
