//
// Syd: rock-solid application kernel
// src/syd-check.rs: View syd logs using journalctl.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::{
    os::unix::process::CommandExt,
    process::{Command, ExitCode},
};

use syd::err::SydResult;

fn main() -> SydResult<ExitCode> {
    syd::set_sigpipe_dfl()?;

    let _ = Command::new("journalctl")
        .arg("SYSLOG_IDENTIFIER=syd")
        .exec();
    Ok(ExitCode::FAILURE)
}
