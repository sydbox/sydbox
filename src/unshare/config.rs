use nix::sys::signal::{Signal, SIGKILL};

#[derive(Debug, Copy, Clone)]
pub struct Config {
    pub death_sig: Option<Signal>,
    pub restore_sigmask: bool,
    pub make_group_leader: bool,
    pub deny_tsc: bool,
    pub keep: bool,
    pub stop: bool,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            death_sig: Some(SIGKILL),
            restore_sigmask: true,
            make_group_leader: false,
            deny_tsc: false,
            keep: false,
            stop: false,
        }
    }
}
