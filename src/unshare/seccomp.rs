//
// syd: seccomp and landlock based application sandbox
// src/unshare/seccomp.rs: Seccomp support for unshare::Command
//
// Copyright (c) 2023, 2025 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use crate::{libseccomp::ScmpFilterContext, unshare::Command};

impl Command {
    /// Set up a `ScmpFilterContext` for the `Command`.
    pub fn seccomp_filter(&mut self, filter_context: ScmpFilterContext) {
        self.seccomp_filter = Some(filter_context)
    }
}
