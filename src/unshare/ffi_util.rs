use std::{
    ffi::{CString, OsStr},
    os::unix::ffi::OsStrExt,
};

pub trait ToCString {
    fn to_cstring(&self) -> CString;
}

impl<T: AsRef<OsStr>> ToCString for T {
    fn to_cstring(&self) -> CString {
        #[allow(clippy::disallowed_methods)]
        CString::new(self.as_ref().as_bytes()).unwrap()
    }
}
