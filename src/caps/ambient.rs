//! Implementation of Ambient set.

use nix::errno::Errno;

use crate::caps::{errors::CapsError, nr, runtime, Capability, CapsHashSet};

pub fn clear() -> Result<(), CapsError> {
    Errno::result(unsafe { libc::prctl(nr::PR_CAP_AMBIENT, nr::PR_CAP_AMBIENT_CLEAR_ALL, 0, 0, 0) })
        .map(std::mem::drop)
        .map_err(CapsError)
}

pub fn drop(cap: Capability) -> Result<(), CapsError> {
    Errno::result(unsafe {
        libc::prctl(
            nr::PR_CAP_AMBIENT,
            nr::PR_CAP_AMBIENT_LOWER,
            libc::c_uint::from(cap.index()),
            0,
            0,
        )
    })
    .map(std::mem::drop)
    .map_err(CapsError)
}

pub fn has_cap(cap: Capability) -> Result<bool, CapsError> {
    let ret = Errno::result(unsafe {
        libc::prctl(
            nr::PR_CAP_AMBIENT,
            nr::PR_CAP_AMBIENT_IS_SET,
            libc::c_uint::from(cap.index()),
            0,
            0,
        )
    })
    .map_err(CapsError)?;

    match ret {
        0 => Ok(false),
        _ => Ok(true),
    }
}

pub fn raise(cap: Capability) -> Result<(), CapsError> {
    Errno::result(unsafe {
        libc::prctl(
            nr::PR_CAP_AMBIENT,
            nr::PR_CAP_AMBIENT_RAISE,
            libc::c_uint::from(cap.index()),
            0,
            0,
        )
    })
    .map(std::mem::drop)
    .map_err(CapsError)
}

pub fn read() -> Result<CapsHashSet, CapsError> {
    let mut res = super::CapsHashSet::default();
    for c in runtime::thread_all_supported() {
        if has_cap(c)? {
            res.insert(c);
        }
    }
    Ok(res)
}

pub fn set(value: &super::CapsHashSet) -> Result<(), CapsError> {
    for c in runtime::thread_all_supported() {
        if value.contains(&c) {
            raise(c)?;
        } else {
            drop(c)?;
        };
    }
    Ok(())
}
