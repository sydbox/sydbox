//! Error handling.

use nix::errno::Errno;

/// Library errors.
#[derive(Debug)]
pub struct CapsError(pub Errno);

impl std::fmt::Display for CapsError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "caps error: {}", self.0)
    }
}

impl std::error::Error for CapsError {}
