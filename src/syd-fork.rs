//
// Syd: rock-solid application kernel
// src/syd-fork.rs: Fork fast in an infinite loop.
//
// Copyright (c) 2024, 2025 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::process::ExitCode;

use syd::err::SydResult;

fn main() -> SydResult<ExitCode> {
    use lexopt::prelude::*;

    syd::set_sigpipe_dfl()?;

    // Parse CLI options.
    let mut parser = lexopt::Parser::from_env();
    #[allow(clippy::never_loop)]
    while let Some(arg) = parser.next()? {
        match arg {
            Short('h') => {
                help();
                return Ok(ExitCode::SUCCESS);
            }
            _ => return Err(arg.unexpected().into()),
        }
    }

    // SAFETY: Do not try this at home!
    loop {
        unsafe { syd::fork_fast() };
    }
}

fn help() {
    println!("Usage: syd-fork [-h]");
    println!("Fork fast in an infinite loop.");
    println!("WARNING: DO NOT TRY THIS AT HOME!");
    println!("WARNING: USE THIS AT YOUR OWN RISK!");
    println!("WARNING: USE THIS ONLY TO STRESS-TEST YOUR PID-LIMITER!");
}
