//
// Syd: rock-solid application kernel
// src/syd-cpu.rs: Print the number of CPUs.
//
// Copyright (c) 2024, 2025 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0

use std::process::ExitCode;

use syd::err::SydResult;

fn main() -> SydResult<ExitCode> {
    use lexopt::prelude::*;

    syd::set_sigpipe_dfl()?;

    // Parse CLI options.
    let mut opt_physical = false;

    let mut parser = lexopt::Parser::from_env();
    while let Some(arg) = parser.next()? {
        match arg {
            Short('h') => {
                help();
                return Ok(ExitCode::SUCCESS);
            }
            Short('l') => opt_physical = false,
            Short('p') => opt_physical = true,
            _ => return Err(arg.unexpected().into()),
        }
    }

    let num = if opt_physical {
        num_cpus::get_physical()
    } else {
        num_cpus::get()
    };
    println!("{num}");

    Ok(ExitCode::SUCCESS)
}

fn help() {
    println!("Usage: syd-cpu [-hlp]");
    println!("Print the number of CPUs.");
    println!("Use -l to print the number of logical CPUs (default).");
    println!("Use -p to print the number of physical CPUs.");
}
