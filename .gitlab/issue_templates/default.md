### Describe the bug
<!--
Describe the bug briefly.
-->

### syd --version
<!--
Insert version here.
-->

### Logs or it did not happen!
<!--
Add all the logs you can find in paludis builddir.
Add a strace log generated with "env SYD_FORCE_TTY=1 SYD_LOG=debug strace -f -s1024 cave..."
Add a strace log generated with "strace -f -s1024 cave..."
This saves a lot of time.
Please compress the files, the size limit is 10MB.
-->

### Poem
<!--
Attach a poem to make Syd happy!
-->

