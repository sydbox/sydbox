ChangeLog
=========

# 0.0.4

- Change project license from `LGPL-3.0-or-later` to `LGPL-3.0`.

# 0.0.3

- Improve documentation.

# 0.0.2

- Improve documentation, add examples

# 0.0.1

- Initial implementation of libsydtime
