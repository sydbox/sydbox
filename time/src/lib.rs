//
// libsydtime: Rust-based C LD_PRELOAD library to replace vDSO time calls with syscalls
// time/src/lib.rs: LD_PRELOAD library
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: LGPL-3.0

use libc::{c_int, time_t, timespec, timeval, timezone};

/// Hook for `gettimeofday`
#[no_mangle]
pub unsafe extern "C" fn gettimeofday(tv: *mut timeval, tz: *mut timezone) -> c_int {
    libc::syscall(libc::SYS_gettimeofday, tv, tz) as c_int
}

/// Hook for `clock_getres`
#[no_mangle]
pub unsafe extern "C" fn clock_getres(clk_id: c_int, tp: *mut timespec) -> c_int {
    libc::syscall(libc::SYS_clock_getres, clk_id, tp) as c_int
}

/// Hook for `clock_gettime`
#[no_mangle]
pub unsafe extern "C" fn clock_gettime(clk_id: c_int, tp: *mut timespec) -> c_int {
    libc::syscall(libc::SYS_clock_gettime, clk_id, tp) as c_int
}

/// Hook for `time`
#[no_mangle]
pub unsafe extern "C" fn time(tloc: *mut time_t) -> time_t {
    libc::syscall(libc::SYS_time, tloc) as time_t
}
