pandora: Syd's log inspector &amp; profile writer

Use `pandora profile command` to generate a sandbox profile for command.

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
[~alip/sydbox-devel@lists.sr.ht](mailto:~alip/sydbox-devel@lists.sr.ht).
Exherbo related bugs/issues can be submitted to
<https://gitlab.exherbo.org/groups/sydbox/-/issues>. Discuss in [#sydbox on Libera
Chat](ircs://irc.libera.chat/#sydbox).
