# ChangeLog

# ?

- Set the environment variable `SYD_QUIET_TTY` to make
  Syd print logs in line-oriented JSON format. Previously,
  this was implied by the option `log/verbose:0` in the
  `trace` profile which is no longer the case since
  Syd-3.32.4.

# 0.14.0

- Sort _ioctl_(2) requests before printing them to the profile.
- Add support to turn UNIX socket paths into globs.
- Add support to confine `!unnamed` dummy path for UNIX unnamed sockets
  for the `net/bind` and `net/connect` sandboxing categories.
- Add support for the `@` prefix for UNIX abstract sockets which is new
  in Syd-3.32.0.

# 0.13.0

- Rewrite option parsing to use the `lexopt` crate rather than `argv`
  and `getargs` crates. Make option parsing [POSIXly
  correct](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html#tag_12_02).
- Add support for new sandbox categories in Syd-3.31.0.

# 0.12.1

- Add support to learn _ioctl_(2) requests.
- Stop using the `dns-lookup` crate and call libc _getnameinfo_(3)
  directly.

# 0.12.0

- Fix passing extra options to Syd.
- Fix static linking.
- Stop using the `built` crate which depends on `libgit2-sys`.
- Replace the `clap` crate with `getargs` crate for option parsing.
- Send the signal `SIGKILL` on timeout rather than `SIGTERM`.
- Avoid locking the sandbox in generated profiles.

# 0.12.0-beta.3

- Fix _syslog_(2) handling to do non-destructive reads.
- Upgrade `hex-conservative` crate dependency from `0.2` to `0.3`.

# 0.12.0-beta.2

- Fix _syslog_(2) handling.

# 0.12.0-beta.1

- Replace `parking_lot` crate with stdlib locks.
- The special input argument `syslog` may now be passed to
  `pandora inspect --input` to read access violations from
  _syslog_(2). This allows easy access to the _syslog_(2)
  emulation new in Syd-3.30.0.
- Do checksum calculation in parallel.
- Support choosing alternative algorithms to use for executable
  verification with the new  `-h` option. The default remains the most
  secure SHA3-512.
- Be permissive in JSON parsing: Lines with invalid JSON are now skipped
  and pandora strips all characters until the first `{` from input lines
  before attempting to parse them as JSON.
- Add support for [`Chdir
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Chdir_Sandboxing),
  [`Readdir
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Readdir_Sandboxing),
  [`Rename
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Create/Delete/Truncate/Rename/Link_Sandboxing),
  and [`Link
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Create/Delete/Truncate/Rename/Link_Sandboxing)
  new in Syd-3.30.0.

# 0.11.1

- Stop using the `pandora` profile which is a no-op with the `trace`
  profile already applied.

# 0.11.0

- Change project license from `GPL-3.0-or-later` to `GPL-3.0`.
- Add support for Create/Delete, Node, Attr, Chown/Chgrp, Tmpfile, and
  Net/Send sandboxing types new in Syd-3.28.0.
- Print progress during checksum generation.
- Sync with logging changes in Syd-3.28.0.
- Remove humantime and time crates from dependencies.
- Replace const-hex crate with hex-conservative crate.

# 0.10.3

- Fix panic when allowlisting UNIX abstract/domain sockets.

# 0.10.2

- Perform reverse DNS lookup on IPs and add hostnames as comments
  to the auto-generated profile.
- Add efficient handling of `/proc/pid/ns/` paths.
- Implement grouping of sandboxing rules together by sandboxing type.

# 0.10.1

- Make various formatting improvements to the auto-generated profile.
- Reduce --limit default from 7 to 3 for practicality.
- Enumerate dynamic libraries for Exec sandboxing.

# 0.10.0

- Add support for calculating the checksums of dynamically linked
  libraries.
- Add support for Ioctl sandboxing of Syd-3.23.0
- Replace hex crate with const-hex crate.

# 0.9.0

- Add support for writing Integrity Force rules. The user should
  turn force sandboxing on manually for this to work, e.g:
  `pandora profile -msandbox/force:on -- true`

## 0.8.4

- Improve documentation.

## 0.8.3

- Add `--timeout` option to `profile` subcommand to limit process
  runtime with a duration. The option takes human-formatted time as
  argument.
- Block SIGINT when profiling so interrupting the underlying process
  does not interrupt profile generation.

## 0.8.2..

See git history
