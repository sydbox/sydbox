ChangeLog
=========

# ?

- Add option `trace/allow_unsafe_prlimit:1` to the `debug` profile
  to ease generating coredumps.
- Add option `trace/allow_unsafe_prctl:1` to the `paludis` profile
  because perl tests checks setting process name using _prctl_(2)
  `PR_SET_NAME` operation.
- Allow `read` and `stat` access to `/proc/uptime` and `/proc/loadavg`
  files in the `linux` profile to provide the randomized versions of the
  files by default.
- Provide virtualized versions of `/proc/uptime` and `/proc/loadavg`
  files consistent with the _sysinfo_(2) randomizer. This access is
  subject to sandbox policy check and can be disabled with
  `trace/allow_unsafe_sysinfo:1`.
- Improve exit tracing in syscall monitor and emulator threads.
  by proactively closing the seccomp-notify file descriptor.
- Grant `/proc` PID traversals, ie identifying fellow processes by
  _readdir_(2)'ing `/proc`, for the `paludis` profile using the rule
  `allow/stat+/proc/[0-9]*`. This is done because package tests often
  assume they can identify fellow processes by _readdir_(2)'ing the
  `/proc` directory. In contrast, the default secure `linux` profile
  denies this access because it is an information leak.
- Grant `/proc` wide access to the `stat` files for the `paludis`
  profile. This is often useful in package testing when packages
  assume files such as `/proc/1/stat` is accessible.
- Grant `read` and `stat` access to `/proc/self/stat` and
  `/proc/self/task/*/stat` files for the `linux` profile.  This is
  consistent with the file `/proc/stat` which is given `read` and `stat`
  access in the `linux` profile.
- Add `trace/allow_unsafe_magiclinks:1` to the `paludis` profile.
- Improve initial core system call emulator thread spawning procedure.
- Add new environment variable `SYD_NPROC_MAX` to define the number of
  maximum number of syscall handler threads. The number must be greater
  than the value of `SYD_NPROC` which defaults to the number of CPUs.
  The default value of `SYD_NPROC_MAX` is currently `usize::MAX`.
- Add `abort` to the list of available actions which causes Syd to deny
  the system call, warn and send the sandbox process a `SIGABRT` signal.
  This action is intended for _pledge_(2) compatibility.

# 3.32.4

- Add `trace/allow_unsafe_sysinfo:1` to the `paludis` profile.
- Add option `trace/allow_unsafe_sysinfo:1` to disable the _sysinfo_(2)
  randomizer.
- Add `trace/allow_unsafe_chroot:1` to the `paludis` profile.
- Add option `trace/allow_unsafe_chroot:1` to turn the _chroot_(2)
  system call into a no-op like it's done by default for the
  _pivot_root_(2) system call.
- Add the new system calls _listmount_(2) and _statmount_(2) to
  the set of the `mount` system calls.
- Add the system calls _lsm_get_self_attr_(2), _lsm_set_self_attr_(2),
  and _lsm_list_modules_(2) to the list of `dead` syscalls. These system
  calls will always error for sandbox processes and Syd.
- The system calls _setxattr_(2) and _setxattrat_(2) now deny access to
  the `user.syd.*` xattr namespace with the errno `EACCES` rather than
  `ENODATA`.
- Add handlers for system calls _getxattrat_(2), _setxattrat_(2),
  _listxattrat_(2), and _removexattrat_(2) which are new in Linux>=6.13.
  _getxattrat_(2) and _listxattrat_(2) are confined as part of `stat`
  sandboxing, meanwhile _setxattrat_(2) and _removexattrat_(2) are
  confined as part of `chattr` sandboxing. Syd's restrictions on
  `user.syd.*` xattr namespace is applied for these system calls as
  well.
- Add the new system call _mseal_(2) to the list of allowed system calls.
- Allow the flag combination `O_PATH|O_NOCTTY` in _open_(2) system call
  handlers. Previously an `EINVAL`, aka "Invalid argument" error was
  returned in this case which notably breaks on Alpine Linux.
- Remove the overly-complex and racy `path->O_PATH-fd` cache and the
  configuration option `cache/capacity/fd`.
- Print the status of the kernel BPF JIT compiler in Syd version output.
- _syd-hex_(1) now refuses to output to standard output in case standard
  output is attached to a TTY when decoding hexadecimal input. The new
  command line flags `-f` and `--force-tty` may be used to
  override this check.
- Mask control characters in pathnames listed in `/proc/pid/maps` for
  logging.
- Set `log/verbose:false` for the `paludis` profile.
- Unmask the file `/proc/version` for the `paludis` profile. This
  file is masked by the `linux` profile for consistency with _uname_(2)
  restrictions.
- Fix adding watches for symbolic links in _inotify_add_watch_(2) handler.
- Fix adding watches for directories in _inotify_add_watch_(2) handler.
- Improve core thread spawn at startup for quicker boot times.
- Harden `syd::pool::ThreadPool` against DOS attacks utilizing blocking
  _open_(2) calls.
- Avoid busy looping in Syd interrupt thread and wait on a condition variable.
- Refine Chroot sandboxing, see
  [sandbox/chroot](https://man.exherbolinux.org/syd.2.html#sandbox/chroot)
  section of the _syd_(2) manual page for more information.
- Remove the incorrect validation where setting both of the flags
  `AT_SYMLINK_NOFOLLOW` and `AT_EMPTY_PATH` would fail the respective
  system call to fail with `EINVAL`.
- Grant `read` and `stat` access to the file `/etc/man_db.conf` for
  the `linux` profile.
- Grant `read`, `stat`, `chdir` and `readdir` access to the directory
  `/etc/groff` recursively for the `linux` profile.
- Grant `read` and `stat` access to the file `/etc/locale.conf` for
  the `linux` profile.
- Setting the option `log/verbose:0` no longer implies line-oriented
  JSON. Line-oriented JSON is controlled by TTY detection and can be
  explicitly set using the environment variables `SYD_FORCE_TTY` and
  `SYD_QUIET_TTY`.
- Change deny action for _dlopen_(3) on a library with executable stack
  to `EACCES` rather than `kill`.
- Return `EACCES`, aka "Access denied", rather than `ENOENT`, aka "No
  such file or directory" for PID mismatches in hardened procfs checks.
  The old behaviour confused programs such as pipewire which check
  for flatpak support using procfs magic symbolic links.
- Allow passing files of unknown types such as event fds and epoll fds
  as part of the `net/sendfd` category. Notably, pipewire depends on
  this functionality. File descriptors referring to block devices,
  directories and symbolic links are still denied.

# 3.32.3

- Grant `read`, `stat`, `chdir` and `readdir` access to the
  directory `/etc/ca-certificates` recursively for the `linux`
  profile.
- Fix _sysinfo_(2) system call randomizer for musl systems.
- Fix a problem with fd-invalidation of fd-path cache. Notably, this
  makes concurrent programs written in golang work under Syd without any
  issues. Previously running such programs would give `EEXIST`, aka
  "File exists", errors sporadically.
- Fix a race with concurrent exclusive creating _open_(2) calls.
- Refine file type validations in _open_(2) system calls.
- Use OS random number generator for FD randomization to mitigate races.
- Ensure no non-standard file descriptors are leaked into the process
  executed by the command `cmd/exec`.
- Ensure the command executed by `cmd/exec` is correctly daemonized.
- Grant `read` and `stat` access to the files `/proc/{cpu,mem}info` and
  `/proc/crypto` for the `paludis` profile.
- Refine access rights of the `/proc/self/fd` directory for the `linux` profile.
- Return `EISDIR` on attempted write opens on directories.
- Grant `stat`, `read`, `chdir` and `readdir` access to the
  `/proc/sys/fs` directory recursively for the `linux` profile.
  Note `linux` profile already allows `/proc/sys/kernel` and
  `/proc/sys/vm` so this is consistent.
- Fix an issue with `l{get,set,remove}xattr` handlers. Previously these
  system call handlers incorrectly returned `ENOTSUP` for paths whose
  last component is a dot, aka `.`.
- Fix `trace/allow_safe_bind` option to correctly allow UNIX abstract
  sockets and unnamed UNIX sockets.
- Fix an issue with setting open file resource limits. Notably, this
  fixes the `test-dup2` test of gnulib.
- Fix an issue with path canonicalization regarding `procfs` files.
- Fix PID calculation for `procfs` in safe path checker.
- Fix access violation reporting for path hiding.
- Remove the _kill_(2) system call from the list of allowed system calls
  in `syd_emu` emulator threads. Note, the `kill` action already uses
  pidfds and _pidfd_send_signal_(2) for race-free signal delivery hence
  _kill_(2) is not needed.
- vim: highlight the new option
  [`trace/allow_unsafe_ebpf`](https://man.exherbolinux.org/syd.2.html#trace/allow_unsafe_ebpf).

# 3.32.2

- Fix a build regression in `syd-test` on arm, loongarch64 and riscv64
  due to missing _open_(2) syscall.

# 3.32.1

- Handle `EINVAL`, aka "Invalid argument" gracefully when loading
  _seccomp_(2) filters for _syd-elf_(1).
- Remove the (now-defunct) utility _syd-chk_(1).
- Add the option `trace/allow_unsafe_ebpf:1` to the `paludis` profile.
- Add the option `trace/allow_unsafe_cbpf:1` to the `paludis` profile.
- Add option `trace/allow_unsafe_ebpf:1` to allow direct eBPF use.
- Deny _seccomp_(2) system call in per-thread seccomp filters of Syd's
  emulator and encryptor threads to avoid potential confused deputy
  problems.
- Deny sandbox processes from installing additional _seccomp_(2) cbpf
  filters by default. This restriction can be lifted at startup with the
  new option
  [`trace/allow_unsafe_cbpf:1`](https://man.exherbolinux.org/syd.2.html#trace/allow_unsafe_cbpf)
  **This is a security issue, users are recommended to update asap:**
  Lack of this mitigation potentially allows sandbox processes to
  install more precedent actions than `seccomp-unotify` and thereby
  bypass Syd's own _seccomp_(2) cbpf filters.
- _syd-ls_(1) learned the `futex` set to print the list of _futex_(2)
  family system calls.
- Allow the _swapcontext_(2) system call by default. Syd used to deny
  this PowerPC-only system call unconditionally.
- Allow _futex_requeue_(2), and _futex_wait_(2) system calls by default.
  Note, Syd already allows the system calls _futex_(2),
  _futex_time64_(2), and _futex_waitv_(2) so this is consistent.
- Return `EEXIST` in _mkdir_(2) handler if path exists after access check.
- Avoid raising `mkdir` access violations for paths that already exist.

# 3.32.0

- Add rules `allow/net/bind+!unnamed` and `allow/net/connect+!unnamed`
  to the `linux` profile to grant network access to unnamed UNIX
  sockets.
- Confine network system calls to unnamed UNIX sockets
  using the dummy path "!unnamed" during access check.
  Previously only the `net/sendfd` used this confinement,
  meanwhile `net/bind` and `net/connect` groups always allowed access to
  unnamed UNIX sockets.
- Prefix UNIX abstract sockets with the "@" character
  before access check for clear distinction between
  UNIX domain and abstract sockets.
- Fix build on libseccomp versions older than `2.6`.

# 3.31.0

- Fix an integer overflow in _sysinfo_(2) uptime randomizer.
- Rewrite option parsing to use the `lexopt` crate rather than `argv`
  and `getargs` crates. Make option parsing [POSIXly
  correct](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html#tag_12_02).
- Set option `trace/allow_unsafe_open_cdev:1` for the `paludis` profile.
- Add new option `trace/allow_unsafe_open_cdev` to access per-process
  character devices, such as `/dev/kfd` of AMD GPUs. See [the
  documentation of this
  option](https://man.exherbolinux.org/syd.2.html#trace/allow_unsafe_open_cdev)
  in _syd_(2) manual page for the security implications.
- Rename option `trace/allow_unsafe_open` to
  `trace/allow_unsafe_open_path` for clarity.
- Ghost mode can now be entered even if Crypt sandboxing is on.
  Previously the `ghost` command returned `EBUSY` in this case.
- Add the system calls _sched_setattr_(2) and _sched_setparam_(2) to the
  list of nice system calls. See `syd-ls nice` for the complete list.
  These system calls are denied unless `trace/allow_unsafe_nice:1` is
  passed at startup.
- Add `nice` to the list of syscall groups listed by _syd-ls_(1).
- Move the rule `allow/net/sendfd+!unnamed` from the `paludis` profile
  to the `linux` profile which is also included by the `user` profile.
- Change the dummy path to confine sending file descriptors
  to unnamed sockets from "/dev/null" to "!unnamed" for clarity.
- Disallow sending file descriptors referring to block devices,
  directories and symbolic links using the _sendmsg_(2) and
  _sendmmsg_(2) system calls. This is in alignment with OpenBSD
  _pledge_(2) which disallows file descriptors referring to directories
  from being sent.
- Rename sandboxing category `net/send` to `net/sendfd` for clarity.
- Split `node` sandboxing category into two new categories: `mkdev` to
  confine character device creation, and `mkfifo` to confine named pipe
  (FIFO) creation.
- Split `attr` sandboxing category into three new categories: `chmod` to
  confine mode changes on files, `chattr` to confine extended attribute
  changes on files, and `utime` to confine last and modification time
  changes on files.
- Add new sandboxing category `mkdir` to confine directory creation.
  Previously, this type of access was confined with the `create` category.
- Rename sandboxing category `tmpfile` to `mktemp` for clarity.
- Rename sandboxing category `link` to `symlink` for clarity.
- Stop using the `os_rng` feature of the `rand_core` crate which would
  cause Syd to leak a file descriptor to `/dev/random` or `/dev/urandom`
  to the sandbox process in some cases. Instead, use the libc function
  _getentropy_(3) to seed the PRNG. _getentropy_(3) is an OpenBSD
  innovation, and is implemented in recent glibc and musl versions.
- Precompute the main _seccomp_(2) filter before loading it in the
  sandbox process.
- Allow the system calls _atomic_barrier_(2), _atomic_cmpxchg_32_(2),
  and _getpagesize_(2).
- Allow the new system call _set_mempolicy_home_node_(2). Note,
  syd already allows _set_mempolicy_(2) so this is consistent.
- Allow `node` access to `/dev/shm`, `/var/cache`, `/var/tmp` and `/tmp`
  directories for the `linux` profile.
- Grant read access to `/var/db/paludis` for the `paludis` profile.
- Add support for Loongarch64 architecture, 比赛开始了，革命万岁！
- Add support for MIPS architectures.
- Avoid hardcoding _fchmodat2_(2) syscall number in chmod handler.
- Upgrade `rand_hc` crate from `0.3` to `0.4`.
- Upgrade `rand_core` crate from `0.6` to `0.9`.

# 3.30.1

- Fix rootless container detection for _syd-oci_(1).
- Fix _connect_(2) on sockets with `AF_UNSPEC` specified as socket family.
- Fix an IPv6-only DNS-name resolution issue in `syd::dns` module.
- _syd-dns_(1) has been promoted to a core utility and installed without
  the `utils` feature.
- _syd-dns_(1) learned _-R_ to do reverse-DNS lookups using the system's
  DNS resolver.
- Allow _set_mempolicy_(2) system call by default which is used by FF.
- Handle `EINVAL` gracefully for _fdatasync_(2) system call during
  logging. This error indicates the system call was called on a file
  descriptor pointing to a special file (pipe, socket etc.)
- Allow access to `/etc/authselect/nssswitch.conf` file for the `linux`
  profile.
- Allow access to `/etc/GREP_COLORS` file for the `linux` profile.
- Allow access to `/etc/terminfo` directory for the `linux` profile.
- Allow _bind_(2) calls with port zero for Landlock in the `paludis`
  profile.

# 3.30.0

- Prevent repeated successful _bind_(2) calls to the same address
  does from overflowing the sandbox policy with
  `trace/allow_safe_bind:1`.
- Set option `trace/allow_unsafe_nice:1` for the `paludis` profile.
- Reintroduce nice and ionice restrictions. Add option
  `trace/allow_unsafe_nice` to disable.
- oci: Upgrade `youki` and dependencies from to version `0.5`.
- Fix offset and length argument handling of the _fallocate_(2) handler
  on 32-bit systems.
- Add new builtin profile `enforce` to easily set default
  action back to `Deny`, useful in undoing `oci` and `trace`
  profiles.
- The `load` command may now be used to load builtin profiles.
- Add the rule `trace/allow_unsafe_nopie:1` to the `oci` profile.
  This is because most containers ship non-PIE binaries, and
  the `oci` profile is only supposed to be run during container
  learning mode.
- Harden the `user` profile by including the `immutable` profile.
- Set sandbox lock to `exec` for the `oci` profile.
- Make `oci` profile more useful utilizing the `trace` profile
  with `trace/allow_safe_syslog:1`.
- Set `log/level:verbose` for the `user` profile.
- Add `trace/allow_safe_syslog:1` to the `user` profile.
- Add option `trace/allow_safe_syslog:1` to allow access to Syd's
  _syslog_(2) emulation using _dmesg_(8). Without this option
  a process needs access to the sandbox lock  to use _dmesg_(8).
  Note, this option has nothing to do with access to the host _syslog_(2)
  which is never allowed.
- Allow read access to system shell initialization files for the `user`
  profile.
- Use CPU pinning for emulator threads to improve performance.
- Replace `parking_lot` crate with stdlib locks.
- Add `trace/allow_unsafe_spec_exec:1` to the `paludis` profile.
- The `silent` profile has been renamed to `quiet`.
- Set `log/verbose` to off for the `trace` profile.
- The new `log/verbose` setting which is on by default may now be used
  to reduce log size.
- The new `log/syslog` setting may now be used in configuration
  to disable logging to host _syslog_(3). This is enabled unless
  `SYD_NO_SYSLOG` is set at startup.
- The new `log/level` setting may now be used in configuration
  to define the log level. Log level defaults to "warn" unless
  `SYD_LOG` environment is set at startup.
- Implement a _syslog_(2) interface to maintain a ring buffer
  of access violations. This allows the sandbox process to
  access these logs using _dmesg_(1) from within the sandbox
  given that the sandbox lock allows it. Above all, this
  provides a practical way to use learning mode for containers
  spawned with _syd-oci_(1).
- The environment variable `SYD_NPROC` can now be used to
  define the number of core syscall handler threads. If unset,
  this defaults to the number of CPUs on the system.
- Add rules `allow/lock/bind+1024-65535` and
  `allow/lock/connect+1024-65535` to the `paludis` profile.
- Fix Landlock _ioctl_(2) request support which is new in Landlock ABI 5
  and newer which requires Linux-6.10 or newer.
- Add option
  [`trace/sync_seccomp`](http://man.exherbolinux.org/syd.2.html#trace/sync_seccomp)
  to use synchronous mode for seccomp-notify which defaults to false
  unless `SYD_SYNC_SCMP` environment variable is set at startup.
- Change Force sandboxing default action from `kill` to `deny`. Now that
  we hook into entry of _execve_(2) we're able to deny execution before
  it happens.
- Change SegvGuard default action from `kill` to `deny`. Now that we
  hook into entry of _execve_(2) we're able to deny execution before
  it happens.
- Abort with error if a specified configuration file has a symbolic
  link in _any_ of its path components.
- Replace the `rustc-hash` crate with the `ahash` crate for hashmaps and
  hashsets.
  The former is not safe to run on untrusted input.
- Support arbitrarily long path names in syscall file descriptor
  arguments, and sandbox process cwd. Notably, this fixes the
  longstanding failure of gnulib's test-getcwd under Syd.
- Add new utility _syd-asm_(1) to disassamble raw bytes and print them
  as a sequence of JSON lines. The disassembly is done natively for
  architectures *x86*, *x86_64*, *x32*, *arm*, *aarch64*, and *riscv64*
  and falls back to GNU _objdump_(1) for other architectures. There's no
  support for LLVM _objdump_(1) yet.
- Add `trace/allow_unsafe_sigreturn:1` to the `paludis` profile.
- Add a mitigation to effectively [`detect and prevent
  SROP`](http://man.exherbolinux.org/syd.7.html#Mitigation_Against_Sigreturn_Oriented_Programming_(SROP)).
- Confine _ioctl_(2) operations for Syd emulator threads to a restricted
  set of seccomp operations on the seccomp-notify fd only.
- Confine _epoll_(7) operations for Syd emulator threads and the main thread
  to a restricted set of epoll operations on the epoll fd only.
- Support the `SYD_PID_FN` environment variable to specify path to a pid
  filename into which Syd will write its process ID at startup. The file
  must not exist and is going to be created with user-only read
  permissions.
- Add a pidfd cache to only ever open each pidfd once in its lifetime.
  The file descriptors are retired effectively using _epoll_(7).
- Handle interrupts in blocking _creat_(2), _open_(2), _openat_(2),
  _openat2_(2), _fallocate_(2), _ftruncate_(2), _fstatfs_(2), _accept_(2),
  _connect_(2), _sendto_(2), _sendmsg_(2), and _sendmmsg_(2)  system calls.
- Log path hiding with `info` level unless explicitly specified to warn.
- Add new sandboxing categories [`Rename
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Create/Delete/Truncate/Rename/Link_Sandboxing)
  and [`Link
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Create/Delete/Truncate/Rename/Link_Sandboxing)
  to confine file renames and symlinking. Previously these types of access was
  checked under Create sandboxing.
- Workaround uninterruptible FIFOs by monitoring for signals in
  the sandbox process.
- The option `trace/allow_unsafe_debug` has been replaced with two
  more refined options `trace/allow_unsafe_ptrace` and
  `trace/allow_unsafe_perf`. The meaning of `trace/allow_unsafe_exec`
  has been changed.
- Add new sandboxing categories [`Chdir
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Chdir_Sandboxing) and
  [`Readdir
  Sandboxing`](http://man.exherbolinux.org/syd.7.html#Readdir_Sandboxing)
  to confine directory changes and listings.
- Harden the existing _exec_(3) ptrace hook, do checks
  both at syscall entry and ptrace exec event so that
  both scripts and binaries are covered.
- Implement a safe _chdir_(2) hook based on _ptrace_(2).
- Start hooking into _fchdir_(2) for stat sandboxing again.
- Bring back the `shellexpand` dependency, set
  [`config/expand`](http://man.exherbolinux.org/syd.2.html#config/expand)
  to zero seconds and use `shellexpand` rather than _wordexp_(3) unless
  a timeout is set. This runs much faster as it does not require
  confinement, however it does not support command substitution and
  recursive environment variable expansion like _wordexp_(3) does.
- Fix `RESOLVE_BENEATH` handling in _openat_(2) handler.
- Start using a cryptographically secure random number generator that
  uses the HC-128 algorithm rather than relying on _getrandom_(2)
  with `GRND_RANDOM` which is slow and overkill.
- Stop hooking into _getrandom_(2) for Read sandboxing. Since
  this system call is soon to be provided by VDSO, there's
  little point.
- Fix a TOCTTOU vector in _mmap_(2) handler.
- Rework path canonicalization making it more efficient and secure
  it against magic symbolic links.
- Rework the `syd::proc` module based on _openat2_(2) system call
  for `/proc` access disabling symbolic link resolution where possible.
- Support the environment variables `SYD_FORCE_TTY` and `SYD_QUIET_TTY`
  to fine tune log format. The former forces pretty-printed JSON,
  whereas the latter disables TTY detection and forces line-oriented
  JSON.

# 3.29.4

- Deny access to the files `/proc/sys/kernel/modprobe` and
  `/sys/kernel/notes` for the `immutable` profile using bind
  mounts. This is consistent with the `linux` profile which
  masks these files.
- Fix bind mound error reporting on non-existent mount-points.

# 3.29.3

- Add new utility _syd-emacs_(1) which is a convenience
  wrapper to run Emacs under Syd.
- Respect magic paths for _access_(2) family system calls.
- Add magic path `/dev/syd.el` to get `syd.el`.
- Add magic path `/dev/syd.sh` to get `esyd` helper function.
- Add CLI option `syd --el` to output `syd.el`.
- Start shipping `syd.el` with Syd rather than libsyd.
- Fix a regression of _stat_(2) handler in handling magic paths.

# 3.29.2

- Fix a regression in unit tests.

# 3.29.1

- Mask the file `/proc/sys/kernel/modprobe` in `linux` profile.
  Modification of this file requires no capabilities (can cause
  arbitrary code to be inserted into the kernel via a replacement
  modprobe). See
  <https://forums.grsecurity.net/viewtopic.php?f=7&t=2522> for more
  information.
- Add `FIBMAP` to the default _ioctl_(2) denylist. Coupled with the
  `CAP_SYS_RAWIO` capability, this _ioctl_(2) operation which would
  potentially allow for exploitation of the kernel via the handling of
  input it doesn't expect from untrusted sources. See
  <http://linux.derkeiler.com/Mailing-Lists/Kernel/2007-11/msg07723.html>
  for the reason for FIBMAP being privileged, and
  <http://linux.derkeiler.com/Mailing-Lists/Kernel/2007-11/msg07723.html>
  for further discussion.
- Add `KDSETKEYCODE` to the default _ioctl_(2) denylist. Coupled with
  the `CAP_SYS_TTY_CONFIG` capability, this _ioctl_(2) operation can be
  used by an attacker to temporarily change the keyboard mapping of an
  administrator's tty to cause a different command to be executed than
  intended. See <https://forums.grsecurity.net/viewtopic.php?f=7&t=2522>
  for more information.

# 3.29.0

- Use TCP Fast Open for _syd-tor_(1).
- Add new utility _syd-pds_(1) to run a command with the parent death
  signal set.
- Sync [`ioctl/allow`](https://man.exherbolinux.org/syd.2.html#ioctl/allow),
  and [`ioctl/deny`](https://man.exherbolinux.org/syd.2.html#ioctl/deny)
  defaults with Landlock ABI 5 ioctl device defaults.
- Confine abstract unix socket namespace and signaling with Landlock ABI
  6 and later.
- The command
  [`allow/lock/write`](http://man.exherbolinux.org/syd.2.html#allow/lock/write)
  now includes _ioctl_(2) operations with Landlock ABI 5 and later.
- Add new command
  [`allow/lock/connect`](http://man.exherbolinux.org/syd.2.html#allow/lock/connect)
  to confine _connect_(2) ports using Landlock ABI 4 and later.
- Add new command
  [`allow/lock/bind`](http://man.exherbolinux.org/syd.2.html#allow/lock/bind)
  to confine _bind_(2) ports using Landlock ABI 4 and later.
- Support Landlock ABI v{4..=6}.
- `syd-lock` has been promoted to core tools and is now installed
  regardless of the `utils` feature.
- Compare HMAC for Crypt sandboxing in constant time.
- Compare checksums for binary verification in constant time.
- Remove the unused `pandora` profile.
- Make wordexp proc mount non-fatal to avoid panics within
  presence of SELinux.
- Allow access to `/proc/filesystems`, `/proc/loadavg`, `/proc/stat`
  for the `linux` profile.

# 3.28.5

- Revert allowing _clone3_(2) for the Syd process, as such
  a comprimised Syd process cannot create sub-namespaces,
  and circumvent sandbox policy this way.

# 3.28.4

- Allow _clone3_(2) system call for emulator and aes threads.
- Fix hiding of unsafe filenames for the _getdents64_(2) handler.
- Fix `tmpfile` access for `/tmp` and `/var/tmp` for the `linux` profile.
- Allow access to the `FS_IOC_SETFLAGS` _ioctl_(2) request for
  the `paludis` profile.
- Disallow access to the system console in `landlock` and `linux`
  profiles.
- Harden `/etc` rules of the `linux` profile.
- Allow read access to `/var/lib/sss/mc/{passwd,group}` files for
  `linux` profile. These files are needed for LDAP authentication.
- Log `/proc/self` in place of `/proc/${pid}` as necessary.
- Fix a minor bug with hidden check and `/proc` paths.

# 3.28.3

- Harden `linux` profile using stricter procfs rules utilizing
  `/proc/self`. This means, the sandbox processes are confined
  to access only their own per-process procfs directory.
- The identifier `/proc/self` may now be used in _glob_(3) rules, which
  is dynamically updated to the PID of the sandbox process making the
  system call. This allows for stricter procfs rules by simply using the
  prefix `/proc/self` rather than `/proc/[0-9]*` in _glob_(3) rules.
- Grant access to `/proc/filesystems` file in the `linux` profile.
- Grant access to `/proc/pid/attr` directory in the `linux` profile.
- Move default mask rules to the `linux` profile.
- Filter process name change attempts for the `paludis` profile.
- Allow read and stat access to the files `/proc/pid/m{aps,ounts}` for
  the `linux` profile. These files are masked by the `linux` profile.
- Provide more fields for _syd-stat_(1) using the file `/proc/pid/stat`.

# 3.28.2

- Fix permissions for `/var/tmp` for `landlock` and `linux` profiles.
- Log emulator panics.
- Crypt sandboxing now implies `trace/exit_wait_all:1` so as not
  to leave any impartial encryption processes behind.
- Fix NULL path handling for _utimensat_(2) and _fanotify_mark_(2).
- Fix a concurrency issue with Crypt sandboxing.
- Log failed sandbox queries with the `info` level rather than `warn`.
- Allow file deletion under `/dev/shm` for the `linux` profile.
- Allow the system call _fallocate_(2) for system call handler threads,
  so it can actually be emulated.
- Fix _ftruncate_(2) on encrypted files.
- Align _sysinfo_(2) random memory sizes with powers-of-two.
- Change safe file opener to do a turn of `O_PATH` _open_(2), followed
  by a _statx_(2) and `/proc/self/fd` _open_(2). Previously Syd did a
  single non-blocking _open_(2).

# 3.28.1

- Avoid binary search in _seccomp_(2) filters. Use manually set
  priorities based on _strace_(1) count information.
- Fix a 32-bit compilation issue with the new _sysinfo_(2) handler.
- Fix a compilation issue on aarch64.

# 3.28.0

- Add new tool _syd-info_(1) to print system information.
- Add options `-c` and `-C` to _syd-path_(1) to calculate CRC32 and CRC64
  checksums respectively.
- Add options `-c` and `-C` to _syd-sha_(1) to calculate CRC32 and CRC64
  checksums respectively.
- Add `crc32` and `crc64` to the list of supported checksum algorithms
  for Force sandboxing.
- Change project license from `GPL-3.0-or-later` to `GPL-3.0`.
- Implemented stricter filename validation to deny unsafe or malicious
  filenames based on [Wheeler's Safename
  LSM](https://dwheeler.com/essays/fixing-unix-linux-filenames.html).
- Make `AT_SECURE` setter work regardless of the number of arguments
  passed to exec by the sandbox program. Previously if more arguments
  than a certain threshold were passed to the program the setter would
  fail with `EINVAL` and terminate the exec program.
- Update [Threat
  Model](http://man.exherbolinux.org/syd.7.html#Threat_Model) section in
  _syd(7)_ manual page to mention the sandbox lock and crypt sandboxing.
- Add new utility _syd-bit_(1) to flip random bits in files. We use this
  tool in our test-suite to verify Crypt sandboxing is safe against
  bit-flip attacks due to HMAC-SHA256 authentication.
- Prevent unsetting the `O_APPEND` flag using _fcntl_(2) `F_SETFL`
  command for append-only files. This could be used to clobber
  files which were marked `append` with Syd. Users are strongly
  recommended to update.
- Add back the utility _syd-key_(1) which can be used to generate
  random keys for Crypt sandboxing.
- Add back the utility _syd-aes_(1) which is an AES-CTR encryption and
  decryption utility which uses the Linux Kernel Cryptography API.
- Add back Crypt sandboxing with a revised implementation that allows
  for read-write and concurrent access. Encrypted files now have a magic
  header to easily identify them. Random per-file IVs are now
  prepended to files rather than stored in extended attributes
  allowing for safe backups. Integrity checking is implemented by
  adding a 32-byte HMAC-SHA256 tag which provides authentication and
  safety against bit-flip attacks.
- Add _syd-sh_(1) which is a simple confined shell.
- Add new utility _syd-rnd_(1) to print `AT_RANDOM` bytes in various
  formats.
- _syd-env_(1) has been promoted to a core tool and is now installed
  even without the `utils` feature. We use _syd-env_(1) in our
  test-suite to test environment expansion and command substitution.
- Implement a confined command substitution interface using
  _wordexp_(3). Both syntaxes `$(cmd)` and ``cmd`` are supported.
  Confinement is done using Landlock, namespaces, and seccomp.
- _syd-env_(1) learned the `-e eval-str` argument to perform environment
  expansion and command substitution on the given string using
  _wordexp_(3). _wordexp_(3) child process is executed in a confined
  environment with a timeout of 3 seconds.
- _syd-hex_(1) learned the `-l limit` argument to exit after limit
  number of characters are read and en/decoded.
- Set parent-death signal to `SIGKILL` as part of the confinement
  procedure of _syd-tor_(1). This is a second layer that ensures
  _syd-tor_(1) does not outlive the parent Syd process. The first layer
  is the pid file descriptor of the Syd process that _syd-tor_(1) holds
  and selects on as part of its epoll process.
- Make syd syscall emulator threads handle panics gracefully and deny
  the syscall in progress to avoid sandbox process hangs. Added new
  action `panic` to test this. This action is of little use outside
  of testing.
- Add `all` keyword as a placeholder to disable all path/glob based
  rules, e.g. `deny/all+${HOME}/.ssh/***` may be used to disallow
  all access to user ssh configuration directory. We plan to add
  this rule and more to the `user` profile for added hardening.
- Add new sandboxing type "tmpfile" to confine _open_(2), _openat_(2),
  and _openat2_(2) calls with the `O_TMPFILE` flag. Previously this
  access was checked under "write" sandboxing.
- Add new sandboxing type "attr" to confine changes to file attributes.
  This includes extended attributes. Previously this access was checked
  under "write" sandboxing.
- Add new sandboxing types "Create", "Delete", "Truncate" and "Node"
  sandboxing.  These types confine regular and special file
  creation/deletion respectively. Previously this access was checked
  under "write" sandboxing.
- Add new sandboxing type "net/send" which is to confine sending file
  descriptors over UNIX sockets.
- Harden path canonicalization against symbolic link attacks.
- Paths in rules can now be specified as hex-encoded strings as well.
- Improve startup process utilizing `CLONE_FILES` avoiding a
  _pidfd_getfd_(2) call.
- Harden the `immutable` profile by mounting the `/dev/shm` and `/tmp`
  tmpfs mounts with the `nosymfollow` option.
- Harden the `linux` profile by deny-listing common sensitive /etc
  paths, such as `/etc/crypttab`, and `/etc/shadow`.
- Support `nosymfollow` in
  [bind](http://man.exherbolinux.org/syd.2.html#bind) options.
- The restriction to turn the system calls _sync_(2), and _syncfs_(2)
  into no-operations (no-ops) can now be disabled with the new option
  `trace/allow_unsafe_sync:1`. This is useful in scenarios where sync is
  actually expected to work when sandboxing databases.
- Log and deny attempts to set process name with `PR_SET_NAME`
  _prctl_(2) request. To disable logging use `filter/read+!proc/name`,
  to allow setting process name inside the sandbox use
  `trace/allow_unsafe_prctl:1`.
- Improve efficiency of signal protection for Syd tasks by avoiding
  procfs reads during access check. Hook into _rt_sigqueueinfo_(2), and
  _rt_tgsigqueueinfo_(2) for signal protection. These system calls may
  be used to send signals to the Syd process and thereby interfere with
  the workings of the sandbox so we now prevent this.
- Add new tool _syd-cap_(1) to print information on Linux capabilities.
- Mask the files `/proc/meminfo`, `/proc/loadavg` and `/proc/version`,
  and return random data as return to _sysinfo_(2) system call to
  prevent information leaks.
- Applied various minor fixes to path hiding.
- Start to log access violations of the `stat` type, aka path hiding.
- Honour the environment variable `SYD_PROXY_PORT` to override the
  default value of `proxy/ext/port`.
- Honour the environment variable `SYD_PROXY_HOST` to override the
  default value of `proxy/ext/host`.
- The environment variable to pick the default shell has been
  changed from `SYD_SH` to `SYD_SHELL` for clarity.
- `proxy/ext/addr` option has been renamed to `proxy/ext/host` and
  now it supports resolving hostnames using the system DNS resolver.
  A random IP is selected in case of multiple responses.
- Add new utility _syd-dns_(1) to resolve hostnames to IP addresses
  using the system DNS resolver.
- PID sandboxing is now checked at _ptrace_(2)
  `PTRACE_EVENT_FORK|VFORK|CLONE` boundaries rather than at the entry of
  system calls with _seccomp_(2).
- Improve the efficiency of the pid-limiter of PID sandboxing.
- Improve logging to make it more elaborate and readable.
- Enable `preserve_order` feature of the `serde_json` crate for more readable logs.
- Further harden procfs and devfs with various restrictions. See the
  [Hardened procfs and
  devfs](http://man.exherbolinux.org/syd.7.html#Hardened_procfs_and_devfs)
  section of the _syd_(7) manual page for more information.
- The option `trace/force_cloexec` has been removed.
- Add new option `trace/allow_unsafe_magiclinks` to disable magic
  symbolic link restrictions. By default, Syd does not follow /proc
  magic symbolic links if the path process id does not match the caller
  process id.
- Avoid caching paths with `/proc/$pid` prefix during access check.
- The `:action` part of `force+/path:hashhex:action` may now be omitted
  in force rules in which case the action defaults to `kill`. This
  allows for more readable rules.
- Check both the path `/dev/tty` and the original controlling process
  terminal path for access for _open_(2), _openat_(2), and _openat2_(2)
  handlers. The access check with the more critical action wins.
  Previously only the original path was checked for access.
- Support the _openat2_(2) resolve flags `RESOLVE_BENEATH`,
  `RESOLVE_NO_MAGICLINKS`, `RESOLVE_NO_SYMLINKS`, and `RESOLVE_NO_XDEV`.
  Make flag `RESOLVE_IN_ROOT` return `ENOSYS`. The `RESOLVE_CACHED`
  option remains a no-op.
- Make `AT_SECURE` setter determine the address of stack using the
  less-privileged `/proc/pid/stat` file rather than the
  `/proc/pid/syscall` file whose access requires ptrace rights.
- Make `AT_SECURE` setter more efficient by reading less bytes from
  child memory.

# 3.27.0

- Mask the file `/etc/machine-id` by default.
- The utility `_syd-tick_(1)` has been renamed to `syd-tck` to
  reduce precious user typing time to invoke this utility.
- Enforce `AT_SECURE` in auxiliary vector at process exec. This
  mitigation may be relaxed with the option `trace/allow_unsafe_libc:1`.
- Add new tool _syd-aux_(1) to print auxiliary vector information.
- Remove _pipe2_(2), from the allowed system call list of emulator threads.
- `trace/allow_safe_bind:1` no longer makes Syd skip the IP blocklist
  check at _accept_(2) and _accept4_(2) boundaries.
- Do not check _accept_(2), and _accept4_(2) calls against the connect
  sandboxing acl. These system calls are checked against the IP
  blocklist only.
- Return `EACCES` rather than `ELOOP` error on procfs symlink
  violations. This is in consistency with SELinux and works around
  pipewire's broken flatpak detection logic.
- Add `trace/force_umask:7077` to the `user` profile. This setting has
  been tested for a long time at the CTF server using the CTF profile.
- Ensure _syd-elf_(1) asserts a single path is required as argument
  during option parsing. Previously, more than a single argument would
  pass through but only the first path is parsed either way.
- Pass `CLONE_IO` to syscall emulator micro-threads in addition to
  `CLONE_FILES`, `CLONE_VFORK`, `CLONE_VM`, and `CLONE_SIGHAND`.
- Use shared memory rather than pipes in syscall emulator micro-threads.
- Fix case-insensitivity of the _syd-err_(1) tool.
- Handle POSIX ACLs without reading on parent directory.

# 3.26.1

- Hook _getsockname_(2) and return correct path of UNIX domain sockets.
- Set `name/host` to `localhost` for the `paludis` profile.
- `name/host` and `name/domain` no longer imply `unshare/uts:1`.  This
  is going to allow profiles such as the `paludis` profile who don't
  depend on namespaces to set a constant host-name and domain-name
  without having to check whether namespaces are supported. The values
  of these variables are inconsequential to the functioning of Syd
  otherwise.
- Return `ENOTCONN` rather than `ECONNREFUSED` on _sendto_(2),
  _sendmsg_(2), and _sendmmsg_(2) access violations. No changes in
  _bind_(2) which returns `EADDRNOTAVAIL` and _connect_(2) which returns
  `ECONNREFUSED`. Notably, this fixes python's `test_logging` under syd.
- Do not hook into _accept_(2) and _accept4_(2) system calls if the
  option `trace/allow_safe_bind:1` was set at startup.
- Add new tool _syd-net_(1) to aggregate IP networks.
- Add `block` command to configure an IP blocklist range for Connect
  network sandboxing. The range can also be populated by including
  ipset and netset files.
- _syd-cat_(1) learned the `-m magic` argument to run a magic command at
  init, and `-M magic` argument to run a magic command at exit.
- Implement rule de-duplication and aggregation when parsing ipset files.
- Replace the `ipnetwork` crate with the more versatile `ipnet` crate.
- _syd-cat_(1) now prints Syd configuration to standard output on
  success. JSON output format may be used with the new `-j` option, and
  compact JSON output format may be used with the new `-J` option. To
  display rules of profiles the new `-p <name>` option is required.
- _syd-cat_(1) learned to parse multiple paths supplied as
  command line arguments.
- _syd-cat_(1) learned to parse `.ipset` and `.netset` files.
- Added initial support for parsing `.ipset` and `.netset` files, which
  are common formats for IP blocklists. These files may be included
  with the `include` directive from within Syd configuration.
- File extension is now required in `include` paths. Currently,
  the only supported file extension is `.syd-3`.
- The option `trace/allow_safe_bind:1` causes Syd to skip the access
  check at _accept_(2)/_accept4_(2) boundary.
- Hook into _accept_(2) and _accept4_(2) system calls as part
  of connect network sandboxing.
- Avoid micro-thread creation in _connect_(2), _sendto_(2), and
  _sendmsg_(2) handlers.
- Reduce allocations when allocating buffers for paths.

# 3.26.0

- The `mimalloc` feature which was default to on has been removed. The
  choice of allocator is no longer optional. There're two main reasons
  for this: First, if you offer a secure allocator only optionally,
  no-one enables it and this breaks the purpose. Second, supporting
  two allocators introduces variance into test results and we don't
  want to go down the rabbit hole of debugging/profiling an
  allocator every time. We'd rather have a single, simple, efficient
  allocator that we can forget about. `mimalloc` serves that purpose
  well for now. Hiding this option from the user will also allow us to
  change the allocator without too much hassle should the need arise in
  the future.
- Change `mem/vm_max` default from `4GB` to `0`.
- Allow the extended attribute prefix `user.syd.*` for {get,list}xattr.
  Access to `{set,rm}xattr` is still denied. This allows for practical
  use of immutable extended attributes without privilege.
- Add the _ioctl_(2) request `KDSIGACCEPT` to the deny list.
- Add the _ioctl_(2) requests `FIOGETOWN`, `FIOSETOWN`, `SIOCGPGRP`,
  and `SIOCSPGRP` to the allow list.
- Fix portability issues with `i586`, `ppc64le`, `riscv64`, and `s390x`
  architectures. Syd passes tests on all now. We added them to our CI
  and we will provide binary releases for them.
- Remove Crypt sandboxing and the utilities _syd-aes_(1), _syd-key_(1),
  and _syd-cp_(1). Extensive testing over different architectures showed
  various issues with the implementation: It was overly complicated,
  racy, and could not offer full transparent encryption due to its
  inability to handle read-write fds. Moreover, removing this sandboxing
  type allows us to further confine emulator threads. Together with this
  change we took the ability of emulator threads to call the syscalls
  _accept4_(2), and _splice_(2). These system calls were used by
  encryption only.
- Improve _socketcall_(2) handling using a seccomp-bpf filter for
  selective socket subcall filtering.
- Improve _sendto_(2) handling and fix a TOCTTOU issue on architectures
  which have the multiplexed _socketcall_(2) system call such as x86 and
  powerpc.
- Drop `trace/allow_unsafe_kill:1` and allow _tkill_(2). Note,
  this syscall is still subject to signal protections.
- Apply `trace/force_cloexec:1` to socket file descriptors.
- Hook the _socket_(2) syscall on 64-bit.
- Add `append` command to set files append-only.
- Revert the change which removes the syscall hooks `fallocate`,
  `ftruncate`, and `ftruncate64`. Check these system calls for Write
  sandboxing.
- Fix an IPv6 address parsing issue in _syd-tor_(1).
- Add option `proxy/addr` to specify alternative internal address for
  Proxy sandboxing. This must be an IPv4 or IPv6 address and defaults to
  127.0.0.1.
- `syd-cpu`, `syd-hex` and `syd-size` have been promoted to core tools
  and are now installed regardless of the `utils` feature.
- Add new profile `off` as a shorthand to turn all sandboxing off.
- Add seccomp self-confinement to _syd-elf_(1).
- Add the path `/sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq`
  to the list of unsafe paths and deny access. See [this
  article](https://arxiv.org/pdf/2404.10715) on how this file can be
  (ab)used for dynamic frequency-based fingerprinting attacks.
- The option `trace/allow_unsafe_debug:1` makes Syd execute `syd-tor`
  without confinement using the `-d` argument. This is useful for
  debugging.
- Stop using the `const-syscall` feature of the libseccomp crate which
  caused problems with new system calls such as _cachestat_(2) and
  _fchmodat2_(2).
- To lock the sandbox, `-mlock` and `-ml` may now be used as aliases for
  `-mlock:on`.
- Apply per-thread seccomp sandboxing to Syd threads post-exec. This is
  an initial basic implementation, yet still it vastly reduces the
  attack surface while being zero-cost user-space. More hardening in
  this area will follow to further confine Syd threads. `syd -Epfc` may
  be used to see the export of new seccomp filters.
- Fix a misaligned pointer dereference issue in getdents handler.
- Make _syd-path_(1) write Integrity Force rules for ELF files only.
  Force Sandboxing is not applicable to scripts anymore as we apply
  it only at `PTRACE_EVENT_EXEC` boundary and not at the seccomp hook
  for _execve_(2).
- Remove `-d` option of _syd-path_(1) and set the default action to
  `kill`. `deny` is not a valid action for Force Sandboxing for some time
  as we apply it only at `PTRACE_EVENT_EXEC` boundary and not at the
  seccomp hook for _execve_(2).
- Set the option `trace/allow_unsafe_dumpable:1` for the `paludis` profile.
- Set process dumpable attribute for Syd to false at startup unless
  the new option `trace/allow_unsafe_dumpable:1` is given.
- Clean consecutive slashes from rule patterns before editing the
  respective access control list. Patterns with consecutive slashes are
  a common case of error as such patterns can _never_ match because
  they're matched against canonicalized path names. Cleaning consecutive
  slashes safely turns such patterns into valid patterns.

# 3.25.1

- Fix **a critical bug** with glob matcher when patterns ending with
  triple-star preceded by globs would not match correctly. Notably, this
  allowed editing of dotfiles for the CTF profile where there rule
  `deny/write+/home/*/.*/***` no longer matched dotfiles as it should.
  The `linux` profile included by the `oci`, `paludis`, and `user`
  profiles also has rules which are affected by this bug. The users are
  recommended to update as soon as possible.

# 3.25.0

- Sync _syd-mdwe_(1) with the hardening of memory-related syscalls.
- Kill rather than deny on executable stack violations at _mmap_(2)
  boundary to be consistent with other memory-related seccomp filters.
- Avoid using lazy loading when loading libraries specified via the
  command line.
- Improve SegvGuard logging.
- Kill process on _execve_(2) calls with NULL argv or envp rather than
  denying with EFAULT. This gives the system administrator higher chance
  to notice potentially malicious activity soon and repeated failures
  are going to trigger SegvGuard.
- Memory protection seccomp filters now kill the process rather than
  returning "EACCES". This gives the system administrator higher chance
  to notice potentially malicious activity soon and repeated failures
  are going to trigger SegvGuard.
- Allow _sched_setparam_(2) system call.
- Allow _sched_rr_get_interval_(2) system call on x86.
- Allow _fadvise64_64_(2) system call on x86.
- Handle the _chown32_(2) system call on x86.
- Allow the _waitpid_(2) system call on x86.
- Fix a panic in parsing control messages during _sendmsg_(2) syscall.
  Spotted by trinity during fuzzing.
- Deny the _mincore_(2) system call by default which is typically not
  needed during a normal run and has been successfully (ab)used for
  [page cache attacks](https://arxiv.org/pdf/1901.01161). Should the
  need arise, we may provide an unsafe option to allow this syscall
  in the future.
- Sync syd-oci with
  [youki-0.4](https://github.com/containers/youki/blob/main/CHANGELOG.md#v040---2024-08-23).
- Upgrade `indexmap` crate from `2.2` to `2.4`.
- Replace the `endorphin` crate used in SegvGuard implementation with
  the `expiringmap` crate. `endorphin` has a lot of unsafe code, is hard
  to audit and does not pass miri tests. In comparison, `expiringmap`
  contains no unsafe code.
- Ensure Crypt sandboxing doesn't run out of memory when the sandbox
  process does many sequential writes of small size (take 2, see 3.24.2
  for take 1. Thanks wikky for the help!).
- Allow the _cachestat_(2) system call which is new in Linux>=6.5 and
  works only on file descriptors and hence is not subject to access
  check.

# 3.24.4

- Stop hooking into the system calls _fstat_(2) and _fstatfs_(2).
  In the operation of Syd, read access implies stat, so this is
  consistent with the general behaviour.
- Stop hooking into _chdir_(2) system call until [kernel bug
  218501](https://bugzilla.kernel.org/show_bug.cgi?id=218501) is fixed.
- Stop hooking into _fchdir_(2) system call which is fd-only.

# 3.24.3

- Continue optimizing the thread pool implementation. Track
  busy threads rather than idle threads. Improve efficiency in
  spawning additional threads.
- Stop setting the CPU affinity of the syscall handler threads,
  which contradicts with MT epoll and hurts performance. Drop
  the `core_affinity` crate dependency.

# 3.24.2

- Continue optimizing the thread pool implementation, and
  fix idle thread tracking. We're mostly at our natural limits
  now as the majority of the overhead stems from kernel-space.
- Opening files marked for encryption by Crypt sandboxing for
  read-write now returns `EINVAL` early on _open_(2).
- Use the flag `SECCOMP_ADDFD_FLAG_SEND` to reduce an _ioctl_(2)
  request on system call handlers returning a file descriptor.
- Ensure Crypt sandboxing doesn't run out of memory when the sandbox
  process does many sequential writes of small size.
- Deny `FS_IOC_SETFLAGS` _ioctl_(2) request by default. This ensures,
  above all, that the "immutable" and "append-only" file flags cannot be
  altered inside the sandbox. Read the [Denying FS\_IOC\_SETFLAGS
  ioctl](https://man.exherbolinux.org/syd.7.html#Denying_FS_IOC_SETFLAGS_ioctl)
  section of the _syd_(7) manual page for more information.
- Deny _execve_(2) and _execveat_(2) system calls when one of pathname,
  argv or envp arguments is NULL. This ensures POSIX compatibility and
  denies some simple Return-Oriented Programming (ROP) attacks. See
  [Enhanced execve and execveat Syscall
  Validation](https://man.exherbolinux.org/syd.7.html#Enhanced_execve_and_execveat_Syscall_Validation)
  section of the _syd_(7) manual page for more information.

# 3.24.1

- Track `SIGKILL` as a coredumping signal as part of SegvGuard.
  GrSecurity includes `SIGKILL`, however HardenedBSD does not.
- Read+Write _open_(2) calls are now only checked against the Write
  sandboxing access list for simplicity. Previously these paths were
  checked against both Read and Write access lists.
- The _readlink_(2) and _readlinkat_(2) system calls are no longer
  checked for Stat sandboxing access. For hidden paths, these
  system calls will return what looks like dangling symbolic links
  and this, on its own, is not an information leak.
- Remove symlink restrictions, added as of version 3.12.0 akin to
  `GRKERNSEC_LINK`, and remove the option `trace/allow_unsafe_link:1`.
  This mitigation comes with considerable performance hit without too
  much benefit and e.g. mounting private `tmpfs` over `/tmp`, and
  `/dev/shm` utilizing mount namespaces is a much cheaper and simpler
  way to avoid the same attack vectors which utilize following symlinks
  in world-writable directories. The `immutable` profile already does
  that.
- Raise MSRV from 1.69 to 1.70 to use `PathBuf::as_mut_os_string` for
  efficient path operations.
- Start caching security policy decisions using the `quick_cache` crate.
  There is one cache for path-based decisions and one cache for IP address
  based decisions. The cache sizes can be customized at startup using
  the new options `cache/path/icap:`, `cache/path/wcap:`,
  `cache/addr/icap:`, and `cache/addr/wcap:`. "icap" stands for estimated
  item capacity and is of type `usize`, whereas "wcap" stands for weight
  capacity and is of type `u64`. See the _syd_(2) manual page for the
  default values.
- Add `trace/allow_unsafe_time:1` to the paludis profile.
- Ensure fstatfs calls are compatible with 32-bit land.
- Workaround BTRFS reporting incorrect device IDs, like
  we already do for overlayfs. People should seriously
  stop using these junk.
- User-space SafeSetID filters now return EACCES,
  rather than EPERM for consistency.
- Fix kernel level SafeSetID filters to return EACCES on error.

# 3.24.0

- Change multithreading architecture to use epoll instead of channels
  and drop the `crossbeam-channel` crate dependency.
- Port rsync's glob matcher to rust and start using it instead of the
  `globset` crate.
- Enforce non-executable stack restriction at _mmap_(2) boundary to
  mitigate exploits that change stack to executable by _dlopen_(3)'ing a
  library with executable stack. See
  [CVE-2023-38408](https://www.qualys.com/2023/07/19/cve-2023-38408/rce-openssh-forwarded-ssh-agent.txt)
  for a recent example.
- Fix ELF parser to detect executable stack correctly when the
  `GNU_STACK` ELF header is missing when the loader defaults to an
  executable stack.

# 3.23.18

- Add `trace/allow_unsafe_msgsnd:1` to the `paludis` profile.
- Deny _msgsnd_(2) system call by default to mitigate kernel heap
  spraying attacks. This restriction may be disabled with the option
  `trace/allow_unsafe_msgsnd:1`. See the [Mitigation Against Heap
  Spraying](http://man.exherbolinux.org/syd.7.html#Mitigation_Against_Heap_Spraying)
  section of the _syd_(7) manual page for more information.
- Optimize glob matcher to use much less memory and perform more
  efficiently, by using pre-compiled accelerated DFAs rather than using
  hybrid NFA/DFAs with a global cache.
- Ioctl sandboxing is no longer on by default, the _ioctl_(2) request
  allowlist and denylist is processed regardless. Denylist is now
  applied at kernel-level in the parent seccomp-bpf filter such that Syd
  process is also included in the restriction.
- Short profile names may be stacked together as a single argument to
  _syd_(1) `-p`.
- The `user` profile may be shortly referred as `u`.
- The `silent` profile may be shortly referred as `s`.
- The `noipv6` profile may be shortly referred as `4`.
- The `noipv4` profile may be shortly referred as `6`.
- The `landlock` profile may be shortly referred as `l`.
- The `immutable` profile may be shortly referred as `i`.
- The `container` profile may be shortly referred as `c`.
- Provide more accurate timing information in logs.
- Fix a segmentation fault in `syd::log` module.
- Add new profile `core` to practically allow generation of coredumps.
  This profile may shortly be referred as just `C`.
- Add new profile `debug` to practically allow debuggers inside the
  sandbox. This profile may shortly be referred as just `D`.
- Add new profile `noexe` to practically relax _exec_(3) restrictions
  and disable Syd's usage of _ptrace_(1). This profile may shortly be
  referred as just `X`.
- Add new profile `nomem` to practically relax MDWE restrictions. This
  profile may shortly be referred as just `M`.
- Add new profile `nopie` to practically relax PIE restrictions. This
  profile may shortly be referred as just `P`.
- Harden initial self-reexecution using `/proc/self/maps` with file
  metadata verification instead of using `/proc/self/exe`. The former
  method was vulnerable to filesystem TOCTOU.
- Replace the `perf` feature with the `-q` CLI option for _syd_(1), and
  the environment variable `SYD_QUICK_BOOT` for _syd_(1), and
  _syd-oci_(1). This may be used for faster startup times sacrificing
  one layer of defense against some container breaks.
- oci: Honour environment variables passed by the OCI spec.
- oci: Improve Syd supervisor invocation.
- Handle `SIGPIPE` before configuring the allocator.
- Fix system call origin offset calculation.
- Optimize remote path reader utilizing the `memchr` crate for SIMD search.
- Optimize path canonicalizer utilizing the `memchr` crate for SIMD search.
- Optimize path canonicalizer by reducing allocations.

# 3.23.17

- Add chat banners to README to attract hippies and hipsters.
- Add more TOCTOU and execstack tests.

# 3.23.16

- Add `trace/allow_unsafe_stack:1` to the `paludis` profile.
- Require executables with non-executable stack by default. This
  restriction can be relaxed with the option
  `trace/allow_unsafe_stack:1`.
- _syd-path_(1) learned the option `-eX` to list ELF binaries with
  executable stack under PATH.
- _syd-elf_(1) learned the option `-X` to exit with success if the given
  ELF binary has executable stack.
- _syd-elf_(1) learned the option `-x` to exit with success if the given
  executable is a script.
- ELF parser learned to detect executable stack for ELF binaries.
- Fix `getxattr` and `listxattr` system call hooks to correctly handle
  paths with no read access.
- The sandbox process could previously bypass [Device Sidechannel
  Mitigations](http://man.exherbolinux.org/syd.7.html#Device_Sidechannel_Mitigations),
  by issuing a special _statx_(2) system call with at least one of the
  flags `STATX_TYPE` and `STATX_MODE` unset. This is now fixed.
- Add `trace/allow_unsafe_link:1` to the `paludis` profile.
- Make it possible to disable [symlink
  restrictions](https://man.exherbolinux.org/syd.7.html#Enhanced_Symbolic_Link_Validation)
  with the option `trace/allow_unsafe_link:1`.
- _syd-elf_(1) now resolves symbolic links in path arguments.
- Optimise remote path reader for small paths which are the common case.
- Set CPU affinity of system call handler threads to reduce cache misses.

# 3.23.15

- Add the rule `allow/read,stat+/run/systemd{,/resolve,/resolve/*.conf}`
  to the `linux` profile which is inherited by `oci`, `paludis` and
  `user` profiles. Systemd may link a file from this directory to
  `/etc/resolv.conf` and this file is already allowlisted by the `linux`
  profile.
- Revert the change to put sandbox process into a new process group.
- Fix an infinite loop which happens at startup when sandbox process
  fails early.
- Fix a startup error which happens under `systemd-run --pipe --pty`.

# 3.23.14

- Drop the `env_logger` crate dependency.
- Reduce MSRV from 1.71 to 1.69.
- Replace const-hex crate with hex-conservative crate.
- Root directory must now be an absolute path, relative paths are no
  longer permitted.
- Bind mount target directory must now be an absolute path, relative
  paths are no longer permitted.
- Symlinks in root directory path are no longer permitted. Path
  traversal using `..` is also not permitted.
- Symlinks in bind mount source and target directory paths are
  no longer permitted. Path traversal using `..` is also not
  permitted.
- Fix a symlink TOCTOU in _open_(2) handler when handling magic
  symlinks.

# 3.23.13

- Revert the change made in 3.23.9, to avoid increasing the _nice_(2)
  value and setting cpu, i/o priority to idle for syscall handler
  threads.
- Use the nix crate for _openat2_(2) system call and drop the `openat2`
  crate dependency.
- Set the process group of the sandbox process the foreground process
  group after putting it into a new process group.

# 3.23.12

- Disable the SROP mitigator when SafeSetID is active. This is needed
  because NPTL uses real-time signals to ensure all threads share the
  same UID/GID.
- Put sandbox process into a new process group with `setpgid(0,0)` at
  startup. Unlike _setsid_(2), this call does not detach the process
  from the controlling terminal.
- Do not check dynamic libraries on _mmap_(2) for exec access if
  `trace/allow_unsafe_exec:1` was specified at startup.
- Fix Force sandboxing on _mmap_(2) for dynamic libraries.

# 3.23.11

- Do not change current working directory to `/` for `cmd/exec`
  programs.
- Disable TPE sandboxing for the `paludis` profile.
- Handle EINTR in _stat_(2) and _statfs_(2) system call handlers.
- Skip executable device ID check on overlayfs which reports incorrect
  device IDs on _stat_(2) and this is a [known
  bug](https://github.com/moby/moby/issues/43512).

# 3.23.10

- Harden exec checks at `PTRACE_EVENT_EXEC` stop by verifying the device
  id and inode of the executable and loader.
- Avoid Force Sandboxing TOCTOU in _mmap_(2) by using _pidfd_getfd_(2)
  to get the remote FD and do verification on that.
- Log the path and offset of the memory mapping from which the system
  call is called on access violations with the `src` key.
- Remove the option `segvguard/filter`.
- Allow read and stat access to `/home` directory in `user` profile.
- Fix valid file descriptor check in _mmap_(2), and _mmap2_(2)
  seccomp-bpf rules.

# 3.23.9

- Fix set-id handlers to handle correctly the `-1` argument.
- Add `trace/allow_unsafe_nopie:1` to the `paludis` profile.
- The option `trace/allow_unsupp_exec` has been removed. Unsupported
  executable formats are always denied. Support for new file formats
  can easily be added to the `syd::elf::ExecutableFile` structure.
- Require PIE executables by default. This restriction can be relaxed
  with the option `trace/allow_unsafe_nopie:1`.
- Enumerate ELF shared library dependencies by reading the
  `/proc/$pid/maps` file on `PTRACE_EVENT_EXEC`. Previously we used the
  [lddtree](https://crates.io/crates/lddtree) crate to read the ELF
  dependency tree which was horribly inefficient in comparison.
- Add a new [minimal ELF
  parser](https://gitlab.exherbo.org/sydbox/sydbox/-/raw/main/src/elf.rs)
  to replace the [Goblin](https://crates.io/crates/goblin) crate
  dependency. Goblin was problematic because it broke static-linking,
  and it is somewhat of an overkill for our limited use case.
- Enable *SafeSetID* by default. Replace the option
  `trace/allow_safe_setid` with the options `trace/allow_safe_setuid`
  and `trace/allow_safe_setgid`.
- Turn unsafe set-id system calls into no-ops rather than denying with
  `EPERM` for compatibility with daemons such as postgres which invokes
  _setgroups_(2) before _setuid_(2) and aborts on failure.
- Mitigate
  [SROP](https://llsoftsec.github.io/llsoftsecbook/#sigreturn-oriented-programming)
  by denying access to the system calls _sigreturn_(2), and
  _rt_sigreturn_(2) for _syd_(1), _syd-oci_(1) and _syd-tor_(1).
  Given the lack of signal handlers, these system calls have no
  legit use.
- Add new `kvm` profile which allowlists KVM _ioctl_(2) requests
  without path check.
- Avoid increasing the _nice_(2) value and setting cpu, i/o priority to
  idle for syscall handler threads.
- Add new `tty` profile which allowlists {T,P}TY _ioctl_(2) requests
  without path check. This profile is included by `oci`, `paludis`,
  and `user` profiles.
- Add `TIOCGETD` to the default allowlisted _ioctl_(2) requests set.
  Note, `TIOCSETD` is already in the default denylisted set.
- Refactor common rules in `oci`, `paludis`, and `user` profiles into
  the new profile `linux`. All three profiles inherit from `linux` now.
- Add `allow/net/link+route` to `oci`, `paludis`, and `user` profiles.
- Add `trace/allow_safe_kcapi:true` to the `oci` profile.
- Add `trace/allow_safe_bind:true` to the `oci` profile.
- Harden the _open_(2) handlers using the `RESOLVE_IN_ROOT` flag.
- Return the original file descriptor on `O_PATH` _open_(2) calls with
  encrypted files as argument for Crypt sandboxing.
- Revert the faulty optimization to increase of `AF_ALG` socket buffer
  sizes for Crypt sandboxing which fixes potential **data corruption**
  under memory-constrained environments. Users of Crypt sandboxing are
  highly recommended to update immediately and run _syd-test_(1) to
  confirm the issue is fixed for them before using Syd.
- Relax xattr name restrictions when the Sandbox lock is off for the
  respective sandbox process.
- Return `ENOTSUP` rather than `EACCES` on _setxattr_(2) family calls
  for `user.syd.` name prefix for stealth. Note, unlike the previous
  mitigation, this one may be easily bypassed by comparing results
  with an allowlisted name prefix.
- Return `ENODATA` rather than `EACCES` on _removexattr_(2) family calls
  for `user.syd.` name prefix for stealth.
- Return `ENODATA` rather than `EACCES` on _getxattr_(2) family calls
  for `user.syd.` name prefix for stealth.
- Add `SIGTRAP` to the list of coredumping signals recorded by
  SegvGuard.

# 3.23.8

- Handle `ENOMEM` gracefully during splicing for Crypt sandboxing which
  fixes potential **data corruption** under memory-constrained
  environments. Users of Crypt sandboxing are highly recommended to
  update immediately and run _syd-test_(1) to confirm the issue is fixed
  for them before using Syd.
- Harden SegvGuard for cases when the sandbox process receives a signal
  with default action `Core` but does not generate a coredump which can
  happen under various circumstances.

# 3.23.7

- Fix an issue with file locking of Crypt sandboxing without which concurrent,
  or near-concurrent writes could cause **data corruption** in certain
  scenarios. Users of Crypt sandboxing are highly recommended to update
  immediately and run _syd-test_(1) to confirm the issue is fixed for them
  before using Syd.
- Fix an issue with _stat_(2) handlers on 32-bit systems.

# 3.23.6

- Revert `oci-spec` crate update which broke `syd-oci`. Kudos goes to
  `oci-spec` developers who broke API with a minor version bump...
- Harden
  [SafeSetID](http://man.exherbolinux.org/syd.7.html#Changing_UID/GID_and_groups)
  by dropping `CAP_SETUID` after the first successful UID transition,
  and `CAP_SETGID` after the first successful GID transition.

# 3.23.5

- Fix an issue with the _open_(2) handler mishandling flags argument
  when `O_PATH` is included in flags. This bug was introduced in
  v3.19.0.
- Decouple `CAP_CHOWN` from SafeSetID and add the option
  `trace/allow_unsafe_chown:1` to keep this capability.
- Set-id system calls are now allowed in the sandbox process when
  [SafeSetID](http://man.exherbolinux.org/syd.7.html#Changing_UID/GID_and_groups)
  is not on. In addition, apply a kernel-level _seccomp_(2) filter to
  deny all UID transits to UIDs less than or equal to 11 (aka
  *operator*), and all GID transits to GIDs less than or equal to 14
  (aka *uucp*).
- Make the `cmd/exec` process the leader of a new session.
- Handle the `RENAME_NOREPLACE` flag of _renameat2_(2) system call.
- Enable the `local-dynamic-tls` and `skip-collect-on-exit` features on
  the `mimalloc2-rust` crate.
- Revert back to using `mimalloc` as the default allocator.
- Add `TIOCSETD` request to the list of denylisted _ioctl_(2) requests.
- Add a _ioctl_(2) request allowlist in addition to the denylist. The
  new command
  [`ioctl/allow`](http://man.exherbolinux.org/syd.2.html#ioctl/allow)
  may be used to interact with the allowlist.
- Reduce extra syscall handler thread keep alive timeout from 90 seconds
  to 250 milliseconds. This can be changed by editing `config.rs` at
  compile-time.
- Handle new syscall handler thread spawn errors gracefully.
- Deny the obsolete _sgetmask_(2), and _ssetmask_(2) system calls.
- Deny _pciconfig_iobase_(2), _pciconfig_read_(2), and
  _pciconfig_write_(2) system calls.
- Deny the deprecated _ustat_(2) system call.
- Deny _oldfstat_(2), _oldlstat_(2), and _oldstat_(2) system calls.
- Deny _vmsplice_(2) by default.
- Replace vmsplice/splice usage in child init with read/write.
- Allow TTY, pipe and socket ioctls for the `oci` profile.

# 3.23.4

- Add `default/ioctl:filter` rule to the `paludis` and `user` profiles.
  This is done to cut down the noise generated by Ioctl sandboxing.
  Users may revert to the previous behaviour with
  `-mdefault/ioctl:deny`.

# 3.23.3

- Stop using the insecure and unmaintained `chrono` dependency
  in _syd-oci_(1). Users of _syd-oci_(1) are recommended to read
  [RUSTSEC-2020-0159](https://rustsec.org/advisories/RUSTSEC-2020-0159.html)
  and update immediately.

# 3.23.2

- Improve memory efficiency of the _getdents_(2) system call handler.
- Improve memory efficiency of transparent decryption.

# 3.23.1

- Add the missing _fsync_(2) during last partial block reencrpytion
  process of Crypt sandboxing without which encrypted file appends could
  cause **data corruption** in certain scenarios. Users of Crypt
  sandboxing are highly recommended to update immediately and run
  _syd-test_(1) to confirm the issue is fixed for them before using Syd.
- Improve the efficiency of _syd-aes_(1) by avoiding _select_(2) calls
  on the kernel crypto socket.
- Fix _syd-cp_(1) build on older Rust.
- Handle broken pipe gracefully in _syd-cp_(1).
- Deny reading the timestamp counter in _syd-tor_(1) as part of the
  confinement procedure.

# 3.23.0

- Upgrade `nix` crate from `0.26` to `0.29`.
- Upgrade `procfs` crate from `0.15` to `0.16`.
- Upgrade `bitflags` crate from `1.3` to `2.6`.
- Add new option `time:<offset>` to set clock monotonic and boottime offset in
  Time namespace. Implies `unshare/time:1`.
- Avoid self reexecution when entering namespaces.
- Support time namespace and add the option `unshare/time:1`.  With
  `unshare/time:1`, Syd resets the boot-time clock such that `uptime`
  will report container uptime rather than host uptime.
- Deny access for the creation of namespaces by default, and
  add `trace/allow_unsafe_namespace` to relax the restriction.
  This option accepts a comma-separated list of namespace types
  to allow.
- Add `segvguard/filter` to filter SegvGuard violations by globs.
- Add `default/` options for each sandboxing type and SegvGuard to
  configure default action which typically defaults to Deny or Kill.
- Require absolute paths in globs.
- Extend sandbox actions with `warn`, `stop`, `kill`, and `exit`.
- Remove the option `trace/allow_safe_kvm`.
- Remove the option `trace/allow_unsafe_ioctl`.
- Implement [Ioctl
  sandboxing](https://man.exherbolinux.org/syd.7.html#Ioctl_Sandboxing).
- Enable the [literal
  separator](https://docs.rs/globset/0.4.14/globset/struct.GlobBuilder.html#method.literal_separator)
  option on glob matching. This means a literal `/` is required to
  match a path separator in globs. `*`, and `?` will not match `/`.
- Add
  [`trace/force_cloexec`](http://man.exherbolinux.org/syd.2.html#trace/force_cloexec)
  option to enforce the `O_CLOEXEC` flag on all _open_(2), _openat_(2),
  and _openat2_(2) calls.  Refer to the [Force
  Close-on-Exec](http://man.exherbolinux.org/syd.7.html#Force_Close-on-Exec)
  section of the [_syd_(7)](http://man.exherbolinux.org/syd.7.html)
  manual page for more information.
- Ensure open and socket handlers always opens files with the `O_CLOEXEC` flag.
- Improve _syd-tor_(1)'s socket performance by setting `TCP_NODELAY`,
  `SO_KEEPALIVE`, `TCP_QUICKACK`, and buffer sizes for reduced latency
  and improved data transfer efficiency.
- Ensure no file descriptors leak from the Syd process into the
  _syd-tor_(1) process other than the fds specifically passed.
- Deny syscalls for Memory protection keys by default, and add the
  option
  [`trace/allow_unsafe_pkey`](http://man.exherbolinux.org/syd.2.html#trace/allow_unsafe_pkey)
  to relax the restriction. Refer to the [Restricting Memory Protection
  Keys System
  Calls](http://man.exherbolinux.org/syd.7.html#Restricting_Memory_Protection_Keys_System_Calls)
  section of the [_syd_(7)](http://man.exherbolinux.org/syd.7.html)
  manual page for more information.
- Deny syscalls for Kernel keyring access by default, and add the option
  [`trace/allow_unsafe_keyring`](http://man.exherbolinux.org/syd.2.html#trace/allow_unsafe_keyring)
  to relax the restriction. Refer to the [Kernel Keyring Access
  Restriction](http://man.exherbolinux.org/syd.7.html#Kernel_Keyring_Access_Restriction)
  section of the [_syd_(7)](http://man.exherbolinux.org/syd.7.html)
  manual page for more information.
- Deny syscalls for CPU emulation functionality by default, and add the
  option
  [`trace/allow_unsafe_cpu`](http://man.exherbolinux.org/syd.2.html#trace/allow_unsafe_cpu)
  to relax the restriction. Refer to the [Restricting CPU Emulation
  Syscalls](http://man.exherbolinux.org/syd.7.html#Restricting_CPU_Emulation_System_Calls)
  section of the [_syd_(7)](http://man.exherbolinux.org/syd.7.html)
  manual page for more information.
- Fix a race condition in Crypt sandboxing which causes **data
  corruption** when handling concurrent writes to the same encrypted
  file. Encrypted files affected by the issue have corrupt
  blocks which are in a practically unrecoverable state.
  To reduce the likelihood of the issue reappearing
  in later versions, we have written [82 integration
  tests](https://gitlab.exherbo.org/sydbox/sydbox/-/blob/b332c892c504cef3f1205c386b760d6a8d5e73e9/src/t/test.rs#L337-418)
  to verify correct functioning of Crypt sandboxing. However, users
  should know this is only tested on the architectures "x86\_64",
  "x86", "aarch64", and "armv7". Users running Syd on other architectures
  are _strongly recommended_ to run integration tests using the tool
  [_syd-test_(1)](https://man.exherbolinux.org/syd-test.1.html)
  before any usage of Syd. In addition, package maintainers are
  _strongly recommended_ to install Syd's installable integration
  tests by installing the two utilities "syd-test" and "syd-test-do"
  along with Syd and advise the user to run the tests with a
  post-install message. This ensures Syd is, to the
  extent possible, verified to work on the target system. These
  utilities come with no additional dependencies and their size
  is negligible taken into account the total size.
- Fix an issue with _utimensat_(2) handler on 32-bit systems.
- Fix an issue with _truncate_(2) handler on 32-bit systems.
- _syd-cp_(1) no longer overwrites the destination file by default.
  The command line argument `-f` can be used to force overwrite.
- _syd-cp_(1) learned to read from standard input when only the
  destination file is passed via command line parameters.
- Make the encryption thread more efficient and drop the _write_(2)
  batching workaround which is no longer necessary.
- Block Crypt readers/writers when a subsequent write is in progress
  rather than failing with `EBUSY`.
- _syd-aes_(1), _syd-cp_(1), and _syd-tor_(1) have been promoted to core
  tools, as such they're installed even with the utils feature disabled.
- _syd-aes_(1) learned `-v` argument to periodically print progress
  report on standard error.
- Ensure files configured for encryption are processed even
  if Read & Write sandboxing is off.
- Increase stack size of encryption threads from 4k to 64k.
- Improve lock usage in `ptrace` exit handler.
- Improve lock usage in `stat` system call handlers.
- The setting `trace/force_umask` no longer applies to
  directory creation for convenience.
- Make `trace/force_umask` override POSIX ACLs during
  _umask_(2) calculation.
- Hook into `fchmod` and `fchown` and check for write access.
  In addition `trace/force_umask` is applied for `fchmod`.
- Add `-b <bufsiz>` argument to _syd-tor_(1) to specify pipe
  buffer size.
- Improve Crypt sandboxing and _syd-tor_(1) throughput by setting pipe
  buffer sizes to the maximum value allowed by the system where errors
  are handled gracefully and reverts to the old default `PIPE_BUF` which
  is typically 4096 bytes.
- Apply the noexec-memfd restriction regardless of the state
  of sandboxing.
- Fix a TOCTOU in socketcall (32-bit) handler when handling
  the _socket_(2) subcall.
- Add Netlink support to Network sandboxing which allows
  the user to filter on netlink family. The new
  [`allow/net/link`](https://man.exherbolinux.org/syd.2.html#allow/net/link)
  setting can be used to add/remove items to the allowlist.
- Confine _syd-tor_(1) process further utilizing namespaces
  if available and permitted.
- Fix PID recycling issue of clone which happens during
  re-exec when namespaces are enabled.
- Sanitize environment of the _syd-tor_(1) process.
- Make file descriptor tracking of _syd-tor_(1) more efficient.

# 3.22.0

- Implement [Proxy
  sandboxing](https://man.exherbolinux.org/syd.7.html#Proxy_Sandboxing).
- Add new utility [syd-tor](https://man.exherbolinux.org/syd-tor.1.html)
  which is a secure and efficient SOCKS proxy forwarder.

# 3.21.4

- Stop using the `inline-more` feature of the vendored `endorphin`
  crate.
- Revert panic strategy back to `abort` for release builds now that we
  don't depend on libunwind.
- Stop using the `std` feature of `anyhow` crate which depends
  on std::backtrace and therefore libunwind.
- Avoid mixing mutexes of `stdlib` with `parking_lot` crate.
- Reduce stack size of syscall handler threads from 2M to 128k.
- Avoid leaking AES Key and IV into insecure memory during random Key/IV
  generation.

# 3.21.3

- Change panic strategy from `abort` to `unwind` for release builds.
- Drop the rule `allow/read,stat+/proc/version` from the `user` profile.
- Randomize the contents of the stack for the initial _clone_(2).
- Use a stack size of 4k instead of 8k for micro-threads spawned during
  system call emulation. This can be changed by editing the
  `THREAD_STACK_SIZE` constant in `config.rs` during compilation.
- Use a stack size of 128k instead of 8k for the initial _clone_(2).
  This can be changed by editing the `FCLONE_STACK_SIZE` constant in
  `config.rs` during compilation.
- Deny `mmap` and `mmap2` calls which create executable anonymous
  memory. This restriction can be relaxed with
  `trace/allow_unsafe_memory:1`.
- Drop the `PROC_BTI|PROC_EXEC` workaround in mprotect filter on arm64,
  which makes the filter weaker and is [no longer
  needed](https://sourceware.org/pipermail/libc-alpha/2020-November/119305.html).
- Check libraries dynamically loaded at runtime via _dlopen_(3) for Exec
  access by hooking into _mmap_(2) and _mmap2_(2) system calls.

# 3.21.2

- Document [uninteruptible
  FIFOs](http://man.exherbolinux.org/syd.7.html#Uninterruptible_FIFOs)
  under BUGS section of the _syd_(7) manual page.
- Deny `execve` and `execveat` system calls for the Syd process when the
  sandbox is locked during runtime with a kernel-level seccomp filter.
  This was already done when sandbox is locked at startup and the reason
  to skip is to allow the Syd command `cmd/exec`.
- Use the flag `MFD_NOEXEC_SEAL` when opening memfds for transparent
  decryption for Crypt sandboxing to ensure transparent decryption can
  not be abused to bypass Exec, Force and TPE sandboxing. This flag
  requires Linux-6.3 or newer. On older kernels, a backing directory
  must be specified with `crypt/tmp` for transparent decryption to work.
- Handle the CLI arguments `--help`, `--version`, and `--sh` before
  self re-execution for convenience.
- Strip `MFD_EXEC` and add `MFD_NOEXEC_SEAL` to flags of `memfd_create`.
  This ensures memory file descriptors are not executable and cannot be
  made executable. This requires Linux-6.3 or later. The option
  `trace/allow_unsafe_memfd:1` is provided to relax this restriction.
  This option, when enabled, also allows the `memfd_secret` system call.
  See the [Enhanced Security for Memory File
  Descriptors](http://man.exherbolinux.org/syd.7.html#Enhanced_Security_for_Memory_File_Descriptors)
  section of the _syd_(7) manual page for more information.

# 3.21.1

- Deny `open_by_handle_at` system call. Since we already deny the
  `name_to_handle_at` system call, there is little sense in allowing
  `open_by_handle_at`.
- Start sandboxing `memfd_create` call as part of Read, Write and Exec
  sandboxing. The name argument is prepended with `/memfd:` before
  access check. Use e.g. `deny/read,write,exec+/memfd:*` to deny access
  to memory file descriptors globally.
- Deny `memfd_secret` system call. This system call requires the boot
  option `secretmem.enable=1` and is rarely used. Disabling this system
  call adheres to the goal to be secure by default: Although file I/O is
  not allowed to secret memfds, this still provides an attacker a way to
  execute denylisted code by writing their payload into the memfd and
  mapping it as executable. Should there ever be a legitimate need to
  use this system call inside a Syd sandbox, it is trivial to add a flag
  to allow it on demand.
- Allow read, write and stat access to memfds in the Paludis profile.
- Stop using `mimalloc` as the default allocator. Notably mimalloc
  breaks build with LTO and maintaining the rust crate for mimalloc is
  an additional burden for us.
- Avoid a bug in network system call handlers that caused them to return
  `EBADF` when when `/proc` is mounted with hidepid=2.
- Avoid a bug in fd-only xattr handlers (`fgetxattr`, `fsetxattr`,
  `flistxattr`, and `fremovexattr`) that caused them to return
  `EBADF` when when `/proc` is mounted with hidepid=2.
- Correct file open mode in xattr handlers, use `O_RDONLY` rather than
  `O_PATH` file descriptors as required.
- Fix a DOS in [PID
  sandboxing](http://man.exherbolinux.org/syd.7.html#PID_sandboxing)
  with `pid/kill:1` when Syd shares the process group with the sandbox
  process. To reproduce do `syd -plib -msandbox/pid:on -mpid/kill:1
  syd-fork` when a vulnerable Syd will kill itself along with the
  sandbox process.

# 3.21.0

- Skip applying umask when the parent directory has POSIX ACLs defined.
- Fix symlink handling in `chown` and `chmod` handlers.
- Add `trace/allow_unsafe_env:1` to the `paludis` profile.
- Mask the paths `/dev/kmsg`, `/proc/kmsg` by default and allow read,
  and stat access to these files in `paludis` and `user` profiles. This
  is done to prevent the _dmesg_(1) tool from falling back to the
  denylisted _syslog_(2) system call which will fail with `ENOSYS`.
- Fix an UB in the utime syscall handlers.
- Ensure the virtual stat API returns `EBUSY` for all settings that must
  be set at startup.
- Align stack pointer to 16 bytes for the clone that spawns the sandox
  process to ensure proper memory alignment.
- Fix PID recycling issues of clones in `l*xattr` and network syscall
  handlers.
- Emulate xattr calls on symbolic links in a safe way and drop the
  setting `trace/allow_unsafe_xattr`.
- Fix a FS TOCTOU in network system call handlers when using UNIX domain
  sockets and mitigate another.
- Upgrade the crate `mimalloc2-rust v0.3.1 -> v0.3.2` which updates
  the vendored `mimalloc` from 2.1.4 to 2.1.7.
- Do not hook into `fchmod`, `fchown` `fallocate`, `ftruncate`, and
  `ftruncate64` anymore.  These system calls operate on file descriptors
  only and the file must be opened for write so Write sandboxing checkes
  them on open.
- Add `/proc` to the read allowlist of Lock sandboxing by default.  Syd
  is included in the Landlock sandbox and Syd requires this directory to
  function.
- Add `/dev/null` to the write allowlist of Lock sandboxing by default.
  Syd is included in the Landlock sandbox and Syd requires this file to
  function.
- Allow read access to `/dev/random` in `paludis` and `user`
  profiles. Read access to `/dev/urandom` was already granted.
- Check for invalid socket descriptors early in network calls.
- Drop `trace/allow_safe_setid:1` from the `paludis` profile.
- Fix symbolic link handling in `stat` handlers.
- Fix symbolic link handling in `utimensat` handler.
- Implement sidechannel mitigation by adjusting timestamps on
  sidechannel devices and stripping access and modify flags
  of _fanotify_(7)/_inotify_(7) calls on such devices.
- Enhance security by denying symlink resolution in insecure writable
  directories.
- Avoid double open in `fchdir`, `fchmod`, `fchown`, `fgetxattr`,
  `fsetxattr`, `flistxattr`, `fremovexattr`, `fstatfs`, `fstatfs64`, and
  `truncate` system call handlers.
- Keep `CAP_CHOWN` when SafeSetID is enabled.
- Make SafeSetID continue system calls when there's no change in {u,g}id.
- Start sandboxing `fanotify_mark` and `inotify_add_watch` system calls
  as part of Stat sandboxing.
- Respect the `AT_EMPTY_PATH` flag in the `faccessat2` handler.
- Avoid double open in `getxattr`, `setxattr`, `listxattr`, and
  `removexattr`.
- Deny `ghost`, `panic`, and `reset` commands when Crypt sandboxing is
  on with `EBUSY` for safety.
- Publish [the CTF
  profile](https://gitlab.exherbo.org/sydbox/sydbox/-/raw/main/data/ctf.syd-3)
  for transparency.
- Enable TPE sandboxing for the `user` profile and set trusted GID to
  sandbox process' current effective GID.
- Enable TPE sandboxing for the `paludis` profile.
- Make the ptrace Exec sandboxing check honour filtered paths to avoid
  reporting access violations.
- Implement [Trusted Path Execution (TPE)
  sandboxing](http://man.exherbolinux.org/syd.7.html#TPE_sandboxing).
- Remove the setting `exec/kill`.
- Ensure the `lib` profile sets the sandbox lock to `exec` as `exec` is
  not the default anymore since 3.17.0.
- Make the `ghost` command imply `reset` to ensure no run-away exec
  processes after the invocation of the `ghost` command.
- Avoid panic when unsetting `SYD_` environment variables when the
  environment key or value contains invalid UTF-8.
- Fix undefined behaviour when unsetting `SYD_` environment variables in
  the child by moving the unset logic to the parent right before process
  spawn.

# 3.20.1

- vim: add syntax highlighting for Crypt Sandboxing and Ghost mode.
- Require absolute paths for `crypt/tmp` for safety.
- Fix out of memory error on encrypted file descriptor when writing
  with very small batch sizes.

# 3.20.0

- syd-err can now be used to match errno descriptions as well as names.
- _open_(2) calls with mode read-write was only checked for write
  sandboxing and not read sandboxing. This is now fixed.
- Open a memfd rather than a tmpfile for `open(/dev/syd, O_RDONLY)`.
- syd-err now accepts an errno name glob rather than an errno regex.
- syd-sys now accepts a system call name glob rather than a name regex.
- syd-sys learned `-g` argument to enable Ghost mode prior to probing.
- syd-sys learned `-p` argument to probe the matching system calls.
- Implement [Ghost mode](http://man.exherbolinux.org/syd.7.html#Ghost_mode).
- Close standard input and standard output after we pass the file
  descriptors to the sandbox process.
- Clear all environment variables that start with `SYD_` from the
  environment of the sandbox process, rather than just the specific Syd
  environment variables. This allows the user to safely set an
  environment variable for the Syd process only and can e.g. be used to
  safely set an AES-CTR key with `config/key:${SYD_KEY}` having set the
  environment variable `SYD_KEY` set on Syd invocation.
- Add new utility `syd-key` to generate AES-CTR key and iv using `/dev/random`.
- Add new utility `syd-cp` for efficient file copying using _splice_(2).
- Add new utility `syd-aes` to encrypt/decrypt files akin to openssl-enc.
- Implement [Crypt
  sandboxing](http://man.exherbolinux.org/syd.7.html#Crypt_Sandboxing) for
  transparent file encryption using AES-CTR.
- oci: Configure tracing when log feature is on, and disable properly when off.
- oci: Honour capabilities specified by the container engine.
- oci: Avoid hitting `UNIX_PATH_MAX` on tenant socket paths.
- oci: Remove unnecessary chdir from init and start which fixes:
  [youki#2772](https://github.com/containers/youki/issues/2772).
- Update: `libcgroups v0.3.2 -> v0.3.3`
- Update: `libcontainer v0.3.2 -> v0.3.3`
- Update: `liboci-cli v0.3.2 -> v0.3.3`

# 3.19.0

- Add new utility `syd-poc` to demonstrate proof of concepts for various
  sandbox break vectors.
- Add `trace/allow_unsafe_debug:1` to the `paludis` profile.
- Add `trace/allow_safe_kvm:1` to allow a predefined set of KVM ioctls.
  This is necessary to run _qemu_(1) under sandbox with hardware
  acceleration. Previously this was only possible with the unsafe
  `trace/allow_unsafe_ioctl:1` which allows the whole ioctl request
  space.
- Make `trace/allow_unsafe_debug:1` imply `trace/allow_unsafe_memory:1`
  in addition to `trace/allow_unsafe_exec:1`. This is necessary to make
  _valgrind_(1) work in the sandbox.
- Unify `trace/allow_unsafe_p{erf,trace}` options into
  `trace/allow_unsafe_debug`. This option is going to be a general way
  to allow debugging and tracing tools such as gdb, perf, strace,
  valgrind in the sandbox. The manual page clearly states they should
  only be used in trusted environments as it allows the sandbox process
  to trivially break from the sandbox.
- Re-add `trace/allow_unsafe_perf` command to allow perf inside the sandbox.
- Re-add `trace/allow_unsafe_ptrace` command to allow ptrace inside the sandbox.
- oci: Do not pass-through the capabilities specified by the container
  engine. This is done to adhere the goal to be secure by default. To
  honour the capabilities specified by the container engine, user may
  configure Syd with `trace/allow_unsafe_caps:1`. The recommended way,
  however, is to specify only the minimum needed set of capabilities
  using the various "allow\_unsafe" options such as
  `trace/allow_unsafe_bind:1` to retain `CAP_NET_BIND_SERVICE`,
  `trace/allow_unsafe_socket:1` to retain `CAP_NET_RAW`,
  `trace/allow_unsafe_syslog:1` to retain `CAP_SYSLOG`,
  `trace/allow_unsafe_time:1` to retain `CAP_SYS_TIME`.
- Add `trace/allow_unsafe_open:1` to the `paludis` profile.
- Mitigate _open_(2) `O_PATH` TOCTOU by turning `O_PATH` into `O_RDONLY`
  by default and add `trace/allow_unsafe_open:1` to disable this. See
  [BUGS](http://man.exherbolinux.org/syd.7.html#BUGS) for more
  information.
- `trace/allow_unsafe_tkill` has been renamed to
  `trace/allow_unsafe_kill` which is a **breaking change**.
- `trace/allow_unsafe_adjtime` has been renamed to
  `trace/allow_unsafe_time` which is a **breaking change**.
- Extend ptrace detection mitigator seccomp filter to turn all ptrace
  operations into no-ops rather than just `PTRACE_TRACEME`.
- syd-test and syd-test-do learned `-h` CLI argument to list test cases.
- Drop `CAP_CHOWN` even with SafeSetID. `CAP_SET[UG]ID` is still kept
  for SafeSetID.
- Ensure ptrace is not in ambient and inheritable caps before fork.
- test-do: sort tests by name in help output.
- More than one namespace can be turned on/off using the syntax
  `unshare/type,type...:state` and queried using the syntax
  `unshare/type,type...?` now (e.g.: `unshare/user,pid:1`,
  `unshare/mount,net?`).
- More than one sandboxing type can be turned on/off using the syntax
  `sandbox/type,type...:state` and queried using the syntax
  `sandbox/type,type...?` now (e.g: `sandbox/read,stat,write:on`,
  `sandbox/net,lock:off`). This is simple, intuitive and similar to the
  syntax for specifying multiple glob rules with e.g:
  `allow/read,stat,exec+/usr/***`. See the
  [SHORTCUTS](http://man.exherbolinux.org/syd.2.html#SHORTCUTS) section
  of the [_syd_(2)](http://man.exherbolinux.org/syd.2.html) manual page
  for more information.

# 3.18.13

- Improve the efficiency of post-exec sandboxing checks.
- Rename option `trace/allow_unsupp_binfmt` to `trace/allow_unsupp_exec`
  and extend it to cover direct shared library execution.
- Deny direct shared library execution by default to prevent the typical
  exec sandbox escape with `/lib/ld-linux.so.1
  /deny/listed/binary`.

# 3.8.12

- The unsafe path check was missed in the `getdents` handler which
  caused it to list unsafe paths. This is now fixed.
- Deny unsafe paths with `ENOENT` rather than `EACCESS` in path canonicaliser for stealth.

# 3.8.11

- Extend signal protections to guard against group signaling the Syd process group.

# 3.18.10

- Add `trace/allow_unsafe_tkill:1` to the `paludis` profile.
- Add `trace/allow_unsafe_xattr:1` to the `paludis` profile.
- Turn the deprecated `tkill` system call into a no-op for safety
  against PID-recycling. The option `trace/allow_unsafe_tkill:1` is
  provided to relax this restriction.
- Deny system calls which work with extended attributes on symbolic
  links by default as they are vulnerable to filesystem TOCTOU. The
  option `trace/allow_unsafe_xattr:1` is provided to relax this
  restriction. See "syd-ls xattr" for the list of system calls in this
  set.
- Protect Syd against signals from the same process group.
- Fix an issue with chmod syscall handler wrt. symbolic links.
- Make syscall path handler TOCTOU-free, see #123 for a list of effected
  system calls.
- Fix an issue with fchmodat handler wrt. symbolic links.

# 3.18.9

- syd-oci no longer honours the environment variables `SYD_OCI` and
  `SYD_OCI_PROFILE`. This ensures sandbox status is invariant once
  container is started (unless lock is off, then sandbox can be
  dynamically edited at runtime).
- vim: Highlight 0 and root in the destination field of set{u,g}id as Error.

# 3.18.8

- Set the sticky bit for /dev/shm and /tmp mounts of the `immutable` profile.

# 3.18.7

- Improve the syd-oci manual page.

# 3.18.6

- Fix the list of io-uring system call list. This list incorrectly
  included the system calls of the POSIX async API before. This means
  the system calls `io_cancel`, `io_destroy`, `io_{p,}getevents`,
  `io_setup` and `io_submit` are now allowed.
- Add `trace/allow_unsafe_filename:1` to the `paludis` profile.
- Add `trace/allow_unsafe_filename:1` to allow creation of files with
  control characters in them.
- Change syd-oci init process name from `youki:INIT` to `syd_init` to avoid confusion.
- Fix an issue with opening the container runtime log file that broke docker.

# 3.18.5

- oci: Do not attempt to log to syslog. Logs go to container runtime log file.
- Log correct UID after user switch.
- Do not truncate the log file if it exists.
- oci: Preserve Syd's log file descriptor inside the container so that access
  violations are correctly sent to the container runtime log file.

# 3.18.4

- oci: Honour log path and debug arguments passed by the container engine.
- Prioritise domain names over host names during configuration parsing for syd-oci.

# 3.18.3

- syd-oci incorrectly tried to keep `CAP_SYS_PTRACE` in rootless
  containers, which resulted in permission denied error on container
  start. This is now fixed.

# 3.18.2

- Update features in version output.
- Do not leak the `SYD_LOG_FD` environment variable to the sandbox process.

# 3.18.0

- Add run subcommand to syd-oci, this completes the set of common subcommands.
- Add exec subcommand to syd-oci.
- Add checkpoint subcommand to syd-oci.
- Add update subcommand to syd-oci.
- Add events subcommand to syd-oci.
- Add pause and resume subcommands to syd-oci.
- Add spec subcommand to syd-oci.
- Add list subcommand to syd-oci.
- Add ps subcommand to syd-oci.
- Add features subcommand to syd-oci.
- Syd configuration files now support the `include_profile` clause to
  include a Syd profile into the configuration. See `syd-cat list` for a
  list of profiles.
- New utility syd-oci which is a container runtime for syd based on yaouki.
  Currently the standard set of subcommands create, start, state, delete,
  and kill are implemented.
- Fix an issue with SegvGuard and realtime signals.

# 3.17.5

- Add `PTRACE_ATTACH` to the kill list of sydbox' own process seccomp
  filter so that a compromised syd process can never attach to another
  process.
- Fix the path control character checker to work with the open syscall
  hook which was missed before.

# 3.17.4

- Deny paths with control characters at the time of sandbox access check.
- Mask control characters when logging paths.

# 3.17.3

- Add initial vim syntax highlighting file for Syd profiles.
- Make configuration parsing more robust by rearranging conditionals.
- Fix handling of paths with spaces in include paths.

# 3.17.2

- Attempt to set the soft limit of the maximum number of open file
  descriptors to the corresponding hard limit on startup for
  convenience.

# 3.17.1

- Add mprotect hardening for the syd process as part of the EEC.
  This helps mitigate ret2mprotect in a compromised syd process.

# 3.17.0

- Add `lock:exec` to the paludis profile for `esandbox`.
- The default of "lock" setting has been changed from "exec" to "unset",
  where unset means lock will be set to on automatically by syd unless
  no lock clause was specified upon process execution allowing prior
  sandbox edits by configuration files, profiles and CLI arguments.
- Improve lock efficiency in SegvGuard crash recorder.
- Fix `trace/exit_wait_all:1` and write integration tests to verify.
- Harden the syd process by denying non-native syscalls. This pertains
  to the syd process only: Non-native syscalls are still supported for
  the sandbox process.
- Harden the syd process by applying a kernel level seccomp-bpf filter
  to deny exec system calls after the successful exec of the
  sandbox process. As such a compromised syd process cannot use these
  system calls.
- Add a parent seccomp filter to restrict socket system call
  based on sandbox flags.
- Reduce locking in open handler.

# 3.16.11

- Work on logging to make log messages consistent.
- Fix regression in sandbox locking causing hang.

# 3.16.10

- Avoid polling the seccomp file descriptor, block on receive ioctl instead.
- Do not allow `PROT_READ|PROT_EXEC` in seccomp mprotect hardener.
- Check for a capability before dropping it.
- Print GNU build id in version output.
- syd-elf learned `-b` to print the GNU build id of a file.
- Improve sandbox lock usage in stat handler.
- syd-elf learned `-v` to print the version of a file.
- syd-elf learned `-t` to print the type of a file.
- syd-elf learned `-r <address>` to resolve the name of the symbol at the given address.
- syd-elf learned `-n <pattern>` to list the addresses and names of all symbols in the file whose name match the given pattern.
- syd-elf learned `-l` to list the addresses and names of all symbols in the file.
- syd-elf root parameter `-r` has been renamed to `-R`.

# 3.16.9

- Fix a syntax issue in syd.7 manual page.

# 3.16.8

- Add `trace/allow_unsupp_binfmt:1` to the `paludis` profile.
- Add `trace/allow_unsafe_exec:1` to the `paludis` profile.
- Handle dangling `/proc/pid/exe` gracefully.
- Remove deny mode from Force sandboxing, only warn and kill are supported.
- Remove `trace/allow_unsafe_binfmt`.
- Exclude scripts from Force sandboxing for performance.
- Make the script interpreter parsing API safe against filesystem attacks such as replacing the script with a FIFO.
- Make the ELF parsing API safe against filesystem attacks such as replacing a library with a FIFO.
- Check for dynamic library injection once post-exec rather than twice pre and post-exec.
  The pre-exec check was broken and did not work at all due to
  `/proc/pid/environ` not being populated at the time (size=0).
- Improve musl detection in `ldd` function and remove excessive globbing.
- Avoid opening the file twice in exec handler.
- Fix an issue with `ldd` causing it to return elf-interpreter paths without canonicalization.
  This affects Exec and Force sandboxing.
- Add `trace/allow_safe_setid:1` to the `paludis` profile.
- Implement SafeSetID. Use `trace/allow_safe_setid:1` to enable,
  `setuid+<user-name|uid>:<user-name|uid>`, and
  `setgid+<group-name|gid>:<group-name|gid>` to define allowed transitions.
- Fix a regex issue in sandbox config parser which caused invalid ops to match.
- Return correct error code for non-executable files in exec handler.
- Handle too short files gracefully when determining script interpreter path.

# 3.16.7

- Implement [Path Masking](http://man.exherbolinux.org/syd.7.html#Path_Masking).
- Upgrade from mimalloc-2.1.2 to mimalloc-2.1.4.

# 3.16.6

- Fix parsing multiple control messages in 32-bit sendmsg emulation.
- `syd-test` learned to randomize the order of integration tests. The
  default seed can be overridden with `SYD_TEST_SEED` to replicate a
  previous order.
- Add `trace/allow_safe_kcapi:1` to the `user` profile.
- Add `trace/allow_safe_kcapi:1` to the `paludis` profile.
- Implement `trace/allow_safe_kcapi:1` to allow safe access to kernel
  crypto API without having to specify `trace/allow_unsupp_socket:1`.
- Fix `ALG_SET_IV` operation in sendmsg emulation.
- Return an empty file for `/proc/cmdline` rather than denying access to
  it outright for compatibility.
- Drop the `trace/force_restart:1` workaround to inject `SA_RESTART`
  to sigaction flags.
- Mention elf feature in version output.

# 3.16.5

- Allow socket configuration control ioctl getters
  to make libc functions such as `if_indextoname`,
  `if_nametoindex` etc. work.
- Drop perf ioctls from the default ioctl allowlist.
- Fix handling of LDD errors in exec-TOCTOU-mitigator.

# 3.16.4

- Prevent denylisted dynamic library injection using `LD_{AUDIT,PRELOAD}` on exec.
- Prevent denylisted dynamic library injection using `LD_LIBRARY_PATH` on exec.
- Make the ELF parser optional via the `elf` feature which defaults to on.
  Turning this feature off allows linking syd statically.
- Add `-s` flag to syd-elf to exit with success if the given binary is
  statically linked.
- Add `-d` flag to syd-elf to exit with success if the given binary is
  dynamically linked..
- Add `-6` flag to syd-elf to exit with success if the given binary is 64-bit.
- Add `-3` flag to syd-elf to exit with success if the given binary is 32-bit.

# 3.16.3

- Plug a file descriptor leak during init with `unshare/user:0`.
- Prevent ptracer detection via failing `PTRACE_TRACEME` by turning this
  ptrace operation into a no-op.
- Enable `union` feature on the smallvec crate. When the union feature is
  enabled smallvec will track its state (inline or spilled) without the use of
  an enum tag, reducing the size of the smallvec by one machine word. This means
  that there is potentially no space overhead compared to Vec. Note that
  smallvec can still be larger than Vec if the inline buffer is larger than two
  machine words.
- New command `trace/deny_elf_static:1` to deny execution of statically linked binaries.
- New command `trace/deny_elf_dynamic:1` to deny execution of dynamically linked binaries.
- New command `trace/deny_elf32:1` to deny execution of 32-bit binaries.
- New command `trace/deny_script:1` to disable execution of scripts.
- Deny access to non-{elf,script} interpreter paths (e.g. binfmt with
  wine and java) by default. This may be disabled with `trace/allow_unsupp_binfmt:1`.
- Deny access to relative #! interpreter paths and paths with ".." in
  them by default. This may be disabled with `trace/allow_unsafe_binfmt:1`.
- New tool `syd-elf` which is a barebones `ldd(1)` alternative.
- Do not pipe `esyd info` output to PAGER.
- Utilize `lddtree` crate to check dynamic libraries for Exec and Force Sandboxing.
- Implement SegvGuard utilizing `PTRACE_EVENT_EXIT`. The implementation
  is inspired by and closely resembles that of HardenedBSD.
- Fix signal forwarding on ptrace-stops.
- Add `filter/net/connect+/run/systemd/userdb/io.systemd.DynamicUser` to
  the paludis profile to reduce noise during package builds. Note, this
  only prevents the reporting, access to the socket is still denied.
- Apply a seccomp filter to deny mount(2) system call after bind mounts
  such that a compromised syd process cannot call mount.
- Improve error handling in initial re-execution.
- Apply a seccomp filter to the syd process to deny `PTRACE_SEIZE` operation,
  after the only necessary call to attach to the child for exec-TOCTOU
  mitigation. This ensures a compromised syd process cannot attach to other
  processes in the system.

# 3.16.2

- Drop `CAP_SETPCAP` capability by default.
- Drop `CAP_MKNOD` capability by default.
- Drop `CAP_FOWNER` capability by default.
- Drop `CAP_FSETID` capability by default.
- Drop `CAP_CHOWN` capability by default.
- Drop `CAP_AUDIT_WRITE` capability by default.
- Drop `CAP_DAC_OVERRIDE` capability regardless of unshare/user.
  Previously we only dropped this capability when we're entering
  a user namespace.
- Drop `CAP_KILL` capability by default.
- Handle realtime signals gracefully in wait loop.
- Add `trace/allow_unsafe_exec:1` to disable exec-TOCTOU mitigator.
  This allows `strace -f syd` to work.

# 3.16.1

- Bypass libc and call waitid syscall directly to workaround a glibc bug.
- Drop the mount, umount, umount2 hooks and deny these syscalls.
- Add code name to --version output.
- Prefix default NIS/YP domainname with `syd-` for UTS namespace.
- Use `lexis` crate to auto-generate default hostname for UTS namespace.
- Use `lexis` crate to log human-readable names generated from process IDs in logs.

# 3.16.0

- Execute `cmd/exec` commands in a new process group.
- Send SIGKILL to offending process' process group on pid access violations.
- Implement an effective `execve(2)` TOCTOU mitigator utilizing `PTRACE_EVENT_EXEC`.
- Sync get shebang program function with the kernel implementation.
- Enable `trace/force_restart:1` for the paludis profile.
- Add `trace/force_restart:1` to inject `SA_RESTART` to sigaction flags.

# 3.15.8

- Drop `CAP_NET_BIND_SERVICE` by default and add `trace/allow_unsafe_bind` to retain it.
- Implement `trace/allow_unsafe_syslog` command to allow privileged syslog operations.
- Drop /proc/kmsg from the list of unsafe paths (required to sandbox syslogd).
- Set panic=abort for release builds.
- Make MDWE errors non-fatal.
- Fix allowing FIOASYNC ioctl (fixes nginx under syd).

# 3.15.7

- Fix a regression in wait handler that caused syd not to reap zombies properly.

# 3.15.6

- Use `PIDFD_THREAD` flag to save a `/proc/pid/status` read if supported.
- Avoid a process reusing the exec pid through a recycle from taking over the
  sandbox by signaling the pidfd to ensure the correct process is still alive.
- USE `CLONE_PIDFD` on initial spawn to avoid wait races and to spare a
  `pidfd_open` to get the seccomp notify fd.
- Fix handling of deleted proc magic symlinks.
- The file creation mode set by `trace/force_umask` is now honoured by
  _chmod_(2) family system calls for added hardening.
- Guard against CVE-2019-5736 by copying /proc/self/exe to an anonymous file
  descriptor, sealing it and re-executing it. Note due the syd's procfs
  protections this was not exploitable so this feature acts as an extra layer of
  defense.
- Configure release script to compress release tarball with XZ rather than GunZip.

# 3.15.5

- Fix statx alignment on 32bit (ARMv7 compat).
- Fix syscall allow on sandbox/{mem,pid}:off.
- Handle `64` and `_time64` versions of various system calls (ARMv7 compat).
- Handle deleted paths gracefully when resolving /proc/self/fd symlinks.
- Add `PR_SET_VMA` to the list of allowed prctls.
- ldd: do not set default log level to error and do not disable syslogging.

# 3.15.4

- Teach mount handler to handle fd= in mount options.
- Fix mount handler to correctly handle filesystem type argument.
- landlock: fix opening paths with `O_PATH`.
- Pass the correct pid in `SCM_CREDENTIALS` with sendmsg.
- Fix root: to correctly imply unshare/mount:1 and unshare/pid:1.
- Fix signal protector to correctly handle negative pid arguments.

# 3.15.3

- **Milestone**: Exherbo system set builds under syd-3 with tests on.
- Fix symbolic link handling in mknod and mknodat handlers.
- Drop `trace/allow_unsafe_sendmsg` command.
- Start sandboxing _sendmmsg_(2) calls.
- Start sandboxing _sendmsg_(2) calls.

# 3.15.2

- The utility **syd-lock** has learned to run a command under landlock. The
  old check mode is available using the `-c` parameter.
- `trace/deny_dotdot` now handles chdir, fchdir calls as well as open calls.
- Allow the system calls sgetmask, ssetmask, signal, sigaction, sigpending,
  sigprocmask, sigsuspend and sigreturn (X86 compat).

# 3.15.1

- New utility **syd-mdwe** to run a command under Memory-Deny-Write-Execute protections.
- New utility **syd-load** to load a dynamic library like syd for testing.
- New utility **syd-tick** to measure command runtime in hardware ticks.
- Support include directives in config files.
- Support environment variable and tilde expansion in config files.
- Add hardware ticks in log output if available (x86-64 and arm64).
- Allow `PROC_BTI|PROT_EXEC` for mprotect on arm64.
- Logs of level error are now also sent to syslog similar to warn.
- Add `trace/force_umask` to set an umask at startup and make umask(2) a noop.
- Add `trace/deny_dotdot` to deny  .. in path resolution for open calls.
- Cover many more tty types in proc ttyname function borrowing code from procps-ng.
- Fix proc ttyname function not to choke when spaces are in arg0.
- Deny access to directories `/sys/kernel/{boot_params,debug,irq,slab,tracing}`.
- Deny access to `/sys/kernel/notes` and `/sys/kernel/vmcoreinfo`.
- Replace hex crate with const-hex crate.
- Deny bind calls with EADDRNOTAVAIL rather than EACCES for syd-1 compat.
- Add `PR_{G,S}ET_MDWE` to the list of allowed prctls.
- Add support to load a dynamic library at startup and execute the function `syd_main` in it.
- Remove `deadlock_detection` feature and enable functionality by default.
  The deadlock detector thread is only spawned if `SYD_LOCK` is set at
  startup.
- Support non-multiplexed network calls on X86.
- Check for execute rights before continue in exec handler to make TOCTOU attacks harder.
- Add a hook to the -1 system call and return API number if the lock allows.
- New tool **syd-cpu** to print the number of CPUs.
- Check shebang program for scripts in force sandboxing.
- Check shebang program for scripts in exec sandboxing.
- Fix git versioning issues breaking paludis interaction.
- Fix the capability drop regression which broke `trace/allow_unsafe_caps:1`.
- Deny executable+shared memory by default, can be relaxed with
  `trace/allow_unsafe_memory:true`.
- Deny /proc/version and add an uname hook to wipe the OS version field.
- Add files /proc/cmdline, /proc/config.gz, /proc/dma, /proc/interrupts,
  /proc/iomem, /proc/ioports, /proc/key-users, /proc/keys, /proc/kmsg,
  /proc/modules, /proc/mtrr, /proc/partitions, /proc/softirqs,
  /proc/sysrq-trigger, /proc/vmallocinfo and directories /proc/fs, /proc/irq,
  and /proc/tty to the list of unsafe paths.
- Revert back to denying PID sandboxing violations with `EACCES` rather than `EAGAIN`.
- Optimize seccomp filter with binary tree sorting.
- Deny access to `/proc/kallsyms` and `/proc/kcore`.
- Deny access to `/dev/mem`, `/dev/kmem`, `/dev/port` and `/dev/cpu/*`.
- Prevent `mmap(NULL, MAP_FIXED)` using a parent seccomp filter.
- syd-path learned `-s` to prefix rules with `/dev/syd/`.
- esyd.sh: Add force sandboxing functions.
- List force sandboxing rules in `/dev/syd/stat` output.
- List force sandboxing rules in `/dev/syd` JSON output.
- Add `arm_fadvise64_64` and `arm_sync_file_range` into the set of safe system calls (ARM compat).
- Add `mq_timedsend_time64` system call into the set of safe system calls (ARM compat).
- Add `send` system call into the set of safe system calls (ARM compat).
- Add `recv` system call into the set of safe system calls (ARM compat).
- Set `SIGPIPE` handler to default at startup for helper tools.
- Implement `syd::fork_fast` for x86 and arm.
- New tool **syd-fork** to fork fast in an infinite loop.

# 3.15.0

- Fix error return when the initial exec fails.
- Fix capability drop when run as root.
- New tool **syd-hex** to hex encode/decode input.
- The new command `force/default` may be used to define the default action for Force sandboxing.
- The new command `sandbox/force` may be used to turn Force sandboxing on or off.
- New tool **syd-path** to write Integrity Force rules for binaries under PATH.
- Implement force sandboxing, which is similar to HardenedBSD's integriforce.
  This sandboxing can be used to verify binaries prior to execution.
- New tool **syd-sha** to calculate md5, sha1, sha3-{256,384,512} checksums.
- Extend the list of unsafe environment variables.
- Allow /sys/bus/cpu for read,stat in paludis profile.
- Start sandboxing `fallocate` system call.

# 3.14.5

- Start sandboxing `statfs`, `statfs64`, `fstatfs` and `fstatfs64` system calls.
- Start sandboxing `ftruncate` and `ftruncate64` system calls.
- New `deadlock_detection` feature to spawn a background thread which polls
  for and reports deadlocks using the API provided by parking\_lot crate.
- Enable `hardware-lock-elision` feature of the parking\_lot crate which
  is supposed to give performance benefits on x86.
- Sync paludis and user profiles with hardened procfs to perform added hardening.
- Deny unconditionally sensitive /proc/pid paths such as "maps" and "mem".
- Drop `trace/allow_unsafe_ptrace` command, ptrace is always denied.
- Drop `trace/allow_unsafe_perf` command, perf is always denied.
- Fix a minor bug in open handler wrt. `O_TMPFILE` flag handling.
- Make magic symlink protection unconditional, previously only readlink
  and open handlers were unconditional.
- Deny PID sandboxing violations with `EAGAIN` rather than `EACCES` for compat.

# 3.14.4

- Fix glibc-2.9 compatibility issues wrt. fchmodat2.
- Deny bad/unknown system calls with `ENOSYS` rather than `EACCES`.
- Enable fchmodat2 handler, skip libseccomp unsupported syscalls gracefully.
- Fix lstat on magic proc symbolic links to work correctly.

# 3.14.3

- Make magic symlink protection of readlink and open handlers unconditional.
  This protection will continue to work even if sandboxing is off.
- Make namespace reexec race less likely by opening an `O_PATH` fd to `/proc/self/exe` at startup.
- Do not search for PATH when reexecuting syd.
- Rework mounts and chroot, syd is reexecuted outside chroot so there is
  no more any requirement to have syd in PATH under the new root
  directory. This makes chrooting much more practical with syd.
- Improve cmd, cmdline and cwd logging.
- Change root directory can now be specified as a relative path.
- Fix return code handling wrt. signal termination when syd is executed in a namespace.
- Process bind mounts after syd reexec for safety and flexibility.

# 3.14.2

- Fix the long standing bug caused by UNIX domain sockets' `UNIX_PATH_MAX` length restriction.

# 3.14.1

- **Milestone**: 15 years of sydbox and version π!
- `trace/private_tmp` has been removed, use `bind+tmpfs:/tmp:nodev,nosuid` instead.
- `trace/private_shm` has been removed, use `bind+tmpfs:/dev/shm:nodev,nosuid,noexec` instead.
- `bind` can now be used to mount special filesystems such as `tmpfs`.
- Report target cpu count in -V/--version output.
- Use `PR_MDWE_NO_INHERIT` flag on `trace/allow_unsafe_memory`. This way
  syd process gets the MDWE protections regardless of the setting.
- Print target persona in `-V/--version` output.
- Print target information in `-V/--version` output.
- Add `trace/lock_personality` to lock `personality(2)` changes.
- Add `trace/allow_unsafe_memory:true` to the Paludis profile.
- Add `trace/allow_unsafe_memory` to bypass MDWE protections.
- Enhance security by applying Memory-Deny-Write-Execute (MDWE) protections.
- Fix mknod handlers to return `EPERM` rather than `EACCESS` on invalid file type for compatibility.
- Drop `trace/allow_unsupp_socket:true` from user profile.
- Drop `trace/allow_unsafe_socket:true` from user profile.
- Denylist `sendmsg` and `sendmmsg` system calls which can be used to circumvent
  network sandboxing. Use `trace/allow_unsafe_sendmsg` to relax this
  restriction.
- Stop sandboxing `recvfrom` calls, this is the same behaviour as syd-1.
- Plug a sandbox break where mkdir/mknod calls' `EEXIST` return value may be
  misused to locate hidden paths.
- Add new profile **privileged** which is a shorthand for `trace/allow_unsafe_caps:1`.
- Check for request validity before sending SIGKILL to process for safety against PID reuse.
- Rework /proc/sys permissions in `immutable` profile.
- Make immutable profile stricter by bind mounting `/var/empty` over `/sys/fs/{bpf,ext4,fuse,pstore,selinux,smackfs}`.
- Return the errno `ECONNREFUSED` rather than `EACCES` for connect/sendto calls.
- Correct IPv6 loopback address to ::1/128 from ::1/8 in network aliases.
- Fix an UB in xattr handlers causing bad data to be used in value field.
- Fix mknod handler to properly treat zero file type as regular file.
- Add `trace/allow_unsafe_setid` config to relax setid restrictions.
- Allow relative pathnames in rules as UNIX socket path matches.
- Fix a bug with umask handling in open handler.
- Optimize current working directory handling in canonicalize function.
- Allow the ioctl operation `FS_IOC_GETFLAGS`.
- Open the /dev/syd virtual `O_TMPFILE` under /tmp rather than current directory.
- Refine handling of CAP_SYS_PTRACE.
- Optimize path component handling in canonicalize function.
- **syd-stat** learned to list blocked, ignored, caught and pending signals.
- Avoid restoring signal masks needlessly in the child process.
- Process bind mounts after chroot.
- Improve the ends with dot check in read path.
- Start using mimalloc with the secure feature as the default allocator.
  The feature flag **mimalloc**, which defaults to on, can be used to turn it off.
- Send seccomp denies to kernel log only if log feature is enabled.
- Various small improvements to the path canonicalizer.
- **syd-mem** now correctly reports errors.
- Ensure config parser gracefully handles binary files.
- **syd-cat** now accepts a path argument to validate the given syd profile.
- Fix tests to run under docker, note `--cap-add CAP_SYS_PTRACE` is
  necessary.

# 3.14.0

- Plug a socket leak bringing loopback device up in net namespace.
- Drop sendfd crate dependency.
- Use a pipe pair rather than a socket pair to transfer seccomp fd.
- Set `trace/allow_unsafe_prlimit:true` for Paludis profile.
- Do not drop `CAP_DAC_OVERRIDE` unless entering a user ns.
- Increase thread pool keep alive timeout from 15 seconds to 90 seconds.
- Drop the capability `CAP_SETFCAP` at startup by default.
- Drop the capability `CAP_DAC_READ_SEARCH` at startup by default.
- Drop the capability `CAP_DAC_OVERRIDE` at startup by default.
- Drop broken /dev/syd handling in getdents handler.
- Detect and handle mapped IPv4 addresses for IPv6 addresses.
- Fix a minor bug with rule parsing for rules with a trailing slash.
- Fix a minor bug with rule parsing for allow/denylists.
- Harden the getdents handler by making an initial access check for the dir before entries.
- Rework network sandboxing, fix recvfrom handler to properly check for source address.
- Do not validate the address length argument network system calls as
  they cannot be fully trusted.
- PID sandboxing can now only be turned on at startup for performance reasons.
- Memory sandboxing can now only be turned on at startup for performance reasons.
- Fix **syd-read** from failing on existing entries.
- Do not do a preliminary normalize pass before canonicalize, breaks
  paths with both symlinks and dots in them.
- The tool **syd-norm** has been removed.
- Trim trailing nul-bytes in UNIX domain sockets before canonicalization.
- Harden landlock profile by allowing /proc read-only rather than read-write.
- Canonicalize paths of UNIX domain sockets.

# 3.13.4

- Revert: Harden `private_tmp` and `private_shm` by bindmounting
  /var/empty over /var/tmp. Breaks Paludis.

# 3.13.3

- Fix mkdir handler to return EEXIST on existing dirs rather than
  raising an access violation.
- Mask kernel filesystems in immutable profile.
- Mount private proc before bind mounts to allow mounting over proc entries.
- Continue execution in case a bindmount fails with ENOENT indicating
  source or target does not exist. This way profiles can provide
  limitations without failing on systems that don't have the respective
  files.
- syd will now fail early if `root:` is specified with one of
  `trace/private_shm` or `trace/private_tmp` as these features
  contradict with each other and do not work together.
- Fix chroot failing in mount namespace due to non-recursive bindmount.
- Harden `private_tmp` and `private_shm` by bindmounting /var/empty over /var/tmp.
- Harden `private_tmp` by mounting private /tmp with nodev and nosuid options.
- Harden `private_shm` by mounting private /dev/shm with nodev, nosuid and noexec options.
- Improve symlink loop detection in path canonicalizer.

# 3.13.2

- Various minor performace improvements to path canonicalizer and normalizer.
- Improve syscall handler lookup, avoid string match on each syscall.
- Fix logging not to create json objects if the log level is disabled.

# 3.13.1

- Fix empty path handling in readlink handlers.

# 3.13.0

- Add handlers for `readlink` and `readlinkat` for stat sandboxing.
- Bump MSRV from 1.70 to 1.71.
- Improve /proc/self, /proc/thread-self magic link handling, fixing a
  known sandbox break, see the integration test
  `procself_escape_symlink` for more information.
- Improve handling of the magic /proc/pid/fd symbolic links, return
  ELOOP if the path does not belong to the current process akin to
  `RESOLVE_NO_MAGICLINKS`. Such fds are often misused to break out of
  containers which makes this an important hardening.
- consolidate error logging in json logs.
- pass `RESOLVE_NO_MAGICLINKS` along with `RESOLVE_NO_SYMLINKS` to the
  openat2 calls in open and stat handlers for added security.
- new tool **syd-open** to lookup open flags by number or name.
- improve the efficiency of the stat handler
- improve open handler and fix handling of dir file descriptors in openat{,2}.
- drop noatime from immutable profile mounts to allow unprivileged mount.
- ldd: fix determining syd path on Exherbo.
- Implement `trace/allow_unsafe_perf` command to allow perf inside the sandbox.
- Implement `trace/allow_unsafe_ptrace` command to allow ptrace inside the sandbox.
- Drop `O_PATH` stat sandbox special casing.
- Add setting `trace/allow_unsafe_caps` to skip dropping Linux capabilities on startup.
- Rename `trace/allow_unsafe_socket_families` `trace/allow_unsafe_socket`.
- Rename `trace/allow_unsupported_socket_families` `trace/allow_unsupp_socket`.
- Rename `trace/allow_successful_bind` `trace/allow_safe_bind`.
- Rename `trace/allow_unsafe_environment` `trace/allow_unsafe_env`.
- Improve getdents handler by skipping dot entries from access check.
- Improve proc umask function.
- Add an improved proc tgid get implementation.
- Start using missing mode handlers for path canonicalization.
- Improve the /proc fd check in path canonicalizer.
- Improve the efficiency of `syd::proc::proc_cmdline` function.
- Verify process using seccomp-id-valid after `pidfd_open` calls.
- Drop excessive seccomp-id-valid calls.
- Avoid a needless `faccessat` in path canonicalizer.
- Improve path sandboxing implementation to reduce the number of syscalls.
- Avoid another needless canonicalize in read path function.
- Keep `CAP_FSETID` capability at startup.
- Keep `CAP_FOWNER` capability at startup.
- Keep `CAP_SETPCAP` capability at startup.
- Keep `CAP_MKNOD` capability at startup.
- Keep `CAP_AUDIT_WRITE` capability at startup.

# 3.12.4

- Avoid needless path canonicalization in getdents handler
- Simplify and improve process read directory function

# 3.12.3

- syd-tty no longer prints the tty of the current process when no pid is given.
- Improve process tty function
- syd-ldd: fix issue determining which syd to execute
- Avoid needlessly determining thread group id once per syscall
- Improve process umask function

# 3.12.2

- Drop `CAP_NET_BROADCAST` capability at startup.
- Do not drop the `CAP_DAC_OVERRIDE` and `CAP_DAC_READ_SEARCH` capabilities
  which may be necessary during path resolution.
- Remove needless, special casing **faccessat** calls for **EEXIST**
  check in **mkdir** and **mknod** handlers.
- Refactor path sandboxing to reduce allocations.
- Improve the path canonicalizer performance by allocating the symlink
  loop detection set only when really necessary.
- Add initial manual page for `syd.7`.

# 3.12.0

- Add initial manual page for `syd-cat`.
- Add initial manual page for `syd-env`.
- Add initial manual page for `syd-err`.
- Add initial manual page for `syd-exec`.
- Add initial manual page for `syd-ldd`.
- Add initial manual page for `syd-lock`.
- Add initial manual page for `syd-log`.
- Add initial manual page for `syd-ls`.
- Add initial manual page for `syd-mem`.
- Add initial manual page for `syd-norm`.
- Add initial manual page for `syd-read`.
- Add initial manual page for `syd-run`.
- Add initial manual page for `syd-size`.
- Add initial manual page for `syd-stat`.
- Add initial manual page for `syd-sys`.
- Add initial manual page for `syd-test`.
- Add initial manual page for `syd-tty`.
- `syd-ls` learned the new sets **setid**, **time**, and **uring**.
- New sandbox command `trace/allow_unsafe_uring` to allow io_uring
  interface.
- Improve symlink loop detection in path canonicalization.
- Consolidate boolean parsing in sandbox commands.
- Add initial `syd.1`, `syd.2`, and `syd.5` manual pages.
- Add initial manual page for `syd-chk`.
- New tool `syd-env` to run a command with the environment of a process.
- Helpers now support `-h` to print help.
- Add sample OpenNTPD profile.
- `syd-ls setid` now lists the setid system calls.
- Add `setgroups` and `setgroups32` to `SETID_SYSCALLS`.
- Add [Known Bugs](#known-bugs) section to the readme.
- Extend the parent seccomp filter, denying many unused system calls.
- Turn `chroot` and `pivot_root` into no-ops rather than denying them with
  `EACCES` for compatibility with daemon which change root into an empty
  directory once all path access is done.
- Include `O_PATH` open requests into access check for stat sandboxing.

# 3.11.4

- Fix a regression caused by the recent `AT_EMPTY_PATH` path fix.
  Notably this makes fstating special files such as pipes and sockets
  work correctly again under syd.

# 3.11.3

- Vendor in the caps crate and avoid using **thiserror** which breaks static
  linking.

# 3.11.2

- Do not resolve symbolic links with `AT_EMPTY_PATH` flag in newfstatat
  and statx handlers. Notably, this fixes tar unpacking symbolic links
  pointing outside tar root.
- Improve path exists check in mkdir, mknod handlers by using access
  instead of stat.
- Fix a symlink TOCTOU in symlink and symlinkat handlers.
- Fix a symlink TOCTOU in open handler.
- Do not prevent access to device special files in open handler. Use
  `bind` with `nodev` for a secure alternative.
- Add sample ntpd profile.
- Drop the `is-terminal` crate dependency.
- Fix an issue with stat handler which caused it to fail with a
  permission error when called with `AT_EMPTY_PATH` in a user namespace
  (but _not_ in a pid namespace), with /proc mounted with the option
  `hidepid=2`.
- Mount private procfs with `hidepid=2` for additional hardening.
- Keep capabilities through user namespaces, this makes `unshare/user:1`
  functionally identical to `unshare -U --keep-caps`.
- Use binary system rather than decimal when parsing human-formatted sizes.
- New tool `syd-mem` to calculate the memory usage of a process.
- Do not drop `CAP_SYS_PTRACE` capability on startup as it is necessary
  to call the system calls `process_vm_readv` and `process_vm_writev`.
- Drop `CAP_CHECKPOINT_RESTORE` capability on startup.
- Drop `CAP_IPC_OWNER` capability on startup.
- Drop `CAP_SYS_TTY_CONFIG` capability on startup.
- Start using the `caps` crate to interact with Linux capabilities.
- New tool `syd-stat` to print detailed statistics about a process in JSON.
- unshare: call `PR_SET_KEEPCAPS` after clone.
- Set `trace/allow_unsafe_socket_families:1` **paludis** and **user**
  profiles.

# 3.11.1

- New sandbox command `trace/allow_unsafe_socket_families` to keep the
  `CAP_NET_RAW` capability. Useful to allow `ping` in the sandbox.
- New sandbox command `trace/allow_unsafe_adjtime` to keep the
  `CAP_SYS_TIME` capability and allow the system calls `adjtimex` and
  `clock_adjtime`. This is mostly useful when sandboxing an ntp daemon.
- `-e var=` may be used to pass-through an unsafe environment variable.
- Clear unsafe environment variables, may be disabled with
  `trace/allow_unsafe_environment:1` on startup.
- New tool `syd-run` to run a program inside a syd container.
- `syd-ldd` now uses the `immutable` profile rather than the `container`
  profile.
- Fix `unshare/pid:1` to properly imply `unshare/mount:1`.
- New tool `syd-tty` to print the controlling terminal of the given PID
  or the current process.
- Simplify symlink loop detection in path canonicalizer.
- Fix a panic in bind config code path.
- Do not send logs to syslog for `-x`.
- Parse user profile on `-f` for login shell compat.

# 3.11.0

- `-f` argument has been renamed to `-P`. `-f` is now ignored for login
  shell compatibility.

# 3.10.2

- Fix a regression with user profile parsing for the login shell.

# 3.10.1

- Clean up temporary tmp and shm directories at exit.
- New sandbox command `trace/private_shm` to mount private /dev/shm in
  the new mount namespace.
- Fix a regular expression issue in **syd-err**, and **syd-sys** helpers.

# 3.10.0

- New sandbox command `trace/private_tmp` to mount private /tmp in the
  new mount namespace.
- Add new profile **immutable** to create immutable containers.
- Command line option `-C` has been renamed to `-f`.
- Simplify command line option parsing and avoid double parsing to
  prioritize CLI options when the user profile is parsed.
- `allowlist/` and `denylist/` prefixes on sandbox commands have been
  changed to `allow/` and `deny/` respectively.
- Move auxiliary functionality into separate binaries:
    - syd-cat profile-name
    - syd-chk
    - syd-err number|name-regex
    - syd-exec
    - syd-lock
    - syd-log
    - syd-ls allow|deny|hook|ioctl|prctl
    - syd-norm path
    - syd-read path
    - syd-size size|human-size
    - syd-sys [-a list|native|x86|x86_64|aarch64...] number|name-regex
- The short form `-v` has been renamed to `-V` for consistency.
- Fix default arg0 for the login shell.
- `SYD_SH` now defaults to `/bin/sh` rather than `/bin/bash`.
- The environment variable `SYD_UNSHARE_MOUNT` is no longer honoured.
- The environment variable `SYD_UNSHARE_UTS` is no longer honoured.
- The environment variable `SYD_UNSHARE_IPC` is no longer honoured.
- The environment variable `SYD_UNSHARE_USER` is no longer honoured.
- The environment variable `SYD_UNSHARE_PID` is no longer honoured.
- The environment variable `SYD_UNSHARE_NET` is no longer honoured.
- The environment variable `SYD_UNSHARE_CGROUP` is no longer honoured.
- `--domainname` has been dropped in favour of the new sandbox command
  `name/domain`.
- `--hostname` has been dropped in favour of the new sandbox command
  `name/host`.
- `--unshare-mount` has been dropped in favour of the sandbox command
  `unshare/mount`.
- `--unshare-uts` has been dropped in favour of the sandbox command
  `unshare/uts`.
- `--unshare-ipc` has been dropped in favour of the sandbox command
  `unshare/ipc`.
- `--unshare-user` has been dropped in favour of the sandbox command
  `unshare/user`.
- `--unshare-pid` has been dropped in favour of the sandbox command
  `unshare/pid`.
- `--unshare-net` has been dropped in favour of the sandbox command
  `unshare/net`.
- `--unshare-cgroup` has been dropped in favour of the sandbox command
  `unshare/cgroup`.
- The long version `--config` has been removed, use `-C`.
- The long version `--magic` has been removed, use `-m`.
- The long version `--arg0` has been removed, `-A` has been renamed to
  `-a`.
- The long version `--env` has been removed, `-E` has been renamed to
  `-e`.
- The `--lock` option has been removed, use the sandbox command
  `lock:on` instead.
- The environment variable `SYD_FAKEROOT` is no longer honoured.
- Change `--root` command line option to `root/fake` sandbox command.
- Change `--map-root` command line option to `root/map` sandbox command.
- Implement `root` sandbox command to change root directory before
  starting the process.
- Implement the `bind` sandbox command to recursively bind mount
  directories on startup.
- Upgrade `smallvec` crate from `1.11` to `1.13`.
- Upgrade `env_logger` crate from `0.10` to `0.11`.
- Drop `trace/allow_unsafe_getrandom` command and make `getrandom`
  system call part of read sandboxing. `getrandom` with the flag
  `GRND_RANDOM` is treated equivalent to calling open on
  `/dev/random`, and without this flag is treated as open on
  `/dev/urandom`.
- Drop the setuid/setgid `chmod` restrictions and remove the sandbox
  command `trace/allow_unsafe_chmod`. Since syd always calls
  `PR_SET_NO_NEW_PRIVS` this restriction did not provide any added
  security and caused issues with e.g. sticky bits on directories.
- `-E` uses the lib profile rather than the Paludis profile now.
- Allow comma delimited list for read, write, exec, stat as capability
  in allow/denylists and filters, see [Command Shortcuts](#command-shortcuts)
  for more information.
- Implement initial trace aka "dry run" mode, activated with `-x`.

# 3.9.14

- Build release binaries with the log feature.
- Add `SYD_LOG_FD` environment variable to override log file descriptor.
- Add --domainname option to set NIS/YP domain name in the UTS namespace.
- Add --hostname option to set host name in the UTS namespace.
- Drop the broken `--chroot` option.
- Add command line option -E to set/unset environment variables.
- Implement sandbox command `trace/deny_tsc` to disable reading timestamp
  counter on x86.

# 3.9.13

- Fix regression causing syd not being able to locate the login shell.
- No longer use nightly rust and `-Zbuild-std` when building release binaries.

# 3.9.12

- Drop `allowlist/lock/write+/dev/std{in,err,out}` from landlock and user
  profiles. This caused landlock to fail on user profile when running
  without a TTY.
- Do not respect `HOME` environment variable when figuring out user home
  directory for added security when used as a login shell.
- Fix user profile parsing on `-c` and `-l`.
- Fix regression causing make not to work under syd due to the
  `setresuid` system call getting denied with the wrong errno.
- Use nightly rust and `-Zbuild-std` when building release binaries.

# 3.9.11

- Fix `--chroot` to work with `--unshare-user` correctly again,
  this was broken by the user subnamespace safety restriction.

# 3.9.10

- When `unshare/user` is active, enter into a sub usernamespace
  after setting the `user.max_user_namespaces` to 1 to ensure
  sandbox process can not do further namespace modification.
  This is similar to bubblewrap's `--disable-userns` option.
- Respect the value of `--arg0` when spawning a login shell.
- Respect `HOME` environment variable when figuring out user home
  directory.
- The user profile is parsed early for login shells now such that
  overriding the configuration using command line parameters is
  possible.
- Fix undefined behaviour when forking into the new pid namespace
  with `unshare/pid:1`.

# 3.9.9

- Errors on disabling of coredumps is no longer fatal.
- Drop the experimental init daemon `syd-init`.
- Relax signal protection such that sending signal 0 (ie check for
  existence) to syd threads are permitted.
- Allowlist `/proc/sys/{fs,kernel,vm}` directories recursively for read
  and stat sandboxing in **paludis** and **user** profiles.
- Fix ioctl allowlisting failing on musl builds.
- Fix an issue with allowlisting TTY devices in **paludis** and **user**
  profiles.

# 3.9.8

- syd now registers itself as a child subreaper unless it is already
  pid1. This fixes issues with background processes getting reparented
  to the actual pid1 after which it is going to require ptrace rights
  to read /proc/pid/mem. With this change, syd works fine as a
  regular user with the sysctl `yama.ptrace_scope` set to 1.

# 3.9.7

- Set CPU scheduling priority to idle for syscall handler threads.
- syd no longer sandboxes **ftruncate** as it is impossible
  to call without bypassing `open()`.

# 3.9.6

- syd now by default disable setting process resource limits for
  sandbox process. Moreover syd also disables coredumps for the
  sandbox process. This may be disabled on startup with the sandbox
  command `trace/allow_unsafe_prlimit:1`.
- Set `SIGPIPE` to defalt earlier so `--syscall`, `--errno` etc. can
  benefit from it.

# 3.9.5

- Add new sandbox command `mem/kill` which may be set to true to kill
  offending processes in Memory sandboxing.

# 3.9.4

- Add new sandbox command `pid/kill` which may be set to true to kill
  offending processes in PID sandboxing.
- Remove the background interrupt handler thread which is not necessary
  with the `WAIT_KILLABLE_RECV` flag.
- Optimize pid sandboxing such that it's much more resillient
  against rapid PID starvation.
- Enable `unshare/net:1` in **container** profile. Now that syd
  brings the loopback interface up, this is actually useful.

# 3.9.3

- Drop the interrupt workaround in the bind handler which is no longer
  necessary.
- Do not check target argument of **symlink**, and **symlinkat** system
  calls. This is consistent with the original system calls.
- Fix **fchmodat** and **faccessat** handlers failing on proc fd links.
- Use OwnedFd more widely to ensure no FDs are leaked.

# 3.9.2

- Mention Sandboxing Emacs with syd asciicast in README.
- Preserve child pid information on **reset** sandbox command.
- Fix case insensitive matching for `--syscall` and `--errno`.
- Implement -R, --chroot=root to change root directory.
- Allowlist `/sbin` for **landlock** profile.
- Allowlist `/sbin` for **paludis** profile.

# 3.9.1

- Make open handler handle `/dev/syd` when both the sandbox lock and
  read sandboxing is off.
- Make getdents handler list `/dev/syd` when both the sandbox lock and
  stat sandboxing is off.
- Fix a segfault on musl during reading `/dev/syd`.

# 3.9.0

- New profile **lib**, the LibSyd helper profile, turns all sandboxing off.
  Useful to configure syd in the application using LibSyd.
- Upgrade **regex** crate from `1.9` to `1.10`.
- Upgrade **once\_cell** crate from `1.18` to `1.19`.
- Upgrade **nonempty** crate from `0.8` to `0.9`.
- The `access`, `faccessat`, and `faccessat2` are handled as part of **stat**
  sandboxing now. Previously the type of sandboxing depended on the access flags
  `R_OK`, `W_OK`, and `X_OK`.
- new tool **syd-ldd** which is a safe `ldd(1)` wrapper.
- use **smallvec** crate to efficiently handle path operations.
- use **itoa** crate to efficiently convert integers to paths (take 2).

# 3.8.9

- Return `EACCES` rather than `ENOSYS` on block device access.
- Use **itoa** crate to efficiently convert pids and file descriptors to
  paths.
- Avoid canonicalizing the current working directory on network calls
  with UNIX domain sockets. Fixes `ENAMETOOLONG` error in some cases.
- Optimize prioritization of syscall handler threads such that
  it only runs once on thread start.

# 3.8.8

- Make the **sync** and **syncfs** calls no-op under syd for added safety.
- Make the **paludis** profile stricter by refining access to `/`.

# 3.8.7

- Further restrict the parent syd process by disallowing **ptrace**,
  **chroot**, and **pivot\_root**.
- syd now brings the loopback interface up with `unshare/net:1`.
- Implement the **load** sandbox command.
- Implement the **panic** sandbox command.
- Implement the **reset** sandbox command.
- Remove the is-terminal check from error, warn level logs.

# 3.8.6

- Set i/o priority of system call handler threads to idle.
- Set parent-death signal to SIGKILL in syd process rather than the
  child for added safety and security.
- Drop Linux capabilities in syd process rather than the child for
  added security.
- Fix unicode issue in regex builder for `--error` and `--syscall` options.
- Reduce the default threadpool keepalive timeout from one minute to 15
  seconds so that syd becomes more reliable during pid starvation.
- Apply a seccomp filter to the syd process such that all set*id
  system calls return 0 without doing anything. This is an important
  security hardening.

# 3.8.5

- Enable debugging information in release mode to help with profiling.
- Use optimization level `3` rather than `z` in release mode.
- Use `unwind` rather than `abort` for panics in release mode.

# 3.8.4

- Implement virtual memory usage limiting for memory sandboxing, the
  command `mem/vm_max` may be used to configure the limit which defaults
  to 4GB.
- Exit with eldest process by default, add `trace/exit_wait_all` sandbox
  command to change behaviour.

# 3.8.3

- Optimize smaps lookup by stopping at the point the memory limit is reached.

# 3.8.2

- Implement `syd --parse human-size` to parse human-formatted size
  strings into bytes.
- Implement [Memory Sandboxing](#memory-sandboxing).

# 3.8.1

- Include build host information into `--version` output.
- Ignore `EACCES` and `ESRCH` errors in proc task counter, we already ignore
  `ENOENT` and `EPERM` so this is consistent.
- Slightly optimize the task limit check of PID sandboxing.
- Remove the broken **kill** mode for PID sandboxing and rename **deny** to
  **on**.
- Set system call handler threads' nice value to 19 to help prevent CPU
  starvation.

# 3.8.0

- Add new operator `^` to remove all matching elements from an allowlist,
  denylist or a filter.
- New sandboxing type called [PID sandboxing](#pid-sandboxing) to set a limit on
  the maximum number of tasks. This is best coupled with a pid name space.
- Guard the parent process with a tight seccomp filter when using namespaces.
- Use the `sendfd` crate and a safe `UnixStream` based socketpair implementation
  to send/receive the seccomp notification fd rather than the non-portable
  internal alternative.
- Avoid loading landlock twice on startup when namespaces are at play.
- `--arch <name>` may now be specified with `--syscall num|regex` to lookup the
  system call for the specified architecture rather than the native
  architecture. `--arch list` may be used to print the list of supported
  architectures.
- Denylist `/proc/1/***` for read, stat and write sandboxing in **container**
  and user profiles.

# 3.7.3

- Fix build on musl broken by recent 32-bit compat changes

# 3.7.2

- Write a socketcall hook for 32-bit systems.
- Optimize seccomp request preparation slightly by avoiding an ioctl call per
  request.
- Fix 32-bit build
- Allowlist the system call `mmap2` and `ugetrlimit` system calls.
- Fix an issue determining the syscall handler for non-native architectures
  (e.g. 32bit sandbox process with 64bit syd)

# 3.7.1

- Make the busy-wait in the background monitor thread less heavy by inserting a
  wait after each request reap cycle.
- Optimize pidfd handling.
- Optimize the `syd::fs::FileInformation::from_path` function which is used very
  frequently in path canonicalization.

# 3.7.0

- Increase the threadpool keepalive timeout from 7 seconds to a minute.
  Benchmarks have shown 7 seconds is actually too short and we're overloading
  the threadpool.
- Make the background monitor thread wait on a `Condvar` rather than waking up
  every n seconds and looping through the whole list of requests. The handler
  thread notifies the `Condvar` which wakes up the background monitor thread to
  handle interrupts for blocking system calls (e.g. interrupted open on a FIFO)
- Improve seccomp syscall priorities to better match a typical build process.
- Protect syd process and their threads from signals. Hook `kill`, `tkill`,
  `tgkill`, and `pidfd_open` calls and return **EACCES** in case sandbox process
  tries to send a signal to a process id related to syd.

# 3.6.6

- Avoid waiting for threads in Supervisor::wait avoiding hangs in some cases.

# 3.6.5

- New profile **container** to activate Linux namespaces. This is currently
  equivalent to `--unshare-mount,uts,ipc,user,pid,net,cgroup`.

# 3.6.4

- Exit with 128 plus signal value rather than **EFAULT** when the sandbox
  process is killed by a signal.
- syd process is included into the namespace now so that it has identical
  view of /proc.
- Mount /proc inside the mount namespace as necessary.
- Return proper exit codes on early spawn failures.
- Allowlist the directory `/sys/devices/system/node` recursively for read & stat
  sandboxing in **paludis** profile.

# 3.6.3

- Fix an issue with symbolic loop detection in path canonicalizer and make it
  more robust. **Milestone** Paludis' tests pass under syd now.
- Ensure seccomp sender and receiver socketpair is closed properly which avoids
  hangs when there is an error spawning the sandbox process.

# 3.6.2

- New `landlock` profile to make practical use of LandLock.
- Drop the interrupt workaround for kernel misbehaving with
  `WAIT_KILLABLE_RECV` seccomp flag.
- Stat handler incorrectly returned a directory when the sandbox process stats
  one of the magic symlinks `/proc/self`, `/proc/thread-self`, `/dev/fd`,
  `/dev/stdin`, `/dev/stderr` and `/dev/stdout`. This is now fixed. Notably,
  this makes `ps` work under syd.
- Report running kernel version and landlock status in `--version`
- Add `--landlock` which checks if LandLock ABI v3 is fully supported.

# 3.6.1

- The `-` op on magic commands now removes the most recently added matching item
  rather than all matching items for predictability.
- Fix `esyd disable` subcommand.
- Allowlist /dev/stdin for landlock read/write in user profile. /dev/stdout and stderr
  were already allowed.

# 3.6.0

- Stat sandboxing can no longer be bypassed by attempting to read, write or
  execute a denylisted/hidden path.
- Log messages with process IDs are enriched using `/proc/pid/comm` rather than
  `/proc/pid/cwd` and `/proc/pid/cmdline` when the **log** feature is disabled
  (default). This is much lightweight since it avoids filesystem access.
- Implemented various small usability improvements for `syd-test`.
- Ioctl restrictions was not applied correctly when syd was built with musl.
  This is now fixed.
- New feature `log` to include debug logging into the program. By default
  logs of severity debug and trace are compiled out. This was previously
  dependent on debug build mode.
- `esyd enable`, `enabled`, `enable_path`, `enabled_path`, `disable`,
  `disabled`, `disable_path`, and `disabled_path` now works for read, write and
  stat sandboxing rather than just write sandboxing. use the `_write` suffixed
  versions of the subcommands for write-only.
- `esyd deny`, `deny_path`, `nodeny`, and `nodeny_path` now works for read,
  write and stat sandboxing rather than just write sandboxing, use `esyd
  deny_write`, `nodeny_write` to add/remove from the write-only denylist.
- `esyd allow`, `allow_path`, `disallow` and `disallow_path` now works for read,
  write and stat sandboxing rather than just write sandboxing, use `esyd
  allow_write`, `disallow_write` to add/remove from write-only allowlist.
- Allowlist the directory `/proc/sys/vm` for read & stat sandboxing in
  **paludis** and **user** profiles.
- Allowlist files with CPU information under `/sys/devices/system/cpu`
  for read & stat sandboxing in **paludis** profile.
- Allowlist the directory `/proc/pid/attr` for read & stat sandboxing in
  **paludis** and **user** profiles.
- Reduce the severity of sandbox config change logs from **warn** to **info**.
- `sandbox/stat`, aka Stat Sandboxing, defaults to **on** rather than **off**
  now.
- `sandbox/read`, aka Read Sandboxing, defaults to **on** rather than **off**
  now.
- `sandbox/exec`, aka Exec Sandboxing, defaults to **on** rather than **off**
  now.
- `trace/allow_unsupported_socket_families` defaults to **false** rather than
  **true** now.
- `trace/allow_successful_bind` default to **false** rather than **true** now.
- Mention asciicasts in README.

# 3.5.2

- Fix various issues with /proc handling of stat and open handlers.
- Support Linux-specific statx flags in statx handler.

# 3.5.1

- Make mkdir, mkdirat, mknod and mknodat handlers more resillient to interrupts.
- Make connect handler more resillient to interrupts.

# 3.5.0

- Make expensive tests usable (preparation for `src_test_expensive` on Exherbo).
- Rename **local** alias to **local4**, define the new **local** alias an union
  of **local{4,6}**.
- Rename **any** alias to **any4**, define the new **any** alias as an union of
  **any{4,6}**.
- Rename **loopback** alias to **loopback4**, define the new **loopback** alias
  as an union of **loopback{4,6}**.
- Add **linklocal**, **linklocal4**, and **linklocal6** network aliases.
- Network aliases are now case-insensitive.
- Support Plan9 style network addresses such as `1.1.1.1!80`. This is the format
  we're going to use moving forward. `@` is still supported as a split character
  for backwards compatibility.
- Make bind handler more resillient to interrupts.

# 3.4.3

- Fix **allowlist/net/bind-**, **allowlist/net/connect-**,
  **denylist/net/bind-**, **denylist/net/connect-** sandbox commands to
  correctly remove the address when the port is given as a single port rather
  than a port range.
- Fix a bug with seccomp request tracking of the background syd::m☮☮n thread
  causing spurious signals to be sent to system call handler threads.

# 3.4.2

- Start making binary releases

# 3.4.1

- Replace `threadpool` crate with the `rusty_poll` crate

# 3.4.0

- Teach syd::m☮☮n thread the ability to resize the syscall handler threadpool
  size upon investigating the current active, queued and maximum count of
  the threadpool. This makes syd automatically adapt when there's a sudden
  burst of blocking system calls (e.g. opening a FIFO, or binding a socket)
  and avoid deadlocks. When the burst is gone, syd::m☮☮n kicks in again and
  decreases the pool size back to a normal state. Since the handling is
  completely automatic, the environment variable `SYD_NPROC` to set the size of
  the system call handler thread pool is no longer supported. The value defaults
  to the number of CPUs on startup and is adapted automatically according to the
  needs of the sandbox process.
- Fix various issues with UNIX domain socket handling.
- Honour process umask properly in bind handler.
- Make the bind syscall handler much more resillient to quickly restarting
  interrupted syscalls.
- Improve interrupt handling by spawning a background thread called syd::m☮☮n,
  to reap invalidated seccomp requests and interrupt respective syscall handler
  threads.

# 3.3.4

- Fix a bug in symlink loop handling of path canonicalization and make it more
  efficient.
- Simplify FIFO handling using a thread rather than forking. Credit goes to
  **Johannes Nixdorf** for coming up with the idea and testing a POC.

# 3.3.3

- Fix handling of unix domain socket connections with relative paths.
- Drop the umask lock and support input/output to FIFOs.

# 3.3.2

- Handle the virtual paths **/dev/stdin**, **/dev/stdout**, and **/dev/stderr**
  specially during syscall emulation.
- Fix fgetxattr handler to correctly determine the path to the file descriptor.
- Fix an issue with fgetxattr handler where the handler would erroneously return
  EFAULT on some valid fgetxattr calls.
- Fix an issue emulating newfstatat calls with `AT_EMPTH_PATH` flag.

# 3.3.1

- Fix another bug with ends with dot check in path canonicalizer which
  caused some paths to erroneously return ENOENT rather than EEXIST.
- Fix the ends with dot check in path canonicalizer which caused
  creating/removing directories with a dot in the name fail with EINVAL.
- Improve handling of the special paths `/dev/fd/$fd` and `/proc/$pid/fd/$fd`.
- Improve path canonicalizer by avoiding double stat on symlinks.
- Allow **TIOCSCTTY** ioctl by default.
- Rather than disallowing access to `/dev/tty` with **ENXIO** unconditionally,
  try to determine sandbox process' controlling terminal and use it.
- New command `syd-init` which is a simple init system to run under syd.
- Switch fuzzer to use afl++ rather than libfuzzer
- Document **-c** and **-l** options correctly. Ignore **--login** as well for
  login shell compatibility.
- Add a CTF guide section in the README

# 3.3.0

- `-r` short option of `--root` has been removed for consistency.
- `-l` option is a no-op now rather than being a short option for `--lock` for
  login shell compatibility.
- `-c` short option has been changed to `-C` for **--config**. **-c** causes
  command to be executed under a shell for login shell compatibility

# 3.2.11

- Announce the CTF game in the README.
- Move the system calls **getxattr**, **lgetxattr**, **fgetxattr**,
  **listxattr**, **flistxattr**, and **llistxattr** from read sandboxing to stat
  sandboxing for consistency with **stat** calls.
- Do not replace `/proc/self` with `/proc/pid` on stat with nofollow. This fixes
  `ps` to work under syd above all.

# 3.2.10

- `syd --read` now works with relative paths as well as absolute paths.
- New profile `silent` to silence all access violations.
- Fix a bug with path normalization where double dots at root position were
  erroneously removed resulting in path not found errors during syscall
  handling.

# 3.2.9

- Drop trailing slash from paths before matching.
- Update bpftrace scripts
- Fix /dev/pts glob in `paludis` and `user` profiles.

# 3.2.8

- Disallow access to `/dev/tty` with `ENXIO` as syd cannot safely emulate
  access to the controlling terminal.
- Implement `syd --syscall number|name-regex` to search for syscall numbers and
  names.
- Fix stat handler from erroneously returning ELOOP on symbolic links with a
  trailing slash.
- Fix a bug with symbolic link loop detection in remote path canonicalization.
- Properly exit with EBUSY when seccomp filter cannot be loaded on startup.
- Print libsecc☮mp version, api version and native architecture in `syd --help`
  output.
- Print libsecc☮mp native architecture in `syd --version` output.
- Implement `syd --arch` to print the name of the native libsecc☮mp
  architecture.
- Implement `syd --errno number|name-regex` to search for errno numbers and
  names.

# 3.2.7

- Move esyd.sh from data/ to src/ as another attempt to fix `cargo install`.
- Use openat2 with `RESOLVE_NO_SYMLINKS` when stating in fs::canonicalize
  function removing another potential TOCTOU vector.

# 3.2.6

- Do not call `include_str!` with a relative path which breaks `cargo install`.
  Use cargo build environment variables instead.
- Always deny access violations with EACCES. Previously syd would deny
  silently with ENOENT if the path does not exist. This was a feature to ease
  test/dev cycle in early stages of syd-3 but it creates confusion, so it is now
  removed.

# 3.2.5

- Fix a file descriptor leak in stat handler. Credit goes to **Johannes
  Nixdorf** for identifying the bug.
- Report libsecc☮mp API in `syd --version`
- `syd-test` now lists known failures at the end of the test run.
- Ensure restarted open system calls with `O_EXCL` flags succeed. With this fix
  `git clone` works under syd.
- Fix parsing of LOCAL and LOCAL6 network aliases.

# 3.2.4

- Fix tests

# 3.2.3

- Ensure opening directories in write mode fails with EISDIR in open handler.
- Deny mknod for fifos and block devices with ENOSYS rather than ENOPERM
  correctly signaling the sandbox process the lack of support for named pipes.
- Do not follow symbolic links in chmod handler.
- Preserve `O_CLOEXEC` flag as necessary in the added fd for open system call
  handlers.
- Ensure system call emulators fail with ENOTDIR when fd argument is a regular
  file and the path argument is a dot.
- Avoid updating file access times during remote path canonicalization which may
  break expectations of sandbox processes.
- open handlers now return ENOENT when the path argument is an empty string.
- unlink, unlinkat, rename, renameat, and renameat2 handlers now return EINVAL
  when the last path of the component is a dot.
- Fix a regression in recvfrom remote socket address writing. This caused UDP
  connections, such as DNS to fail under syd.
- Handle task death between seccomp notify poll event receive and seccomp
  request receive gracefully.

# 3.2.2

- Add statistics about the file in reports for path access violations.
- Access violation returns EACCES if file exists and the errno if the file does
  not exist. Previously it would always return ENOENT in the latter case.
- Do not follow symbolic links in mkdir and mkdirat handlers.
- Lift chmod and getrandom restrictions for the paludis profile.
- `trace/allow_unsafe_getrandom` sandbox command may be used to lift getrandom
  restrictions and allow the use of `GRND_RANDOM` flag with getrandom which
  accesses `/dev/random` under the hood.
- `trace/allow_unsafe_chmod` sandbox command may be used to lift chmod
  restrictions and allow the creation of setuid/setgid files.
- Return correct errno on open errors due to remote path canonicalization
  failures.
- System call handlers properly return EBADF on invalid fd arguments now.
- Fix symbolic link handling in open syscall handlers.
- Fix symlink loop detection in remote path canonicalization.
- We issue continue syscall for connection-mode sockets in recvfrom/sendto
  system calls. Since the pointer argument is NULL in these cases we're safe
  from TOCTOU.
- Do not follow symbolic links in rename, renameat, and renameat2 handlers.
- Return correct errno on failures from statx and newfstatat handlers.
- Use original target argument in symlink, symlinkat handlers so that creation
  of relative symbolic links is now possible under syd.
- Honor sandbox process umask in link and linkat system calls.
- Honor sandbox process umask when creating UNIX sockets.
- Honor sandbox process umask in mkdir, mkdirat, mknod, and mknodat syscall handlers.
- Trailing slash handling has been improved across all system call handlers.
- link, and linkat handlers no longer follow symbolic links in newpath as
  mandated by POSIX.
- linkat now honours `AT_SYMLINK_FOLLOW` correctly when following symlinks.
- link no longer follows symbolic links on its first argument as it should.
- open, and openat with `O_CREAT` now properly returns ENOENT on paths ending
  with a trailing slash.
- Handle mkdir, mkdirat, rmdir, and unlinkat correctly and return EINVAL when
  the last component is a dot.
- Fix a path canonicalization bug to follow symbolic links in the last component
  in case the component ends with a slash, ie if it has to be a directory.
- Simplify stat handling.
- Various fixes for xattr related system call handlers, above all handle value
  argument being NULL gracefully.
- Avoid resolving target path in **symlink** and **symlinkat** emulators.

# 3.2.1

- Fix handling of `lchown{,32}` emulators where we mistakenly followed symbolic
  links before.
- Use use a fd with `O_PATH+RESOLVE_NO_SYMLINKS` during syscall emulation for
  safety against symlink attacks, we hard require Linux-5.6 or newer with this.
- Sandbox **ftruncate**, **fgetxattr** and **lgetxattr**.
- Call renameat2 directly as a syscall as musl libc is lacking this function at
  the moment and their usage breaks musl builds.

# 3.2.0

- Numerous minor fixes to path normalization and canonicalization.
- Emulate all sandboxing calls but **exec**, and **chdir**.
- Handle symbolic links and the `AT_SYMLINK_NOFOLLOW` flag correctly.
- Handle empty paths and the `AT_EMPTY_PATH` flag correctly in system calls.
- `trace/allow_successful_bind` is now fixed to correctly allow successful bind
  calls.
- syd now emulates all the respective system calls for network sandboxing
  **making network sandboxing completely TOCTOU-free.**
- syd no longer allows the opening of existing device special files or named pipes.
- syd no longer allows the creation of device special files or named pipes.

# 3.1.11

- Fix an issue with network address filtering causing some filters to match
  regardless of their port restrictions.
- Fix an issue with network address matching causing some rules to match
  regardless of their port restrictions.

# 3.1.10

- Add sample user configuration file under `data/user.syd-3`.
- Use `/etc/user.syd-3` rather than `/etc/rc.syd-3` which is more consistent.
- syd now properly spawns the underlying shell as a login shell when syd
  itself is invoked as a login shell.
- Add sandbox commands **unshare/{mount,uts,ipc,user,pid,net,cgroup}** which are
  equivalent to the command line options
  `--unshare-{mount,uts,ipc,user,pid,net,cgroup}`. In addition they may be
  queried using the stat interface during runtime, e.g. `test -e
  /dev/syd/unshare/user?'
- Implement `trace/allow_unsafe_{io,pr}ctl` sandbox commands which may be
  used to lift the restrictions on the respective system calls.
- The function `syd::proc::proc_cmdline` now trims overly long command lines.
- Simplify capabilities handling. Drop `CAP_BPF`.

# 3.1.9

- The lock is honoured during initial configuration updates so e.g.
  setting the sandbox lock in the file `/etc/rc.syd-3` will prevent
  `~/.user.syd-3` from loading. This is useful to enforce site-wide
  configuration.
- **user** profile now parser `/etc/rc.syd-3` before `~/.user.syd-3`.
- syd now honours the environment variables
  `SYD_UNSHARE_{MOUNT,UTS,IPC,USER,PID,NET,CGROUP}` to create namespaces.
- You may now use syd as your login shell by adding it to `/etc/shells`. The
  actual shell to execute under syd defaults to `/bin/bash` and can be
  changed on runtime via `SYD_SHELL` environment variable or during compile time
  by changing the variable `SYD_SH` in `src/config.rs`.
- Fix a bug with path normalization to handle double dots at root position
  correctly.
- The set-id family calls are now no-ops under syd.
- The `/dev/syd` may be read to get syd state in JSON in case sandbox is
  unlocked.
- Better ZSH compatibility for the `data/esyd.sh` script which is also available
  via `esyd --sh`.

# 3.1.8

- Fix linkat, renameat, and renameat2 system call handlers' argument handling.
- Fix dropping of capabilities with `--map-root`.
- Listing `/dev` now lists `/dev/syd` in case the sandbox lock is off.
- Simplify handling of the special paths `/proc/self` and `/dev/fd`.
- syd now properly returns `ENAMETOOLONG` for too long paths.
- Ensure the validity of the sandbox process is checked using
  `SECCOMP_IOCTL_NOTIF_ID_VALID` after every interaction with the sandbox
  process memory.
- syd now allows **ioctl** requests for **PTY** handling.
- syd now properly closes the seccomp notify file descriptor after poll
  errors.
- syd now sets the **no\_new\_privs** attribute for the syd process as
  well as the sandbox process. Previously we only set this in the child process.
- Fix a bug in path canonicalization function preventing an infinite loop,
  when following certain symbolic links.

# 3.1.7

- Vendor in the caps crate and avoid using **thiserror** which breaks static
  linking.

# 3.1.6

- Stop using the **thiserror** crate which breaks static linking.

# 3.1.5

- Stop using the `derive` feature of the **serde** crate which breaks static
  linking.

# 3.1.4

- Allow the system calls **setgid**, **setgriups**, **setregid**, **setresgid**,
  **setresuid**, **setreuid**, **setuid** inside the sandbox. Since we drop the
  capabilities `CAP_SETUID` and `CAP_SETGID` on startup this is safe.
- Vendor in the landlock create, use bitflags rather than enumflags2 which
  depends on emumflags2\_derive crate and that used to break both static linking
  and address sanitizer.
- Reading from files under `/dev/syd` succeeds with the lock off. This is to
  provide consistency with the stat interface. The open system call handler just
  opens `/dev/null` instead under the hood.
- Handle pipes under `/proc/pid/task/fd` directories correctly.
- `syd-test` now honours the **SYD\_TEST\_VALGRIND** environment variable to run
  syd under valgrind during integration tests.
- syd now logs the current user id with the log messages.
- The stack size of the syd execve child has been increased from 4k to 128k.
- Block **getrandom** calls with **GRND\_RANDOM** flag. Sandbox processes are
  not allowed to access **/dev/random**. Access to **/dev/urandom** is fine.
- Fix environment clearing code which fixes the broken functionality of
  `SYD_NO_SYSLOG` and `SYD_NO_CROSS_MEMORY_ATTACH` environment variables.
- The **stat** system call handler now properly handles symbolic links.
- **paludis** and **user** profiles allow access to files `/proc/version` and
  `/proc/pid/map`.
- Fix and document **ioctl**, **prctl** restrictions.
- syd now writes "deny" to `/proc/pid/setgroups` before writing the `gid_map`
  file. This way `setgroups(2)` is permanently disabled in user namespace and
  writing to the gid map file can succeed without having the `CAP_SETGID`
  capability.

# 3.1.3

- syd restricts prctl usage with a list of allowlisted prctls. This prevents
  potentially dangerous prctls such as **PR_SET_MM** which can create
  self-modifying executables. The list of allowlisted prctls can be listed using
  `syd --list prctl`.
- syd restricts ioctl usage with a list of allowlisted ioctls. This prevents
  sandbox escapes such as utilizing **TIOCSTI** to write to the controlling
  terminal. The list of allowlisted ioctls can be listed using `syd --list
  ioctl`.
- Use the errno **EACCES** rather than **EPERM** on access violations.
- **paludis** profile disables read access to `/dev/random`. stat access to this
  file is granted. Read access to `/dev/urandom` works too.

# 3.1.2

- The stat system call handler now handles deleted files correctly and fstats on
  the fd rathet than the dangling /proc symlink
- The stat system call handler now handles special files such as sockets or poll
  file descriptors correctly and fstats on the fd rather than the dangling
  /proc symbolic link.
- **paludis** and **user** profiles allow read/stat access to `/proc/stat` now
  so that `ps` works correctly in the sandbox.
- Add `--sh` option which makes syd drop a shell script to standard output
  which defines **esyd** the sandbbox helper.

# 3.1.1

- CGroups support has been dropped, use other means to create CGroups and then
  spawn syd inside.
- The *paludis* and *user* profiles now allow read/stat access to
  the files `/proc/sys/kernel/osrelease` and `/proc/uptime`.
- Fix a panic trying to log paths with non UTF-8 pathnames.

# 3.1.0

- The **stat** system call emulator no longer fails to fstat on pipes.
  The solution is **TOCTOU-free**, when we hit on a pipe fd, we get the
  file descriptor, fstat it and close it, then return the stat buffer.
- Add support for CGroups via `--limit-{cpu,io,mem,pid}`. The command-line
  arguments have conservative defaults. RTFM for more information.
- Disallow the system calls **bpf**, **ioperm**, **iopl**, **setgid**,
  **setgroups**, **setregid**, **setresgid**, **setresuid**, setreuid**, and
  **vhangup** inside the sandbox to improve security.
- Improve architecture-dependent code, improve support for ARM and S390.
- Edit **paludis** and **user** profiles to have a "deny-by-default and
  allowlist known goods" strategy for the directories `/dev` and `/proc`. This
  brings added safety as it adds read restrictions and hides many sensitive
  paths such as `/dev/kmem` or `/proc/pid/mem`.
- The **memfd_secret** system call is now allowed in the sandbox.
- The **act** and **syslog** system calls are no longer allowed in the sandbox.
- syd drops some capabilities on startup which provides added safety to the
  sandbox. The list of dropped capabilities are listed under
  [Security](#security).
- Implement **--map-root** command line flag to map current user to root in the
  sandbox. This implies **--unshare-user**.
- Fix the prevention of  **setuid**/**setgid** files to be created in the
  sandbox.

# 3.0.16

- syd now allows the system calls **setdomainname**, **sethostname**,
  **syslog**, and **signalfd4** system calls inside the sandbox.
- The **stat** family system calls are no fully emulated and do not suffer from
  **TOCTOU** issues.
- syd no longer allows the `TIOCSTI` **ioctl** call which can be used to
  write to the controlling terminal for added security.
- When syd is invoked with `--unshare-user` option to create a new user
  namespace, the creation of new user namespaces inside the sandbox is no longer
  allowed for added security.
- syd now allows the system calls **pidfd\_open** and **unshare**.
- syd no longer allows the system calls **mbind**, **migrate\_pages**,
  **move\_pages**, **perf\_event\_open**, **set\_mempolicy**, and
  **userfaultfd** inside the sandbox for added security.
- syd no longer allows setuid/setgid files to be created inside the sandbox.
- **fchmod**, and **fchown** system calls are now sandboxed.

# 3.0.15

- Turn on the [empty
  alternates](https://docs.rs/globset/latest/globset/struct.GlobBuilder.html#method.empty_alternates)
  building Globs such that `foo{,txt}` in a pattern will match both `foo` and
  `foo.txt`.
- Take advantage of **globset** crate's ability to match a set of patterns at
  once. This way regardless of how many rules are present in a glob pattern
  list, such as allowlist/read, denylist/stat, syd does a single pattern
  match during access control. This increase performance considerably,
  especially for very long rulesets.
- replace **glob-match** crate with **globset** crate. **globset** can work
  directly on `Path`s and requires no `String` conversion.
- Use `Path`/`PathBuf` rather than `&str`/`String` in many places where we
  handle path names. This ensures path names with invalid UTF-8 in their names
  are handled correctly.

# 3.0.14

- syd now uses Landlock ABI version 3 rather than version 1. A Linux kernel
  running version 6.2 or newer is required to get the most out of it. However
  older versions also work quite well. See [this
  table](https://man.archlinux.org/man/landlock.7.en#VERSIONS) for an overview
  on Landlock features and the corresponding kernel version when they were
  implemented.

# 3.0.13

- **esyd check** now utilizes `syd --check` rather than stating the file
  `/dev/syd`. This way it can still detect if the process is running under
  syd despite the sandbox lock being on.
- **esyd exec** subcommand has been fixed.
- The **user** profile added `/dev/tty` to the list of read-write allowed paths
  for LandLock sandboxing.
- The **user** profile now allows read access to **/var/log/journal** for
  systemd journal access.
- **esyd dump** subcommand now forwards it command line arguments and pipes its
  output to **jq** if it's available.
- **Security**: Start emulating **creat** system call which prevents the
  `TOCTOU` scenario where an attacker can create a denylisted file by
  editing the dereferenced pointer argument after the access control but
  before the system call actually happens. We have an integration test,
  called **ptrmod_toctou_creat** which confirms the fix.
- The **esyd** helper saw some fixes, fixing `deny*` subcommands.

# 3.0.12

- syd now logs sandbox command attempts so as to better couple with **esyd**.
- Many improvements, fixes and documentation for the **esyd** helper.

# 3.0.11

- Added new network aliases `ANY` and `ANY6` which match the whole Ipv4 and Ipv6
  address spaces respectively.
- **Security**: Add `NULL` guards to all system call hooks which prevents
  potential crashes if one of the pointer arguments is 0, one of which was
  discovered by trinity on the getdents handler here:
  https://builds.sr.ht/~alip/job/1077263
- **Security**: Fix a crash in getdents handler discovered by trinity fuzzer in
  this build: https://builds.sr.ht/~alip/job/1077263
- Support compatible system call ABIs as necessary, e.g. on `x86-64`, we now
  support `x86`, and `x32` ABIs, on `aarch64` we support `arm` too etc. With
  this out of the way, the default bad architecture action has been changed to
  "kill process".
- Added helper script `data/esyd.bash` which when sourced into a bash
  environment, defines the convenience function `esyd` to interact with syd
  sandbox commands.
- Stat'ing the magic path `/dev/syd/stat` prints the syd status on standard
  error.
- Reading from the magic path `/dev/syd/dump` returns the current syd
  state as JSON. This is only available when the sandbox is not locked, or
  it's only available to the syd execve child via `lock:exec`.
- `syd --read path` may be used to canonicalize a path.
- Log messages with process ID information are now enriched with the current
  working directory of the process.
- **lchown**, and **lgetxattr** system calls are now sandboxed.
- Implement `--list set` to display the list of system calls in the given set.
  The supported sets are **allow**, **deny**, and **hook**.
- Fix BusyBox compatibility issues in integration tests.

# 3.0.10

- Fix unit tests

# 3.0.9

- Fix yet another case where a path with invalid UTF-8 would make syd panic.
- **Security**: syd now normalizes the **path** argument of the emulated
  **open** system call which prevents some jail breaks, the simplest being to
  invoke `cat /proc/./self/status` inside syd which erroneously opens the
  proc directory of syd rather then that of the process. We have added about
  80 integration tests which test various relative paths to break the sandbox
  and syd passes all these tests after this fix.
- Use the **paludis** profile rather than the **user** in tests to improve
  reproducibility. Since the **user** profile parsers `~/.user.syd-3` this could
  cause random test failures.
- Calling a system call in an inaccessible directory would fail with `EACCES`
  even if the path argument is an absolute path. This is now fixed.

# 3.0.8

- Fix a panic in open system call hook for invalid UTF-8 paths.
- Add `/home` to the list of read-only directories for Landlock for `user`
  profile.
- `SYD_NPROC` environment variable can be used to configure the number of system
  call handler threads.
- Command options are now pretty printed in `test -e /dev/syd/dump` output.
- Reduce the duration of write lock contention in open system call handlers.
- Consider open calls with the flag `O_CREAT` for write sandboxing regardless of
  access mode.

# 3.0.7

- Use `epoll` rather than `poll` in the syd poll thread.
- Ensure the syd process supervisor does not leak the seccomp file descriptor
  on error conditions.
- syd's thread group id determiner function which reads `/proc/pid/status`
  would hang forever in case the process exits after we open the file but before
  we're finished with reading. This is now fixed.
- The --print-profile CLI option has been renamed to --print.
- Added `syd --check` to check if the process is running under syd.

# 3.0.6

- syd now honors the umask of the environment rather than setting a strict
  umask.
- Fix the open emulator to properly handle open calls with `O_TMPFILE` flag.

# 3.0.5

- Handle **AT\_EMPTY\_PATH** flag properly in **execveat**, **fchownat**,
  **linkat**, **statx**, **newfstatat**, and **utimensat** syscall hooks.

# 3.0.4

- The system call hook of **open** family system calls now properly sets umask
  to that of the process before emulating open so the umasks in sandbox are now
  properly honoured.
- Properly handle system calls with a file descriptor and an empty path as
  argument.
- Follow symbolic links in path resolution regardless of the system call.
- New command line option **--print-profile** to print the rules of the given
  sandbox profile.
- The sandbox profiles **paludis** and **user** have been hardened by utilizing
  [Read Sandboxing](#read-sandboxing) and [Stat Sandboxing](#stat-sandboxing).
  Many sensitive paths such as **/proc/pid/mem**, **/dev/mem** are both hidden
  and denylisted for read.
- **Landlock** errors are no longer fatal.
- **syd** has now basic support for UID/GID mapping inside user namespaces,
  where by default the current user is mapped with the same UID/GID inside the
  container.
- **syd-test** now changes its current working directory to a temporary
  directory before running integration tests. There is also a new validation in
  place when **syd-test** will refuse to run as root. This is due to the fact
  that the integration tests will fail randomly when run with elevated
  privileges.
- Use **SECCOMP_IOCTL_NOTIF_ADDFD** in **open**, **openat** and **openat2**
  calls to close the **TOCTOU** window, providing security. Once POC for
  **open** system call which utilizes pointer modification to break out of jail
  has been included in the test suite and is fixed with this change.

# 3.0.3

- **Security**: syd did not check the target argument of **symlink** and
  **symlinkat** system calls which makes a jail break possible through a symlink
  attack. Two POCs, one for each system call respectively, are included in the
  test suite. With syd checking the target argument these breaks no longer
  work.
- `syd -t`, and `syd-test` now accept many of either a name regex, a test index,
  or a test index range as arguments to filter which integration tests to run.

# 3.0.2

- `-H, --hostname name`, `-D, --domainname name` added to set host, domain name
  of sandboxed process. This requires `--unshare-uts`.
- `-u name, --uid=name` and `-g name, --gid=name` options have been added to run
  the sandboxed process as another user.
- `-A alias, --arg0=alias` has been added to set an alias for the sandbox
  process.
- `-W dir, --work-dir=dir` option has been added to change into a directory before
  executing sandbox process.
- `-C dir, --chroot=dir` option has been added to chroot into a directory before
  executing sandbox process.
- `--unshare-pid,net,mount,uts,ipc,user` command line arguments have been added
  for namespaces support.
- `--export pfc` now has detailed information about the seccomp rules, and lists
  of allowed and notified system calls.
- The old and unused **_sysctl** system call is no longer allowed by syd.
- syd now reports libsecc☮mp version in `--version` output.
- Remove read beneath /home for landlock in user profile.
- Clean syd related environment variables from the environment of the
  sandboxed process.

# 3.0.1

- New sandboxing type [Lock Sandboxing](#lock-sandboxing) to utilize
  [Landlock](https://landlock.io/)
  [LSM](https://en.wikipedia.org/wiki/Linux_Security_Modules).
- syd no longer sets umask to 077 for the sandbox process.
- Disable **setuid** system call in the sandbox for added security. Since this
  system call normally requires an environment with new privileges, this is not
  possible under syd as the sandbox has "no new privileges" flag set.

# 3.0.0

- **Milestone**: Paludis builds under syd with recommended tests using this
  [MR](https://gitlab.exherbo.org/paludis/paludis/-/merge_requests/86).
- Sandbox command lock now defaults to **exec** rather than **off** for added
  security.
- `allowlist/successful_bind` was broken by a recent change. This is now fixed.
- The `trace/memory_access` command is fixed, `strace -c` confirms the results

# 3.0.0-beta.15

- Test suite now properly recognizes that it is running under syd and skips
  the integration tests.
- syd now properly exits with the exit code of the sandbox process and exit
  codes for error conditions are documented in `--help`.
- Fix an issue with triple star extension in path glob matches.

# 3.0.0-beta.14

- Fix an issue with /proc/pid/cmdline reader.
- `symlink` and `symlinkat` system call interceptors no longer check the target
  for access.
- Skip running integration tests when running under syd.
- `lock:exec` no longer waits for the initial **exec** call to lock the sandbox
  for all processes except the syd exec child.

# 3.0.0-beta.13

- Drop the `built` crate dependency.
- Drop the `procfs` crate dependency.
- Use the `built` crate without the `git2` feature.
- Don't use `snmalloc` as the global allocator anymore. This fixes issues with
  static linking on Gentoo.

# 3.0.0-beta.12

- Fix an issue of **stat** sandboxing with path hiding.
- The environment variable **SYD\_NO\_CROSS\_MEMORY\_ATTACH** may be set to
  disable using cross memory attach and fallback to `/proc/pid/mem`.
- The environment variable **SYD\_NO\_SYSLOG** may be set to disable logging to **syslog**.
- Canonicalize UNIX socket addresses before sandbox access check.
- Add common system directories to the allowlist in **user** profile to make
  usage more practical.
- Add `--export` argument to export secure computing rules in binary **Berkeley
  Packet Filter** format and textual **Pseudo Filter Code** formats.
- System call hooks now use system call name and arguments to determine whether
  remote path canonicalization should resolve symbolic links.
- bump MSRV from `1.69` to `1.70`.
- `error` and `warn` level logs are not written to standard error unless
  standard error is a terminal. Since logs of these levels also go to **syslog**
  this is no loss for the user. This is merely to provide convenience when
  running terminal user interfaces under syd.
- `user` profile now enables `stat` sandboxing with the user home directory
  allowlisted.

# 3.0.0-beta.11

- Added `stat` sandboxing which can be used to hide files and directories from
  the sandboxed process.
- The sandbox command `denylist/network` has been renamed to `denylist/net`.
- The sandbox command `allowlist/network` has been renamed to `allowlist/net`.
- The sandbox command `filter/network` has been renamed to `filter/net`.
- The sandbox command `sandbox/network` has been renamed to `sandbox/net`.
- `user` profile now properly allowlists screen and tmux connections.

# 3.0.0-beta.10

- When debug mode is enabled with `SYD_LOG=debug`, syd now logs all system
  calls with seccomp action other than `Allow` to the kernel log. This is useful
  in tackling problems with build failures.
- System calls with bad architecture know return `ENOSYS` rather than syd
  killing the thread.
- Disallowed system calls are now denied with `EACCES` rather than `ENOSYS`.
- syd now sets seccomp system call priority of hotter system calls to a
  higher value to improve performance.
- Fix a potential panic with `/proc/self` -> `/proc/pid`  handling in remote
  paths.

# 3.0.0-beta.9

- Fix an issue with remote path canonicalization.

# 3.0.0-beta.8

- Consolidate error handling, making it faster and more robust.
- Various fixes and improvements for the remote path canonicalization code which
  makes it faster and more robust with regards to error handling.

# 3.0.0-beta.7

- syd now ignores the signals `SIGHUP`, `SIGTSTP`, `SIGTTOU`, and `SIGTTIN`
  for uninterrupted tracing.
- The **user** profile now sets the environment variable
  `GIT_CEILING_DIRECTORIES` to `HOME` to save the user from some useless and
  annoying access violations.

# 3.0.0-beta.6

- Make the **user** profile Exherbo friendly.

# 3.0.0-beta.5

- The `user` profile now has **read** and **exec** sandboxing enabled as well as
  **write** and **network** sandboxing.
- The **triple star** extension is applied to glob patterns, ie `/dev/***`
  matches both `/dev` and any file recursively under `/dev`.
- When run without arguments, the home directory of the current user is now
  looked up from `passwd(5)` data rather than using the `HOME`
  environment variable.
- The clause **last matching rule wins** was not honored at all times. This is
  now fixed.

# 3.0.0-beta.4

- The `user` profile now also parses the file `~/.user.syd-3` if it exists.
  Note, syd uses this profile when invoked without arguments. This provides an
  easy way to spawn a working shell under sandbox.
- Fix UDP network sandboxing which was broken due to invalid error handling for
  connection-mode sockets.
- Some glob patterns in sandbox profiles `paludis`, and `user` have been fixed.

# 3.0.0-beta.3

- Run tests as integration tests, drop the `test-bin` development dependency.

# 3.0.0-beta.2

- Added the new `user` sandbox profile which allows access to user-specific
  directories such as `HOME`, and connections such as `X`, `screen`, `tmux` etc.
  When invoked without arguments, `syd` now drops to a shell with this profile.
- Replace `regex` crate with the more lightweight and performant `regex-lite`
  crate.
- Implement the `cmd/exec` sandbox command and the `syd exec` subcommand.
- Switch from `glob` crate to the `glob-match` crate for matching glob patterns.
- Fallback to `/proc/$pid/mem` if cross memory attach is not enabled in the
  kernel. Use `SYD_PROC_MEM` environment variable or the sandbox command
  `trace/memory_access:1` to force `/proc` fallback.
- `exec/kill_if_match` has been renamed to `exec/kill` which is a **breaking
  change**.
- Set `panic = abort` in release builds for reduced binary size.
- Name the polling thread `syd-poll`.
- Better error handling, and cleaner code.
- Use `parking_lot` crate for `Mutex`, and `RwLock`.
- The default magic virtual device path has been updated from `/dev/syd` to
  `/dev/syd` saving three letters on each typing!! This is a **breaking
  change**.
- The `core/` prefix has been removed from the configuration items
  `core/sandbox`, e.g use `sandbox/exec:on` rather than `core/sandbox/exec:on`.
  `allowlist/successful_bind` has been renamed to `trace/allow_successful_bind`,
  and `allowlist/unsupported_socket_families` has been renamed to
  `trace/allow_unsupported_socket_families`. Moreover the config item
  `core/trace/magic_lock` has been renamed to simply `lock`. This is a
  **breaking change**.
- The prefixes `unix:`, `unix-abstract:`, `inet:`, `inet6:` are no longer used
  in network addresses. Instead the pattern is treated as a UNIX shell style
  pattern if it starts with `/`, and as an IP address otherwise. There is no
  distinction between unix sockets and abstract unix sockets anymore. This is a
  **breaking change**. Check the `data/` subdirectory for a `syd.bash` for
  use with `Paludis`.
- Fix a bug with remote path canonicalization.
- Access violations are logged to syslog now. Use, e.g. `journalctl
  SYSLOG_IDENTIFIER=syd` to view them.

# 3.0.0-alpha.2

- When run without arguments, `syd` now drops into user's current running
  shell allowlisting the `HOME` directory.
- Document the CLI option `-p`, `--profile` and add `noipv4` and `noipv6`
  profiles in addition the `paludis` profile. These profiles may be stacked by
  specifying more than one `-p` arguments.
- Use a Seccomp `BPF` filter rather than a `Notify` filter for fakeroot mode.
- Improve logging to achieve consistency. We have a very simple Logger which logs
  to standard error in format `JSON` lines. There are some common keys `id` is
  always `syd`, `l` gives the `Log::Level` as an integer whereby the lower the
  value of the integer the more severe is the log condition. `t` gives a UNIX
  time stamp in seconds, and `ctx` has short context on the log entry. Errors are
  represented with the `err` key, and system call names are given with the `sys`
  key.
- The `--profile <profile-name>` and `--config @<profile-name>` is now
  supported. `Paludis` uses the former so it is important for compatibility.
  The profile file is **no longer** installed under `${sharedir}/syd` where
  `{sharedir}` is usually `/usr/share` and is kept as a static array in the
  program itself. In the future when `syd-3` has an exheres we can improve on
  this but for now this gets us going.
- The `setuid` system call is now allowed in the sandbox.
- Use `snmalloc` as the global allocator for improved performance.

# 3.0.0-alpha.1

- **New**: Added `core/allowlist/successful_bind`.
  - Utilizes `getsockname` hook, `pidfd_getfd`, and `process_vm_writev` for complete emulation.
  - Features a `TTL` of 3 mins for tracking addresses to manage zero port arguments in `bind()` system calls.

- **Improved**: Refined read, write, network/{bind,connect} sandboxing.
  - Simpler implementation, yet compatible with `Paludis` via `esandbox`.
  - No per-process sandboxing or process tree tracking; uses `/proc/$pid/cwd` when required.
  - Single set of sandbox rules with configurations pushed upfront.
  - **API Change**: Replaced `allow`, `deny` modes with simpler `on/off` toggle.
  - `core/sandbox/network` can be set to `bind` or `connect` for selective sandboxing.
  - Rule matching favors the latest rule for configuration stacking.
  - Streamlined `core/trace/magic_lock:exec` due to lack of parent/child tracking.

- **New**: Introduced `seccomp` process supervision.
  - Implemented primarily in `syd::hook` and `syd::remote`.
  - Derived from the `greenhook` crate, but with a deny-by-default `seccomp` policy.
  - Allowlisted system calls maintained in `syd::config` (currently immutable by users).
  - Notable system calls like `ptrace`, `process_vm_writev`, and `io-uring` are disabled to counteract `TOCTOU` vulnerabilities.

<!-- vim: set spell spelllang=en tw=80 : -->
